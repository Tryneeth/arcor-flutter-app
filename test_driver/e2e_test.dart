// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('First actions App', () {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    Future<void> delay([int second = 1]) async {
      await Future<void>.delayed(Duration(seconds: second));
    }

    test('Entrando datos de registro', () async {
      //var flipTutorial= find.byValueKey("termsConditionCheckBox");
      await driver.waitFor(find.text("Aviso Importante".toUpperCase()));
      var inputField = find.byValueKey("emailFirstContact");
      var termsCondition = find.byValueKey("termsConditionCheckBox");
      var buttonAccept = find.text("ACEPTAR");
      await driver.tap(termsCondition);
      await driver.tap(inputField);
      await driver.enterText("deandresjoel@gmail.com");
      await driver.waitFor(find.text("deandresjoel@gmail.com"));
      await driver.tap(buttonAccept);
      await driver.waitForAbsent(find.text("Aviso Importante".toUpperCase()));
      //await delay(5);
      await driver.tap(find.byValueKey("checkboxInitialHelp"));
      expect(
          find.text("Bienvenido a nuestro\ncanal de comunicación"), isNotNull);
    });

    test("Realizando login", () async {
      var signinButton = find.byValueKey("signinButton");
      await driver.waitFor(find.text("deandresjoel@gmail.com"));
      await driver.tap(find.byValueKey("keepAliveCheckbox"));
      await driver.tap(find.byValueKey("passwordField"));
      await driver.enterText("277");
      await delay();
      await driver.tap(signinButton);
      await driver.waitForAbsent(find.text("Aviso Importante".toUpperCase()));
    });
    
    test("Scrolling novedades", () async {
      await driver.waitFor(find.text("Novedades"));
      var listFinder = find.byValueKey("newsList");
      var lastFinder = find.text("producto tope 99999");
      await driver.scrollUntilVisible(listFinder, lastFinder, dyScroll: -300);
      expect(await driver.getText(lastFinder), "producto tope 99999");
      print("termino de esperar que saliera bien el sroll");
      await driver.tap(lastFinder);
      await driver.waitFor(find.byValueKey("singleNew"));
      print("espera a que salga el singleNew");
      await delay();
      await driver.tap(find.byValueKey("singleNewsBackButton"));
      print("presiona el boton de atras");
      await driver.waitForAbsent(find.byValueKey("singleNew"));
    });

    test("Scrolling activities", () async {
      await driver.tap(find.text("Mi actividad"));
      await driver.waitFor(find.byValueKey("activityPage"));
      await driver.scroll(
          find.byValueKey("activitiesList"), 0, -300, Duration(seconds: 2));
    });

    test("Scrolling contacts", () async {
      await driver.tap(find.text("Contactos"));
      await driver.scroll(
          find.byValueKey("claimList"), 0, -300, Duration(seconds: 2));
      await driver.tap(find.text("PRUBA"));
      await driver.waitFor(find.byValueKey("contactDetails"));
      expect(find.byValueKey("contactDetails"), isNotNull);
      await driver.tap(find.byValueKey("backToContactsButton"));
      await driver.waitForAbsent(find.byValueKey("contactDetails"));
      await driver.tap(find.byType("FloatingActionButton"));
      await driver.waitFor(find.byValueKey("createContact"));
      await delay();
      await driver.tap(find.byValueKey("backButtonCreateNewContact"));
      await delay();
    });

    test("Scrolling track", () async {
      var trackTab = find.text("Seguimiento");
      await driver.scrollUntilVisible(find.byValueKey("tabbar"), trackTab,
          dxScroll: 200);
      await driver.tap(trackTab);
      await driver.scroll(
          find.byValueKey("trackList"), 0, -300, Duration(seconds: 2));
      await driver.tap(find.text("Cod. Pedido: 02718991230000022"));
      await delay(2);
    });

    test("Scrolling encuestas", () async {
      var trackTab = find.text("Encuestas");
      await driver.scrollUntilVisible(find.byValueKey("tabbar"), trackTab,
          dxScroll: 200);
      await driver.tap(trackTab);
      await delay(2);
    });

    test("Scrolling premios", () async {
      var trackTab = find.text("Premios");
      await driver.scrollUntilVisible(find.byValueKey("tabbar"), trackTab,
          dxScroll: 200);
      await driver.tap(trackTab);
      await delay(2);
    });

  });
}
