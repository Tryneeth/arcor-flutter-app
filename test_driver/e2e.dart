import 'package:flutter_driver/driver_extension.dart';
import 'package:tokin/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}