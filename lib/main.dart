import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:tokin/src/app.dart';
import 'package:tokin/src/blocs/polls/polls_model.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/tracks.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
  }
}

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  DBInitHelper helper = DBInitHelper(dbTablesName: [
    'news',
    'activity',
    'contacts',
    'track',
    'poll'
  ], tablesField: [
    NotificationAPI.getFieldsForDB(),
    ActivityAPI.getFieldsForDB(),
    ClaimAPI.getFieldsForDB(),
    TrackAPI.getFieldsForDB(),
    Poll.getFieldsForDB()
  ]);
  helper.initDb();
  runApp(App());
}
