import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tokin/src/common/theme_data.dart';
import 'package:tokin/src/routing.dart';

//import 'package:tokin/src/ui/welcome.dart';
import 'package:tokin/src/utils/firebase_messaging_config.dart';

import 'blocs/welcome/index.dart';
import 'controllers/sharedPreferencesController.dart';
import 'models/shop_iw.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with FirebaseMessagingConfig {
  List<ShoppingCartItem> products = List();

  @override
  void initState() {
    setUp(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var idPedido = "";

    return FutureBuilder(
        future: getShoppingCartFromPreferences(),
        builder: (context, AsyncSnapshot<List<ShoppingCartItem>> snapshot) {
          if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done) {
            return ShoppingCartBinding(
                shoppingCartInfo: ShoppingCartInfo(
                    order: ShoppingCartOrder(
                        idPedido: "", products: snapshot.data))
                  ..flagCart = snapshot.data.length == 0 ? false : true,
                child: MaterialApp(
                  theme: tokinThemeData,
                  home: WelcomeScreen(
                    welcomeBloc: WelcomeBloc(),
                  ),
                  debugShowCheckedModeBanner: false,
                  routes: routes,
                ));
          } else {
            return Container(
                color: Colors.blue.shade400,
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }
        });
    // return ShoppingCartBinding(
    //     shoppingCartInfo: ShoppingCartInfo(
    //         order: ShoppingCartOrder(idPedido: "", products: products)),
    //     child: MaterialApp(
    //       theme: tokinThemeData,
    //       home: WelcomeScreen(
    //         welcomeBloc: WelcomeBloc(),
    //       ),
    //       debugShowCheckedModeBanner: false,
    //       routes: routes,
    //     ));
  }

  Future<List<ShoppingCartItem>> getShoppingCartFromPreferences() async {
    try {
      List<ShoppingCartItem> products = List();
      var productsJson = await SharedPreferencesController.getPrefCart();
      if (productsJson.length > 0) {
        productsJson.forEach((item) {
          return products.add(ShoppingCartItem.fromJson(json.decode(item)));
        });
      }
      return products;
    } catch (e) {
      print("Error on AppData: e");
      return List();
    }
  }
}
