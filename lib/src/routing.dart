import 'package:flutter/material.dart';
import 'package:tokin/src/ui/home_widgets.dart';

final routes = <String, WidgetBuilder>{
  HomeActivity.route: (context) => new HomepageUI(
        tab: HomepageTab.MY_ACTIVITY,
      ),
  HomeContacts.route: (context) => new HomepageUI(
        tab: HomepageTab.CONTACT,
      ),
  HomeNews.route: (context) => new HomepageUI(
        tab: HomepageTab.NEWS,
      ),
  HomeTrack.route: (context) => new HomepageUI(
        tab: HomepageTab.TRACK,
      ),
  HomePolls.route: (context) => new HomepageUI(
        tab: HomepageTab.POLLS,
      ),
  HomeAwards.route: (context) => new HomepageUI(
        tab: HomepageTab.AWARD,
      ),
};
