import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:tokin/src/blocs/index.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:http/http.dart' as http;
import 'package:tokin/src/models/distributor.dart';
import 'package:tokin/src/models/shop_iw.dart';

import 'base_http.dart';

class UpdateData with BaseHttp {
  final ContactsRepository _contactsRepository = new ContactsRepository();
  final NewsRepository _newsRepository = new NewsRepository();
  final TrackRepository _trackRepository = new TrackRepository();
  final PollsRepository _pollsRepository = PollsRepository();

  Future<bool> updateAllData(BuildContext context) async {
    try {
      var token = await updateToken();

      var contacts = await _contactsRepository.storeAllData(context);

      var news = await _newsRepository.storeAllData(context);

      var tracks = await _trackRepository.storeAllData();

      //var poll = await _pollsRepository.findAll(context: context);

      var distributor = await updateDistributor();

      var clientLogin = await clientLoginData();

      ShoppingCartInfo.of(context).flagContact = contacts["flag"];
      ShoppingCartInfo.of(context).flagNews = news["flag"];
      //ShoppingCartInfo.of(context).flagPolls = poll["flag"];
      ShoppingCartInfo.update(context);

      return contacts["values"] && news["values"] && tracks && token && distributor && clientLogin;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> clientLoginData() async {
    String endpoint = await new DataConnection().getClientLoginUri();
    var headers = {
      'Content-Type': 'application/json',
      "Authorization": await SharedPreferencesController.getPrefToken()
    };
    var response = await http
        .post(Uri.encodeFull(endpoint),
            headers: headers,
            body: json.encode(
              {
                "android_id": await SharedPreferencesController.getDeviceID(),
                "email": await SharedPreferencesController.getPrefEmail(),
                ...await getBodyData(),
                "username": "",
                "password": ""
              },
            ))
        .timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      String category = json.decode(response.body)['categoria'].toString();
      String channel = json.decode(response.body)['canal'].toString();
      String idPriceList = json.decode(response.body)['id_lista_precio'].toString();
      if (category != null && channel != null && idPriceList != null) {
        await SharedPreferencesController.setCatalogBodyData(
          category: category,
          channel: channel,
          idPriceList: idPriceList,
        );
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<bool> updateToken() async {
    try {
      var endpoint = await new DataConnection().getAuthUri();
      Map<String, String> bodyRequest = new Map();
      bodyRequest["username"] = "mobile_pdv";
      bodyRequest["password"] = "Sarmiento1150!";
      var headers = {'Content-Type': 'application/json'};
      var response = await http
          .post(Uri.encodeFull(endpoint), headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));
      if (response.statusCode == 200) {
        var token = json.decode(response.body)['token'].toString();
        if (token != null) {
          await SharedPreferencesController.setPrefToken(token);
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> updateDistributor() async {
    //try {
    var endpoint = await new DataConnection().getDistribuidor();
    var bodyRequest = await getBodyData();
    var headers = await getHeaders();
    var response = await http
        .post(Uri.encodeFull(endpoint), headers: headers, body: json.encode(bodyRequest))
        .timeout(Duration(seconds: 20));
    if (response.statusCode == 200 && response.body != "") {
      var bodyData = json.decode(utf8.decode(response.bodyBytes));
      Distributor distributor = Distributor.fromJson(bodyData);
      await SharedPreferencesController.setPrefHabilitaCompras(distributor.habilitaCompras);
      await SharedPreferencesController.setPrefHabilitaContactos(distributor.habilitaContactos);
      await SharedPreferencesController.setPrefHabilitaCuentacorriente(distributor.habilitaCuentacorriente);
      await SharedPreferencesController.setPrefHabilitaEncuestas(distributor.habilitaEncuestas);
      await SharedPreferencesController.setPrefHabilitaNotificaciones(distributor.habilitaNotificaciones);
      await SharedPreferencesController.setPrefHabilitaPremios(distributor.habilitaPremios);
      await SharedPreferencesController.setPrefHabilitaTokin(distributor.habilitaTokin);
      await SharedPreferencesController.setPrefPuertoServidor(distributor.puertoServidor);
      await SharedPreferencesController.setPrefUrlServidorPrincipal(distributor.urlServidorPrincipal);
      await SharedPreferencesController.setPrefUrlServidorAlternativa(distributor.urlServidorAlternativa);
      await SharedPreferencesController.setPrefNombreDistribuidor(distributor.nombreDistribuidor);
      return true;
    }
    return false;
    // } catch (e) {
    //   print(e);
    //   return false;
    // }
  }
}
