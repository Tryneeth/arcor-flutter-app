import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesController {
  //Each preference in a individual object to know where is each preference
  static final urlServidorPrincipal = "urlServidorPrincipal";
  static final urlServidorAlternativa = "urlServidorAlternativa";
  static final puertoServidor = "puertoServidor";
  static final habilitaTokin = "habilitaTokin";
  static final habilitaNotificaciones = "habilitaNotificaciones";
  static final habilitaCuentacorriente = "habilitaCuentacorriente";
  static final habilitaContactos = "habilitaContactos";
  static final habilitaEncuestas = "habilitaEncuestas";
  static final habilitaPremios = "habilitaPremios";
  static final habilitaCompras = "habilitaCompras";
  static final nombreDistribuidor = "nombre_distribuidor";
  static final token = "token";

  //Other Pref
  static final email = "email";
  static final pass = "pass";
  static final keepSession = "keepSession";
  static final distribuidor = "ID_DISTRIBUIDOR";
  static final clientCode = "ID_CLIENTE";
  static final fullClientCode = "fullClientCode";
  static final countryCode = "ID_PAIS";
  static final sucursalId = "ID_SUCURSAL";
  static final atpdvId = "ATPDV_ID";
  static final atpdvDirection = "ATPDV_DIRECCION";
  static final emailVerified = "emailVerified";
  static final baseUrl = "baseUrl";
  static final baseUrlServiceTokin = "urlServiceTokin";
  static final imageUrl = "imageUrl";
  static final pollsUrl = "pollsUrl";
  static final k1 = "k1";
  static final k2 = "k2";
  static final termConditions = "termConditions";
  static final atpdvNombre = "ATPDV_NOMBRE";

  //Options needed
  static final closeInitialDialog = "closeInitialDialog";
  static final cart = "cart";
  static final urlServiceLoadTokin = "URL_SERVICE_LOAD_TOKIN";
  static final urlServiceUpdateTokin = "URL_SERVICE_UPDATE_TOKIN";
  static final Map<String, String> catalogBodyData = Map();

  static String _catChannelKey = "canal";
  static String _catIdPriceKey = "id_lista_precio";
  static String _catCategoryKey = "categoria";

  static String _deviceId = "device_id";

  static Future<String> getDeviceID() => getString(_deviceId);
  static Future<bool> setDeviceID(String id) => setString(_deviceId, id);

  static Future<String> getUrlServiceLoadTokin() {
    return getString(urlServiceLoadTokin);
  }

  static Future<void> setUrlServiceLoadTokin(String value) {
    return setString(urlServiceLoadTokin, value);
  }

  static Future<String> getUrlServiceUpdateTokin() {
    return getString(urlServiceUpdateTokin);
  }

  static Future<void> setUrlServiceUpdateTokin(String value) {
    return setString(urlServiceUpdateTokin, value);
  }

  static Future<void> setCatalogBodyData({@required category, @required channel, @required idPriceList}) async {
    setString(_catCategoryKey, category);
    setString(_catChannelKey, channel);
    setString(_catIdPriceKey, idPriceList);
    return;
  }

  static Future<Map<String, String>> getCatalogBodyData() async {
    return {
      _catCategoryKey: await getString(_catCategoryKey),
      _catChannelKey: await getString(_catChannelKey),
      _catIdPriceKey: await getString(_catIdPriceKey)
    };
  }

  //closeInitialDialog
  static Future<bool> getPrefCloseInitialDialog() async {
    var result = getBool(closeInitialDialog);
    if (await result == null) {
      return false;
    }
    return result;
  }

  static Future<bool> setPrefCloseInitialDialog(bool value) async {
    return setBool(closeInitialDialog, value);
  }

  //termConditions
  static Future<bool> getPrefTermConditions() async {
    var result = getBool(termConditions);
    if (await result == null) {
      return false;
    }
    return result;
  }

  static Future<bool> setPrefTermConditions(bool value) async {
    return setBool(termConditions, value);
  }

  //fullkeepSession
  static Future<bool> getPrefKeepSession() async {
    var result = getBool(keepSession);
    if (await result == null) {
      return false;
    }
    return result;
  }

  static Future<bool> setPrefKeepSession(bool value) async {
    return setBool(keepSession, value);
  }

  //fullClientCode
  static Future<String> getPrefPass() async {
    return getString(pass);
  }

  static Future<bool> setPrefPass(String value) async {
    return setString(pass, value);
  }

  //fullClientCode
  static Future<String> getPreffullClientCode() async {
    return getString(fullClientCode);
  }

  static Future<bool> setPreffullClientCode(String value) async {
    return setString(fullClientCode, value);
  }

  //k2
  static Future<String> getPrefk2() async {
    return getString(k2);
  }

  static Future<bool> setPrefk2(String value) async {
    return setString(k2, value);
  }

  //k1
  static Future<String> getPrefk1() async {
    return getString(k1);
  }

  static Future<bool> setPrefk1(String value) async {
    return setString(k1, value);
  }

  //token
  static Future<String> getPrefToken() async {
    return getString(token);
  }

  static Future<bool> setPrefToken(String value) async {
    return setString(token, value);
  }

  //pollsUrl
  static Future<String> getPrefpollsUrl() async {
    return getString(pollsUrl);
  }

  static Future<bool> setPrefpollsUrl(String value) async {
    return setString(pollsUrl, value);
  }

  //imageUrl
  static Future<String> getPrefimageUrl() async {
    return getString(imageUrl);
  }

  static Future<bool> setPrefimageUrl(String value) async {
    return setString(imageUrl, value);
  }

  //baseUrl
  static Future<String> getPrefBaseUrl() async {
    return getString(baseUrl);
  }

  static Future<bool> setPrefBaseUrl(String value) async {
    return setString(baseUrl, value);
  }

  //atpdvIdDirection
  static Future<String> getPrefatpdvDirection() async {
    return getString(atpdvDirection);
  }

  static Future<bool> setPrefatpdvDirection(String value) async {
    return setString(atpdvDirection, value);
  }

  //atpdvId
  static Future<String> getPrefatpdvId() async {
    return getString(atpdvId);
  }

  static Future<bool> setPrefatpdvId(String value) async {
    return setString(atpdvId, value);
  }

  //sucursalId
  static Future<String> getPrefSucursalId() async {
    return getString(sucursalId);
  }

  static Future<bool> setPrefSucursalId(String value) async {
    return setString(sucursalId, value);
  }

  //ATPDV_NOMBRE
  static Future<String> getPrefAtpdvNombre() async {
    return getString(atpdvNombre);
  }

  static Future<bool> setPrefAtpdvNombre(String value) async {
    return setString(atpdvNombre, value);
  }

  static Future<bool> getPrefEmailVerified() async {
    var result = getBool(emailVerified);
    if (await result == null) {
      return false;
    }
    return result;
  }

  static Future<bool> setPrefEmailVerified(bool value) async {
    return setBool(emailVerified, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefEmail() async {
    return getString(email);
  }

  static Future<bool> setPrefEmail(String value) async {
    return setString(email, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefDistrib() async {
    return getString(distribuidor);
  }

  static Future<bool> setPrefDistrib(String value) async {
    return setString(distribuidor, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefClientCode() async {
    return getString(clientCode);
  }

  static Future<bool> setPrefClientCode(String value) async {
    return setString(clientCode, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefCountryCode() async {
    return getString(countryCode);
  }

  static Future<bool> setPrefCountryCode(String value) async {
    return setString(countryCode, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefUrlServidorPrincipal() async {
    return getString(urlServidorPrincipal);
  }

  static Future<bool> setPrefUrlServidorPrincipal(String value) async {
    return setString(urlServidorPrincipal, value);
  }

  //UrlServidorAlternativa
  static Future<String> getPrefUrlServidorAlternativa() async {
    return getString(urlServidorAlternativa);
  }

  static Future<bool> setPrefUrlServidorAlternativa(String value) async {
    return setString(urlServidorAlternativa, value);
  }

  //UrlServiceTokin
  static Future<String> getPrefBaseUrlServiceTokin() async {
    return getString(baseUrlServiceTokin);
  }

  static Future<bool> setPrefBaseUrlServiceTokin(String value) async {
    return setString(baseUrlServiceTokin, value);
  }

  //PuertoServidor
  static Future<String> getPrefPuertoServidor() async {
    return getString(puertoServidor);
  }

  static Future<bool> setPrefPuertoServidor(String value) async {
    return setString(puertoServidor, value);
  }

  //HabilitaTokin
  static Future<String> getPrefHabilitaTokin() async {
    return getString(habilitaTokin);
  }

  static Future<bool> setPrefHabilitaTokin(String value) async {
    return setString(habilitaTokin, value);
  }

  //HabilitaNotificaciones
  static Future<String> getPrefHabilitaNotificaciones() async {
    return getString(habilitaNotificaciones);
  }

  static Future<bool> setPrefHabilitaNotificaciones(String value) async {
    return setString(habilitaNotificaciones, value);
  }

  //HabilitaCuentacorriente
  static Future<String> getPrefHabilitaCuentacorriente() async {
    return getString(habilitaCuentacorriente);
  }

  static Future<bool> setPrefHabilitaCuentacorriente(String value) async {
    return setString(habilitaCuentacorriente, value);
  }

  //HabilitaContactos
  static Future<String> getPrefHabilitaContactos() async {
    return getString(habilitaContactos);
  }

  static Future<bool> setPrefHabilitaContactos(String value) async {
    return setString(habilitaContactos, value);
  }

  //HabilitaEncuestas
  static Future<String> getPrefHabilitaEncuestas() async {
    return getString(habilitaEncuestas);
  }

  static Future<bool> setPrefHabilitaEncuestas(String value) async {
    return setString(habilitaEncuestas, value);
  }

  //HabilitaPremios
  static Future<String> getPrefHabilitaPremios() async {
    return getString(habilitaPremios);
  }

  static Future<bool> setPrefHabilitaPremios(String value) async {
    return setString(habilitaPremios, value);
  }

  //HabilitaCompras
  static Future<String> getPrefHabilitaCompras() async {
    return await getString(habilitaCompras);
  }

  static Future<bool> setPrefHabilitaCompras(String value) async {
    return setString(habilitaCompras, value);
  }

  //UrlServidorPrincipal
  static Future<String> getPrefNombreDistribuidor() async {
    return getString(nombreDistribuidor);
  }

  static Future<bool> setPrefNombreDistribuidor(String value) async {
    return setString(nombreDistribuidor, value);
  }

  //Cart
  static Future<List<String>> getPrefCart() async {
    var result = await getStringList(cart);
    if (result == null) {
      return List();
    } else {
      return result;
    }
  }

  static Future<bool> setPrefCart(List<String> value) async {
    return setStringList(cart, value);
  }

  //GeneralActions
  static Future<String> getString(String key) async {
    var preference = await SharedPreferences.getInstance();
    var result = preference.getString(key);
    return result;
  }

  static Future<bool> setString(String key, String value) async {
    var preference = await SharedPreferences.getInstance();
    var result = await preference.setString(key, value);
    return result;
  }

  //GeneralActions
  static Future<bool> getBool(String key) async {
    var preference = await SharedPreferences.getInstance();
    var result = preference.getBool(key);
    return result;
  }

  static Future<bool> setBool(String key, bool value) async {
    var preference = await SharedPreferences.getInstance();
    var result = await preference.setBool(key, value);
    return result;
  }

  static Future<bool> clear() async {
    var preference = await SharedPreferences.getInstance();
    var result = await preference.clear();
    return result;
  }

  //GeneralActions
  static Future<List<String>> getStringList(String key) async {
    var preference = await SharedPreferences.getInstance();
    var result = preference.getStringList(key);
    return result;
  }

  static Future<bool> setStringList(String key, List<String> value) async {
    var preference = await SharedPreferences.getInstance();
    var result = await preference.setStringList(key, value);
    return result;
  }
}
