import 'package:tokin/src/controllers/activitiesController.dart';
import 'package:tokin/src/controllers/currentAccountController.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/models/current_account.dart';

class ActivitiesViewModel {
  List<ActivityAPI> activities;
  CurrentAccountAPI currentAccount;

  ActivitiesViewModel({this.activities, this.currentAccount});

  static Future<ActivitiesViewModel> getActivitiesViewModel() async {
    var activities = await ActivitiesController().getData();
    var currentAccount = await CurrentAccountController().getData();

    return new ActivitiesViewModel(
        activities: activities, currentAccount: currentAccount);
  }

  static Future<ActivitiesViewModel> getFilterActivitiesViewModel(
      Function filterFunction) async {
    var activities = await ActivitiesController().getData();
    var currentAccount = await CurrentAccountController().getData();

    activities = activities.where(filterFunction).toList();
    return new ActivitiesViewModel(
        activities: activities, currentAccount: currentAccount);
  }
}
