import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/data_connection.dart';

class RegisterController with BaseHttpRegister {
  Future<bool> toRegister({String phoneId, String email, String distributor, String clientId}) async {
    try {
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(new DataConnection().getRegisterUri()), headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(milliseconds: 20000));
      var bodyData = json.decode(utf8.decode(response.bodyBytes));

      if (bodyData["status"] == "OK" && bodyData["message"] == "OK") {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      print(error);
      return false;
    }
  }

  Future<bool> toValidate({String email, String phoneId}) async {
    try {
      String url = new DataConnection().getLicensesUri();
      String urlHeaders =
          "$url\/?email=$email&android_id=$phoneId&key=6c542b1771cdbbad40b68d319e6766ad&module_name=ArcorPDVAndroidApp&format=json&action=update&key_version_instalada=1.1.195.187-20190804004713F&fecha_hora_carga=20190911120500";

      var response = await http.get(Uri.encodeFull(urlHeaders)).timeout(Duration(seconds: 20));
      var bodyData = json.decode(utf8.decode(response.bodyBytes));

      if (response.statusCode != 200) {
        return false;
      } else {
        if (bodyData != 0) {
          var fullClientId = bodyData['codigo_usuario'];
          List<dynamic> parameters = bodyData['parametros'];
          var baseUrl = parameters.singleWhere((t) => t['nombre'] == "URL_LOAD_DOWNLOAD_PDV")['valor'];
          var imagesUrl = parameters.singleWhere((t) => t['nombre'] == "TOKIN_NOTIFICATIONS_IMAGES")['valor'];
          var pollsUrl = parameters.singleWhere((t) => t['nombre'] == "URL_LOAD_DOWNLOAD_SURVEY")['valor'];
          var k1 = bodyData['k1'];
          var k2 = bodyData['k2'];
          var idPais = parameters.singleWhere((t) => t['nombre'] == 'ID_PAIS')['valor'];
          var idDist = parameters.singleWhere((t) => t['nombre'] == 'ID_DISTRIBUIDOR')['valor'];
          var idBranch = parameters.singleWhere((t) => t['nombre'] == 'ID_SUCURSAL')['valor'];
          var atpdvId = parameters.singleWhere((t) => t['nombre'] == 'ATPDV_ID')['valor'];
          var idcliente = parameters.singleWhere((t) => t['nombre'] == 'ID_CLIENTE')['valor'];
          var atpdvAddress = parameters.singleWhere((t) => t['nombre'] == 'ATPDV_DIRECCION')['valor'];
          var atpdvnombre = parameters.singleWhere((t) => t['nombre'] == 'ATPDV_NOMBRE')['valor'];
          var urlServiceTokin = parameters.singleWhere((t) => t['nombre'] == 'URL_SERVICE_TOKIN')['valor'];
          var urlServiceLoadTokin = parameters.singleWhere((t) => t['nombre'] == 'URL_SERVICE_LOAD_TOKIN')['valor'];
          var urlServiceUpdateTokin = parameters.singleWhere((t) => t['nombre'] == 'URL_SERVICE_UPDATE_TOKIN')['valor'];

          //Storing data
          await SharedPreferencesController.setPrefBaseUrl(baseUrl);
          await SharedPreferencesController.setPrefimageUrl(imagesUrl);
          await SharedPreferencesController.setPrefpollsUrl(pollsUrl);
          await SharedPreferencesController.setPrefk1(k1);
          await SharedPreferencesController.setPrefk2(k2);
          await SharedPreferencesController.setPrefCountryCode(idPais);
          await SharedPreferencesController.setPrefDistrib(idDist);
          await SharedPreferencesController.setPrefSucursalId(idBranch);
          await SharedPreferencesController.setPrefatpdvId(atpdvId);
          await SharedPreferencesController.setPrefClientCode(idcliente);
          await SharedPreferencesController.setPrefatpdvDirection(atpdvAddress);
          await SharedPreferencesController.setPrefAtpdvNombre(atpdvnombre);
          await SharedPreferencesController.setPreffullClientCode(fullClientId);
          await SharedPreferencesController.setPrefBaseUrlServiceTokin(urlServiceTokin);
          await SharedPreferencesController.setUrlServiceLoadTokin(urlServiceLoadTokin);
          await SharedPreferencesController.setUrlServiceUpdateTokin(urlServiceUpdateTokin);

          return true;
        } else {
          return false;
        }
      }
    } catch (error) {
      print(error);
      return false;
    }
  }
}
