import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/info_petitions.dart';
import 'package:tokin/src/models/data_connection.dart';

class ClaimsController with BaseHttp {
  Future<List<ClaimAPI>> getData(bool singlePage) async {
    try {
      var endpoint = await new DataConnection().getReclamosV2();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));
      var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
      var pages = int.tryParse(
          json.decode(utf8.decode(response.bodyBytes))['num_pages'].toString());

      if (response.statusCode == 200) {
        List<ClaimAPI> claimsList = new List();
        for (int i = 0; i < bodyData.length; i++) {
          claimsList.add(ClaimAPI.fromJson(bodyData[i]));
        }

        if (pages > 1 && !singlePage)
          for (var i = 2; i <= pages; i++) {
            bodyRequest['page'] = i;
            var responseAux = await http
                .post(Uri.encodeFull(endpoint),
                    headers: headers, body: json.encode(bodyRequest))
                .timeout(Duration(milliseconds: 7000));
            var bodyDataAux =
                json.decode(utf8.decode(response.bodyBytes))['data'];

            if (responseAux.statusCode == 200) {
              for (int i = 0; i < bodyData.length; i++) {
                claimsList.add(ClaimAPI.fromJson(bodyDataAux[i]));
              }
            }
          }
        return claimsList;
      }

      return new List();
    } catch (error) {
      print(error);
      return new List();
    }
  }

  Future<bool> createData(PostClaims claim) async {
    try {
      var list = new List<PostClaims>();
      list.add(claim);
      var headers = await getHeaders();
      var body = toMap(list);
      var endpoint = await new DataConnection().getCreateReclamos();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(body))
          .timeout(Duration(milliseconds: 7000));
      if (response.statusCode == 200) {
        var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
        var numSucess =
            json.decode(utf8.decode(response.bodyBytes))['num_sucess'];
        if (numSucess == 1)
          return true;
        else {
          return false;
        }
      }
      return false;
    } catch (error) {
      return false;
      // throw error;
    }
  }

  Future<bool> updateViewField(ClaimAPI claimAPI) async {
    try {
      var result = await createData(PostClaims().fromClaim(claimAPI));
      DBHelper<ClaimAPI> _dbHelper = new DBHelper<ClaimAPI>(
          dbTableName: 'contacts', tableField: ClaimAPI.getFieldsForDB());
      claimAPI.visto = "visto";
      if (result) {
        claimAPI.syncronized = 1;
      } else {
        claimAPI.syncronized = 0;
      }
      await _dbHelper.update(claimAPI.toMap(), claimAPI.id.toString());
      return true;
    } catch (e) {
      print(e);
      DBHelper<ClaimAPI> _dbHelper = new DBHelper<ClaimAPI>(
          dbTableName: 'contacts', tableField: ClaimAPI.getFieldsForDB());
      claimAPI.visto = "visto";
      claimAPI.syncronized = 0;
      await _dbHelper.update(claimAPI.toMap(), claimAPI.id.toString());
      return true;
    }
  }

  Map<String, dynamic> toMap(List<PostClaims> claims) {
    List<Map> mapClaims = new List<Map>();
    for (var item in claims) {
      mapClaims.add(item.toMap());
    }
    List<String> empty = new List(0);
    var map = new Map<String, dynamic>();
    map['adjuntos'] = empty;
    map['reclamos'] = mapClaims;
    return map;
  }
}
