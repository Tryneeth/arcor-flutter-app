import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/models/data_connection.dart';

class ActivitiesController with BaseHttp {
  Future<List<ActivityAPI>> getData() async {
    try {
      var endpoint = await new DataConnection().getActivityUrl();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));
      var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
      var pages = int.tryParse(
          json.decode(utf8.decode(response.bodyBytes))['num_pages'].toString());

      if (response.statusCode == 200 && bodyData != null) {
        List<ActivityAPI> activityApiList = new List();
        for (int i = 0; i < bodyData.length; i++) {
          activityApiList.add(ActivityAPI.fromJson(bodyData[i]));
        }

        if (pages > 1)
          for (var i = 2; i <= pages; i++) {
            bodyRequest['page'] = i;
            var responseAux = await http
                .post(Uri.encodeFull(endpoint),
                    headers: headers, body: json.encode(bodyRequest))
                .timeout(Duration(milliseconds: 7000));
            var bodyDataAux =
                json.decode(utf8.decode(response.bodyBytes))['data'];

            if (responseAux.statusCode == 200) {
              for (int i = 0; i < bodyData.length; i++) {
                activityApiList.add(ActivityAPI.fromJson(bodyDataAux[i]));
              }
            }
          }

        return activityApiList;
      }
      return new List();
    } catch (error) {
      print(error);
      return new List();
    }
  }
}
