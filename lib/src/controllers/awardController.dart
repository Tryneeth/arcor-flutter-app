import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/awards.dart';
import 'package:tokin/src/models/data_connection.dart';

import 'base_http.dart';

class AwardController with BaseHttp{
  Future<Awards> getData() async {
    try {
      var endpoint = await new DataConnection().getAwardsData();
      
      var bodyRequest = new Map<String, String>();

      bodyRequest['user'] = await SharedPreferencesController.getPrefEmail();
      bodyRequest['password'] = await calcHash();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));

      var bodyData = json.decode(utf8.decode(response.bodyBytes))['response'];
      var body2=bodyData['data'];
      var user =body2['usuario'];
      var success =  json.decode(utf8.decode(response.bodyBytes))['success'];

      if (response.statusCode == 200 && success==true && bodyData!=null) {
        var award= Awards.fromJson(user);
        return award;
      }

      return new Awards();
    } catch (error) {
      print(error);
      return new Awards();
    }
  }

  Future<String> calcHash() async {
    var pass = await SharedPreferencesController.getPrefPass();
    //Pass process
    var passBytes = utf8.encode(pass); // data being hashed
    var shaPass = sha1.convert(passBytes);
    return shaPass.toString().toUpperCase();
  }
}
