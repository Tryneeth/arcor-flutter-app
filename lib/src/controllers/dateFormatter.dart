import 'package:intl/intl.dart';

mixin DateFormatter {
  static DateTime toFormatDate(String apiTime) {
    if (apiTime != null && apiTime.length != 0) {
      int year = int.parse(apiTime.substring(0, 4));
      int month = int.parse(apiTime.substring(4, 6));
      int day = int.parse(apiTime.substring(6, 8));
      int hour = int.parse(apiTime.substring(8, 10));
      int min = int.parse(apiTime.substring(10, 12));
      int seconds = int.parse(apiTime.substring(12, 14));
      DateTime date = new DateTime(year, month, day,hour,min,seconds);
      return date;
    } else {
      return new DateTime(DateTime.now().year);
    }
  }

  static String toStringDate(DateTime dateTime) {
    if (dateTime != null) {
      return DateFormat("yyyyMMddhhmmss").format(dateTime);
    } else {
      return "";
    }
  }
}
