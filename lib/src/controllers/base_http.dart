import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/info_petitions.dart';
import 'package:tokin/src/models/order.dart';
import 'package:tokin/src/models/register.dart';

mixin BaseHttp {
  Future<Map<String, String>> getHeaders() async {
    var headers = Map<String, String>();
    var token = await SharedPreferencesController.getPrefToken();
    if (token != null && token != "")
      headers = {
        'Content-Type': 'application/json',
        'Authorization': "Token $token"
      };
    return headers;
  }

  Future<Map> getBodyData() async {
    var idpais = await SharedPreferencesController.getPrefCountryCode();
    var idDist = await SharedPreferencesController.getPrefDistrib();
    var idsucursal = await SharedPreferencesController.getPrefSucursalId();
    var idcliente = await SharedPreferencesController.getPreffullClientCode();
    var bodyRequest = Map();
    if (idpais != null &&
        idDist != null &&
        idsucursal != null &&
        idcliente != null) {
      bodyRequest = new PostNews(
              countryId: idpais,
              distributorId: idDist,
              branchId: idsucursal,
              clientCode: idcliente,
              dateSince: '',
              page: 1)
          .toMap();
    }
    return bodyRequest;
  }

  Future<Map> getCatalogBodyData() async => await SharedPreferencesController.getCatalogBodyData();
}

mixin BaseHttpRegister {
  String apiUrl = new DataConnection().getRegisterUri();

  Future<Map> getBodyData(
      {String androidId,
      String email,
      String distributor,
      String clientId}) async {
    Register bodyRequest;
    var codigoUsuario = "";
    for (var i = 0; i < 10 - clientId.length; i++) {
      codigoUsuario += "0";
    }
    codigoUsuario += clientId;
    bodyRequest = new Register(
        empresa: "ARCORTOKIN",
        androidId: androidId,
        email: email,
        key: "8a236d1632cdbaca21a41d124a2932de",
        modulo: "ArcorPDVAndroidApp",
        codigoUsuario: codigoUsuario,
        parametros: [
          new Parameter(name: "ID_PAIS", value: "ARG").toMap(),
          new Parameter(name: "ATPDV_NOMBRE", value: "EMSER PRUEBAS").toMap(),
          new Parameter(name: "ATPDV_DIRECCION", value: "PRINGLES 435").toMap(),
          new Parameter(
                  name: "ATPDV_ID",
                  value: "ARG-$distributor-$distributor-$clientId")
              .toMap(),
          new Parameter(name: "ID_DISTRIBUIDOR", value: "$distributor").toMap(),
          new Parameter(name: "ID_SUCURSAL", value: "$distributor").toMap(),
          new Parameter(name: "ID_CLIENTE", value: "$clientId").toMap()
        ]);
    return bodyRequest.toMap();
  }

  Future<Map<String, String>> getHeaders() async {
    var headers;
    var preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("token");
    if (token != null && token != "")
      headers = {
        'Content-Type': 'application/json',
        'Authorization': "Token $token"
      };

    return headers;
  }
}

mixin BaseHttpChargeArticle {
  Future<Map<String, String>> getHeaders() async {
    var headers;
    var preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("token");
    if (token != null && token != "")
      headers = {
        'Content-Type': 'application/json',
        'Authorization': "Token $token"
      };
    return headers;
  }

  Future<Map> getBodyData(
      {@required String articleCode, @required String listPriceId}) async {
    var idpais = await SharedPreferencesController.getPrefCountryCode();
    var idDist = await SharedPreferencesController.getPrefDistrib();
    var idsucursal = await SharedPreferencesController.getPrefSucursalId();
    var bodyRequest;
    if (idpais != null && idDist != null && idsucursal != null) {
      bodyRequest = new PostArticle(
          countryId: idpais,
          distributorId: idDist,
          branchId: idsucursal,
          listPriceId: listPriceId,
          articleCode: <String>[articleCode]).toMap();
      return bodyRequest;
    }
    return bodyRequest;
  }
}

mixin BaseHttpUploadArticle {
  Future<Map<String, String>> getHeaders() async {
    var headers;
    var token = await SharedPreferencesController.getPrefToken();
    if (token != null && token != "")
      headers = {
        'Content-Type': 'application/json',
        'Authorization': "Token $token"
      };
    return headers;
  }

  Future<Map<String, dynamic>> getBodyData({
    @required Order order,
  }) async {
    Map<String, dynamic> bodyRequest = {"records": null};
    var orders = <Order>[order];
    List<Map<dynamic, dynamic>> records = List();
    orders.forEach((item) {
      records.add(item.toMap());
    });
    bodyRequest["records"] = records;
    return bodyRequest;
  }
}
