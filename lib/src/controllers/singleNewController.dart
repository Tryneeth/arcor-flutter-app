import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/product.dart';

class SingleNewsController with BaseHttpChargeArticle, DateFormatter {
  SingleNewsController();

  Future<List<Product>> getData(NotificationAPI singleNew) async {
    try {
      var endpoint = await new DataConnection().getArticles();
      var body = await getBodyData(
          articleCode: singleNew.codigoProducto, listPriceId: "NENE");
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(body))
          .timeout(Duration(seconds: 20));

      if (response.statusCode == 200 && response.body != "") {
        var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
        if (body["status"] != "[PRODUCTO INEXISTENTE]") {
          List<Product> productList = new List();
          for (int i = 0; i < bodyData.length; i++) {
            var product = Product.fromJson(bodyData[i]['articulo']);
            productList.add(product);
          }
          return productList;
        } else {
          return new List();
        }
      } else {
        return new List();
      }
    } catch (error) {
      return new List();
    }
  }
}
