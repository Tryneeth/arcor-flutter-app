import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/upload_image.dart';

class UploadImagesController with BaseHttp {
  Future<bool> createData(UploadImage uploadIamge) async {
    try {
      var endpoint = await new DataConnection().getUploadImages();
      var body = uploadIamge.toMap();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(body))
          .timeout(Duration(milliseconds: 30000));
      String bodyData =
          json.decode(utf8.decode(response.bodyBytes))['image_filename'];

      if (bodyData.length > 0)
        return true;
      else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}
