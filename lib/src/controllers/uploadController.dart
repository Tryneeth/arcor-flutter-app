import 'dart:convert';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/upload_image.dart';

class UploadController with BaseHttp {
  Future<bool> createData(UploadImage uploadImage) async {
    try {
      var endpoint = await new DataConnection().getUploadImages();
      var body = uploadImage.toMap();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(body))
          .timeout(Duration(milliseconds: 30000));
      String bodyData =
          json.decode(utf8.decode(response.bodyBytes))['image_filename'];
      if (bodyData.length != 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
      // throw error;
    }
  }

  Future<String> compressAndStoreImages(
      List<File> images, String filesName) async {
    try {
      var dir = images[0].parent;
      final path = dir.path;
      String finalPath = "$path/$filesName";
      new Directory(finalPath)
          .create()
          .then((Directory directory) => print(directory.path));
      File newImage;
      for (var i = 1; i < images.length + 1; i++) {
        var image = images[i - 1];
        newImage = await image.copy("$finalPath/$filesName-$i.jpg");
      }

      var encoder = ZipFileEncoder();
      encoder.zipDirectory(Directory(finalPath));
      newImage = new File(encoder.zip_path);
      var encodedZip = base64Encode(newImage.readAsBytesSync());
      print(encodedZip.substring(0, 500));
      return (encodedZip);
    } catch (e) {
      print(e);
      return "";
    }
  }
}
