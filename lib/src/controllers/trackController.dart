import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/tracks.dart';

class TrackController with BaseHttp {
  Future<List<TrackAPI>> getData(bool singlePage) async {
    try {
      var endpoint = await new DataConnection().getTracks();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));
      var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
      var pages = int.tryParse(
          json.decode(utf8.decode(response.bodyBytes))['num_pages'].toString());

      if (response.statusCode == 200) {
        List<TrackAPI> trackpApiList = new List();
        for (int i = 0; i < bodyData.length; i++) {
          trackpApiList.add(TrackAPI.fromJson(bodyData[i]));
        }

        if (pages > 1 && !singlePage)
          for (var i = 2; i <= pages; i++) {
            bodyRequest['page'] = i;
            var responseAux = await http
                .post(Uri.encodeFull(endpoint),
                    headers: headers, body: json.encode(bodyRequest))
                .timeout(Duration(seconds: 20));
            var bodyDataAux =
                json.decode(utf8.decode(response.bodyBytes))['data'];

            if (responseAux.statusCode == 200) {
              for (int i = 0; i < bodyData.length; i++) {
                trackpApiList.add(TrackAPI.fromJson(bodyDataAux[i]));
              }
            }
          }
        var res = groupActions(trackpApiList);
        return res;
      }
      return new List();
    } catch (error) {
      print(error);
      return new List();
    }
  }

  List<TrackAPI> groupActions(List<TrackAPI> tracks) {
    var newMap = groupBy(tracks, (i) => i.idPedido);
    List<TrackAPI> listResult = new List<TrackAPI>();
    TrackAPI track;

    for (var i = 0; i < newMap.length; i++) {
      var idPedido = newMap.keys.elementAt(i);
      var item = newMap[idPedido];
      track = new TrackAPI();
      track.estado = "EPR";
      track.idPedido = idPedido;
      track.fechaUltimoMovimiento = item.first.fechaUltimoMovimiento;
      track.cantidadItemsFacturado = "0.00";
      track.cantidadItemsPedido = "0.00";
      track.montoFacturado = "0.00";
      track.montoRegistro = "0.00";

      for (var j = 0; j < item.length; j++) {
        if (getValueStatus(item[j].estado) > getValueStatus(track.estado)) {
          track.estado = item[j].estado;
          track.fechaUltimoMovimiento = item[j].fechaUltimoMovimiento;
        }

        switch (track.estado) {
          case "EPR":
          case "R":
            track.fechaRegistro = item[j].fechaRegistro;
            track.cantidadItemsPedido = item[j].cantidadItemsPedido;
            track.montoRegistro = item[j].montoRegistro;
            break;
          case "F":
            track.idFactura = item[j].idFactura;
            track.montoFacturado = item[j].montoFacturado;
            track.cantidadItemsFacturado = item[j].cantidadItemsFacturado;
            track.fechaFacturado = item[j].fechaFacturado;
            break;
          case "EC":
            track.fechaEnCamino = item[j].fechaEnCamino;
            break;
          case "EN":
            track.fechaEntregado = item[j].fechaEntregado;
            track.tipoEntrega = item[j].tipoEntrega;
            break;
          default:
        }
      }
      listResult.add(track);
    }
    return listResult;
  }

  int getValueStatus(String current) {
    switch (current) {
      case "EN":
        return 4;
      case "EC":
        return 3;
      case "F":
        return 2;
      case "R":
        return 1;
      case "EPR":
        return 0;
      default:
        return 1;
    }
  }
}
