import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/models/current_account.dart';
import 'package:tokin/src/models/data_connection.dart';

class CurrentAccountController with BaseHttp {
  Future<CurrentAccountAPI> getData() async {
    try {
      var endpoint = await new DataConnection().getCuentaCorriente();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(milliseconds: 7000));
      var bodyData = json.decode(utf8.decode(response.bodyBytes));

      CurrentAccountAPI currentAccountAPI =
          CurrentAccountAPI.fromJson(bodyData);

      return currentAccountAPI;
    } catch (error) {
      print(error);
      return new CurrentAccountAPI();
    }
  }
}
