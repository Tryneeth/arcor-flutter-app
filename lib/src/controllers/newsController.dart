import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

class NewsController with BaseHttp, DateFormatter {
  DBHelper<NotificationAPI> _dbHelper;

  NewsController() {
    _dbHelper = new DBHelper<NotificationAPI>(
        dbTableName: 'news', tableField: NotificationAPI.getFieldsForDB());
  }

  Future<List<NotificationAPI>> getData(bool singlePage) async {
    try {
      var endpoint = await new DataConnection().getNotificationUri();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));

      if (response.statusCode == 200 && response.body != "") {
        var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
        var pages = int.tryParse(json
            .decode(utf8.decode(response.bodyBytes))['num_pages']
            .toString());

        List<NotificationAPI> notificationApiList = new List();
        for (int i = 0; i < bodyData.length; i++) {
          var news = NotificationAPI.fromJson(bodyData[i]);
          if (news.eliminado != 1 &&
              DateTime.now().difference(news.fechaUltimaActualizacion).inDays <=
                  45) {
            notificationApiList.add(news);
          }
        }
        if (pages > 1 && !singlePage)
          for (var i = 2; i <= pages; i++) {
            bodyRequest['page'] = i;
            var responseAux = await http
                .post(Uri.encodeFull(endpoint),
                    headers: headers, body: json.encode(bodyRequest))
                .timeout(Duration(seconds: 20));
            var bodyDataAux =
                json.decode(utf8.decode(response.bodyBytes))['data'];

            if (responseAux.statusCode == 200) {
              for (int i = 0; i < bodyData.length; i++) {
                var news = NotificationAPI.fromJson(bodyDataAux[i]);
                if (news.eliminado != 1 &&
                    DateTime.now()
                            .difference(news.fechaUltimaActualizacion)
                            .inDays <=
                        45) {
                  notificationApiList.add(news);
                }
              }
            }
          }
        return notificationApiList;
      } else {
        List<NotificationAPI> notifications = new List();
        var dbData = await _dbHelper.findAll();
        if (dbData.length > 0) {
          dbData.forEach(
              (data) => notifications.add(NotificationAPI.fromJson(data)));
        }
        return notifications;
      }
    } catch (error) {
      List<NotificationAPI> notifications = new List();
      var dbData = await _dbHelper.findAll();
      if (dbData.length > 0) {
        dbData.forEach(
            (data) => notifications.add(NotificationAPI.fromJson(data)));
      }
      return notifications;
    }
  }

  Future<bool> updateViewField(NotificationAPI notificationAPI) async {
    try {
      var endpoint = await new DataConnection().getUpdateNotif();
      Map<dynamic, dynamic> bodyRequestUpdate = new Map<dynamic, dynamic>();
      List<Map<dynamic, dynamic>> notifListData = new List();
      Map<dynamic, dynamic> notifData = new Map();
      var bodyRequest = await getBodyData();
      var headers = await getHeaders();

      notifData["codigo_notificacion"] =
          "${bodyRequest["id_pais"]}-${bodyRequest["id_distribuidor"]}-${bodyRequest["id_sucursal"]}-${bodyRequest["codigo_cliente"]}-${DateFormatter.toStringDate(DateTime.now())}";
      notifData["id_notificacion"] = "${notificationAPI.id}";
      notifData["recibido"] = 1;
      notifData["visto"] = "Visto";

      notifListData.add(notifData);

      bodyRequestUpdate["notificaciones"] = notifListData;

      DBHelper<NotificationAPI> _dbHelper = new DBHelper<NotificationAPI>(
          dbTableName: 'news', tableField: NotificationAPI.getFieldsForDB());
      notificationAPI.visto = "Visto";
      notificationAPI.sincronizado = "false";
      await _dbHelper.update(
          notificationAPI.toMap(), notificationAPI.id.toString());
      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequestUpdate))
          .timeout(Duration(milliseconds: 20000));

      if (response.statusCode == 200) {
        var pages = int.tryParse(json
            .decode(utf8.decode(response.bodyBytes))['num_sucess']
            .toString());

        if (pages > 0) {
          return true;
        }
      }
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
