import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/order.dart';

class CartController with BaseHttpUploadArticle, DateFormatter {
  CartController();

  Future<bool> processShop(Order order) async {
    try {
      var endpoint = await new DataConnection().getUploadArticles();
      var bodyRequest = await getBodyData(order: order);
      var headers = await getHeaders();

      var response = await http
          .post(Uri.encodeFull(endpoint),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(seconds: 20));

      if (response.statusCode == 200 && response.body != "") {
        var bodyData = json.decode(utf8.decode(response.bodyBytes))['data'];
        var success =
            json.decode(utf8.decode(response.bodyBytes))['sucess_count'];
        var error = json.decode(utf8.decode(response.bodyBytes))['error_count'];
        if (success > 0) {
          return true;
        }
        if (error > 0) {
          if (bodyData["status"] == "EXISTE") print("Petición existente");
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}
