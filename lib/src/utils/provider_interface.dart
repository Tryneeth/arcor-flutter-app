import 'package:tokin/src/controllers/base_http.dart';

import 'base_http.dart';

typedef CreateModelFromJson = dynamic Function(Map<String, dynamic> json);
typedef CreateJsonFromModel<T> = Map<String, dynamic> Function(T model);

class IProvider<T> extends DioBase with BaseHttp {
  IProvider(this.endpoint,
      {this.createModelFromJson, this.createJsonFromModel});

  CreateJsonFromModel<T> createJsonFromModel;
  CreateModelFromJson createModelFromJson;
  final endpoint;

  Future<List<T>> findAll() async {
    var endpointProcessed = await endpoint;
    var response = await client.post(endpointProcessed);
    return createModelFromJson(response.data);
  }

  Future<List<T>> findAllAsGet({data}) async {
    var endpointProcessed = await endpoint;
    var response = await client.get(endpointProcessed, queryParameters: data);
    return createModelFromJson({"data": response.data});
  }

  Future<T> findById(int id) async {
    var endpointProcessed = await endpoint;
    var response = await client.get(endpointProcessed + "/$id");
    return createModelFromJson(response.data);
  }

  Future<T> create(T object) async {
    var endpointProcessed = await endpoint;
    var response =
        await client.post(endpointProcessed, data: createJsonFromModel(object));
    return createModelFromJson(response.data);
  }

  Future<T> update(int id, T object) async {
    var endpointProcessed = await endpoint;
    var response = await client.put(endpointProcessed + "/$id",
        data: createJsonFromModel(object));
    return createModelFromJson(response.data);
  }

  Future<T> patch(int id, Map<String, dynamic> properties) async {
    var endpointProcessed = await endpoint;
    var response = await client.patch(endpointProcessed, data: properties);
    return createModelFromJson(response.data);
  }

  Future<T> delete(int id) async {
    var endpointProcessed = await endpoint;
    var response = await client.delete(endpointProcessed + "/$id");
    return createModelFromJson(response.data);
  }
}
