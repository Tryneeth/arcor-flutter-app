import 'dart:io';

import 'package:dio/dio.dart';

import '../controllers/base_http.dart';

class BaseHttpConfig {
  factory BaseHttpConfig() {
    return _baseHttpConfig;
  }

  static final BaseHttpConfig _baseHttpConfig = new BaseHttpConfig._internal();

  BaseHttpConfig._internal() {
    this.dioClient.options.connectTimeout = 20000;
    this.dioClient.options.sendTimeout = 20000;
    this.dioClient.options.receiveTimeout = 20000;
    this.dioClient.options.contentType = ContentType.parse("application/json");
    this.dioClient.options.headers = {"Accept": "application/json"};
  }

  Dio _dioClient = Dio();
  var _token = {
    'Authorization': 'Token 5871c1f3a2def4ef19a6fd8d694e9a72a6ca3758'
  };

  Map get token => _token;

  set token(token) {
    _token = token;
  }

  Dio get dioClient => _dioClient;
}

class DioBase with BaseHttp {
  final _baseHttpConfig = new BaseHttpConfig();

  Dio get client => _baseHttpConfig.dioClient
    ..options.headers.addAll(_baseHttpConfig.token)
    ..interceptors.add(InterceptorsWrapper());
}
