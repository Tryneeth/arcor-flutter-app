//import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/ui/home_activity.dart';

mixin FirebaseMessagingConfig {
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  setUp(BuildContext context) async {
    /*_firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(context, message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        Navigator.of(context).pushReplacementNamed(HomeActivity.route);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        Navigator.of(context).pushReplacementNamed(HomeActivity.route);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {});
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      var sp = await SharedPreferences.getInstance();
      sp.setString("FCM", token);
      print(token);
    });*/
  }

  Widget _buildDialog(BuildContext context, String message, String title) {
    return AlertDialog(
      title: Text(title ?? ""),
      content: Text(message ?? ""),
      actions: <Widget>[
        FlatButton(
          child: const Text('Cerrar'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
      ],
    );
  }

  void _showItemDialog(BuildContext context, Map<String, dynamic> message) {
    /*if (message != null) {
      String finalMessage = "";
      String title = "";
      if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
        if (message.containsKey("notification")) {
          finalMessage = message["notification"]["body"].toString();
          title = message["notification"]["title"];
        } else if (message.containsKey("aps")) {
          finalMessage = message["aps"]["alert"]["body"];
          title = message["aps"]["alert"]["title"];
        }
      } else {
        if (message.containsKey("notification")) {
          finalMessage = message["notification"]["body"].toString();
          title = message["notification"]["title"];
        } else
          finalMessage = message["body"];
      }
      showDialog<bool>(
              context: context,
              builder: (_) => _buildDialog(context, finalMessage, title))
          .then((bool shouldNavigate) {
        if (shouldNavigate == true) {}
      });
    }*/
  }
}
