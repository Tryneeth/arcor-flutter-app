import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/login/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/ui/home_widgets.dart';

import '../tc.dart';

class AppDrawer extends StatefulWidget {
  AppDrawer({Key key, this.activeRoute}) : super(key: key);

  final String activeRoute;

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  var kActiveColor = Colors.grey.shade800;
  var kInactiveColor = Colors.grey.shade500;
  bool supportToolsEnabled = true;
  String userId;
  String userEmail;
  String companyName;
  String companyCode;
  String atpdvNombre;
  String compras;
  String contactos;
  String cuentaCorriente;
  String encuestas;
  String notifications;
  String awards;

  @override
  void initState() {
    SharedPreferences.getInstance().then((preferences) {
      setState(() {
        userId = preferences.getString("ID_CLIENTE");
        userEmail = preferences.getString("email");
        companyName = "ARCORTOKIN";
        companyCode = preferences.getString("ID_DISTRIBUIDOR");
        atpdvNombre = preferences.getString("ATPDV_NOMBRE");
        //Secciones
        compras = preferences.getString("habilitaCompras");
        contactos = preferences.getString("habilitaContactos");
        cuentaCorriente = preferences.getString("habilitaCuentacorriente");
        encuestas = preferences.getString("habilitaEncuestas");
        notifications = preferences.getString("habilitaNotificaciones");
        awards = preferences.getString("habilitaPremios");
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.grey.shade200,
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Container(
                    color: Color.fromRGBO(85, 158, 215, 1),
                    padding: const EdgeInsets.only(top: 30),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Text("1.0.0.20",
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Image.asset(
                                "assets/menu/ic_user-web.png",
                                height: 50.0,
                              ),
                              Text(
                                userEmail != null
                                    ? userEmail.toUpperCase()
                                    : "",
                                style: TextStyle(color: Colors.white),
                              ),
                              Text(
                                userEmail != null ? userEmail : "",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 20.0, horizontal: 20.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Text(
                                  userId != null
                                      ? "Cliente $userId"
                                      : "Cliente",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Text(
                                  "ARCORTOKIN",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Text(
                                  atpdvNombre != null
                                      ? atpdvNombre.toUpperCase()
                                      : "",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  /*supportToolsEnabled
                      ? Container(
                          decoration: BoxDecoration(
                              border: Border(
                            top: BorderSide(color: Colors.grey.shade300),
                            bottom: BorderSide(color: Colors.grey.shade300),
                          )),
                          child: ListTile(
                            leading: DrawerLeadingIcon(
                              icon: Image.asset(
                                "assets/menu/herramienta_soporte.png",
                                width: 30,
                              ),
                              activeRoute: widget.activeRoute,
                              route: "/",
                            ),
                            title: Text("Herramienta Soporte"),
                            selected: widget.activeRoute == "/",
                            onTap: () => _navigationDrawerTap(
                              context,
                              widget.activeRoute,
                              "/",
                            ),
                          ),
                        )
                      : Container(),*/
                  ListTile(title: Text("Tokin"), onTap: () {}),
                  ListTile(
                    enabled: true,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset("assets/menu/alarma.png",
                          width: 30, color: kActiveColor),
                      activeRoute: widget.activeRoute,
                      route: HomeNews.route,
                    ),
                    title: Text("Notificaciones"),
                    selected: widget.activeRoute == HomeNews.route,
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomeNews.route,
                    ),
                  ),
                  ListTile(
                    enabled: true,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset(
                        "assets/menu/ic_mis_actividades_menu-web.png",
                        height: 30,
                        color: kActiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: HomeActivity.route,
                    ),
                    title: Text("Actividades"),
                    selected: widget.activeRoute == HomeActivity.route,
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomeActivity.route,
                    ),
                  ),
                  ListTile(
                    enabled: true,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset(
                        "assets/menu/ic_contacto_menu-web.png",
                        height: 30,
                        color: kActiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: HomeContacts.route,
                    ),
                    title: Text("Contactos"),
                    selected: widget.activeRoute == HomeContacts.route,
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomeContacts.route,
                    ),
                  ),
                  ListTile(
                    enabled: true,
                    leading: DrawerLeadingIcon(
                      icon: SvgPicture.asset(
                        "assets/ic_etapa_registro.svg",
                        height: 30,
                        color: kActiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: HomePolls.route,
                    ),
                    title: Text("Encuestas"),
                    selected: widget.activeRoute == HomePolls.route,
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomePolls.route,
                    ),
                  ),
                  ListTile(
                    enabled: true,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset(
                        "assets/menu/ic_seguimiento_menu.png",
                        height: 30,
                        color: kActiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: HomeTrack.route,
                    ),
                    title: Text("Seguimiento"),
                    selected: widget.activeRoute == HomeTrack.route,
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomeTrack.route,
                    ),
                  ),
                  ListTile(
                    enabled: false,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset(
                        "assets/menu/ic_premio_menu-web.png",
                        height: 30,
                        color: kActiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: "/",
                    ),
                    title: Text("Premios"),
                    selected: widget.activeRoute == "/",
                    onTap: () => _navigationDrawerTap(
                      context,
                      widget.activeRoute,
                      HomeAwards.route,
                    ),
                  ),
                  ListTile(
                    enabled: compras == "1" ? true : false,
                    leading: DrawerLeadingIcon(
                      icon: Image.asset(
                        "assets/menu/ic_shop_black-web.png",
                        height: 30,
                        color: kInactiveColor,
                      ),
                      activeRoute: widget.activeRoute,
                      route: "/",
                    ),
                    title: Text("Compras"),
                    selected: widget.activeRoute == "/",
                    onTap: () =>
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => HomepageUI(
                                  tab: HomepageTab.SHOPPINGCART,
                                ))),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(color: Colors.grey.shade300))),
                    child: ListTile(title: Text("Otros"), onTap: () {}),
                  ),
                  ListTile(
                      leading: DrawerLeadingIcon(
                        icon: Image.asset(
                          "assets/menu/privacidad-de-datos.png",
                          height: 25,
                          color: kActiveColor,
                        ),
                        activeRoute: widget.activeRoute,
                        route: "/",
                      ),
                      title: Text("Términos y condiciones"),
                      selected: widget.activeRoute == "/",
                      onTap: () => Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => TermsAndConditionsUI(
                                readOnly: true,
                              )))),
                  ListTile(
                      enabled: false,
                      leading: DrawerLeadingIcon(
                        icon: Icon(Icons.contacts),
                        activeRoute: widget.activeRoute,
                        route: "/",
                      ),
                      title: Text("Contacto"),
                      selected: widget.activeRoute == "/",
                      onTap: null),
                  ListTile(
                      enabled: false,
                      leading: DrawerLeadingIcon(
                        icon: Icon(Icons.help_outline),
                        activeRoute: widget.activeRoute,
                        route: "/",
                      ),
                      title: Text("Ayuda"),
                      selected: widget.activeRoute == "/",
                      onTap: null),
                  ListTile(
                      leading: DrawerLeadingIcon(
                        icon: Image.asset(
                          "assets/menu/icono-apagar.png",
                          height: 25,
                          color: kActiveColor,
                        ),
                        activeRoute: widget.activeRoute,
                        route: "/",
                      ),
                      title: Text("Cerrar sesión"),
                      selected: widget.activeRoute == "/",
                      onTap: () async {
                        await SharedPreferencesController.setPrefKeepSession(
                            false);
                        LoginBloc().dispatch(InitialLoadLoginEvent());
                        // Navigator.of(context).pushReplacement(MaterialPageRoute(
                        //     builder: (context) => LoginScreen(
                        //           loginBloc: LoginBloc(),
                        //         )));
                      }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DrawerLeadingIcon extends StatelessWidget {
  final Widget icon;
  final String activeRoute;
  final String route;
  final Color color;

  const DrawerLeadingIcon(
      {Key key, @required this.icon, this.activeRoute, this.route, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: this.icon);
  }
}

_navigationDrawerTap(context, activeRoute, route) {
  activeRoute == route
      ? Navigator.pop(context)
      : Navigator.pushReplacementNamed(context, route);
}
