import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/shared/drawer.dart';

class AppScaffold extends StatefulWidget {
  final bool logged;
  final AppDrawer appDrawer;
  final Widget child;
  final PreferredSizeWidget bottom;
  final List<Widget> actions;
  final Widget titleChild;
  final bool hideBackButton;
  final bool activeChart;
  final Widget floatingActionButton;
  final Function leadingAction;
  final Widget bottomNavigationBar;

  const AppScaffold(
      {Key key,
      this.appDrawer,
      @required this.logged,
      @required this.child,
      this.bottom,
      this.actions,
      this.titleChild,
      this.floatingActionButton,
      this.hideBackButton = false,
      this.activeChart = true,
      this.leadingAction, this.bottomNavigationBar})
      : super(key: key);

  @override
  _AppScaffoldState createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  bool activeChart;

  @override
  void initState() {
    SharedPreferencesController.getPrefHabilitaCompras().then((value) {
      setState(() {
        activeChart = value == "1";
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (activeChart == null) {
      return Container();
    }
    return Scaffold(
      key: widget.key,
      floatingActionButton: this.widget.floatingActionButton,
      bottomNavigationBar: this.widget.bottomNavigationBar,
      appBar: AppBar(
          automaticallyImplyLeading: !widget.hideBackButton,
          title: title(),
          centerTitle: true,
          bottom: this.widget.bottom,
          actions: widget.actions ?? [cart()],
          leading: widget.appDrawer == null
              ? IconButton(
                  icon: Icon(defaultTargetPlatform == TargetPlatform.iOS
                      ? CupertinoIcons.back
                      : Icons.arrow_back),
                  onPressed: widget.leadingAction == null
                      ? () => Navigator.pop(context, false)
                      : widget.leadingAction,
                )
              : null),
      drawer: widget.logged ? this.widget.appDrawer : null,
      body: SafeArea(
        top: false,
        bottom: false,
        child: Center(child: this.widget.child),
      ),
    );
  }

  Widget title() {
    return this.widget.titleChild == null
        ? Image.asset("assets/ic_tokin_toolbar.png")
        : this.widget.titleChild;
  }

  Widget cart() {
    int shoppingCartCount = ShoppingCartInfo.of(context).order.products.length;

    if (activeChart && widget.activeChart)
      return GestureDetector(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 1.0),
                    child: Icon(Icons.shopping_cart),
                  ),
                  shoppingCartCount > 0
                      ? Text("$shoppingCartCount",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w700))
                      : Container()
                ],
              )),
          onTap: () async {
            var enableNews = await SharedPreferencesController
                .getPrefHabilitaNotificaciones();
            var enableShop =
                await SharedPreferencesController.getPrefHabilitaCompras();
            // if (enableNews == "1" && enableShop == "1") {
            //   ShoppingCartInfo.of(context).controller.animateTo(2);
            // } else if (enableNews != "1" && enableShop == "1") {
            //   ShoppingCartInfo.of(context).controller.animateTo(0);
            // }
            ShoppingCartInfo.of(context).controller.animateTo(2);
          });
    else {
      return Container();
    }
  }
}
