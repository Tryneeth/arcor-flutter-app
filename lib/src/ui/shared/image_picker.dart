import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CustomImagePicker extends StatefulWidget {
  CustomImagePicker({Key key}) : super(key: key);

  @override
  _CustomImagePickerState createState() => _CustomImagePickerState();
}

class _CustomImagePickerState extends State<CustomImagePicker> {
  var imageSelected;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Seleccione la imagen",
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
        textAlign: TextAlign.center,
      ),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: imageSelected != null ? 50 : 0,
                  width: 150,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: imageSelected != null
                        ? Image.file(
                            imageSelected,
                            fit: BoxFit.cover,
                            filterQuality: FilterQuality.low,
                          )
                        : Container(),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                IconButton(
                  iconSize: 48,
                  icon: Icon(
                    Icons.photo_library,
                    color: Theme.of(context).primaryColor,
                  ),
                  onPressed: () async {
                    var image = await ImagePicker.pickImage(
                        source: ImageSource.gallery);
                    setState(() {
                      imageSelected = image;
                    });
                  },
                ),
              ],
            )
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar"),
          onPressed: () {
            Navigator.pop(context, null);
          },
        ),
        if (imageSelected != null)
          FlatButton(
            child: Text("Aceptar"),
            onPressed: () {
              Navigator.pop(context, imageSelected);
            },
          ),
      ],
    );
  }
}
