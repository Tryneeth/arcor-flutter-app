import 'package:flutter/material.dart';

class NoDataWidget extends StatelessWidget {
  final String message;
  
  const NoDataWidget({
    Key key, this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, Colors.grey.shade400])),
      child: Center(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Opacity(
              child:
                  Image.asset("assets/ic_img_tokin.png", height: 250),
              opacity: 0.5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                this.message ?? "No hay datos en estos momentos",
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}