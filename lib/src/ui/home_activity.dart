import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tokin/src/controllers/activities_viewmodel.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class HomeActivity extends StatefulWidget {
  static String route = "activity";
  final ActivitiesViewModel viewModel;

  HomeActivity({this.viewModel});
  @override
  _HomeActivityState createState() => _HomeActivityState();
}

class _HomeActivityState extends State<HomeActivity> {
  String selectedFilter;

  List<ActivityAPI> activities;

  @override
  void initState() {
    activities = widget.viewModel.activities;
    selectedFilter = "Todos";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: Column(
      key: ValueKey("activityPage"),
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          color: Colors.green,
          height: 150,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.viewModel.currentAccount.balance == null
                        ? "\$0.00"
                        : "\$" + widget.viewModel.currentAccount.balance,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .title
                        .copyWith(fontSize: 30),
                  ),
                  Text(
                      "Cuenta corriente al " +
                          DateFormat("dd/MM/y").format(
                              widget.viewModel.currentAccount.lastUpdateDate),
                      style: Theme.of(context)
                          .primaryTextTheme
                          .title
                          .copyWith(fontSize: 16)),
                ],
              ),
              Positioned(
                  right: 0,
                  child: PopupMenuButton<String>(
                      color: Colors.green,
                      icon: Icon(
                        Icons.more_vert,
                        color: Theme.of(context).primaryTextTheme.title.color,
                        size: 40,
                      ),
                      padding: EdgeInsets.zero,
                      initialValue: selectedFilter,
                      onSelected: (String selection) {
                        setState(() {
                          switch (selection) {
                            case "FA":
                              selectedFilter = "Compras";
                              activities = widget.viewModel.activities
                                  .where((item) =>
                                      item.movType == "FA" ||
                                      item.movType == "FB" ||
                                      item.movType == "FC")
                                  .toList();
                              break;
                            case "NC":
                              selectedFilter = "Créditos/Débitos";
                              activities = widget.viewModel.activities
                                  .where((item) => item.movType == "NC")
                                  .toList();
                              break;
                            case "All":
                              selectedFilter = "Todos";
                              activities = widget.viewModel.activities;
                              break;
                          }
                        });
                      },
                      itemBuilder: (BuildContext context) {
                        var style = TextStyle(
                            color: Colors.white,
                            textBaseline: TextBaseline.alphabetic,
                            fontWeight: FontWeight.w400,
                            fontSize: 16);

                        return <PopupMenuItem<String>>[
                          PopupMenuItem<String>(
                            value: "FA",
                            child: Text("Compras"),
                            textStyle: style,
                          ),
                          PopupMenuItem<String>(
                            value: "NC",
                            child: Text("Créditos/Débitos"),
                            textStyle: style,
                          ),
                          PopupMenuItem<String>(
                            value: "All",
                            child: Text("Todos"),
                            textStyle: style,
                          ),
                        ];
                      })),
              Positioned(
                left: 20,
                bottom: 10,
                child: Text(
                  "Filtro: " + selectedFilter,
                  style: Theme.of(context)
                      .primaryTextTheme
                      .title
                      .copyWith(fontSize: 14),
                ),
              )
            ],
          ),
        ),
        Flexible(
            child: activities.isNotEmpty
                ? ListView.builder(
                    key: ValueKey("activitiesList"),
                    shrinkWrap: true,
                    itemCount: activities.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: Image.asset(
                          activities[index].getIconUrl(),
                          height: 50,
                          width: 35,
                          fit: BoxFit.cover,
                        ),
                        title: Text(DateFormat("dd/MM/y")
                            .format(activities[index].fechaMov)),
                        subtitle: Text(activities[index].getSubtitle()),
                        trailing: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              "Importe:",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .body1
                                  .copyWith(fontSize: 18),
                            ),
                            Text(
                              "\$ " + activities[index].montoMov.toString(),
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .body1
                                  .copyWith(fontSize: 21),
                            ),
                          ],
                        ),
                      );
                    })
                : NoDataWidget(
                    message:
                        "Filtro de $selectedFilter Sin Datos",
                  )),
      ],
    ));
  }
}
