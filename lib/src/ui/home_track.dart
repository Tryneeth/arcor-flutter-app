import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:tokin/src/controllers/trackController.dart';
import 'package:tokin/src/models/tracks.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class HomeTrack extends StatefulWidget {
  static String route = "track";

  @override
  _HomeTrackState createState() => _HomeTrackState();
}

class _HomeTrackState extends State<HomeTrack> {
  Future<List<TrackAPI>> getTracks;

  @override
  void initState() {
    getTracks = TrackController().getData(false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: FutureBuilder(
        future: getTracks,
        builder: (BuildContext context, AsyncSnapshot<List<TrackAPI>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Text('Sin conexión');
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.active:
              return new Text('lol');
            case ConnectionState.done:
              if (!snapshot.hasData) {
                return NoDataWidget(
                  message: "No hay datos de Seguimiento en estos momentos",
                );
              }
              if (snapshot.hasError) {
                print(snapshot.error);
                return NoDataWidget(
                  message: "No hay datos de Seguimiento en estos momentos debido a problemas de conexión",
                );
              } else {
                return ExpandedTrack(
                  tracks: snapshot.data,
                );
              }
          }
        },
      ),
    );
  }
}

class ExpandedTrack extends StatefulWidget {
  final List<TrackAPI> tracks;

  const ExpandedTrack({Key key, this.tracks}) : super(key: key);

  @override
  _ExpandedTrackState createState() => _ExpandedTrackState();
}

class _ExpandedTrackState extends State<ExpandedTrack> {
  String trackTitle;
  List<bool> expanded;

  @override
  void initState() {
    super.initState();
    widget.tracks.sort((tr1, tr2) {
     return tr1.fechaUltimoMovimiento.isAfter(tr2.fechaUltimoMovimiento) ? -1 : 1;

    });
    expanded = List<bool>.generate(widget.tracks?.length ?? 0, (context) => false);
  }

  @override
  Widget build(BuildContext context) {
    var tracks = widget.tracks;

    return tracks.isNotEmpty
        ? ListView.builder(
            key: ValueKey("trackList"),
            itemCount: tracks.length,
            itemBuilder: (BuildContext context, int index) {
              String logoUrl = "";
              switch (tracks[index].estado) {
                case "F":
                  trackTitle = "Facturado";
                  logoUrl = "assets/ic_etapa_facturado.svg";
                  break;
                case "EC":
                  trackTitle = "En Camino";
                  logoUrl = "assets/ic_etapa_enviado.png";
                  break;
                case "EN":
                  trackTitle = "Entregado";
                  logoUrl = "assets/ic_etapa_entregado.png";
                  break;
                case "R":
                  trackTitle = "Registrado";
                  logoUrl = "assets/ic_etapa_registro.svg";
                  break;
                case "EPR":
                  trackTitle = "Pendiente de Registro";
                  logoUrl = "assets/ic_etapa_registro.svg";
                  break;
                default:
                  trackTitle = "Estado Desconocido";
                  break;
              }
              return ExpansionTile(
                title: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Pedido"),
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0),
                      child: Text(
                        trackTitle ?? "",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                    ),
                  ],
                ),
                leading: tracks[index].estado == 'F' || tracks[index].estado == 'R' || tracks[index].estado == 'EPR'
                    ? SvgPicture.asset(
                        logoUrl,
                        width: 40.0,
                      )
                    : Image.asset(
                        logoUrl,
                        width: 40.0,
                      ),
                trailing: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      tracks[index].fechaUltimoMovimiento != null
                          ? DateFormat("dd/MM/y").format(tracks[index].fechaUltimoMovimiento)
                          : "Sin Datos",
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(
                        "${"Cod. Pedido:"} ${tracks[index].idPedido}",
                        style: TextStyle(fontSize: 9),
                      ),
                    ),
                  ],
                ),
                initiallyExpanded: false,
                onExpansionChanged: (ex) {
                  expanded.replaceRange(0, tracks.length, List<bool>.generate(tracks.length, (context) => false));
                  expanded[index] = ex;
                  setState(() {});
                },
                backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
                children: <Widget>[
                  OrderStepTile(
                    title: "Registrado",
                    icon: SvgPicture.asset(
                      "assets/ic_etapa_registro.svg",
                      width: 40.0,
                    ),
                    date: tracks[index].fechaRegistro,
                    amount: tracks[index].montoRegistro,
                    items: tracks[index].cantidadItemsPedido,
                    active: tracks[index].estado == "R" ? true : false,
                  ),
                  OrderStepTile(
                    title: "Facturado",
                    icon: SvgPicture.asset(
                      "assets/ic_etapa_facturado.svg",
                      width: 40.0,
                    ),
                    date: tracks[index].fechaFacturado,
                    amount: tracks[index].montoFacturado,
                    items: tracks[index].cantidadItemsFacturado,
                    active: tracks[index].estado == "F" ? true : false,
                  ),
                  OrderStepTile(
                    title: "En Camino",
                    icon: Image.asset(
                      "assets/ic_etapa_enviado.png",
                      width: 40.0,
                    ),
                    date: tracks[index].fechaEnCamino,
                    amount: "",
                    items: "",
                    active: tracks[index].estado == "EC" ? true : false,
                  ),
                  OrderStepTile(
                    title: "Entregado",
                    icon: Image.asset(
                      "assets/ic_etapa_entregado.png",
                      width: 40.0,
                    ),
                    amount: "",
                    items: "",
                    date: tracks[index].fechaEntregado,
                    active: tracks[index].estado == "EN" ? true : false,
                    isLast: true,
                  ),
                ],
              );
            })
        : Center(child: Text("No hay datos, puede haber problemas con la conexión", style: TextStyle(fontSize: 16)));
  }
}

class OrderStepTile extends StatelessWidget {
  final bool active;
  final Widget icon;
  final String title;
  final DateTime date;
  final String amount;
  final String items;
  final bool isLast;

  const OrderStepTile(
      {Key key, this.active = false, this.icon, this.title, this.date, this.amount, this.items, this.isLast = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: this.items.isEmpty || this.items == "0.00"
          ? Text(this.title)
          : Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(this.title),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: Text(
                    "Items: ${this.items}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
      leading: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                width: 1,
                color: Colors.black,
              ),
              if (isLast)
                Positioned(
                    bottom: 0,
                    child: Container(
                      width: 1,
                      height: 25,
                      color: Colors.white,
                    )),
              Positioned(
                right: 5.0,
                child: Container(
                  height: 1,
                  width: 15.0,
                  color: Colors.black,
                ),
              ),
              Container(
                width: 15,
                height: 15,
                margin: EdgeInsets.only(right: 15.0, left: 15.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    color: this.active ? Colors.lightGreenAccent : Colors.white,
                    border: Border.all(color: Colors.black)),
              ),
            ],
          ),
          this.icon
        ],
      ),
      trailing: this.date == null && (this.amount.isEmpty || this.amount == "0.00")
          ? Text("Sin Datos")
          : Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  this.date != null ? DateFormat("dd/MM/y").format(this.date) : "Sin Datos",
                ),
                this.amount.isEmpty || this.amount == "0.00"
                    ? Padding(padding: const EdgeInsets.only(top: 5.0))
                    : Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(
                          "\$ ${this.amount}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
              ],
            ),
    );
  }
}
