import 'package:carousel_slider/carousel_slider.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/login/index.dart';
import 'package:tokin/src/blocs/register/index.dart';
import 'package:tokin/src/controllers/register.dart';
import 'package:tokin/src/ui/tc.dart';

class WelcomeUI extends StatefulWidget {
  @override
  _WelcomeUIState createState() => _WelcomeUIState();
}

class _WelcomeUIState extends State<WelcomeUI> {
  var _pageController = PageController();
  bool _hideInitialHelp;

  @override
  void initState() {
    _hideInitialHelp = false;
    _checkPreferences();
    super.initState();
  }

  _checkPreferences() async {
    /* var value = await SharedPreferencesController.getPrefHideInitialHelp();
    try {
      setState(() {
        _hideInitialHelp = value;
      });
    } catch (_) {
      SharedPreferencesController.setPrefHideInitialHelp(false);
      setState(() {
        _hideInitialHelp = false;
      });
    }*/
  }

  @override
  Widget build(BuildContext context) {
    if (_hideInitialHelp == null) _hideInitialHelp = false;
    return PageView(
      controller: _pageController,
      children: <Widget>[
        if (_hideInitialHelp != null && !_hideInitialHelp)
          WelcomeScreen(
            hideInitialHelp: _hideInitialHelp,
            onHideInitialHelp: (hide) async {
              //await SharedPreferencesController.setPrefHideInitialHelp(hide);
              setState(() {
                _hideInitialHelp = hide;
                if (hide) _pageController.jumpToPage(1);
              });
            },
          ),
        (_hideInitialHelp != null && _hideInitialHelp)
            ? LoginPage()
            : InitialHelpScreen()
      ],
    );
  }
}

typedef HideInitialHelpCallback(bool hide);

class WelcomeScreen extends StatefulWidget {
  final bool hideInitialHelp;
  final HideInitialHelpCallback onHideInitialHelp;

  const WelcomeScreen({Key key, this.onHideInitialHelp, this.hideInitialHelp})
      : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  bool _hideInitialHelp;
  bool _hideRegister = false;

  @override
  void initState() {
    _validar();
    _hideInitialHelp = this.widget.hideInitialHelp ?? false;
    super.initState();
    _checkPermissions();
    if (!_hideRegister) {
      _importantWarning(context);
    }
  }

  _checkPermissions() async {
    var hide = await SharedPreferences.getInstance()
        .then((pref) => pref.getBool("hideRegister"));
    setState(() {
      _hideRegister = hide == null ? false : hide;
    });
  }

  _validar() async {
    var info;
    if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
      info = await DeviceInfoPlugin().iosInfo;
    } else {
      info = await DeviceInfoPlugin().androidInfo;
    }
    String id = info is IosDeviceInfo
        ? "${info.identifierForVendor}"
        : "${(info as AndroidDeviceInfo).androidId}";
    var email = await SharedPreferences.getInstance()
        .then((pref) => pref.getString("email"));
    if (email != null && email.isNotEmpty) {
      await RegisterController().toValidate(email: email, phoneId: id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade400,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "assets/ic_tokin.png",
                    height: MediaQuery.of(context).size.height / 3,
                  ),
                  Text(
                    "Bienvenido a nuestro\ncanal de comunicación",
                    style: Theme.of(context).primaryTextTheme.title,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            Positioned(
              left: 0.0,
              right: 0.0,
              bottom: 20.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Theme(
                    data: Theme.of(context)
                        .copyWith(unselectedWidgetColor: Colors.white),
                    child: Checkbox(
                        key: ValueKey("checkboxInitialHelp"),
                        activeColor: Colors.white,
                        checkColor: Colors.blue,
                        value: _hideInitialHelp,
                        onChanged: (value) {
                          setState(() {
                            _hideInitialHelp = value;
                          });
                          this.widget.onHideInitialHelp(value);
                        }),
                  ),
                  Text(
                    "Ocultar ayuda inicial",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _importantWarning(context) async {
    var info;
    if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
      info = await DeviceInfoPlugin().iosInfo;
    } else {
      info = await DeviceInfoPlugin().androidInfo;
    }

    var hide = await SharedPreferences.getInstance()
        .then((pref) => pref.getBool("hideRegister"));
    if (hide == null) hide = false;
    if (!hide) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
              title: Text("Aviso Importante".toUpperCase()),
              content: RegisterContent(
                info: info,
              )));
    }
  }
}

bool _getTermConditions() {
  var approved = false;
  SharedPreferences.getInstance()
      .then((pref) => approved = pref.getBool("termConditions") ?? false);
  return approved;
}

class RegisterContent extends StatefulWidget {
  final info;

  const RegisterContent({foundation.Key key, this.info}) : super(key: key);

  @override
  _RegisterContentState createState() => _RegisterContentState();
}

class _RegisterContentState extends State<RegisterContent> {
  bool tc = false;
  String email;
  bool charging;
  var _pageController = PageController();
  GlobalKey<FormState> _key = GlobalKey();

  bool terms = _getTermConditions();

  _register() {
    setState(() {
      charging = true;
    });
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RegisterScreen(
            phoneId: widget.info,
            email: email,
            registerBloc: new RegisterBloc())));
  }

  Future<bool> _validar() async {
    String id = widget.info is IosDeviceInfo
        ? "${widget.info.identifierForVendor}"
        : "${(widget.info as AndroidDeviceInfo).androidId}";
    var result =
        await RegisterController().toValidate(email: email, phoneId: id);
    return result;
  }

  @override
  void initState() {
    super.initState();
    email = "";
    charging = true;
  }

  @override
  Widget build(BuildContext context) {
    var info = this.widget.info;

    return Form(
        key: _key,
        child: Container(
          height: MediaQuery.of(context).size.height - 100,
          width: MediaQuery.of(context).size.width - 50,
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Text(
                        "Para el correcto funcionamiento de TOKIN, le solicitamos el uso de sus datos de Email e ID del dispositivo para el registro de su equipo en nuestros servidores."),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(widget.info),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0.0),
                        child: TextFormField(
                            key: ValueKey("emailFirstContact"),
                            decoration: InputDecoration(
                              labelText: "Email",
                              hasFloatingPlaceholder: true,
                            ),
                            validator: (value) {
                              var emailExp = RegExp(
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                              if (value.isEmpty)
                                return "Campo obligatorio";
                              else if (!emailExp.hasMatch(value) &&
                                  value.isNotEmpty) return "Correo inválido";
                              return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                email = value;
                              });
                            })),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text("Muchas Gracias"),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                              key: ValueKey("termsCondition"),
                              value: terms,
                              onChanged: (value) {
                                SharedPreferences.getInstance().then((pref) =>
                                    pref.setBool("termConditions", value));

                                setState(() {
                                  terms = value;
                                });
                              }),
                          Column(
                            children: <Widget>[
                              Text(
                                "Acepto los ",
                                style: TextStyle(fontSize: 12),
                              ),
                              InkWell(
                                onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TermsAndConditionsUI(
                                              readOnly: false,
                                            ))),
                                child: Text(
                                  "Términos y Condiciones",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            "CERRAR",
                          )),
                      charging
                          ? FlatButton(
                              onPressed: terms
                                  ? () async {
                                      if (_key.currentState.validate()) {
                                        _key.currentState.save();
                                        setState(() {
                                          charging = false;
                                        });
                                        if (!await _validar()) {
                                          _register();
                                        } else {
                                          SharedPreferences.getInstance()
                                              .then((pref) {
                                            pref.setBool("hideRegister", true);
                                            pref.setString("email", email);
                                          });
                                          Navigator.of(context).pop();
                                        }
                                      }
                                    }
                                  : null,
                              child: Text(
                                "ACEPTAR",
                              ),
                            )
                          : FlatButton(
                              onPressed: () {},
                              child: CircularProgressIndicator(),
                            )
                    ],
                  ),
                ],
              )
            ],
          ),
        ));
  }
}

class InitialHelpScreen extends StatefulWidget {
  @override
  _InitialHelpScreenState createState() => _InitialHelpScreenState();
}

class _InitialHelpScreenState extends State<InitialHelpScreen> {
  var _selectedIndex = 0;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    var _items = [
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_novedad.png",
        title: "Novedades",
        description:
            "Vas a recibir todas las\nnovedades y promociones\na tiempo.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_cuenta.png",
        title: "Mi Actividad",
        description: "Tendrás toda la\ninformación de tu\ncuenta corriente.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_contacto.png",
        title: "Contactos",
        description: "Vas a poder contactarse\ncon nosotros.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_encuesta.png",
        title: "Encuetas",
        description:
            "Podrás responder\npreguntas y sumar puntos\npara canjear.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_seguimiento.png",
        title: "Seguimiento",
        description: "Vas a tener a mano el\nestado de tu pedido.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_concursos.png",
        title: "Premio",
        description: "Podrás seguir los\nconcursos y premios.",
      ),
      SliderInfo(
        imageUrl: "assets/ic_hor/ic_hor_compras.png",
        title: "Compras",
        description:
            "Vas a poder cargar un\npedido co los productos\nque necesites.",
      ),
    ];

    return Scaffold(
      backgroundColor: Colors.blue.shade300,
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: CarouselSlider(
                    // aspectRatio: 0.5,
                    aspectRatio: 1,
                    enlargeCenterPage: true,
                    autoPlay: false,
                    enableInfiniteScroll: false,
                    initialPage: 0,
                    onPageChanged: (index) {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    items: _items,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 0.0,
            right: 0.0,
            bottom: 30.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(
                _items,
                (index, url) {
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _selectedIndex == index
                            ? Colors.white
                            : Colors.blue.shade200),
                  );
                },
              ),
            ),
          ),
          Positioned(
            bottom: 20.0,
            right: 30.0,
            child: FlatButton(
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => LoginPage())),
                child: _selectedIndex != _items.length - 1
                    ? Text(
                        "OMITIR",
                        style: TextStyle(color: Colors.white),
                      )
                    : Text(
                        "INGRESAR",
                        style: TextStyle(color: Colors.white),
                      )),
          )
        ],
      ),
    );
  }
}

class SliderInfo extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String description;

  const SliderInfo({
    Key key,
    @required this.imageUrl,
    @required this.title,
    @required this.description,
  })  : assert(imageUrl != null),
        assert(title != null),
        assert(description != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned(
                top: 3,
                left: 3,
                child: Container(
                  height: 120,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.asset(
                    this.imageUrl,
                    height: 120,
                    color: Colors.black.withOpacity(0.3),
                  ),
                ),
              ),
              Container(
                height: 120,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  this.imageUrl,
                  height: 120,
                ),
              ),
            ],
          ),
          Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10.0),
            color: Colors.grey.shade200,
          ),
          Text(this.title, style: Theme.of(context).primaryTextTheme.title),
          Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10.0),
            color: Colors.grey.shade200,
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 40.0, right: 30, left: 30),
              child: Text(
                this.description,
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
