import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tokin/src/blocs/activity/index.dart';
import 'package:tokin/src/blocs/awards/index.dart';
import 'package:tokin/src/blocs/cart/index.dart';
import 'package:tokin/src/blocs/catalog/index.dart';
import 'package:tokin/src/blocs/contacts/index.dart';
import 'package:tokin/src/blocs/news/index.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/blocs/polls/polls_start_screen.dart';
import 'package:tokin/src/blocs/track/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';

enum HomepageTab { NEWS, CATALOG, MY_ACTIVITY, CONTACT, POLLS, TRACK, AWARD, SHOPPINGCART }

class _Page {
  const _Page({this.icon, this.text, @required this.child, this.tag});

  final dynamic icon;
  final String text;
  final Widget child;
  final HomepageTab tag;
}

class HomepageUI extends StatefulWidget {
  static String route = '/';

  final HomepageTab tab;

  const HomepageUI({Key key, this.tab}) : super(key: key);

  @override
  _HomepageUIState createState() => _HomepageUIState();
}

class _HomepageUIState extends State<HomepageUI> with SingleTickerProviderStateMixin {
  TabController _controller;
  List<_Page> pagesDynamically;

  Future<List<_Page>> populateTabs() async {
    var _iconWidth = 50.0;
    var compras = await SharedPreferencesController.getPrefHabilitaCompras();
    /* var contactos =
        await SharedPreferencesController.getPrefHabilitaContactos();
    var cuentaCorriente =
        await SharedPreferencesController.getPrefHabilitaCuentacorriente();
    var encuestas =
        await SharedPreferencesController.getPrefHabilitaEncuestas();
    var notifications =
        await SharedPreferencesController.getPrefHabilitaNotificaciones();
    var awards = await SharedPreferencesController.getPrefHabilitaPremios();*/
    List<_Page> pages = <_Page>[
      _Page(
          icon: Image.asset(
            "assets/ic_hor/ic_hor_novedad.png",
            width: _iconWidth,
          ),
          text: "Novedades",
          child: NewsScreen(
            newsBloc: NewsBloc(),
          ),
          tag: HomepageTab.NEWS),
      if (compras == "1")
        _Page(
            icon: SvgPicture.asset(
              "assets/ic_hor/ic_menu_catalog.svg",
              width: _iconWidth,
            ),
            text: "Catálogo",
            child: CatalogScreen(
              catalogBloc: CatalogBloc(),
            ),
            tag: HomepageTab.CATALOG),
      if (compras == "1")
        _Page(
            icon: Image.asset(
              "assets/ic_hor/ic_hor_compras.png",
              width: _iconWidth,
            ),
            text: "Compras",
            child: CartScreen(
              cartBloc: CartBloc(),
            ),
            tag: HomepageTab.SHOPPINGCART),
      _Page(
          icon: Image.asset(
            "assets/ic_hor/ic_hor_encuesta.png",
            width: _iconWidth,
          ),
          text: "Encuestas",
          child: PollsStartScreen(
            pollsBloc: PollsBloc(),
          ),
          tag: HomepageTab.POLLS),
      _Page(
          icon: Image.asset(
            "assets/ic_hor/ic_hor_contacto.png",
            width: _iconWidth,
          ),
          text: "Contactos",
          child: ContactsPage(),
          tag: HomepageTab.CONTACT),
      _Page(
          icon: Image.asset(
            "assets/ic_hor/ic_hor_cuenta.png",
            width: _iconWidth,
          ),
          text: "Mi actividad",
          child: ActivityScreen(
            activityBloc: ActivityBloc(),
          ),
          tag: HomepageTab.MY_ACTIVITY),
      _Page(
          icon: Image.asset(
            "assets/ic_hor/ic_hor_seguimiento.png",
            width: _iconWidth,
          ),
          text: "Seguimiento",
          child: TrackScreen(
            trackBloc: TrackBloc(),
          ),
          tag: HomepageTab.TRACK),
//      _Page(
//          icon: Image.asset(
//            "assets/ic_hor/ic_hor_concursos.png",
//            width: _iconWidth,
//          ),
//          text: "Premios",
//          child: AwardsScreen(
//            awardsBloc: AwardsBloc(),
//          ),
//          tag: HomepageTab.AWARD)
    ];
    return pages;
  }

  @override
  void initState() {
    super.initState();

    populateTabs().then((value) {
      ShoppingCartInfo.of(context).controller = TabController(vsync: this, length: value.length);
      ShoppingCartInfo.update(context);
      _controller = ShoppingCartInfo.of(context).controller;
      if (widget.tab != null) {
        var index = value.indexWhere((page) => page.tag == widget.tab);
        if (index >= 0) {
          _controller.animateTo(index);
        }
      }
      setState(() {
        pagesDynamically = value;
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (pagesDynamically == null) {
      return Container();
    }
    return AppScaffold(
      logged: true,
      appDrawer: AppDrawer(
        activeRoute: HomepageUI.route,
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: Container(
          child: TabBar(
            key: ValueKey("tabbar"),
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorColor: Colors.white,
            controller: _controller,
            isScrollable: true,
            tabs: pagesDynamically.map<Container>((_Page page) {
              switch (page.tag) {
                case HomepageTab.NEWS:
                  return drawTab(page, ShoppingCartInfo.of(context).flagNews);
                case HomepageTab.SHOPPINGCART:
                  return drawTab(page, ShoppingCartInfo.of(context).flagCart);
                case HomepageTab.CONTACT:
                  return drawTab(page, ShoppingCartInfo.of(context).flagContact);
                case HomepageTab.POLLS:
                  return drawTab(page, ShoppingCartInfo.of(context).flagPolls);
                case HomepageTab.TRACK:
                  return drawTab(page, ShoppingCartInfo.of(context).flagTrack);
                default:
                  return drawTab(page, false);
              }
            }).toList(),
          ),
        ),
      ),
      child: TabBarView(
          controller: _controller,
          children: pagesDynamically.map<Widget>((_Page page) {
            return SafeArea(
              top: false,
              bottom: false,
              child: Container(
                key: ObjectKey(page.icon),
                padding: const EdgeInsets.all(0.0),
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Center(child: page.child),
                ),
              ),
            );
          }).toList()),
    );
  }

  Widget drawTab(_Page page, bool flag) {
    return Container(
      height: 70.0,
      child: Tab(
        child: Column(
          children: <Widget>[
            Stack(children: <Widget>[
              page.icon,
              if (flag)
                Positioned(
                  child: Icon(
                    Icons.brightness_1,
                    color: Colors.greenAccent,
                    size: 15,
                  ),
                  top: 5,
                  right: 0,
                )
            ]),
            Text(page.text)
          ],
        ),
      ),
    );
  }
}
