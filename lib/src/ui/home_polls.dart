import 'package:flutter/material.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class HomePolls extends StatelessWidget {
  static String route = "polls";

  @override
  Widget build(BuildContext context) {
    return NoDataWidget(
      message: "No hay datos de Encuestas en estos momentos",
    );
  }
}
