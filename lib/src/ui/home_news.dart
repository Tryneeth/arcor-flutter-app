import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tokin/src/blocs/single_new/index.dart';
import 'package:tokin/src/controllers/newsController.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class HomeNews extends StatefulWidget {
  static String route = "news";

  @override
  _HomeNewsState createState() => _HomeNewsState();
}

class _HomeNewsState extends State<HomeNews> {
  Future<List<NotificationAPI>> getNewsData;

  @override
  void initState() {
    getNewsData = NewsController().getData(false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

typedef NewsAPICallback(NotificationAPI model);

Widget newsListView(List<NotificationAPI> data, NewsAPICallback onUpdateNews) {
  SingleNewBloc singleNewBloc = new SingleNewBloc();
  if (data.length == 0) {
    return NoDataWidget(
      message: "No hay datos de Novedades en estos momentos",
    );
  }
  return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () async {
            onUpdateNews(data[index]);
            if (data[index].visto.toLowerCase() != null &&
                (data[index].visto.toLowerCase() == "no visto")) {
              await NewsController().updateViewField(data[index]);
            }
            print("cosaaaaaa");
            /*Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SingleNewScreen(
                        singleNew: data[index],
                        newsBloc: singleNewBloc,
                      )),
            );*/
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey.shade200, width: 1))),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          data[index].categoria,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .title
                              .copyWith(color: Colors.blue),
                        ),
                        if (data[index].opcionCompra ?? false)
                          Container(
                              decoration: ShapeDecoration(
                                color: Colors.green.shade800,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                ),
                              ),
                              alignment: Alignment.center,
                              child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 2),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 1),
                                          child: Text(
                                            "COMPRAR",
                                            style: TextStyle(
                                              fontSize: 9,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.white,
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                      Icon(
                                        Icons.shopping_cart,
                                        color: Colors.white,
                                        size: 12,
                                      )
                                    ],
                                  ))),
                        Text(DateFormat("dd/MM/y")
                            .format(data[index].fechaUltimaActualizacion))
                      ],
                    )),
                data[index].imagenAdjunto.isNotEmpty
                    ? Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.only(top: 15),
                            //height: 200,
                            child: Center(
                                child: FutureBuilder(
                              future: DataConnection().obtainImageUrl(),
                              builder: (context, snapshot) {
                                if (snapshot.hasData &&
                                    snapshot.connectionState ==
                                        ConnectionState.done) {
                                  return new CachedNetworkImage(
                                    imageUrl: snapshot.data +
                                        data[index].imagenAdjunto,
                                    errorWidget: (context, url, error) =>
                                        Container(),
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            )),
                          ),
                          if (data[index].visto.toLowerCase() != "visto")
                            Positioned(
                              right: 0,
                              child: Image.asset(
                                "assets/new_novedad_new.png",
                                height: 50,
                              ),
                            ),
                        ],
                      )
                    : Container(),
                data[index].imagenAdjunto.isEmpty
                    ? Stack(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 12.0, left: 20, right: 20),
                            child: Container(
                              constraints: BoxConstraints.expand(height: 30),
                              child: Text(
                                data[index].titulo,
                                textAlign: TextAlign.left,
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .title
                                    .copyWith(
                                        color: Colors.blue,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w300),
                              ),
                            ),
                          ),
                          if (data[index].visto.toLowerCase() != "visto")
                            Positioned(
                              right: 0,
                              child: Image.asset(
                                "assets/new_novedad_new.png",
                                height: 40,
                              ),
                            ),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.only(
                            top: 12.0, left: 20, right: 20),
                        child: Container(
                          constraints: BoxConstraints.expand(height: 20),
                          child: Text(
                            data[index].titulo,
                            textAlign: TextAlign.left,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .title
                                .copyWith(
                                    color: Colors.blue,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 12.0, left: 20, right: 20),
                  child: Row(
                    children: <Widget>[
                      Text(
                        data[index].descripcion.length > 30
                            ? "${data[index].descripcion.substring(0, 10)}..."
                            : data[index].descripcion,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .body1
                            .copyWith(fontSize: 18),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });
}

class NewsDetails extends StatelessWidget {
  final NotificationAPI singleNew;

  const NewsDetails({Key key, @required this.singleNew}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      logged: true,
      child: Column(
        children: <Widget>[
          Flexible(
            child: ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                            width: (MediaQuery.of(context).size.width - 40) / 3,
                            child: Container()),
                        Container(
                          width: (MediaQuery.of(context).size.width - 40) / 3,
                          child: Center(
                            child: Text(
                              this.singleNew.categoria,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .title
                                  .copyWith(color: Colors.lightBlue),
                            ),
                          ),
                        ),
                        Container(
                          width: (MediaQuery.of(context).size.width - 40) / 3,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  DateFormat('dd/MM/y').format(DateTime.now()),
                                  style: TextStyle(fontSize: 12),
                                )
                              ]),
                        ),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 20),
                  child: Row(
                    children: <Widget>[
                      Text(
                        this.singleNew.titulo,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(color: Colors.lightBlue),
                      ),
                    ],
                  ),
                ),
                Container(
                    child: Center(
                        child: singleNew.imagenAdjunto.length != 0
                            ? FutureBuilder(
                                future: DataConnection().obtainImageUrl(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData &&
                                      snapshot.connectionState ==
                                          ConnectionState.done) {
                                    return new CachedNetworkImage(
                                      imageUrl: snapshot.data +
                                          singleNew.imagenAdjunto,
                                      errorWidget: (context, url, error) =>
                                          Container(),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                              )
                            : Container())),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 20),
                  child: Row(
                    children: <Widget>[
                      new Container(
                        child: Text(
                          this.singleNew.descripcion,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            wordSpacing: 2,
                          ),
                        ),
                        width: MediaQuery.of(context).size.width - 40,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(vertical: 10),
            color: Colors.black.withOpacity(0.75),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 3),
                  child: Text(
                    "Precio s/imp",
                    style: TextStyle(color: Colors.white, fontSize: 11),
                  ),
                ),
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 3),
                  child: Text(
                    "No se cargó el precio Consulta al vendedor",
                    style: TextStyle(color: Colors.white, fontSize: 11),
                  ),
                ),
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 3),
                  child: FlatButton(
                      color: Colors.green,
                      padding: const EdgeInsets.all(0),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      onPressed: () {},
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              color: Colors.white,
                              icon: Icon(
                                Icons.remove_circle_outline,
                                size: 22,
                              ),
                              onPressed: () {},
                            ),
                            Text("1",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14)),
                            IconButton(
                              color: Colors.white,
                              icon: Icon(
                                Icons.add_circle_outline,
                                size: 22,
                              ),
                              onPressed: () {},
                            ),
                          ],
                        ),
                      )),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
