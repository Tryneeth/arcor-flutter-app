import 'package:flutter/material.dart';

class HomeAwards extends StatelessWidget {
  static String route = "awards";

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      color: Color.fromRGBO(56, 167, 222, 1),
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Container(
            width: 150,
            height: 150,
            color: Colors.white,
          ),
          Padding(padding: EdgeInsets.only(bottom: 20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Créditos: ",
                style: TextStyle(fontSize: 18),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(250, 197, 58, 1),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Text(
                  "118",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Text(
            "EMSER PRUEBAS",
            style: Theme.of(context).primaryTextTheme.title.copyWith(
                color: Color.fromRGBO(28, 76, 151, 1),
                fontWeight: FontWeight.bold),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          RaisedButton(
            onPressed: () {},
            elevation: 0.0,
            color: Color.fromRGBO(28, 76, 151, 1),
            shape: StadiumBorder(),
            child: Text(
              "Canjear",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
