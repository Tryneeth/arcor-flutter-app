import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tokin/src/controllers/claimsController.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/controllers/uploadController.dart';
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/models/info_petitions.dart';
import 'package:tokin/src/models/upload_image.dart';
import 'package:tokin/src/ui/homepage.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class HomeContacts extends StatefulWidget {
  static String route = "contacts";

  @override
  _HomeContactsState createState() => _HomeContactsState();
}

class _HomeContactsState extends State<HomeContacts> {
  Future<List<ClaimAPI>> claims;

  @override
  void initState() {
    claims = ClaimsController().getData(false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue.shade900,
          child: Icon(
            Icons.add,
            size: 55,
          ),
          onPressed: () async {
            var name = await SharedPreferencesController.getPrefatpdvId();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => NewContact(imageName: name)));
          },
        ),
        body: FutureBuilder(
          future: claims,
          builder:
              (BuildContext context, AsyncSnapshot<List<ClaimAPI>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return new Text('Input a URL to start');
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              case ConnectionState.active:
                return new Text('Active');
              case ConnectionState.done:
                if (!snapshot.hasData) {
                  return new Container();
                } else if (snapshot.hasError) {
                  print(snapshot.error);
                  return Center(
                      child: Container(
                    child: Text("There is a problem with your connection."),
                  ));
                } else {
                  return claimListView(snapshot.data);
                }
                break;
              default:
                return Container();
            }
          },
        ));
  }
}

Widget claimListView(List<ClaimAPI> claimList) {
  return claimList.isNotEmpty
      ? ListView.builder(
          key: ValueKey("claimList"),
          shrinkWrap: true,
          itemCount: claimList.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: Stack(
                children: <Widget>[
                  claimList[index].estado == "Cerrado"
                      ? Image.asset(
                          "assets/ic_list_contacto_cerrado.png",
                          height: 50,
                          width: 45,
                        )
                      : Image.asset(
                          "assets/ic_list_contacto.png",
                          height: 50,
                          width: 45,
                        ),
                  if (claimList[index].visto.toLowerCase() != "visto")
                    Positioned(
                      left: 0,
                      top: 0,
                      child: Image.asset(
                        "assets/new_novedad_new.png",
                        height: 25,
                      ),
                    )
                ],
              ),
              title: Text(claimList[index].titulo),
              trailing: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    DateFormat("dd/MM/y").format(claimList[index].fecha),
                    style: Theme.of(context)
                        .primaryTextTheme
                        .body1
                        .copyWith(fontSize: 16),
                  ),
                  Text(
                    claimList[index].estado,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .body1
                        .copyWith(fontSize: 16),
                  ),
                ],
              ),
              onTap: () async {
                try {
                  await ClaimsController().updateViewField(claimList[index]);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          ContactDetails(claimAPI: claimList[index])));
                } catch (ex) {
                  print(ex);
                }
              },
            );
          })
      : NoDataWidget(
          message:
              "No hay datos de Contactos, puede haber problemas con la conexión");
}

class NewContact extends StatefulWidget {
  final String imageName;
  NewContact({@required this.imageName});
  @override
  _NewContactState createState() => _NewContactState();
}

class _NewContactState extends State<NewContact> {
  String claimType;
  String claimCategory;
  final TextEditingController titleController = new TextEditingController();
  final TextEditingController descriptionController =
      new TextEditingController();

  final _formKey = GlobalKey<FormState>();

  File image;
  List<File> images = new List<File>();

  Image imageLoaded;

  Future<String> createStatus(PostClaims claim, List<File> uploadImages) async {
    bool resultImagesUpload = true;
    bool resultClaimCreate = await ClaimsController().createData(claim);

    if (resultClaimCreate) {
      var content = await UploadController()
          .compressAndStoreImages(uploadImages, claim.idReclamo);
      UploadImage imageToUpload = new UploadImage(
          extension: "zip",
          //oldFilenam: "",
          content: content,
          fileName: claim.idReclamo);
      resultImagesUpload = resultImagesUpload &&
          await UploadController().createData(imageToUpload);
    }
    if (resultClaimCreate && resultImagesUpload) {
      return "Se guardó el mensaje correctamente.\nGracias por contactarse con nosotros.";
    } else if (resultClaimCreate && !resultImagesUpload) {
      return "Se guardó el mensaje correctamente pero las imágenes no se pudieron enviar.\nGracias por contactarse con nosotros.";
    } else {
      return "No se pudo guardar el mensaje correctamente.";
    }
  }

  void createClaim() async {
    PostClaims claim = new PostClaims();
    claim.syncronized = 1;
    claim.activo = false;
    claim.adjuntos = images.length;
    claim.categoria = claimCategory;
    claim.descripcion = descriptionController.text;
    claim.tipo = claimType;
    claim.titulo = titleController.text;
    claim.fecha = DateTime.now();
    claim.fechaCierre = null;
    claim.id = 1;
    claim.descripcionCierre = "";
    claim.codigoCliente =
        await SharedPreferencesController.getPreffullClientCode();
    claim.estado = "Registrado";
    claim.idPais = await SharedPreferencesController.getPrefCountryCode();
    claim.idReclamo =
        "${await SharedPreferencesController.getPrefatpdvId()}-${DateFormatter.toStringDate(claim.fecha)}";
    claim.idDistribuidor = await SharedPreferencesController.getPrefDistrib();
    claim.idSucursal = await SharedPreferencesController.getPrefSucursalId();
    claim.transportista = "0";
    claim.vendedor = "";
    claim.visto = "Visto";

    _showDialogCreateClaim(claim);
  }

  void _showDialogCreateClaim(PostClaims claim) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("ENVIADO"),
          content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            FutureBuilder(
              future: createStatus(claim, images),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return new Text('Input a URL to start');
                  case ConnectionState.waiting:
                    return Center(child: CircularProgressIndicator());
                  case ConnectionState.active:
                    return new Text('');
                  case ConnectionState.done:
                    return Text(snapshot.data);
                  default:
                    return Container();
                }
              },
            )
          ]),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.pushReplacementNamed(context, "contacts");
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _getImage() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Seleccione la fuente",
            textAlign: TextAlign.center,
          ),
          content: new ImagePickerDialog(
            onImageSelected: (file) {
              image = file;
            },
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "Cancelar",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            if (image != null)
              FlatButton(
                child: Text(
                  "Aceptar",
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  if (image != null) {
                    images.add(image);
                    setState(() {});
                  }
                  Navigator.pop(context);
                },
              ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<String> claimTypes = <String>["Reclamo", "Sugerencia", "Otros"];
    List<String> claimCategories = <String>["Ventas", "Mercadería", "Otros"];

    return AppScaffold(
      key: ValueKey("createContact"),
      logged: true,
      appDrawer: AppDrawer(
        activeRoute: "contacts",
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            color: Colors.blue.shade900,
            height: 120,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "NUEVO CONTACTO",
                      style: Theme.of(context)
                          .primaryTextTheme
                          .title
                          .copyWith(fontSize: 25),
                    ),
                    Text(DateFormat("dd/MM/y").format(DateTime.now()),
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(
                                fontSize: 14, color: Colors.grey.shade400)),
                  ],
                ),
                Positioned(
                  left: 10,
                  top: 10,
                  child: IconButton(
                    key: ValueKey("backButtonCreateNewContact"),
                    onPressed: () => Navigator.of(context).pop(),
                    icon: Theme.of(context).platform == TargetPlatform.iOS
                        ? Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 30,
                          )
                        : Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Form(
              key: _formKey,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30, left: 15, right: 15, bottom: 15),
                    child: Text("SELECCIONE"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 6),
                    child: DropdownButtonFormField<String>(
                      onChanged: (String value) {
                        setState(() {
                          claimType = value;
                        });
                      },
                      hint: Text("Motivo"),
                      value: claimType,
                      items: claimTypes.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 6),
                    child: DropdownButtonFormField<String>(
                      onChanged: (String value) {
                        setState(() {
                          claimCategory = value;
                        });
                      },
                      hint: Text("Tema"),
                      value: claimCategory,
                      items: claimCategories.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "TÍTULO",
                          hasFloatingPlaceholder: true,
                        ),
                        controller: titleController,
                        validator: (value) {
                          if (value.isEmpty) return "Campo obligatorio";
                          return null;
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Descripción",
                        hasFloatingPlaceholder: true,
                      ),
                      controller: descriptionController,
                      validator: (value) {
                        if (value.isEmpty) return "Campo obligatorio";
                        return null;
                      },
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30.0, vertical: 5.0),
                          child: IconButton(
                              icon: Icon(Icons.add_a_photo),
                              onPressed: () async {
                                var perm = await PermissionHandler()
                                    .requestPermissions([
                                  PermissionGroup.photos,
                                  PermissionGroup.camera
                                ]);
                                if (perm[PermissionGroup.photos] ==
                                        PermissionStatus.granted ||
                                    perm[PermissionGroup.camera] ==
                                        PermissionStatus.granted) _getImage();
                              })),
                    ],
                  ),
                  if (images.length > 0)
                    Container(
                      height: 100,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: ListView.builder(
                        itemCount: images.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            leading: Container(
                                height: 50,
                                width: 50,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(1),
                                  child: Image.file(images[index]),
                                )),
                            title: Text("${widget.imageName}-${index + 1}"),
                            trailing: IconButton(
                              icon: Icon(Icons.cancel),
                              onPressed: () {
                                setState(() {
                                  images.removeAt(index);
                                });
                              },
                            ),
                          );
                        },
                      ),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(horizontal: 40.0),
                            child: Text(
                              "Enviar",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.blue.shade900,
                            onPressed: () async {
                              if (claimType != null &&
                                  claimCategory != null &&
                                  (claimType.isEmpty ||
                                      claimCategory.isEmpty)) {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text(
                                        'Por favor revise debe seleccionar algún valor en los campos Motivo y Tema ')));
                              } else if (_formKey.currentState.validate()) {
                                setState(() {
                                  titleController.text =
                                      titleController.text.toUpperCase();
                                });
                                createClaim();
                              }
                            },
                            shape: StadiumBorder(),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ContactDetails extends StatefulWidget {
  final ClaimAPI claimAPI;

  ContactDetails({this.claimAPI});

  @override
  _ContactDetailsState createState() =>
      _ContactDetailsState(claimAPI: claimAPI);
}

class _ContactDetailsState extends State<ContactDetails> {
  ClaimAPI claimAPI;
  File image;

  _ContactDetailsState({this.claimAPI, this.image});

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      key: ValueKey("contactDetails"),
      logged: true,
      appDrawer: AppDrawer(activeRoute: HomepageUI.route),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            color: Colors.blue.shade900,
            height: 120,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      this.claimAPI.categoria,
                      style: Theme.of(context)
                          .primaryTextTheme
                          .title
                          .copyWith(fontSize: 25),
                    ),
                    Text(claimAPI.tipo,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(
                                fontSize: 14, color: Colors.grey.shade400)),
                    Text(DateFormat("dd/MM/y").format(claimAPI.fecha),
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(
                                fontSize: 14, color: Colors.grey.shade400)),
                  ],
                ),
                Positioned(
                  right: 10,
                  bottom: 25,
                  child: Text(
                    claimAPI.estado,
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                Positioned(
                  key: ValueKey("backToContactsButton"),
                  left: 10,
                  top: 10,
                  child: IconButton(
                    onPressed: () =>
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => HomepageUI(
                                  tab: HomepageTab.CONTACT,
                                ))),
                    icon: Theme.of(context).platform == TargetPlatform.iOS
                        ? Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 30,
                          )
                        : Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Form(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20.0, left: 20.0, bottom: 10.0),
                    child: Text(
                      claimAPI.titulo,
                      style: Theme.of(context)
                          .primaryTextTheme
                          .title
                          .copyWith(color: Colors.black),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10.0),
                    child: Text(claimAPI.descripcion),
                  ),
                  if (claimAPI.descripcionCierre != "")
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 20.0, bottom: 10.0),
                              child: Text(
                                "Respuesta",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .title
                                    .copyWith(color: Colors.black),
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    top: 20.0, right: 20.0, bottom: 10.0),
                                child: Text(
                                  DateFormat("dd/MM/y").format(claimAPI.fecha),
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey.shade800),
                                ))
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0),
                          child: Text(claimAPI.descripcionCierre),
                        )
                      ],
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ImageGetter extends StatefulWidget {
  ImageGetter({Key key}) : super(key: key);

  _ImageGetterState createState() => _ImageGetterState();
}

class _ImageGetterState extends State<ImageGetter> {
  File imageSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlatButton(
              child: Icon(
                Icons.photo_library,
                color: Theme.of(context).primaryColor,
                size: 48,
              ),
              onPressed: () async {
                ImagePicker.pickImage(source: ImageSource.gallery).then((a) {
                  setState(() {
                    imageSelected = a;
                  });
                });
              },
            ),
            FlatButton(
              child: Icon(
                Icons.camera_alt,
                color: Theme.of(context).primaryColor,
                size: 48,
              ),
              onPressed: () async {
                var image =
                    await ImagePicker.pickImage(source: ImageSource.camera);
                setState(() {
                  imageSelected = image;
                });
              },
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            imageSelected != null
                ? Center(
                    child: Image.file(
                    imageSelected,
                    height: 100,
                    width: 100,
                  ))
                : Center()
          ],
        )
      ],
    ));
  }
}

typedef OnImageSelected(File file);

class ImagePickerDialog extends StatefulWidget {
  const ImagePickerDialog({
    Key key,
    this.onImageSelected,
  }) : super(key: key);

  final OnImageSelected onImageSelected;

  @override
  _ImagePickerDialogState createState() => _ImagePickerDialogState();
}

class _ImagePickerDialogState extends State<ImagePickerDialog> {
  var imageSelected;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: imageSelected != null ? 50 : 0,
              width: 150,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: imageSelected != null
                    ? Image.file(
                        imageSelected,
                        fit: BoxFit.cover,
                      )
                    : Container(),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              iconSize: 48,
              icon: Icon(
                Icons.photo_library,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () async {
                var image =
                    await ImagePicker.pickImage(source: ImageSource.gallery);
                setState(() {
                  imageSelected = image;
                });
                widget.onImageSelected(imageSelected);
              },
            ),
            /*
            IconButton(
              iconSize: 48,
              icon: Icon(
                Icons.camera_alt,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () async {
                imageSelected =
                    await ImagePicker.pickImage(source: ImageSource.camera);
                setState(() {});
                widget.onImageSelected(imageSelected);
              },
            )*/
          ],
        ),
      ],
    );
  }
}
