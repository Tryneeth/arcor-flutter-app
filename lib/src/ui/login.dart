import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/updateAllData.dart';
import 'package:tokin/src/ui/homepage.dart';

class LoginUII extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUII> {
  Future<String> userFromStore;
  String user;
  String pass;
  GlobalKey<FormState> key = new GlobalKey();
  var _inputFieldBorder =
      UnderlineInputBorder(borderSide: BorderSide(color: Colors.white));
  var _maintainSession = false;
  bool wrongPass;

  Future<bool> validateLogin() async {
    //User process
    var userBytes = utf8.encode(user);
    var shaUser = sha1.convert(userBytes);

    var base64UserHash =
        utf8.fuse(base64).encode(shaUser.toString().toUpperCase());

    var concatenatedUser =
        "K1${base64UserHash.substring(0, base64UserHash.length - 2)}PK";

    //Pass process
    var passBytes = utf8.encode(pass); // data being hashed
    var shaPass = sha1.convert(passBytes);

    var passBytes2 =
        utf8.encode(shaPass.toString().toUpperCase()); // data being hashed
    var shaPass2 = sha1.convert(passBytes2);

    var base64PassHash =
        utf8.fuse(base64).encode(shaPass2.toString().toUpperCase());

    var concatenatedPass =
        "K2${base64PassHash.substring(0, base64PassHash.length - 2)}ZW";

    //Validation zone
    var k1 = await SharedPreferences.getInstance()
        .then((pref) => pref.getString("k1"));
    var k2 = await SharedPreferences.getInstance()
        .then((pref) => pref.getString("k2"));

    if (k1 == concatenatedUser && k2 == concatenatedPass) {
      print("iguales");
      return true;
    } else {
      print("distintos");
      return false;
    }
  }

  Future<String> _getUser() async {
    var user = await SharedPreferences.getInstance()
        .then((pref) => pref.getString("email"));

    if (user != null && user.isNotEmpty) {
      print(user);
      return user;
    } else {
      print("nada ningun valor");
      return "";
    }
  }

  @override
  void initState() {
    userFromStore = _getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: userFromStore,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return Scaffold(
            backgroundColor: Colors.blue.shade400,
            body: SafeArea(
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15)),
                            Image.asset(
                              "assets/ic_tokin.png",
                              height: MediaQuery.of(context).size.height / 3,
                            ),
                            Container(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 50),
                                child: Form(
                                    key: key,
                                    child: Column(
                                      children: <Widget>[
                                        TextFormField(
                                          style: TextStyle(color: Colors.white),
                                          initialValue: snapshot.data,
                                          decoration: InputDecoration(
                                              labelText: "Usuario",
                                              labelStyle: TextStyle(
                                                  color: Colors.white),
                                              border: _inputFieldBorder,
                                              enabledBorder: _inputFieldBorder,
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors.white,
                                                          width: 2))),
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          validator: (value) {
                                            var emailExp = RegExp(
                                                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                                            if (value == null || value.isEmpty)
                                              return "Campo obligatorio";
                                            else if (!emailExp
                                                    .hasMatch(value) &&
                                                value.isNotEmpty)
                                              return "Correo inválido";
                                            return null;
                                          },
                                          onSaved: (value) {
                                            setState(() {
                                              user = value;
                                            });
                                          },
                                        ),
                                        TextFormField(
                                          decoration: InputDecoration(
                                              labelText: "Contraseña",
                                              labelStyle: TextStyle(
                                                  color: Colors.white),
                                              border: _inputFieldBorder,
                                              enabledBorder: _inputFieldBorder,
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors.white,
                                                          width: 2))),
                                          obscureText: true,
                                          keyboardType: TextInputType.number,
                                          onSaved: (value) {
                                            setState(() {
                                              pass = value;
                                            });
                                          },
                                        ),
                                        wrongPass != null && wrongPass
                                            ? Padding(
                                                padding:
                                                    const EdgeInsets.all(5),
                                                child: Text(
                                                  "Contraseña incorrecta",
                                                  style: TextStyle(
                                                      color: Colors.red,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 13),
                                                ),
                                              )
                                            : Container(),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              RaisedButton(
                                                onPressed: () {},
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20.0),
                                                shape: StadiumBorder(),
                                                elevation: 0.0,
                                                color: Colors.grey.shade300,
                                                child: Text("Cancelar"),
                                              ),
                                              RaisedButton(
                                                onPressed: () async {
                                                  key.currentState.save();
                                                  if (await validateLogin()) {
                                                    await UpdateData()
                                                        .updateAllData(context);
                                                    Navigator.of(context)
                                                        .pushReplacement(
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        HomepageUI()));
                                                  } else {
                                                    setState(() {
                                                      wrongPass = true;
                                                    });
                                                  }
                                                },
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20.0),
                                                shape: StadiumBorder(),
                                                elevation: 0.0,
                                                color: Colors.grey.shade300,
                                                child: Text("Ingresar"),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Theme(
                                                data: Theme.of(context)
                                                    .copyWith(
                                                        unselectedWidgetColor:
                                                            Colors.white),
                                                child: Checkbox(
                                                  value: _maintainSession,
                                                  checkColor: Colors.blue,
                                                  activeColor: Colors.white,
                                                  onChanged: (value) async {
                                                    var preferencess= await SharedPreferences.getInstance();
                                                      await preferencess.setBool("keepLogged",value);
                                                    setState(()async {
                                                      _maintainSession = value;                                                      
                                                    });
                                                  },
                                                ),
                                              ),
                                              Text(
                                                "Mantener sesión iniciada",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
              backgroundColor: Colors.blue.shade400,
              body: SafeArea(
                  child:
                      Stack(children: <Widget>[CircularProgressIndicator()])));
        }
      },
    );
  }
}
