import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/register.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';
import 'package:tokin/src/ui/welcome.dart';

class RegisterUI extends StatefulWidget {
  static String route = "register";
  final String phoneId;

  RegisterUI({Key key, this.phoneId}) : super(key: key);

  _RegisterUIState createState() => _RegisterUIState();
}

class _RegisterUIState extends State<RegisterUI> {
  GlobalKey<FormState> _key = new GlobalKey();
  var emailController = new TextEditingController();
  var distController = new TextEditingController();
  var userIdController = new TextEditingController();
  bool chargingData;

  @override
  void initState() { 
    super.initState();
    chargingData=false;
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        logged: true,
        appDrawer: AppDrawer(
        ),
        child: Padding(
          padding: const EdgeInsets.all(1),
          child:
        ListView(          
          children: <Widget>[
          Container(
            child: Form(
                key: _key,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.only(
                                top: 30, left: 30, right: 30, bottom: 15),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Registro de Licencia",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: TextFormField(
                            initialValue: "ARG",
                            enabled: false,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              labelText: "Email",
                              hasFloatingPlaceholder: true,
                            ),
                            controller: emailController,
                            validator: (value) {
                              var emailExp = RegExp(
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                              if (value.isEmpty)
                                return "Campo obligatorio";
                              else if (!emailExp.hasMatch(value) &&
                                  value.isNotEmpty) return "Correo inválido";
                              return null;
                            },
                            onSaved: (value) {
                              SharedPreferences.getInstance().then(
                                  (pref) => pref.setString("email", value));                              
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              labelText: "Distribuidor",
                              hasFloatingPlaceholder: true,
                            ),
                            controller: distController,
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty) return "Campo obligatorio";
                              return null;
                            },
                            onSaved: (value) {
                              SharedPreferences.getInstance()
                                  .then((pref) => pref.setString("distribuidor", value));
                              print(value);
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              labelText: "Código Cliente",
                              hasFloatingPlaceholder: true,
                            ),
                            controller: userIdController,
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty) return "Campo obligatorio";
                              return null;
                            },
                            onSaved: (value) {
                              SharedPreferences.getInstance().then((pref) =>
                                  pref.setString("clientCode", value));
                              print(value);
                            },
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 10, left: 30),
                          child: Row(
                            children: <Widget>[
                              Text("Id Dispositivo:"),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 30, bottom: 10),
                          child: Row(
                            children: <Widget>[
                              Text(widget.phoneId),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 30, right: 30, bottom: 15),
                            child: Container(
                              child: Text(
                                  "Los datos proporcionados se enviarán al servidor para su registro.",
                                  style: TextStyle(fontSize: 11)),
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 30, right: 30, bottom: 5),
                            child: RaisedButton(
                              child: Text("Registrar"),
                              onPressed: () async {
                                if (_key.currentState.validate()) {
                                  var result = await RegisterController()
                                      .toRegister(
                                          email: emailController.text,
                                          phoneId: widget.phoneId,
                                          distributor: distController.text,
                                          clientId: userIdController.text);
                                  await RegisterController().toValidate(
                                      email: emailController.text,
                                      phoneId: widget.phoneId);
                                  if (result) {
                                    _key.currentState.save();
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => WelcomeUI()));
                                    SharedPreferences.getInstance().then(
                                        (pref) =>
                                            pref.setBool("hideRegister", true));
                                  }
                                }
                              },
                              color: Colors.grey,
                              shape: StadiumBorder(),
                              padding:
                                  const EdgeInsets.only(left: 100, right: 100),
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 30, right: 30, bottom: 5),
                            child: RaisedButton(
                              child: Text("Salir"),
                              onPressed: () => Navigator.of(context).pop(),
                              color: Colors.grey,
                              shape: StadiumBorder(),
                              padding:
                                  const EdgeInsets.only(left: 113, right: 113),
                            )),
                      ],
                    )
                  ],
                )),
          ),
        ])));
  }
}
