import 'package:flutter/material.dart';

import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';

class TermsAndConditionsUI extends StatelessWidget {
  final bool readOnly;

  TermsAndConditionsUI({this.readOnly});

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        logged: false,
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.grey.shade100,
              height: readOnly
                  ? MediaQuery.of(context).size.height - 100
                  : MediaQuery.of(context).size.height - 150,
              child: Markdown(                
                data: """ 
    Términos y condiciones:
    1.- Alcance:
    El registro dentro del Tokin por parte de un usuario y el uso de la aplicación (en adelante el/los “Usuario/s”) indica la aceptación absoluta de los presentes Términos y Condiciones y de la Política de Privacidad.
    El acceso al Tokin es libre y gratuito. “El Usuario” garantiza y declara ser mayor de 18 años y que es competente para entender y aceptar sin reservas las obligaciones, afirmaciones, representaciones y garantías establecidas en estos Términos y Condiciones.
    Por su parte, Arcor S.A.I.C, en adelante, “El Administrador”, se deslinda de responsabilidad en el caso de no ser veraz la información suministrada al respecto.
    Para poder realizar un completo uso de la aplicación, “El Usuario” deberá estar dado de alta cómo punto de venta en los registros del “Administrador” y contar previamente con un número de cliente.
    “El Administrador” no se responsabiliza por los daños o perjuicios derivados del acceso, uso o mala utilización de los contenidos de la aplicación, ni por las temporales suspensiones del servicio, que por cualquier causa impidan el ingreso a la aplicación, así como tampoco de la intromisión de virus o fallas del sistema.
    Asimismo, “El Administrador” no controla, ni es responsable, por el accionar de otros sitios web y otros productos y servicios a los cuales se puede acceder por medio de la aplicación.
    2.- Obligaciones del Usuario, Condiciones de uso y responsabilidades.
    “El Usuario” deberá respetar estos Términos y Condiciones de Uso de la aplicación “Tokin”. Las infracciones por acción u omisión de los presentes “Términos y Condiciones de Uso” facultarán “El Administrador” para suspender al Usuario que haya incumplido.
    “El Usuario” se obliga a utilizar la aplicación en forma correcta y de manera lícita. Caso contrario, “El Administrador” podrá suspender la cuenta de “El Usuario”, por considerarlo: violatorio de estos “Términos y Condiciones”, y/o violatorio de la Política de Privacidad, ofensivo, ilegal, violatorio de derechos de terceros, contrario a la moral y/o buenas costumbres y/o resultar una amenaza a la seguridad de otros Usuarios.
    “El Usuario” se compromete a:
    No intentar acceder a datos restringidos o intentar violar las barreras de seguridad informática con los que cuenta la aplicación, para llegar a ellos.
    Dar inmediato aviso a “El Administrador”, ante el acceso a información que pudiera implicar un compromiso a la seguridad de la información o el funcionamiento de la aplicación.
    3.- Recolección de información y uso de datos:
    “El Usuario” que se registre y utilice la aplicación “Tokin” podrá proporcionar algunos datos personales a efectos de poder realizar un normal uso de la aplicación y acceder a los beneficios que puedan otorgarse por el uso de esta.  Asimismo, podrá proporcionar información para mantenerse en contacto con “El Administrador” y para que se lo pueda mantener al tanto sobre las novedades de la aplicación.
    La empresa  EMSER, con domicilio legal en Sarmiento 1150, Provincia de Córdoba, Ciudad de Córdoba, es la responsable de las Bases de Datos a las que se incorporarán los datos suministrados por “El Usuario”.
    “El Administrador”, brinda a “El Usuario” la posibilidad de que introduzca a la aplicación información veraz y necesaria, “El Usuario” al momento de ingresar su información personal, lo hará de forma expresa, libre y voluntaria, en pleno conocimiento de los presentes “TERMINOS Y CONDICIONES” y/o la utilización de la aplicación, de conformidad con lo dispuesto en el presente título.
    Proporcionar datos personales no es condición para el uso de la aplicación y sus servicios vinculados. No obstante, si “El Usuario” decide voluntariamente proporcionar sus datos personales, debe brindar datos veraces, exactos y completos. La inexactitud de estos puede suponer dificultades para establecer un vínculo directo con “El Administrador” y perjudicar el normal uso de la aplicación.
    “El Usuario” y/o titular de los datos personales podrá solicitar la actualización, rectificación y, cuando corresponda, la supresión o el sometimiento a confidencialidad de los datos personales provistos.
    La información que se obtenga de los datos suministrados por el usuario cuando se ingrese y se registre en la aplicación: Número de Usuario y correo electrónico, será registrada por “El Administrador” y podrá ser utilizada para la mejora de la aplicación. La misma será protegida por “El Administrador”, mediante el cifrado de los datos.
    “El Administrador” no revelará ni compartirá esta información con ninguna otra empresa u organización sin el consentimiento informado del Usuario que proporciona dicha información, salvo en caso disposición legal o judicial.
    El Administrador podrá usar la información proporcionada por el Usuario para obtener información estadística y general del mercado. Estos datos se recolectan de manera general, mediante el uso de la aplicación.
    La aplicación puede utilizar cookies para rastrear los patrones de tráfico del usuario. El usuario puede rechazar el uso de cookies, la aplicación no requiere del mismo para su normal funcionamiento.
    Una cookie es un pequeño archivo de datos que puede grabarse en su dispositivo. Una cookie no puede leer datos ni leer los archivos cookie creados por otros sitios, pero puede leer los creados para la aplicación “Tokin”.
    Para un uso completo de “Tokin” existe un servicio de localización, mediante el mismo la aplicación podrá utilizar y compartir datos exactos sobre ubicaciones, incluyendo la situación geográfica en tiempo real de los ordenadores o dispositivos. Todo ello con previa autorización de  “El Usuario”.
    El uso de la aplicación puede generar un aumento en el consumo de los datos móviles por parte del dispositivo propiedad del “El Usuario”.  El consumo de datos y su control, resulta responsabilidad exclusiva del “El Usuario”, no resultando el Administrador responsable por los eventuales costos que pudiera arrojarse por el uso de “Tokin”.
    El Administrador no compartirá la información proporcionada por los usuarios de manera individual, con ningún tercero. Pero, podrá compartir información estadística generada por el uso de la aplicación para poder mejorar el contenido y adaptarlo al perfil de los usuarios.
    4.- Sobre los pedidos mediante Tokin.
    En cuanto a los servicios, bienes y productos que pudieran ser promocionados mediante la aplicación, el Administrador se reserva el derecho de suprimir, cancelar y/o modificar de cualquier modo los datos o características de ellos no generando esto responsabilidad alguna.
    Será facultad del Administrador el otorgamiento de cada uno de los servicios, bienes y productos que pudieran promocionarse mediante la aplicación, encontrándose sujeta su aprobación al cumplimiento de las condiciones, stock, y disponibilidad.

    La información, de los servicios, bienes o productos que se promocionen en la aplicación están sujetos a cambios. Puede darse el supuesto que los servicios, bienes y/o productos promocionados en la aplicación, no estén disponibles en todas las áreas geográficas de la Argentina y/o del exterior.
    5.- Sobre los sorteos.
    El uso de la aplicación podrá generar en favor del usuario una sumatorio de “puntos”, los mismos podrán ser cambiados por determinados bienes, servicios y/o prodcutos mediante una pagina web perteneciente a la empresa  Av- Business &amp; Communication,
    El acceso a dicha página a través de “Tokin” no importará, para “EL Administrador”, obligación alguna respecto al sistema de puntos, el intercambio de estos por servicios, bienes o productos; el stock de cada uno, el proceso de asignación, entrega y descuento o asignación de puntos. Todo ello resulta responsabilidad única y excluyente de las empresas antes mencionada, quien provee el servicio y de “El Usuario” participante.
    Asimismo, el acceso a otras páginas a través de “Tokin” no importará obligación alguna, de “El Administrador”, respecto de la información que en estas otras páginas pudieran describirse.
    6.- Propiedad Intelectual:
    La expresión de ideas, procedimientos, métodos de operación y conceptos matemáticos, así como las marcas, avisos, nombres comerciales, publicidad, dibujos, diseños, logotipos, textos, entre otros, resultan propiedad del Administrador, salvo expresa manifestación en contrario por parte de este. Todo ello de conformidad con lo dispuesto en el artículo 1º de la Ley 11.723 y sus modificatorias.
    El Administrador deja expresamente prohibida la copia, duplicación, redistribución, comercialización o cualquier otra actividad que pudiera ser realizada con los contenidos de la aplicación, ni aun citando las fuentes, excepto que se otorgue consentimiento por escrito.
    7.- Modificación de los términos y condiciones.
    El Administrador se reserva el derecho de modificar estos Términos y Condiciones en cualquier momento, notificando dicha modificación a los “Usuarios”.""",
              ),
            ),
            if (!readOnly)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FlatButton(
                      onPressed: () async {
                        await SharedPreferences.getInstance().then(
                            (pref) => pref.setBool("termConditions", false));
                        Navigator.of(context).pop();
                      },
                      child: Text("Cancelar")),
                  FlatButton(
                      onPressed: () async {
                        await SharedPreferences.getInstance().then(
                            (pref) => pref.setBool("termConditions", true));
                        Navigator.of(context).pop();
                      },
                      child: Text("Aceptar"))
                ],
              )
          ],
        ));
  }
}
