import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/models/claims.dart';

class PostNews {
  final String countryId;
  final String distributorId;
  final String branchId;
  final String clientCode;
  final String dateSince;
  final int page;

  PostNews(
      {this.countryId,
      this.distributorId,
      this.branchId,
      this.clientCode,
      this.dateSince,
      this.page});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['id_pais'] = countryId;
    map['id_distribuidor'] = distributorId;
    map['id_sucursal'] = branchId;
    map['codigo_cliente'] = clientCode;
    map['fechahora_desde'] = dateSince;
    map['page'] = page;
    return map;
  }
}

class PostClaims extends ClaimAPI with DateFormatter {
  //bool syncronized;

  // Map toMap() {
  //   if (codigoCliente.length < 10) {
  //     print("paso por aqui con valor $codigoCliente");
  //     for (var i = 0; i < 10 - codigoCliente.length; i++) {
  //       codigoCliente = "0$codigoCliente";
  //     }
  //   }

  //   var map = new Map<String, dynamic>();
  //   map['id'] = id;
  //   map['codigo_cliente'] = codigoCliente;
  //   map['id_pais'] = idPais;
  //   map['id_distribuidor'] = idDistribuidor;
  //   map['id_sucursal'] = idSucursal;
  //   map['id_empresa'] = idEmpresa;
  //   map['tipo'] = tipo;
  //   map['fecha'] = DateFormatter.toStringDate(fecha);
  //   map['categoria'] = categoria;
  //   map['estado'] = estado;
  //   map['vendedor'] = vendedor;
  //   map['transportista'] = transportista;
  //   map['descripcion'] = descripcion;
  //   map['fecha_cierre'] = DateFormatter.toStringDate(fechaCierre);
  //   map['descripcion_cierre'] = descripcionCierre;
  //   map['activo'] = activo;
  //   map['id_reclamo'] = idReclamo;
  //   map['titulo'] = titulo;
  //   map['visto'] = visto;
  //   map['adjuntos'] = adjuntos;
  //   return map;
  // }

  PostClaims fromClaim(ClaimAPI claimAPI) {
    PostClaims postClaim = new PostClaims();
    postClaim.id = claimAPI.id;
    postClaim.visto = claimAPI.visto;
    return postClaim;
  }

  @override
  toMap() {
    return {
      'id': this.id,
      'codigo_cliente': this.codigoCliente,
      'id_pais': this.idPais,
      'id_distribuidor': this.idDistribuidor,
      'id_sucursal': this.idSucursal,
      'tipo': this.tipo,
      'fecha': DateFormatter.toStringDate(this.fecha),
      'categoria': this.categoria,
      'estado': this.estado,
      'vendedor': this.vendedor,
      'transportista': this.transportista,
      'descripcion': this.descripcion,
      'fecha_cierre': DateFormatter.toStringDate(this.fechaCierre),
      'descripcion_cierre': this.descripcionCierre,
      'activo': this.activo,
      'id_reclamo': this.idReclamo,
      'titulo': this.titulo,
      'visto': this.visto,
      'adjuntos': this.adjuntos,
      'sincronizado': "Nuevo"
    };
  }
}
//tipo=> motivo categoria=> Tema

class PostArticle {
  final List<String> articleCode;
  final String distributorId;
  final String listPriceId;
  final String countryId;
  final String branchId;

  PostArticle(
      {this.articleCode,
      this.distributorId,
      this.branchId,
      this.countryId,
      this.listPriceId});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['codigo_articulo'] = articleCode;
    map['id_distribuidor'] = distributorId;
    map['id_lista_precio'] = listPriceId;
    map['id_pais'] = countryId;
    map['id_sucursal'] = branchId;
    return map;
  }
}
