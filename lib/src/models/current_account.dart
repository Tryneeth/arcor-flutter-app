import 'dart:convert';

import 'package:tokin/src/controllers/dateFormatter.dart';

class CurrentAccountAPI with DateFormatter {
  final int id;
  final String clientCode;
  final String countryId;
  final String distributorId;
  final String branchId;
  final String companyId;
  final String balance;
  final DateTime lastUpdateDate;

  CurrentAccountAPI(
      {this.id,
      this.clientCode,
      this.countryId,
      this.distributorId,
      this.branchId,
      this.companyId,
      this.balance,
      this.lastUpdateDate});

  CurrentAccountAPI.fromJson(Map json)
      : id = json['id'],
        clientCode = json['codigo_cliente'],
        countryId = json['id_pais'],
        distributorId = json['id_distribuidor'],
        branchId = json['id_sucursal'],
        companyId = json['id_empresa'],
        balance = json['saldo'],
        lastUpdateDate =
            DateFormatter.toFormatDate(json['fecha_ultima_actualizacion']);

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'codigo_cliente': this.clientCode,
      'id_pais': this.countryId,
      'id_distribuidor': this.distributorId,
      'id_sucursal': this.branchId,
      'id_empresa': this.companyId,
      'saldo': this.balance,
      'fecha_ultima_actualizacion':
          DateFormatter.toStringDate(this.lastUpdateDate),
    };
  }

  static DateTime toFormatDate2(String apiTime) {
    int year = int.parse(apiTime.substring(0, 4));
    int month = int.parse(apiTime.substring(5, 6));
    int day = int.parse(apiTime.substring(7, 8));
    DateTime date = new DateTime(year, month, day);
    print(date.day.toString() +
        "D-" +
        date.month.toString() +
        "M-" +
        date.year.toString());
    return date;
  }

  @override
  String toString() {
    return json.encode(this.toMap());
  }

  static CurrentAccountAPI fromString(String object) {
    return CurrentAccountAPI.fromJson(
        jsonDecode(object) as Map<String, dynamic>);
  }
}
