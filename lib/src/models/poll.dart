class PollAPI {
  final String title;

  PollAPI({this.title});

  PollAPI.fromJson(Map json) : title = json["title"];
}
