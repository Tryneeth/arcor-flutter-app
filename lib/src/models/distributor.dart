class Distributor {
  int id;
  String idDistribuidor;
  String idPais;
  String nombreDistribuidor;
  String idSucursal;
  String urlServidorPrincipal;
  String urlServidorAlternativa;
  String puertoServidor;
  String habilitaTokin;
  String habilitaNotificaciones;
  String habilitaCuentacorriente;
  String habilitaContactos;
  String habilitaEncuestas;
  String habilitaPremios;
  String habilitaCompras;
  String compromisoEntrega;

  Distributor({
    this.nombreDistribuidor,
    this.habilitaCompras,
    this.habilitaContactos,
    this.habilitaCuentacorriente,
    this.habilitaEncuestas,
    this.habilitaNotificaciones,
    this.habilitaPremios,
    this.habilitaTokin,
    this.puertoServidor,
    this.urlServidorAlternativa,
    this.urlServidorPrincipal,
    this.compromisoEntrega,
    this.id,
    this.idDistribuidor,
    this.idPais,
    this.idSucursal
  });

  Distributor.fromJson(Map json):
      id=json['id'],
      idDistribuidor=json['id_distribuidor'],
      idPais=json['id_pais'],
      nombreDistribuidor=json['nombre_distribuidor'],
      idSucursal=json['id_sucursal'],
      urlServidorPrincipal=json['url_servidor_principal'],
      urlServidorAlternativa=json['url_servidor_alternativa'],
      puertoServidor=json['puerto_servidor'],
      habilitaTokin=json['habilita_tokin'],
      habilitaNotificaciones=json['habilita_notificaciones'],
      habilitaCuentacorriente=json['habilita_cuentacorriente'],
      habilitaContactos=json['habilita_contactos'],
      habilitaEncuestas=json['habilita_encuestas'],
      habilitaPremios=json['habilita_premios'],
      habilitaCompras=json['habilita_compras'],
      compromisoEntrega=json['compromiso_entrega'];   
}