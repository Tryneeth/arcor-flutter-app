import 'package:quiver/core.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

class NotificationAPI with DateFormatter {
  final int id;
  final String codigoCliente;
  final String idPais;
  final String idDistribuidor;
  final String idSucursal;
  final DateTime fecha;
  final String codigoPtVenta;
  final String titulo;
  final String descripcion;
  final String categoria;
  final String imagenAdjunto;
  final String idEmpresa;
  final bool activo;
  String visto;
  final int recibido;
  final int idGrupoNotificacion;
  final int eliminado;
  String sincronizado;
  final DateTime fechaUltimaActualizacion;
  final String codigoNotificacion;
  final String fechaInicio;
  final String fechaFin;
  final bool opcionCompra;
  final String fechaInicioCompra;
  final String fechaFinCompra;
  final String codigoProducto;
  final String unidadMedida;
  final int topeCantidadVenta;
  final int multiploCompra;
  final int puntosTokinpremia;

  NotificationAPI(
      {this.id,
      this.codigoCliente,
      this.idPais,
      this.idDistribuidor,
      this.idSucursal,
      this.fecha,
      this.codigoPtVenta,
      this.titulo,
      this.descripcion,
      this.categoria,
      this.imagenAdjunto,
      this.idEmpresa,
      this.activo,
      this.visto,
      this.recibido,
      this.idGrupoNotificacion,
      this.eliminado,
      this.sincronizado,
      this.fechaUltimaActualizacion,
      this.codigoNotificacion,
      this.fechaInicio,
      this.fechaFin,
      this.opcionCompra,
      this.fechaInicioCompra,
      this.fechaFinCompra,
      this.codigoProducto,
      this.unidadMedida,
      this.topeCantidadVenta,
      this.multiploCompra,
      this.puntosTokinpremia});

  NotificationAPI.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        codigoCliente = json['codigo_cliente'],
        idPais = json['id_pais'],
        idDistribuidor = json['id_distribuidor'],
        idSucursal = json['id_sucursal'],
        fecha = DateFormatter.toFormatDate(json['fecha']),
        codigoPtVenta = json['codigo_pt_venta'],
        titulo = json['titulo'],
        descripcion = json['descripcion'],
        categoria = json['categoria'],
        imagenAdjunto = json['imagen_adjunto'],
        idEmpresa = json['id_empresa'],
        activo = json['activo'].toString() == "1" ||
                json['activo'].toString() == "true"
            ? true
            : false,
        visto = json['visto'] == "" || json['visto'] == null
            ? "No Visto"
            : json['visto'],
        recibido = json['recibido'],
        idGrupoNotificacion = json['id_grupo_notificacion'],
        eliminado = json['eliminado'],
        sincronizado = json['sincronizado'],
        fechaUltimaActualizacion =
            DateFormatter.toFormatDate(json['fecha_ultima_actualizacion']),
        codigoNotificacion = json['codigo_notificacion'],
        fechaInicio = json['fecha_inicio'],
        fechaFin = json['fecha_fin'],
        opcionCompra = json['opcion_compra'].toString() == "1" ||
                json['opcion_compra'].toString() == "true"
            ? true
            : false,
        fechaInicioCompra = json['fecha_inicio_compra'],
        fechaFinCompra = json['fecha_fin_compra'],
        codigoProducto = json['codigo_producto'],
        unidadMedida = json['unidad_medida'],
        topeCantidadVenta = json['tope_cantidad_venta'],
        multiploCompra = json['multiplo_compra'],
        puntosTokinpremia = json['puntos_tokinpremia'];

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "codigo_cliente": DBType.TEXT,
      "id_pais": DBType.TEXT,
      "id_distribuidor": DBType.TEXT,
      "id_sucursal": DBType.TEXT,
      "fecha": DBType.TEXT,
      "codigo_pt_venta": DBType.TEXT,
      "titulo": DBType.TEXT,
      "descripcion": DBType.TEXT,
      "categoria": DBType.TEXT,
      "imagen_adjunto": DBType.TEXT,
      "id_empresa": DBType.TEXT,
      "activo": DBType.TEXT,
      "visto": DBType.TEXT,
      "recibido": DBType.INTEGER,
      "id_grupo_notificacion": DBType.INTEGER,
      "eliminado": DBType.INTEGER,
      "sincronizado": DBType.TEXT,
      "fecha_ultima_actualizacion": DBType.TEXT,
      "codigo_notificacion": DBType.TEXT,
      "fecha_inicio": DBType.TEXT,
      "fecha_fin": DBType.TEXT,
      "opcion_compra": DBType.INTEGER,
      "fecha_inicio_compra": DBType.TEXT,
      "fecha_fin_compra": DBType.TEXT,
      "codigo_producto": DBType.TEXT,
      "unidad_medida": DBType.TEXT,
      "tope_cantidad_venta": DBType.INTEGER,
      "multiplo_compra": DBType.INTEGER,
      "puntos_tokinpremia": DBType.INTEGER,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "codigo_cliente": this.codigoCliente,
      "id_pais": this.idPais,
      "id_distribuidor": this.idDistribuidor,
      "id_sucursal": this.idSucursal,
      "fecha": DateFormatter.toStringDate(this.fecha),
      "codigo_pt_venta": this.codigoPtVenta,
      "titulo": this.titulo,
      "descripcion": this.descripcion,
      "categoria": this.categoria,
      "imagen_adjunto": this.imagenAdjunto,
      "id_empresa": this.idSucursal,
      "activo": this.activo ? 1 : 0,
      "visto": this.visto,
      "recibido": this.recibido,
      "id_grupo_notificacion": this.idGrupoNotificacion,
      "eliminado": this.eliminado,
      "sincronizado": this.sincronizado,
      "fecha_ultima_actualizacion":
          DateFormatter.toStringDate(this.fechaUltimaActualizacion),
      "codigo_notificacion": this.codigoNotificacion,
      "fecha_inicio": this.fechaInicio,
      "fecha_fin": this.fechaFin,
      "opcion_compra": this.opcionCompra ? 1 : 0,
      "fecha_inicio_compra": this.fechaInicioCompra,
      "fecha_fin_compra": this.fechaFinCompra,
      "codigo_producto": this.codigoProducto,
      "unidad_medida": this.unidadMedida,
      "tope_cantidad_venta": this.topeCantidadVenta,
      "multiplo_compra": this.multiploCompra,
      "puntos_tokinpremia": this.puntosTokinpremia
    };
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "codigo_cliente": this.codigoCliente,
      "id_pais": this.idPais,
      "id_distribuidor": this.idDistribuidor,
      "id_sucursal": this.idSucursal,
      "fecha": DateFormatter.toStringDate(this.fecha),
      "codigo_pt_venta": this.codigoPtVenta,
      "titulo": this.titulo,
      "descripcion": this.descripcion,
      "categoria": this.categoria,
      "imagen_adjunto": this.imagenAdjunto,
      "id_empresa": this.idSucursal,
      "activo": this.activo ? 1 : 0,
      "visto": this.visto,
      "recibido": this.recibido,
      "id_grupo_notificacion": this.idGrupoNotificacion,
      "eliminado": this.eliminado,
      "sincronizado": this.sincronizado,
      "fecha_ultima_actualizacion":
          DateFormatter.toStringDate(this.fechaUltimaActualizacion),
      "codigo_notificacion": this.codigoNotificacion,
      "fecha_inicio": this.fechaInicio,
      "fecha_fin": this.fechaFin,
      "opcion_compra": this.opcionCompra ? 1 : 0,
      "fecha_inicio_compra": this.fechaInicioCompra,
      "fecha_fin_compra": this.fechaFinCompra,
      "codigo_producto": this.codigoProducto,
      "unidad_medida": this.unidadMedida,
      "tope_cantidad_venta": this.topeCantidadVenta,
      "multiplo_compra": this.multiploCompra,
      "puntos_tokinpremia": this.puntosTokinpremia
    };
  }

  @override
  bool operator ==(Object other) {
    return other is NotificationAPI && this.id == other.id;
  }

  @override
  int get hashCode {
    return hashObjects([
      this.id,
    ].where((s) => s != null));
  }
}
