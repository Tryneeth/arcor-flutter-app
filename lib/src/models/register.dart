class Register{
  String androidId;
  String codigoUsuario;
  String email;
  String empresa;
  String key;
  String modulo;
  List<Map<String,String>> parametros;
  
  Register({this.androidId,this.codigoUsuario,this.email,this.empresa,this.key,this.modulo,this.parametros});
   
   Map<String,dynamic> toMap(){
    var map = new Map<String,dynamic>();
    map['android_id']=androidId;
    map['codigo_usuario']=codigoUsuario;
    map['email']=email;
    map['empresa']=empresa;
    map['key']=key;
    map['modulo']=modulo;
    map['parametros']=parametros;
    return map;
  }
}
class Parameter{
  String name;
  String value;

  Parameter({this.name,this.value});

  Map toMap(){
    var map = new Map<String,String>();
    map['nombre']=name;
    map['valor']=value;
    return map;
  }

}