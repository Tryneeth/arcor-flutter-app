import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';

class DataConnection {
  String _baseUrl =
      "http://arcor-tokin-backend-v1-4-77.us-west-2.elasticbeanstalk.com/";
  String _imageUrl =
      "https://s3-us-west-2.amazonaws.com/arcor-tokin-development-files/notifications/images/";
  String _registerServer =
      "https://www.emserlicensesserver.com/api/license_set/";
  String _licenses = "https://www.emserlicensesserver.com/api/licencias";
  //Premios
  String premios = "https://tokinpremia.com.ar";
  String premiosAuth = "https://www.tokinpremia.com.ar/hooks/tokinuser/auth/";

  //String _imageUrl="";
  String _auth = "/auth/";
  String _api = "/api/";
  String _client_login = "cliente_login/";
  String _notificationV2 = "notificaciones_pdv_v2/";
  String _activity = "clientes_movimientos_cuenta_v2/";
  String _cuentaCorriente = "clientes_cuenta_corriente/";
  String _reclamos = "reclamos_pdv_v2/";
  String _createReclamos = "create_reclamo_mobile/";
  String _seguimiento = "clientes_seguimiento_pedidos_v2/";
  String _uploadImages = "upload_images/";
  String _updateNotificacione = "update_notificaciones_historial/";
  String _updateContact = "update_contactos_historial/";
  String _distribuidor = "getOneDistribuidor/";
  /*Award data */
  String _awardsData = "tokin_premia/";
  String _awardsUrlLink = "https://tokinpremia.com.ar";

  //Charge articles
  String _getArticle = "/get_articulos/";
  String _uploadArticle = "/update_clientes_pedidos_tokin_v2/";

  Future<String> getApiUrl() async {
    var baseUrl = await SharedPreferences.getInstance()
        .then((pref) => pref.getString("baseUrl"));
    if (baseUrl != null && baseUrl != "") {
      return baseUrl;
    } else {
      return _baseUrl;
    }
  }

  Future<String> getApiUrlService() async {
    var baseUrlService =
        await SharedPreferencesController.getPrefBaseUrlServiceTokin();
    if (baseUrlService != null && baseUrlService != "") {
      return baseUrlService;
    } else {
      return baseUrlService;
    }
  }

  Future<String> obtainImageUrl() async {
    return await SharedPreferences.getInstance()
        .then((pref) => pref.getString("imageUrl"));
  }



  String getImageUrl() {
    obtainImageUrl();
    return _imageUrl;
  }

  String getRegisterUri() {
    return _registerServer;
  }

  String getLicensesUri() {
    return _licenses;
  }

  Future<String> getNotificationUri() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _notificationV2);
    return baseUrl + _api + _notificationV2;
  }

  Future<String> getClientLoginUri() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _client_login);
    return baseUrl + _api + _client_login;
  }

  Future<String> getAuthUri() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _auth);
    return baseUrl + _auth;
  }

  Future<String> getActivityUrl() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _activity);
    return baseUrl + _api + _activity;
  }

  Future<String> getCuentaCorriente() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _cuentaCorriente);
    return baseUrl + _api + _cuentaCorriente;
  }

  Future<String> getReclamosV2() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _reclamos);
    return baseUrl + _api + _reclamos;
  }

  Future<String> getCreateReclamos() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _createReclamos);
    return baseUrl + _api + _createReclamos;
  }

  Future<String> getTracks() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _seguimiento);
    return baseUrl + _api + _seguimiento;
  }

  Future<String> getUploadImages() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _uploadImages);
    return baseUrl + _api + _uploadImages;
  }

  Future<String> getUpdateNotif() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _updateNotificacione);
    return baseUrl + _api + _updateNotificacione;
  }

  Future<String> getUpdateContact() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _updateContact);
    return baseUrl + _api + _updateContact;
  }

  Future<String> getDistribuidor() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _distribuidor);
    return baseUrl + _api + _distribuidor;
  }

  Future<String> getAwardsData() async {
    var baseUrl = await getApiUrl();
    print(baseUrl + _api + _awardsData);
    return baseUrl + _api + _awardsData;
  }

  Future<String> getAwardsLink() async {
    return _awardsUrlLink;
  }

  Future<String> getArticles() async {
    var baseUrlService = await getApiUrlService();
    print(baseUrlService + _getArticle);
    return baseUrlService + _getArticle;
  }

  Future<String> getUploadArticles() async {
    var baseUrlService = await getApiUrlService();
    print(baseUrlService + _uploadArticle);
    return baseUrlService + _uploadArticle;
  }
}
