import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

class TrackAPI with DateFormatter {
  int id;
  String codigoCliente;
  String idEmpresa;
  String idPais;
  String idDistribuidor;
  String idSucursal;
  DateTime fechaUltimoMovimiento;
  String estado;
  String cantidadItemsPedido;
  DateTime fechaRegistro;
  String montoRegistro;
  String idFactura;
  String cantidadItemsFacturado;
  DateTime fechaFacturado;
  String montoFacturado;
  DateTime fechaEnCamino;
  DateTime fechaEntregado;
  String tipoEntrega;
  String idPedido;
  String visto;

  TrackAPI({
    this.id,
    this.codigoCliente,
    this.idEmpresa,
    this.idPais,
    this.idDistribuidor,
    this.idSucursal,
    this.fechaUltimoMovimiento,
    this.estado,
    this.cantidadItemsPedido,
    this.fechaRegistro,
    this.montoRegistro,
    this.idFactura,
    this.cantidadItemsFacturado,
    this.fechaFacturado,
    this.montoFacturado,
    this.fechaEnCamino,
    this.fechaEntregado,
    this.tipoEntrega,
    this.idPedido,
    this.visto,
  });

  TrackAPI.fromJson(Map json)
      : id = json['id'],
        codigoCliente = json['codigo_cliente'],
        idEmpresa = json['id_empresa'],
        idPais = json['id_pais'],
        idDistribuidor = json['id_distribuidor'],
        idSucursal = json['id_sucursal'],
        fechaUltimoMovimiento = json['fecha_ultimo_movimiento'] != ""
            ? DateFormatter.toFormatDate(json['fecha_ultimo_movimiento'])
            : null,
        estado = json['estado'],
        cantidadItemsPedido = json['cantidad_items_pedido'],
        fechaRegistro = json['fecha_registro'] != ""
            ? DateFormatter.toFormatDate(json['fecha_registro'])
            : null,
        montoRegistro = json['monto_registro'],
        idFactura = json['id_factura'],
        cantidadItemsFacturado = json['cantidad_items_facturado'],
        fechaFacturado = json['fecha_facturado'] != ""
            ? DateFormatter.toFormatDate(json['fecha_facturado'])
            : null,
        montoFacturado = json['monto_facturado'],
        fechaEnCamino = json['fecha_en_camino'] != ""
            ? DateFormatter.toFormatDate(json['fecha_en_camino'])
            : null,
        fechaEntregado = json['fecha_entregado'] != ""
            ? DateFormatter.toFormatDate(json['fecha_entregado'])
            : null,
        tipoEntrega = json['tipo_entrega'],
        idPedido = json['id_pedido'],
        visto = json['visto'];

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "codigo_cliente": DBType.TEXT,
      "id_empresa": DBType.TEXT,
      "id_pais": DBType.TEXT,
      "id_distribuidor": DBType.TEXT,
      "id_sucursal": DBType.TEXT,
      "fecha_ultimo_movimiento": DBType.TEXT,
      "estado": DBType.TEXT,
      "cantidad_items_pedido": DBType.TEXT,
      "fecha_registro": DBType.TEXT,
      "monto_registro": DBType.TEXT,
      "id_factura": DBType.TEXT,
      "cantidad_items_facturado": DBType.TEXT,
      "fecha_facturado": DBType.TEXT,
      "monto_facturado": DBType.TEXT,
      "fecha_en_camino": DBType.TEXT,
      "fecha_entregado": DBType.TEXT,
      "tipo_entrega": DBType.TEXT,
      "id_pedido": DBType.TEXT,
      "visto": DBType.TEXT,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "codigo_cliente": this.codigoCliente,
      "id_empresa": this.idEmpresa,
      "id_pais": this.idPais,
      "id_distribuidor": this.idDistribuidor,
      "id_sucursal": this.idSucursal,
      "fecha_ultimo_movimiento":
          DateFormatter.toStringDate(this.fechaUltimoMovimiento),
      "estado": this.estado,
      "cantidad_items_pedido": this.cantidadItemsPedido,
      "fecha_registro": DateFormatter.toStringDate(this.fechaRegistro),
      "monto_registro": this.montoRegistro,
      "id_factura": this.idFactura,
      "cantidad_items_facturado": this.cantidadItemsFacturado,
      "fecha_facturado": DateFormatter.toStringDate(this.fechaFacturado),
      "monto_facturado": this.montoFacturado,
      "fecha_en_camino": DateFormatter.toStringDate(this.fechaEnCamino),
      "fecha_entregado": DateFormatter.toStringDate(this.fechaEntregado),
      "tipo_entrega": this.tipoEntrega,
      "id_pedido": this.idPedido,
      "visto": this.visto,
    };
  }
}
