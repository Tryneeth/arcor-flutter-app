import 'dart:core';

import 'package:tokin/src/utils/db/dbhelper.dart';

class Product {
  final String idPais;
  final String idDistribuidor;
  final String idSucursal;
  final String codigoArticulo;
  final String descArticulo;
  final String idListaPrecio;
  final String precioLista;
  final String montoDescuento;
  final String precioConDescuento;
  final String fechaUltimaModificacion;
  final String tipoProducto;
  final String unidadMedidaVenta;
  final String idDivision;
  final String idRubro;
  final String idSubRubro;
  final String idSegmento;
  final String idLinea;
  final String unidadMinimaVenta;
  final String precioListaDoubleDigits;
  bool highlighted = false;
  bool favorite = false;

  Product({
    this.idPais,
    this.idDistribuidor,
    this.idSucursal,
    this.codigoArticulo,
    this.descArticulo,
    this.idListaPrecio,
    this.precioLista,
    this.montoDescuento,
    this.precioConDescuento,
    this.fechaUltimaModificacion,
    this.tipoProducto,
    this.unidadMedidaVenta,
    this.idDivision,
    this.idRubro,
    this.idSubRubro,
    this.idSegmento,
    this.idLinea,
    this.unidadMinimaVenta,
    this.precioListaDoubleDigits,
  });

  Product.fromJson(Map<String, dynamic> json)
      : idPais = json['IDPais'],
        idDistribuidor = json['IDDistribuidor'],
        idSucursal = json['IDSucursal'],
        codigoArticulo = json['CodigoArticulo'],
        descArticulo = json['DescripcionArticulo'],
        idListaPrecio = json['IDListaPrecio'],
        precioLista = json['PrecioLista'],
        montoDescuento = json['MontoDescuento'],
        precioConDescuento = json['PrecioConDescuento'],
        fechaUltimaModificacion = json['FechaUltimaModificacion'],
        tipoProducto = json['TipoProducto'],
        unidadMedidaVenta = json['UnidadMedidaVenta'],
        idDivision = json['IDDivision'],
        idRubro = json['IDRubro'],
        idSubRubro = json['IDSubRubro'],
        idSegmento = json['IDSegmento'],
        idLinea = json['IDLinea'],
        unidadMinimaVenta = json['UnidadMinimaVenta'],
        precioListaDoubleDigits = double.tryParse(json['PrecioLista']).toStringAsFixed(2);

  static Map<String, String> getFieldsForDB() {
    return {
      "idPais": DBType.TEXT,
      "idDistribuidor": DBType.TEXT,
      "idSucursal": DBType.TEXT,
      "codigoArticulo": DBType.TEXT,
      "descArticulo": DBType.TEXT,
      "idListaPrecio": DBType.TEXT,
      "precioLista": DBType.TEXT,
      "montoDescuento": DBType.TEXT,
      "precioConDescuento": DBType.TEXT,
      "fechaUltimaModificacion": DBType.TEXT,
      "tipoProducto": DBType.TEXT,
      "unidadMedidaVenta": DBType.TEXT,
      "idDivision": DBType.TEXT,
      "idRubro": DBType.TEXT,
      "idSubRubro": DBType.TEXT,
      "idSegmento": DBType.TEXT,
      "idLinea": DBType.TEXT,
      "unidadMinimaVenta": DBType.TEXT,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "IDPais": this.idPais,
      "IDDistribuidor": this.idDistribuidor,
      "IDSucursal": this.idSucursal,
      "CodigoArticulo": this.codigoArticulo,
      "DescripcionArticulo": this.descArticulo,
      "IDListaPrecio": this.idListaPrecio,
      "PrecioLista": this.precioLista,
      "MontoDescuento": this.montoDescuento,
      "PrecioConDescuento": this.precioConDescuento,
      "FechaUltimaModificacion": this.fechaUltimaModificacion,
      "TipoProducto": this.tipoProducto,
      "UnidadMedidaVenta": this.unidadMedidaVenta,
      "IDDivision": this.idDivision,
      "IDRubro": this.idRubro,
      "IDSubRubro": this.idSubRubro,
      "IDSegmento": this.idSegmento,
      "IDLinea": this.idLinea,
      "UnidadMinimaVenta": this.unidadMinimaVenta
    };
  }

  Map<String, dynamic> toJson() {
    return {
      "IDPais": this.idPais,
      "IDDistribuidor": this.idDistribuidor,
      "IDSucursal": this.idSucursal,
      "CodigoArticulo": this.codigoArticulo,
      "DescripcionArticulo": this.descArticulo,
      "IDListaPrecio": this.idListaPrecio,
      "PrecioLista": this.precioLista,
      "MontoDescuento": this.montoDescuento,
      "PrecioConDescuento": this.precioConDescuento,
      "FechaUltimaModificacion": this.fechaUltimaModificacion,
      "TipoProducto": this.tipoProducto,
      "UnidadMedidaVenta": this.unidadMedidaVenta,
      "IDDivision": this.idDivision,
      "IDRubro": this.idRubro,
      "IDSubRubro": this.idSubRubro,
      "IDSegmento": this.idSegmento,
      "IDLinea": this.idLinea,
      "UnidadMinimaVenta": this.unidadMinimaVenta
    };
  }

  String getThumbnailUrl() {
    var urlStart = "https://resources.ebiz.arcornet.com/images/catalog/ARCOR_AR/products/";
    var urlEnd = "_detail.jpg";
    var code = "";
    if (codigoArticulo.length == 4)
      code = "100$codigoArticulo";
    else if (codigoArticulo.length == 5)
      code = "10$codigoArticulo";
    else
      code = "$codigoArticulo";

    return "$urlStart$code$urlEnd";
  }
}

class ProductType {
  static const String CANDIES = "01";
  static const String FOOD = "03";
  static const String CHOCOLATES = "10";
  static const String FLOUR = "12";
  static const String ICE_CREAMS = "1077";
  static const String HIGHLIGHTED = "HIGHLIGHTED";
  static const String OTHERS = "OTHERS";

  static String getString(code) {
    switch (code) {
      case CANDIES:
        return "Golosinas";
        break;
      case FOOD:
        return "Alimentos";
        break;
      case CHOCOLATES:
        return "Chocolates";
        break;
      case FLOUR:
        return "Harinas";
        break;
      case ICE_CREAMS:
        return "Helados";
        break;
      case HIGHLIGHTED:
        return "Destacados";
        break;
      default:
        return "Todo";
    }
  }

  static List<String> getTypesAsArray() {
    return [
      ProductType.HIGHLIGHTED,
      ProductType.CHOCOLATES,
      ProductType.CANDIES,
      ProductType.FOOD,
      ProductType.FLOUR,
      ProductType.ICE_CREAMS,
      ProductType.OTHERS,
    ];
  }
}
