import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tokin/src/models/product.dart';

import 'new.dart';

class ShoppingCartInfo {
  ShoppingCartOrder order;
  TabController controller;
  bool flagNews = false;
  bool flagContact = false;
  bool flagCart = false;
  bool flagTrack = false;
  bool flagPolls = false;

  ShoppingCartInfo({@required this.order});

  @override
  bool operator ==(other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    final ShoppingCartInfo otherShoppingCart = other;
    return otherShoppingCart.order == order;
  }

  @override
  int get hashCode => order.hashCode;

  static ShoppingCartInfo of(BuildContext context) {
    final ShoppingCartBindingScope scope =
        context.inheritFromWidgetOfExactType(ShoppingCartBindingScope);
    return scope.shoppingCartBindingState.shoppingCart;
  }

  static void update(BuildContext context) {
    final ShoppingCartBindingScope scope =
        context.inheritFromWidgetOfExactType(ShoppingCartBindingScope);
    scope.shoppingCartBindingState
        .updateBalance(scope.shoppingCartBindingState.shoppingCart);
  }
}

class ShoppingCartOrder {
  ShoppingCartOrder({@required this.idPedido, @required this.products});
  String idPedido;
  List<ShoppingCartItem> products;
}

class ShoppingCartItem {
  int ammount;
  NotificationAPI product;
  Product articles;

  ShoppingCartItem(
      {@required this.ammount,
      @required this.product,
      @required this.articles});

  ShoppingCartItem.fromJson(Map json)
      : product = NotificationAPI.fromJson(json["product"]),
        articles = Product.fromJson(json["articles"]),
        ammount = json["ammount"];

  String toJson() {
    return jsonEncode(
        ShoppingCartItem(product: product, articles: articles, ammount: ammount)
            .toMap());
  }

  Map<String, dynamic> toMap() {
    return {
      "product": this.product,
      "articles": this.articles,
      "ammount": this.ammount
    };
  }
}

class ShoppingCartBindingScope extends InheritedWidget {
  final _ShoppingCartBindingState shoppingCartBindingState;

  const ShoppingCartBindingScope({
    Key key,
    this.shoppingCartBindingState,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(ShoppingCartBindingScope old) {
    return true;
  }
}

class ShoppingCartBinding extends StatefulWidget {
  final ShoppingCartInfo shoppingCartInfo;
  final Widget child;

  const ShoppingCartBinding({Key key, this.shoppingCartInfo, this.child})
      : super(key: key);

  @override
  _ShoppingCartBindingState createState() => _ShoppingCartBindingState();
}

class _ShoppingCartBindingState extends State<ShoppingCartBinding> {
  ShoppingCartInfo shoppingCart;

  @override
  void initState() {
    super.initState();
    shoppingCart = widget.shoppingCartInfo;
  }

  void updateBalance(ShoppingCartInfo newBalance) {
    setState(() {
      shoppingCart = newBalance;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ShoppingCartBindingScope(
      shoppingCartBindingState: this,
      child: widget.child,
    );
  }
}
