import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

class Order {
  String id_pais;
  String id_distribuidor;
  String id_sucursal;
  String codigo_cliente;
  String id_pedido;
  String fecha_pedido;
  bool anulado;
  String observaciones_anulado;
  int cant_item;
  int total_item_pedido;
  String estado;
  String observaciones_facturacion;
  String fecha_entrega_pedido;
  String fecha_inicio_pedido;
  String hora_inicio_pedido;
  String fecha_fin_pedido;
  String hora_fin_pedido;
  String fecha_ultima_modificacion;
  String hora_ultima_modificacion;
  String fecha_hora_ultima_modificacion;
  String fecha_proceso;
  double mon_tot_ped_c_dto_imp;
  double monto_tot_ped_c_dto_s_imp;
  String id_condicion_venta;
  String id_direccion_entrega;
  String gps_latitud;
  String gps_longitud;
  String gps_precision;
  String tipo_pedido;
  List<OrderDetail> records_details;

  Order(
      {this.id_pais,
      this.id_distribuidor,
      this.id_sucursal,
      this.codigo_cliente,
      this.id_pedido,
      this.fecha_pedido,
      this.anulado,
      this.observaciones_anulado,
      this.cant_item,
      this.total_item_pedido,
      this.estado,
      this.observaciones_facturacion,
      this.fecha_entrega_pedido,
      this.fecha_inicio_pedido,
      this.hora_inicio_pedido,
      this.fecha_fin_pedido,
      this.hora_fin_pedido,
      this.fecha_ultima_modificacion,
      this.hora_ultima_modificacion,
      this.fecha_hora_ultima_modificacion,
      this.fecha_proceso,
      this.mon_tot_ped_c_dto_imp,
      this.monto_tot_ped_c_dto_s_imp,
      this.id_condicion_venta,
      this.id_direccion_entrega,
      this.gps_latitud,
      this.gps_longitud,
      this.gps_precision,
      this.tipo_pedido,
      this.records_details});

  Order.fromJson(Map<String, dynamic> json)
      : id_pais = json["id_pais"],
        id_distribuidor = json["id_distribuidor"],
        id_sucursal = json["id_sucursal"],
        codigo_cliente = json["codigo_cliente"],
        id_pedido = json["id_pedido"],
        fecha_pedido = json["fecha_pedido"],
        anulado = json["anulado"],
        observaciones_anulado = json["observaciones_anulado"],
        cant_item = json["cant_item"],
        total_item_pedido = json["total_item_pedido"],
        estado = json["estado"],
        observaciones_facturacion = json["observaciones_facturacion"],
        fecha_entrega_pedido = json["fecha_entrega_pedido"],
        fecha_inicio_pedido = json["fecha_inicio_pedido"],
        hora_inicio_pedido = json["hora_inicio_pedido"],
        fecha_fin_pedido = json["fecha_fin_pedido"],
        hora_fin_pedido = json["hora_fin_pedido"],
        fecha_ultima_modificacion = json["fecha_ultima_modificacion"],
        hora_ultima_modificacion = json["hora_ultima_modificacion"],
        fecha_hora_ultima_modificacion = json["fecha_hora_ultima_modificacion"],
        fecha_proceso = json["fecha_proceso"],
        mon_tot_ped_c_dto_imp = json["mon_tot_ped_c_dto_imp"],
        monto_tot_ped_c_dto_s_imp = json["monto_tot_ped_c_dto_s_imp"],
        id_condicion_venta = json["id_condicion_venta"],
        id_direccion_entrega = json["id_direccion_entrega"],
        gps_latitud = json["gps_latitud"],
        gps_longitud = json["gps_longitud"],
        gps_precision = json["gps_precision"],
        tipo_pedido = json["tipo_pedido"],
        records_details = json["records_details"];

  static Map<String, String> getFieldsForDB() {
    return {
      "id_pais": DBType.TEXT,
      "id_distribuidor": DBType.TEXT,
      "id_sucursal": DBType.TEXT,
      "codigo_cliente": DBType.TEXT,
      "id_pedido": DBType.TEXT,
      "fecha_pedido": DBType.TEXT,
      "anulado": DBType.INTEGER,
      "observaciones_anulado": DBType.TEXT,
      "cant_item": DBType.INTEGER,
      "total_item_pedido": DBType.INTEGER,
      "estado": DBType.TEXT,
      "observaciones_facturacion": DBType.TEXT,
      "fecha_entrega_pedido": DBType.TEXT,
      "fecha_inicio_pedido": DBType.TEXT,
      "hora_inicio_pedido": DBType.TEXT,
      "fecha_fin_pedido": DBType.TEXT,
      "hora_fin_pedido": DBType.TEXT,
      "fecha_ultima_modificacion": DBType.TEXT,
      "hora_ultima_modificacion": DBType.TEXT,
      "fecha_hora_ultima_modificacion": DBType.TEXT,
      "fecha_proceso": DBType.TEXT,
      "mon_tot_ped_c_dto_imp": DBType.DOUBLE,
      "monto_tot_ped_c_dto_s_imp": DBType.DOUBLE,
      "id_condicion_venta": DBType.TEXT,
      "id_direccion_entrega": DBType.TEXT,
      "gps_latitud": DBType.TEXT,
      "gps_longitud": DBType.TEXT,
      "gps_precision": DBType.TEXT,
      "tipo_pedido": DBType.TEXT,
      "records_details": DBType.TEXT,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "id_pais": this.id_pais,
      "id_distribuidor": this.id_distribuidor,
      "id_sucursal": this.id_sucursal,
      "codigo_cliente": this.codigo_cliente,
      "id_pedido": this.id_pedido,
      "fecha_pedido": this.fecha_pedido,
      "anulado": this.anulado,
      "observaciones_anulado": this.observaciones_anulado,
      "cant_item": this.cant_item,
      "total_item_pedido": this.total_item_pedido,
      "estado": this.estado,
      "observaciones_facturacion": this.observaciones_facturacion,
      "fecha_entrega_pedido": this.fecha_entrega_pedido,
      "fecha_inicio_pedido": this.fecha_inicio_pedido,
      "hora_inicio_pedido": this.hora_inicio_pedido,
      "fecha_fin_pedido": this.fecha_fin_pedido,
      "hora_fin_pedido": this.hora_fin_pedido,
      "fecha_ultima_modificacion": this.fecha_ultima_modificacion,
      "hora_ultima_modificacion": this.hora_ultima_modificacion,
      "fecha_hora_ultima_modificacion": this.fecha_hora_ultima_modificacion,
      "fecha_proceso": this.fecha_proceso,
      "mon_tot_ped_c_dto_imp": this.mon_tot_ped_c_dto_imp,
      "monto_tot_ped_c_dto_s_imp": this.monto_tot_ped_c_dto_s_imp,
      "id_condicion_venta": this.id_condicion_venta,
      "id_direccion_entrega": this.id_direccion_entrega,
      "gps_latitud": this.gps_latitud,
      "gps_longitud": this.gps_longitud,
      "gps_precision": this.gps_precision,
      "tipo_pedido": this.tipo_pedido,
      "records_details": toMapRecord()
    };
  }

  static String generateOrderCode(String clientCode) {
    String datePart = DateFormat("yyMMddhhmmss").format(DateTime.now());
    int i = 0;
    bool stopFlag = false;
    String clientCodeClean = "";
    do {
      if (clientCode[i] != "0") {
        clientCodeClean = clientCode.substring(i);
        stopFlag = true;
      }
      i++;
    } while (!stopFlag && i < clientCode.length);
    var orderCode = clientCodeClean + datePart;
    if (orderCode.length > 20) {
      orderCode = orderCode.substring(0, 20);
    }
    return orderCode;
  }

  List<Map<dynamic, dynamic>> toMapRecord() {
    List<Map<dynamic, dynamic>> records = List();
    records_details.forEach((item) {
      records.add(item.toMap());
    });
    return records;
  }
}

class OrderDetail {
  String id_pais;
  String id_distribuidor;
  String id_sucursal;
  String codigo_cliente;
  String id_pedido;
  String id_detalle_pedido;
  String id_lista_precio;
  String codigo_articulo;
  String codigo_notificacion;
  String tipo_pedido;
  String porcentaje_impuesto;
  String otros_descuentos;
  double monto_otros_descuentos;
  double precio_unitario_lista;
  String porcentaje_descuento_aplicado;
  double precio_unitario_venta;
  double cantidad_pedida;
  String fecha_ultima_modificacion;
  String flag_sin_cargo;
  int sincronizado;
  String medio_descarga;
  String fecha_hora_descarga;

  OrderDetail({
    this.id_pais,
    this.id_distribuidor,
    this.id_sucursal,
    this.codigo_cliente,
    this.id_pedido,
    this.id_detalle_pedido,
    this.id_lista_precio,
    this.codigo_articulo,
    this.codigo_notificacion,
    this.tipo_pedido,
    this.porcentaje_impuesto,
    this.otros_descuentos,
    this.monto_otros_descuentos,
    this.precio_unitario_lista,
    this.porcentaje_descuento_aplicado,
    this.precio_unitario_venta,
    this.cantidad_pedida,
    this.fecha_ultima_modificacion,
    this.flag_sin_cargo,
    this.sincronizado,
    this.medio_descarga,
    this.fecha_hora_descarga,
  });

  OrderDetail.fromJson(Map<String, dynamic> json)
      : id_pais = json["id_pais"],
        id_distribuidor = json["id_distribuidor"],
        id_sucursal = json["id_sucursal"],
        codigo_cliente = json["codigo_cliente"],
        id_pedido = json["id_pedido"],
        id_detalle_pedido = json["id_detalle_pedido"],
        id_lista_precio = json["id_lista_precio"],
        codigo_articulo = json["codigo_articulo"],
        codigo_notificacion = json["codigo_notificacion"],
        tipo_pedido = json["tipo_pedido"],
        porcentaje_impuesto = json["porcentaje_impuesto"],
        otros_descuentos = json["otros_descuentos"],
        monto_otros_descuentos = json["monto_otros_descuentos"],
        precio_unitario_lista = json["precio_unitario_lista"],
        porcentaje_descuento_aplicado = json["porcentaje_descuento_aplicado"],
        precio_unitario_venta = json["precio_unitario_venta"],
        cantidad_pedida = json["cantidad_pedida"],
        fecha_ultima_modificacion = json["fecha_ultima_modificacion"],
        flag_sin_cargo = json["flag_sin_cargo"],
        sincronizado = json["sincronizado"],
        medio_descarga = json["medio_descarga"],
        fecha_hora_descarga = json["fecha_hora_descarga"];

  OrderDetail.fromArticle(NotificationAPI news, Product article,
      String idPedido, String idDetallePedido, int cantidad)
      : id_pais = article.idPais,
        id_distribuidor = article.idDistribuidor,
        id_sucursal = article.idSucursal,
        codigo_cliente = news.codigoCliente,
        id_pedido = idPedido,
        id_detalle_pedido = idDetallePedido,
        id_lista_precio = article.idListaPrecio,
        codigo_articulo = article.codigoArticulo,
        codigo_notificacion = news.codigoNotificacion,
        tipo_pedido = "",
        porcentaje_impuesto = "",
        otros_descuentos = "",
        monto_otros_descuentos = 0,
        precio_unitario_lista = double.parse(article.precioConDescuento),
        porcentaje_descuento_aplicado = "",
        precio_unitario_venta = double.parse(article.precioConDescuento),
        cantidad_pedida = cantidad.toDouble(),
        fecha_ultima_modificacion =
            DateFormat("yyyyMMddhhmmss").format(DateTime.now()),
        flag_sin_cargo = "",
        sincronizado = 0,
        medio_descarga = "TOKIN",
        fecha_hora_descarga =
            DateFormat("yyyyMMddhhmmss").format(DateTime.now());

  static Map<String, String> getFieldsForDB() {
    return {
      "id_pais": DBType.TEXT,
      "id_distribuidor": DBType.TEXT,
      "id_sucursal": DBType.TEXT,
      "codigo_cliente": DBType.TEXT,
      "id_pedido": DBType.TEXT,
      "id_detalle_pedido": DBType.TEXT,
      "id_lista_precio": DBType.TEXT,
      "codigo_articulo": DBType.TEXT,
      "codigo_notificacion": DBType.TEXT,
      "tipo_pedido": DBType.TEXT,
      "porcentaje_impuesto": DBType.TEXT,
      "otros_descuentos": DBType.TEXT,
      "monto_otros_descuentos": DBType.TEXT,
      "precio_unitario_lista": DBType.TEXT,
      "porcentaje_descuento_aplicado": DBType.TEXT,
      "precio_unitario_venta": DBType.TEXT,
      "cantidad_pedida": DBType.TEXT,
      "fecha_ultima_modificacion": DBType.TEXT,
      "flag_sin_cargo": DBType.TEXT,
      "sincronizado": DBType.TEXT,
      "medio_descarga": DBType.TEXT,
      "fecha_hora_descarga": DBType.TEXT,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "id_pais": this.id_pais,
      "id_distribuidor": this.id_distribuidor,
      "id_sucursal": this.id_sucursal,
      "codigo_cliente": this.codigo_cliente,
      "id_pedido": this.id_pedido,
      "id_detalle_pedido": this.id_detalle_pedido,
      "id_lista_precio": this.id_lista_precio,
      "codigo_articulo": this.codigo_articulo,
      "codigo_notificacion": this.codigo_notificacion,
      "tipo_pedido": this.tipo_pedido,
      "porcentaje_impuesto": this.porcentaje_impuesto,
      "otros_descuentos": this.otros_descuentos,
      "monto_otros_descuentos": this.monto_otros_descuentos,
      "precio_unitario_lista": this.precio_unitario_lista,
      "porcentaje_descuento_aplicado": this.porcentaje_descuento_aplicado,
      "precio_unitario_venta": this.precio_unitario_venta,
      "cantidad_pedida": this.cantidad_pedida,
      "fecha_ultima_modificacion": this.fecha_ultima_modificacion,
      "flag_sin_cargo": this.flag_sin_cargo,
      "sincronizado": this.sincronizado,
      "medio_descarga": this.medio_descarga,
      "fecha_hora_descarga": this.fecha_hora_descarga
    };
  }
}
