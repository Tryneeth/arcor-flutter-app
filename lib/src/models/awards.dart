class Awards {
  int creditosDisponibles;
  String name;
  String avatar;

  Awards({this.creditosDisponibles, this.name, this.avatar});

  Awards.fromJson(Map json)
      : creditosDisponibles = json['CreditosDisponibles'],
        name = json['Nombre'],
        avatar = json['Avatar'];
  bool isValid() {
    return name != null && avatar != null;
  }
}
