import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

class ActivityAPI with DateFormatter {
  final int id;
  final String codigoCliente;
  final String idPais;
  final String idDistribuidor;
  final String idSucursal;
  final String idEmpresa;
  final String idMov;
  final String movType;
  final String montoMov;
  final DateTime fechaMov;
  final DateTime fechaVencimiento;

  ActivityAPI(
      {this.id,
      this.codigoCliente,
      this.idPais,
      this.idDistribuidor,
      this.idSucursal,
      this.idEmpresa,
      this.idMov,
      this.movType,
      this.montoMov,
      this.fechaMov,
      this.fechaVencimiento});

  ActivityAPI.fromJson(Map json)
      : id = json['id'],
        codigoCliente = json['codigo_cliente'],
        idPais = json['id_pais'],
        idDistribuidor = json['id_distribuidor'],
        idSucursal = json['id_sucursal'],
        idEmpresa = json['id_empresa'],
        idMov = json['id_movimiento'],
        movType = json['tipo_movimiento'],
        montoMov = json['monto_movimiento'],
        fechaMov = DateFormatter.toFormatDate(json['fecha_movimiento']),
        fechaVencimiento =
            DateFormatter.toFormatDate(json['fecha_vencimiento']);

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "codigo_cliente": DBType.TEXT,
      "id_pais": DBType.TEXT,
      "id_distribuidor": DBType.TEXT,
      "id_sucursal": DBType.TEXT,
      "id_empresa": DBType.TEXT,
      "id_movimiento": DBType.TEXT,
      "tipo_movimiento": DBType.TEXT,
      "monto_movimiento": DBType.TEXT,
      "fecha_movimiento": DBType.TEXT,
      "fecha_vencimiento": DBType.TEXT,
    };
  }

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "codigo_cliente": this.codigoCliente,
      "id_pais": this.idPais,
      "id_distribuidor": this.idDistribuidor,
      "id_sucursal": this.idSucursal,
      "id_empresa": this.idEmpresa,
      "id_movimiento": this.idMov,
      "tipo_movimiento": this.movType,
      "monto_movimiento": this.montoMov,
      "fecha_movimiento": DateFormatter.toStringDate(this.fechaMov),
      "fecha_vencimiento": DateFormatter.toStringDate(this.fechaVencimiento),
    };
  }

  String getIconUrl() {
    switch (movType) {
      case "FA":
        return "assets/ic_fb_cuenta.png";
      case "FB":
        return "assets/ic_fb_cuenta.png";
      case "FC":
        return "assets/ic_fb_cuenta.png";
      case "P":
        return "assets/ic_pag_cuenta.png";
      case "NC":
        return "assets/ic_nc_cuenta.png";
      case "ND":
        return "assets/ic_nd_cuenta.png";
    }
    return "";
  }

  String getSubtitle() {
    switch (movType) {
      case "FA":
        return "Compraste";
      case "FB":
        return "Compraste";
      case "FC":
        return "Compraste";
      case "P":
        return "Pagaste";
      case "NC":
        return "Acreditaste";
      case "ND":
        return "Ni idea ND";
    }
    return "";
  }
}
