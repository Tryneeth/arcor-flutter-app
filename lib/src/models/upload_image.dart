class UploadImage{
  String content;
  String extension;
  String fileName;
  String oldFilename;

  UploadImage({this.content,this.extension,this.fileName,this.oldFilename});

  Map toMap(){
    var map = new Map<String,String>();
    map['content']=content;
    map['extension']=extension;
    map['file_name']=fileName;
    map['old_filename']=oldFilename;
    return map;
  }
}