import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

class ClaimAPI with DateFormatter {
  int id;
  String codigoCliente;
  String idPais;
  String idDistribuidor;
  String idSucursal;
  String idEmpresa;
  String titulo;
  DateTime fecha;
  String tipo;
  String categoria;
  String estado;
  String vendedor;
  String transportista;
  String descripcion;
  DateTime fechaCierre;
  String descripcionCierre;
  bool activo;
  String idReclamo;
  int adjuntos;
  String visto;
  int syncronized;

  ClaimAPI(
      {this.id,
      this.codigoCliente,
      this.idPais,
      this.idDistribuidor,
      this.idEmpresa,
      this.idSucursal,
      this.tipo,
      this.fecha,
      this.categoria,
      this.estado,
      this.vendedor,
      this.transportista,
      this.descripcion,
      this.fechaCierre,
      this.descripcionCierre,
      this.activo,
      this.idReclamo,
      this.titulo,
      this.visto,
      this.adjuntos,
      this.syncronized});

  ClaimAPI.fromJson(Map json)
      : id = json['id'],
        codigoCliente = json['codigo_cliente'],
        idPais = json['id_pais'],
        idDistribuidor = json['id_distribuidor'],
        idSucursal = json['id_sucursal'],
        idEmpresa = json['id_empresa'],
        tipo = json['tipo'],
        fecha = DateFormatter.toFormatDate(json['fecha']),
        categoria = json['categoria'],
        estado = json['estado'],
        vendedor = json['vendedor'],
        transportista = json['transportista'],
        descripcion = json['descripcion'],
        fechaCierre = DateFormatter.toFormatDate(json['fecha_cierre']),
        descripcionCierre = json['descripcion_cierre'],
        activo =
            json['activo'] == true || json['activo'] == 'true' ? true : false,
        idReclamo = json['idReclamos'],
        titulo = json['titulo'],
        visto = json['visto'],
        adjuntos = json['adjuntos'],
        syncronized=json['syncronized'];

  static Map<String, String> getFieldsForDB() {
    return {
      'id': DBType.PRIMARY_KEY,
      'codigo_cliente': DBType.TEXT,
      'id_pais': DBType.TEXT,
      'id_distribuidor': DBType.TEXT,
      'id_sucursal': DBType.TEXT,
      'id_empresa': DBType.TEXT,
      'tipo': DBType.TEXT,
      'fecha': DBType.TEXT,
      'categoria': DBType.TEXT,
      'estado': DBType.TEXT,
      'vendedor': DBType.TEXT,
      'transportista': DBType.TEXT,
      'descripcion': DBType.TEXT,
      'fecha_cierre': DBType.TEXT,
      'descripcion_cierre': DBType.TEXT,
      'activo': DBType.TEXT,
      'idReclamos': DBType.TEXT,
      'titulo': DBType.TEXT,
      'visto': DBType.TEXT,
      'adjuntos': DBType.INTEGER,
      'syncronized': DBType.INTEGER
    };
  }

  toMap() {
    return {
      'id': this.id,
      'codigo_cliente': this.codigoCliente,
      'id_pais': this.idPais,
      'id_distribuidor': this.idDistribuidor,
      'id_sucursal': this.idSucursal,
      'id_empresa': this.idEmpresa,
      'tipo': this.tipo,
      'fecha': DateFormatter.toStringDate(this.fecha),
      'categoria': this.categoria,
      'estado': this.estado,
      'vendedor': this.vendedor,
      'transportista': this.transportista,
      'descripcion': this.descripcion,
      'fecha_cierre': DateFormatter.toStringDate(this.fechaCierre),
      'descripcion_cierre': this.descripcionCierre,
      'activo': this.activo.toString(),
      'idReclamos': this.idReclamo,
      'titulo': this.titulo,
      'visto': this.visto,
      'adjuntos': this.adjuntos,
      'syncronized': this.syncronized
    };
  }

  @override
  int get hashCode{
    return id.hashCode^
    codigoCliente.hashCode^
    idPais.hashCode^
    idDistribuidor.hashCode^
    idSucursal.hashCode^
    idEmpresa.hashCode^
    tipo.hashCode^
    fecha.hashCode^
    categoria.hashCode^
    estado.hashCode^
    vendedor.hashCode^
    transportista.hashCode^
    descripcion.hashCode^
    fechaCierre.hashCode^
    descripcionCierre.hashCode^
    activo.hashCode^
    idReclamo.hashCode^
    titulo.hashCode^
    visto.hashCode^
    adjuntos.hashCode;
  }

  @override
  bool operator ==(dynamic other){
    return other.hashCode==hashCode;
  }
}
