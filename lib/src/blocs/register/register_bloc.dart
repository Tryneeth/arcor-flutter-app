import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  static final RegisterBloc _newsBlocSingleton = new RegisterBloc._internal();
  factory RegisterBloc() {
    return _newsBlocSingleton;
  }
  RegisterBloc._internal();

  RegisterState get initialState => new UnRegisterState();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
