import 'dart:convert';

import 'package:tokin/src/controllers/base_http.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/register.dart';
import 'package:http/http.dart' as http;

import '../../models/data_connection.dart';

//OJOJ ver esto//
class RegisterProvider with BaseHttpRegister {
  RegisterProvider();

  Future<bool> register(
      {String phoneId,
      String email,
      String distributor,
      String clientId}) async {
    try {
      var bodyRequest = await getBodyData(
          androidId: phoneId,
          email: email,
          distributor: distributor,
          clientId: clientId);
      var headers = await getHeaders();
      var response = await http
          .post(Uri.encodeFull(new DataConnection().getRegisterUri()),
              headers: headers, body: json.encode(bodyRequest))
          .timeout(Duration(milliseconds: 20000));
      if (response.statusCode == 200) {
        var bodyData = json.decode(utf8.decode(response.bodyBytes));

        if (bodyData["status"] == "OK" && bodyData["message"] == "OK") {
          SharedPreferencesController.setPrefEmail(email);
          SharedPreferencesController.setPrefDistrib(distributor);
          SharedPreferencesController.setPrefClientCode(clientId);
          SharedPreferencesController.setPrefCountryCode("ARG");
          SharedPreferencesController.setPrefEmailVerified(true);
          return true;
        } else {
          return false;
        }
      }
      return false;
    } catch (error) {
      print(error);
      return false;
    }
  }
}
