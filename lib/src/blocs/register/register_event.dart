import 'dart:async';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:tokin/src/blocs/welcome/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';

import 'index.dart';

@immutable
abstract class RegisterEvent {
  Future<RegisterState> applyAsync(
      {RegisterState currentState, RegisterBloc bloc});
}

class InitRegisterEvent extends RegisterEvent {
  final String phoneId;
  final String email;
  final String distributor;
  final String clientId;
  InitRegisterEvent(
      {this.phoneId, this.email, this.clientId, this.distributor});

  @override
  String toString() => 'InitRegisterEvent';

  @override
  Future<RegisterState> applyAsync(
      {RegisterState currentState, RegisterBloc bloc}) async {
    try {
      return ChargingRegisterState(
          email: email,
          clientId: clientId,
          distributor: distributor,
          phoneId: phoneId);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorRegisterState(_?.toString());
    }
  }
}

class ChargingDataRegisterEvent extends RegisterEvent {
  final String phoneId;
  final String email;
  final String distributor;
  final String clientId;
  final BuildContext context;
  ChargingDataRegisterEvent(
      {this.phoneId,
      this.email,
      this.clientId,
      this.distributor,
      this.context});

  @override
  String toString() => 'ChargingDataRegisterEvent';

  @override
  Future<RegisterState> applyAsync(
      {RegisterState currentState, RegisterBloc bloc}) async {
    try {
      var result = await RegisterProvider().register(
          phoneId: phoneId,
          clientId: clientId,
          distributor: distributor,
          email: email);
      if (result) {
        var info;
        if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
          info = (await DeviceInfoPlugin().iosInfo).identifierForVendor;
        } else {
          info = (await DeviceInfoPlugin().androidInfo).androidId;
        }
        await SharedPreferencesController.setDeviceID(info);
        await WelcomeRepository().validateEmail(email, info);
        return SuccessRegisterState();
      } else {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (context) => AlertDialog(
                  title: Text("Error"),
                  content: Text("Ha ocurrido un error en la carga de datos."),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Aceptar"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ));
        return ErrorRegisterState("Problema");
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorRegisterState(_?.toString());
    }
  }
}

class UnRegisterEvent extends RegisterEvent {
  @override
  String toString() => 'UnRegisterEvent';

  @override
  Future<RegisterState> applyAsync(
      {RegisterState currentState, RegisterBloc bloc}) async {
    try {
      return InRegisterState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorRegisterState(_?.toString());
    }
  }
}

class ErroRegisterEvent extends RegisterEvent {
  @override
  String toString() => 'ErroRegisterEvent';

  @override
  Future<RegisterState> applyAsync(
      {RegisterState currentState, RegisterBloc bloc}) async {
    try {
      return ErrorRegisterState("");
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}
