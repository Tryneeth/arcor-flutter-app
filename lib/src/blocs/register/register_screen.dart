import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/blocs/login/index.dart';
import 'package:tokin/src/blocs/welcome/index.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';

import 'index.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({
    Key key,
    @required RegisterBloc registerBloc,
    @required String phoneId,
    @required String email,
  })  : _registerBloc = registerBloc,
        _phoneId = phoneId,
        _email = email,
        super(key: key);

  final RegisterBloc _registerBloc;
  final String _phoneId;
  final String _email;

  @override
  RegisterScreenState createState() {
    return new RegisterScreenState(_registerBloc);
  }
}

class RegisterScreenState extends State<RegisterScreen> {
  final RegisterBloc _registerBloc;
  static const offsetVisibleThreshold = 50;

  RegisterScreenState(this._registerBloc);

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  void initState() {
    super.initState();
    _registerBloc.dispatch(UnRegisterEvent());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
          backgroundColor: Colors.blue.shade400,
          body: ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: BlocBuilder<RegisterBloc, RegisterState>(
              bloc: widget._registerBloc,
              builder: (
                BuildContext context,
                RegisterState currentState,
              ) {
                if (currentState is UnRegisterState) {
                  print(currentState);
                  return Container();
                } else if (currentState is SuccessRegisterState) {
                  print(currentState);
                  return LoginPage();
                } else if (currentState is ErrorRegisterState) {
                  print(currentState);
                  return RegisterView(
                    registerBloc: _registerBloc,
                    email: widget._email,
                    phoneId: widget._phoneId,
                    charging: false,
                  );
                } else if (currentState is InRegisterState) {
                  print(currentState);
                  return RegisterView(
                    registerBloc: _registerBloc,
                    email: widget._email,
                    phoneId: widget._phoneId,
                    charging: false,
                  );
                } else if (currentState is ChargingRegisterState) {
                  print(currentState);
                  _registerBloc.dispatch(ChargingDataRegisterEvent(
                      clientId: currentState.clientId,
                      distributor: currentState.distributor,
                      email: currentState.email,
                      phoneId: currentState.phoneId,
                      context: context));
                  return RegisterView(
                    registerBloc: _registerBloc,
                    email: widget._email,
                    phoneId: widget._phoneId,
                    charging: true,
                  );
                } else
                  return Container();
              },
            ),
          ),
        ));
  }
}

class RegisterView extends StatefulWidget {
  final RegisterBloc registerBloc;
  final String phoneId;
  final String email;
  final bool charging;
  RegisterView(
      {Key key, this.phoneId, this.charging, this.registerBloc, this.email})
      : super(key: key);

  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  GlobalKey<FormState> _key = new GlobalKey();
  var emailController = new TextEditingController();
  var distController = new TextEditingController();
  var userIdController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    emailController.text = widget.email;
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        logged: true,
        appDrawer: AppDrawer(),
        child: Padding(
            padding: const EdgeInsets.all(1),
            child: ListView(children: <Widget>[
              Container(
                child: Form(
                    key: _key,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.only(
                                    top: 30, left: 30, right: 30, bottom: 15),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Registro de Licencia",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextFormField(
                                initialValue: "ARG",
                                enabled: false,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextFormField(
                                  decoration: InputDecoration(
                                    labelText: "Email",
                                    hasFloatingPlaceholder: true,
                                  ),
                                  controller: emailController,
                                  validator: (value) {
                                    var emailExp = RegExp(
                                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                                    if (value.isEmpty)
                                      return "Campo obligatorio";
                                    else if (!emailExp.hasMatch(value) &&
                                        value.isNotEmpty)
                                      return "Correo inválido";
                                    return null;
                                  }),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextFormField(
                                  decoration: InputDecoration(
                                    labelText: "Distribuidor",
                                    hasFloatingPlaceholder: true,
                                  ),
                                  controller: distController,
                                  keyboardType: TextInputType.number,
                                  validator: (value) {
                                    if (value.isEmpty)
                                      return "Campo obligatorio";
                                    return null;
                                  }),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextFormField(
                                  decoration: InputDecoration(
                                    labelText: "Código Cliente",
                                    hasFloatingPlaceholder: true,
                                  ),
                                  controller: userIdController,
                                  keyboardType: TextInputType.number,
                                  validator: (value) {
                                    if (value.isEmpty)
                                      return "Campo obligatorio";
                                    return null;
                                  }),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 10, left: 30),
                              child: Row(
                                children: <Widget>[
                                  Text("Id Dispositivo:"),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 30, bottom: 10),
                              child: Row(
                                children: <Widget>[
                                  Text(widget.phoneId),
                                ],
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    top: 10, left: 30, right: 30, bottom: 15),
                                child: Container(
                                  child: Text(
                                      "Los datos proporcionados se enviarán al servidor para su registro.",
                                      style: TextStyle(fontSize: 11)),
                                )),
                            widget.charging
                                ? Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10,
                                        left: 30,
                                        right: 30,
                                        bottom: 5),
                                    child: CircularProgressIndicator(
                                        backgroundColor: Colors.white),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10,
                                        left: 30,
                                        right: 30,
                                        bottom: 5),
                                    child: RaisedButton(
                                      child: Text("Registrar"),
                                      onPressed: () async {
                                        if (_key.currentState.validate()) {
                                          widget.registerBloc.dispatch(
                                              InitRegisterEvent(
                                                  clientId:
                                                      userIdController.text,
                                                  distributor:
                                                      distController.text,
                                                  email: emailController.text,
                                                  phoneId: widget.phoneId));
                                        }
                                      },
                                      color: Colors.grey,
                                      shape: StadiumBorder(),
                                      padding: const EdgeInsets.only(
                                          left: 100, right: 100),
                                    )),
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 30, right: 30, bottom: 5),
                                child: RaisedButton(
                                  child: Text("Salir"),
                                  onPressed: () {
                                    //Navigator.of(context).pop();
                                    Navigator.of(context)
                                        .pushReplacement(MaterialPageRoute(
                                            builder: (context) => WelcomeScreen(
                                                  welcomeBloc: WelcomeBloc(),
                                                )));
                                  },
                                  color: Colors.grey,
                                  shape: StadiumBorder(),
                                  padding: const EdgeInsets.only(
                                      left: 113, right: 113),
                                )),
                          ],
                        )
                      ],
                    )),
              ),
            ])));
  }
}
