import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterState extends Equatable {
  RegisterState([Iterable props]) : super(props);

  /// Copy object for use in action
  RegisterState copyWith();
}

/// UnInitialized
class UnRegisterState extends RegisterState {
  @override
  String toString() => 'UnRegisterState';

  @override
  RegisterState copyWith() {
    return UnRegisterState();
  }
}

/// Initialized
class InRegisterState extends RegisterState {
  @override
  String toString() => 'InRegisterState';

  @override
  RegisterState copyWith() {
    return InRegisterState();
  }
}

/// On Error
class ErrorRegisterState extends RegisterState {
  final String message;
  ErrorRegisterState(this.message);

  @override
  String toString() => 'ErrorNewsState';

  @override
  RegisterState copyWith() {
    return ErrorRegisterState("");
  }
}

class SuccessRegisterState extends RegisterState {
  @override
  String toString() => 'SuccessRegisterState';

  @override
  RegisterState copyWith() {
    return this;
  }
}

class ChargingRegisterState extends RegisterState {
  final String phoneId;
  final String email;
  final String distributor;
  final String clientId;
  ChargingRegisterState({this.phoneId, this.email, this.clientId, this.distributor});
  @override
  String toString() => 'ChargingRegisterState';

  @override
  RegisterState copyWith() {
    return this;
  }
}
