import 'package:flutter/material.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:intl/intl.dart';

class CartProvider {
  CartProvider();

  static Future<String> generateIDPedido(BuildContext context) async {
    var id_pedido = "";
    if (ShoppingCartInfo.of(context).order.idPedido != null && ShoppingCartInfo.of(context).order.idPedido != "") {
      id_pedido = ShoppingCartInfo.of(context).order.idPedido;
    } else {
      var cid = await SharedPreferencesController.getPreffullClientCode();
      while (cid[0] == '0') {
        cid = cid.substring(1, cid.length);
      }
      var timeForId = DateFormat("yyMMddHHmmss").format(DateTime.now());
      id_pedido = "$cid$timeForId";
      if (id_pedido.length > 20) {
        id_pedido = id_pedido.substring(0, 19);
      }
      ShoppingCartInfo.of(context).order.idPedido = id_pedido;
      ShoppingCartInfo.update(context);
    }
    return id_pedido;
  }
}
