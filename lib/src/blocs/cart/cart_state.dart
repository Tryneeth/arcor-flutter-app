import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CartState extends Equatable {
  CartState([Iterable props]) : super(props);

  /// Copy object for use in action
  CartState copyWith();
}

/// UnInitialized
class UnCartState extends CartState {
  @override
  String toString() => 'UnCartState';

  @override
  CartState copyWith() {
    return UnCartState();
  }
}

/// Initialized
class InCartState extends CartState {
  InCartState();

  @override
  String toString() => 'InCartState';

  @override
  CartState copyWith() {
    return InCartState();
  }
}

/// On Error
class ErrorCartState extends CartState {
  final String user;
  final String errorMessage;

  ErrorCartState(this.errorMessage,{this.user});

  @override
  String toString() => 'ErrorNewsState';

  @override
  CartState copyWith() {
    return ErrorCartState(this.errorMessage,user:this.user);
  }
}

class InitBuyCartState extends CartState {
  @override
  String toString() => 'InitBuyCartState';

  @override
  CartState copyWith() {
    return this;
  }
}

class SuccessCartState extends CartState {
  @override
  String toString() => 'SuccessCartState';

  @override
  CartState copyWith() {
    return this;
  }
}

class EndCartState extends CartState {
  @override
  String toString() => 'EndCartState';

  @override
  CartState copyWith() {
    return this;
  }
}
