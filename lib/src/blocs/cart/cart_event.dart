import 'dart:async';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/cart_controller.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/order.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:intl/intl.dart';

import 'index.dart';

@immutable
abstract class CartEvent {
  Future<CartState> applyAsync({CartState currentState, CartBloc bloc});
}

class InitCartEvent extends CartEvent {
  @override
  String toString() => 'InitCartEvent';

  @override
  Future<CartState> applyAsync({CartState currentState, CartBloc bloc}) async {
    try {
      return InCartState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorCartState(_?.toString());
    }
  }
}

class InitBuyCartEvent extends CartEvent {
  @override
  String toString() => 'InitBuyCartEvent';

  @override
  Future<CartState> applyAsync({CartState currentState, CartBloc bloc}) async {
    try {
      return InitBuyCartState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorCartState(_?.toString());
    }
  }
}

class ProcessBuyCartEvent extends CartEvent {
  ProcessBuyCartEvent({@required this.shoppingCartItems, this.context});

  final List<ShoppingCartItem> shoppingCartItems;
  final BuildContext context;

  @override
  String toString() => 'ProcessBuyCartEvent';

  @override
  Future<CartState> applyAsync({CartState currentState, CartBloc bloc}) async {
    try {
      List<OrderDetail> orderDetails = List();
      Order order = await createOrder();
      int cantItem = 0;
      double price = 0.0;
      for (var i = 0; i < shoppingCartItems.length; i++) {
        var item = shoppingCartItems[i];
        var orderDetail = OrderDetail.fromArticle(
            item.product, item.articles, order.id_pedido, "${order.id_pedido}-$i", item.ammount);
        orderDetails.add(orderDetail);
        cantItem += item.ammount;
        price += item.ammount * double.parse(item.articles.precioLista);
      }
      order.records_details = orderDetails;
      order.cant_item = orderDetails.length; //i supouse here will be cantItem
      order.total_item_pedido = orderDetails.length;
      order.mon_tot_ped_c_dto_imp = price;
      order.monto_tot_ped_c_dto_s_imp = price;
      var result = await CartController().processShop(order);
      if (result) {
        var products = ShoppingCartInfo.of(context).order.products;
        ShoppingCartInfo.of(context).order.idPedido = "";
        products.clear();
        ShoppingCartInfo.of(context).flagCart = false;
        ShoppingCartInfo.of(context).flagTrack = true;
        ShoppingCartInfo.update(context);
      }

      return InCartState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }

  Future<Order> createOrder() async {
    var idpais = await SharedPreferencesController.getPrefCountryCode();
    var idDist = await SharedPreferencesController.getPrefDistrib();
    var idsucursal = await SharedPreferencesController.getPrefSucursalId();
    var clientId = await SharedPreferencesController.getPreffullClientCode();
    var fechaHora = DateFormat("yyyyMMddhhmmss").format(DateTime.now());
    var fecha = DateFormat("yyyyMMdd").format(DateTime.now());
    var hora = DateFormat("hhmmss").format(DateTime.now());
    var id_pedido = await CartProvider.generateIDPedido(context);
    Order order = new Order(
        id_pais: idpais,
        id_distribuidor: idDist,
        id_sucursal: idsucursal,
        codigo_cliente: clientId,
        id_pedido: id_pedido,
        fecha_pedido: fecha,
        anulado: false,
        observaciones_anulado: "",
        cant_item: 0,
        total_item_pedido: 0,
        estado: "",
        observaciones_facturacion: "",
        fecha_entrega_pedido: "",
        fecha_inicio_pedido: fecha,
        hora_inicio_pedido: hora,
        fecha_fin_pedido: fecha,
        hora_fin_pedido: hora,
        fecha_ultima_modificacion: fecha,
        hora_ultima_modificacion: hora,
        fecha_hora_ultima_modificacion: fechaHora,
        fecha_proceso: fechaHora,
        mon_tot_ped_c_dto_imp: 0,
        monto_tot_ped_c_dto_s_imp: 0,
        id_condicion_venta: "",
        id_direccion_entrega: "",
        gps_latitud: "",
        gps_longitud: "",
        gps_precision: "",
        tipo_pedido: "TOKIN",
        records_details: <OrderDetail>[]);
    return order;
  }
}
