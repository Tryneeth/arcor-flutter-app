import 'package:flutter/material.dart';

import 'index.dart';

class CartPage extends StatelessWidget {
  static const String routeName = "/cart";

  @override
  Widget build(BuildContext context) {
    var _cartBloc = new CartBloc();
    return new Scaffold(      
      body: new CartScreen(cartBloc: _cartBloc),
    );
  }
}
