import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'index.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({
    Key key,
    @required CartBloc cartBloc,
  })  : _cartBloc = cartBloc,
        super(key: key);

  final CartBloc _cartBloc;

  @override
  CartScreenState createState() {
    return new CartScreenState(_cartBloc);
  }
}

class CartScreenState extends State<CartScreen> {
  final CartBloc _cartBloc;
  static const offsetVisibleThreshold = 50;

  CartScreenState(this._cartBloc);

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  void initState() {
    super.initState();
    _cartBloc.dispatch(InitCartEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ConstrainedBox(
        constraints: BoxConstraints.expand(),
        child: BlocBuilder<CartBloc, CartState>(
          bloc: widget._cartBloc,
          builder: (
            BuildContext context,
            CartState currentState,
          ) {
            if (currentState is UnCartState) {
              print("UnCartState");
              return loading();
            } else if (currentState is SuccessCartState) {
              widget._cartBloc.dispatch(InitCartEvent());
              return loading();
            } else if (currentState is ErrorCartState) {
              print("ErrorCartUi");
              //TODO: Aqui diferenciar cuando haya error
              return CartWidget();
            } else if (currentState is InCartState) {
              print("CartUi");
              var shoppingCartItems =
                  ShoppingCartInfo.of(context).order.products;
              return CartWidget(
                bloc: widget._cartBloc,
                shoppingCartItems: shoppingCartItems,
                loading: false,
              );
            } else if (currentState is InitBuyCartState) {
              var shoppingCartItems =
                  ShoppingCartInfo.of(context).order.products;
              widget._cartBloc.dispatch(ProcessBuyCartEvent(
                  shoppingCartItems: shoppingCartItems, context: context));
              return CartWidget(
                bloc: widget._cartBloc,
                shoppingCartItems: shoppingCartItems,
                loading: true,
              );
            } else
              return Container();
          },
        ),
      ),
    );
  }

  Widget loading() {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ));
  }
}

class CartWidget extends StatefulWidget {
  CartWidget({Key key, this.bloc, this.shoppingCartItems, this.loading})
      : super(key: key);
  final CartBloc bloc;
  final List<ShoppingCartItem> shoppingCartItems;
  final bool loading;

  _CartWidgetState createState() => _CartWidgetState();
}

class _CartWidgetState extends State<CartWidget> {
  GlobalKey<FormState> key = new GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Column(
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints(minHeight: 50),
            child: Container(
                padding: const EdgeInsets.all(5),
                width: MediaQuery.of(context).size.width,
                color: Colors.green,
                child: Center(
                    child: Column(children: <Widget>[
                  ConstrainedBox(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width),
                      child: Text(
                        "No Pedido: ${ShoppingCartInfo.of(context).order.idPedido}",
                        style: TextStyle(color: Colors.white),
                      )),
                  ConstrainedBox(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width),
                      child: Text(
                        "Entrega zona centro 24hrs y Campaña 42hs.",
                        style: TextStyle(color: Colors.white),
                      ))
                ]))),
          ),
          Flexible(
              child: ListView.builder(
            itemCount: widget.shoppingCartItems.length,
            itemBuilder: (context, int index) {
              return SingleCartItem(
                  shoppingItem: widget.shoppingCartItems.elementAt(index));
            },
          )),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            color: Colors.black.withOpacity(0.75),
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Total sin Impuestos",
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        "\$ ${totalAmmount()}",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                ),
                widget.loading
                    ? Container(
                        height: 50,
                        width: 50,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ))
                    : Container(
                        child: FlatButton(
                          color: Colors.green.withOpacity(0.45),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.white,
                                ),
                                Text(
                                  "CONFIRMAR LA COMPRA",
                                  style: TextStyle(color: Colors.white),
                                ),
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                          ),
                          onPressed: () {
                            widget.bloc.dispatch(InitBuyCartEvent());
                          },
                        ),
                      )
              ],
            ),
          )
        ],
      ),
    );
  }

  String totalAmmount() {
    double ammount = 0;
    var products = ShoppingCartInfo.of(context).order.products;
    products.forEach((f) {
      ammount += f.ammount * double.parse(f.articles.precioLista);
    });
    return ammount.toStringAsFixed(2);
  }
}

class SingleCartItem extends StatefulWidget {
  final ShoppingCartItem shoppingItem;
  SingleCartItem({Key key, @required this.shoppingItem}) : super(key: key);

  _SingleCartItemState createState() => _SingleCartItemState();
}

class _SingleCartItemState extends State<SingleCartItem> {
  int ammount;

  @override
  void initState() {
    super.initState();
    ammount = 1;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            widget.shoppingItem.product != null
                ? Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    width: MediaQuery.of(context).size.width / 3,
                    child: CachedNetworkImage(
                      imageUrl: !widget.shoppingItem.product.imagenAdjunto
                              .contains("http")
                          ? DataConnection().getImageUrl() +
                              widget.shoppingItem.product.imagenAdjunto
                          : widget.shoppingItem.product.imagenAdjunto,
                      errorWidget: (context, url, error) => Container(),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )
                : Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    width: MediaQuery.of(context).size.width / 3,
                    child: CachedNetworkImage(
                      imageUrl: DataConnection().getImageUrl() +
                          widget.shoppingItem.articles.getThumbnailUrl(),
                      errorWidget: (context, url, error) => Container(),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
            Container(
                width: MediaQuery.of(context).size.width / 3 * 2,
                child: Column(
                  children: <Widget>[
                    ConstrainedBox(
                        constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width / 3 * 2,
                            minWidth:
                                MediaQuery.of(context).size.width / 3 * 2),
                        child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "${widget.shoppingItem.product.titulo}",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.lightBlue,
                              ),
                            ))),
                    Container(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "Importe s/Impuestos",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 10),
                                    )),
                                Text(
                                  "\$ ${(double.parse(widget.shoppingItem.articles.precioLista) * widget.shoppingItem.ammount).toStringAsFixed(2)}",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            )),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              IconButton(
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    size: 25,
                                  ),
                                  onPressed: () async {
                                    int min = int.tryParse(widget.shoppingItem
                                        .articles.unidadMinimaVenta);
                                    //This if is for validate that exists a minimum limmit
                                    if ((min != null &&
                                            widget.shoppingItem.ammount >
                                                min) ||
                                        (min == null &&
                                            widget.shoppingItem.ammount > 1)) {
                                      var itemToAdd = widget.shoppingItem;
                                      itemToAdd.ammount--;
                                      var products =
                                          ShoppingCartInfo.of(context)
                                              .order
                                              .products;
                                      var indexOfItem =
                                          products.indexOf(widget.shoppingItem);
                                      products[indexOfItem] = itemToAdd;
                                      ShoppingCartInfo.update(context);
                                    } else {
                                      ShoppingCartInfo.of(context)
                                          .order
                                          .products
                                          .remove(widget.shoppingItem);
                                      if (ShoppingCartInfo.of(context)
                                              .order
                                              .products
                                              .length ==
                                          0) {
                                        ShoppingCartInfo.of(context).flagCart =
                                            false;
                                      }
                                      ShoppingCartInfo.update(context);
                                    }
                                    //Sub cicle for update cart in sharedPreferences
                                    var products = ShoppingCartInfo.of(context)
                                        .order
                                        .products;
                                    List<String> productJsons = List();
                                    products.forEach((item) =>
                                        productJsons.add(item.toJson()));
                                    await SharedPreferencesController
                                        .setPrefCart(productJsons);
                                    //end sub cicle
                                    if (ShoppingCartInfo.of(context)
                                            .order
                                            .products
                                            .length ==
                                        0) {
                                      ShoppingCartInfo.of(context).flagCart =
                                          false;
                                      ShoppingCartInfo.update(context);
                                    }
                                  }),
                              Text("${widget.shoppingItem.ammount}"),
                              IconButton(
                                  icon: Icon(
                                    Icons.add_circle_outline,
                                    size: 25,
                                  ),
                                  onPressed: () async {
                                    if (widget.shoppingItem != null &&
                                        widget.shoppingItem.product
                                                .topeCantidadVenta !=
                                            null &&
                                        widget.shoppingItem.product
                                                .topeCantidadVenta >
                                            widget.shoppingItem.ammount) {
                                      var itemToAdd = widget.shoppingItem;
                                      itemToAdd.ammount++;
                                      var products =
                                          ShoppingCartInfo.of(context)
                                              .order
                                              .products;
                                      var indexOfItem =
                                          products.indexOf(widget.shoppingItem);
                                      products[indexOfItem] = itemToAdd;
                                      ShoppingCartInfo.update(context);
                                      //Sub cicle for update cart in sharedPreferences

                                      List<String> productJsons = List();
                                      products.forEach((item) =>
                                          productJsons.add(item.toJson()));
                                      await SharedPreferencesController
                                          .setPrefCart(productJsons);
                                      //end sub cicle
                                    } else if (widget.shoppingItem != null &&
                                        widget.shoppingItem.product
                                                .topeCantidadVenta !=
                                            null &&
                                        widget.shoppingItem.product
                                                .topeCantidadVenta ==
                                            widget.shoppingItem.ammount) {
                                      showModalBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.transparent,
                                          builder: (context) => Container(
                                                height: 50,
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 40,
                                                    horizontal: 20),
                                                decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.8),
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    "El pedido no puede superar la cantidad disponible: ${widget.shoppingItem.product.topeCantidadVenta} ",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ));
                                    }
                                  }),
                            ],
                          ),
                        )
                      ],
                    ))
                  ],
                )),
          ],
        ));
  }
}
