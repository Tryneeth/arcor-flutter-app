import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  static final CartBloc _newsBlocSingleton = new CartBloc._internal();
  factory CartBloc() {
    return _newsBlocSingleton;
  }
  CartBloc._internal();

  CartState get initialState => new UnCartState();

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
