import 'dart:io';

import 'package:flutter/material.dart';

class CreateContact {
  final BuildContext context;
  final List<File> images;
  final String category;
  final String type;
  final String description;
  final String title;

  CreateContact(
      {this.context,
      this.images,
      this.category,
      this.type,
      this.description,
      this.title});
}
