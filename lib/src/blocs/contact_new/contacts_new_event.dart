import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/claimsController.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/controllers/uploadController.dart';
import 'package:tokin/src/models/info_petitions.dart';
import 'package:tokin/src/models/upload_image.dart';

import 'index.dart';

@immutable
abstract class ContactNewEvent {
  Future<ContactNewState> applyAsync(
      {ContactNewState currentState, ContactNewBloc bloc});
}

class LoadContactNewEvent extends ContactNewEvent {
  @override
  String toString() => 'LoadContactNewEvent';

  @override
  Future<ContactNewState> applyAsync(
      {ContactNewState currentState, ContactNewBloc bloc}) async {
    try {
      var name = await SharedPreferencesController.getPrefatpdvId();
      return InContactNewState(imageName: name);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorContactNewState(_?.toString());
    }
  }
}

class LoadingCreateContactNewEvent extends ContactNewEvent {
  final CreateContact createContact;

  LoadingCreateContactNewEvent({this.createContact});
  @override
  String toString() => 'LoadContactNewEvent';

  @override
  Future<ContactNewState> applyAsync(
      {ContactNewState currentState, ContactNewBloc bloc}) async {
    try {
      var name = await SharedPreferencesController.getPrefatpdvId();
      return LoadingCreateContactNewState(
          imageName: name, createContact: createContact);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorContactNewState(_?.toString());
    }
  }
}

class CreateContactNewEvent extends ContactNewEvent {
  final CreateContact createContact;
  CreateContactNewEvent({this.createContact});

  @override
  String toString() => 'CreateContactNewEvent';

  @override
  Future<ContactNewState> applyAsync(
      {ContactNewState currentState, ContactNewBloc bloc}) async {
    try {
      var result = await createClaim();
      if (result) {
        return EndContactNewState();
      } else {
        var name = await SharedPreferencesController.getPrefatpdvId();
        return InContactNewState(imageName: name);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }

  Future<bool> createClaim() async {
    //Creating the claim
    PostClaims claim = new PostClaims();
    claim.syncronized = 1;
    claim.activo = false;
    claim.adjuntos = createContact.images.length;
    claim.categoria = createContact.category;
    claim.descripcion = createContact.description;
    claim.tipo = createContact.type;
    claim.titulo = createContact.title;
    claim.fecha = DateTime.now();
    claim.fechaCierre = null;
    claim.id = 1;
    claim.descripcionCierre = "";
    claim.codigoCliente =
        await SharedPreferencesController.getPreffullClientCode();
    claim.estado = "Registrado";
    claim.idPais = await SharedPreferencesController.getPrefCountryCode();
    claim.idReclamo =
        "${await SharedPreferencesController.getPrefatpdvId()}-${DateFormatter.toStringDate(claim.fecha)}";
    claim.idDistribuidor = await SharedPreferencesController.getPrefDistrib();
    claim.idSucursal = await SharedPreferencesController.getPrefSucursalId();
    claim.transportista = "0";
    claim.vendedor = "";
    claim.visto = "Visto";

    //sending data
    bool resultImagesUpload;
    bool resultClaimCreate = await ClaimsController().createData(claim);

    if (resultClaimCreate && createContact.images.length > 0) {
      var content = await UploadController()
          .compressAndStoreImages(createContact.images, claim.idReclamo);
      UploadImage imageToUpload = new UploadImage(
          extension: "zip", content: content, fileName: claim.idReclamo);
      resultImagesUpload = await UploadController().createData(imageToUpload);
    }

    var response = "";
    if (resultClaimCreate) {
      if (resultImagesUpload == null) {
        response = "Contacto creado.";
      } else if (resultImagesUpload) {
        response = "Contacto creado.";
      } else {
        response = "Contacto creado, pero no se pudieron enviar las imágenes.";
      }
    } else {
      response = "No se pudo crear el contacto.";
    }
    return await _showDialogCreateClaim(response);
  }

  Future<bool> _showDialogCreateClaim(String response) async {
    return await showDialog<bool>(
      context: createContact.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("ENVIADO"),
          content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                response,
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Gracias por contactarse con nosotros.",
                textAlign: TextAlign.start,
              ),
            )
          ]),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
            new FlatButton(
              child: new Text("Aceptar"),
              onPressed: () {
                Navigator.pop(context, true);
              },
            ),
          ],
        );
      },
    );
  }
}
