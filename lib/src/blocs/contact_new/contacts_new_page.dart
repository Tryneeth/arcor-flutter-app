import 'package:flutter/material.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';

import 'index.dart';

class ContactNewPage extends StatelessWidget {
  static const String routeName = "/contactNew";

  @override
  Widget build(BuildContext context) {
    var _contactNewBloc = new ContactNewBloc();
    return new Scaffold(
        body: new ContactNewScreen(contactNewBloc: _contactNewBloc));
  }
}
