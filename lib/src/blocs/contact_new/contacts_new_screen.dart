import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/ui/home_widgets.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';
import 'package:tokin/src/ui/shared/image_picker.dart';
import 'package:tokin/src/ui/shared/loading.dart';
import 'package:tokin/src/ui/shared/no_data.dart';
import 'package:intl/intl.dart';

import 'contacts_new_bloc.dart';
import 'index.dart';

class ContactNewScreen extends StatefulWidget {
  const ContactNewScreen({
    Key key,
    @required ContactNewBloc contactNewBloc,
  })  : _contactNewBloc = contactNewBloc,
        super(key: key);

  final ContactNewBloc _contactNewBloc;

  @override
  ContactNewScreenState createState() {
    return new ContactNewScreenState(_contactNewBloc);
  }
}

class ContactNewScreenState extends State<ContactNewScreen> {
  final ContactNewBloc _contactNewBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  ContactNewScreenState(this._contactNewBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _contactNewBloc.dispatch(LoadContactNewEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<ContactNewBloc, ContactNewState>(
        bloc: widget._contactNewBloc,
        builder: (
          BuildContext context,
          ContactNewState currentState,
        ) {
          print(currentState);
          if (currentState is UnContactNewState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorContactNewState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Contactos en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is InContactNewState) {
            return NewContact(imageName: currentState.imageName);
          } else if (currentState is EndContactNewState) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              widget._contactNewBloc.dispatch(LoadContactNewEvent());
              Navigator.pushReplacementNamed(context, "contacts");
            });
            return Loading();
          } else if (currentState is LoadingCreateContactNewState) {
            ContactNewBloc().dispatch(CreateContactNewEvent(
              createContact: currentState.createContact,
            ));
            return NewContact(imageName: currentState.imageName);
          }
          return Container(
            height: 20,
            color: Colors.red,
          );
        },
      ),
    );
  }
}

class NewContact extends StatefulWidget {
  final String imageName;
  NewContact({@required this.imageName});
  @override
  _NewContactState createState() => _NewContactState();
}

class _NewContactState extends State<NewContact> {
  String claimType;
  String claimCategory;
  List<String> claimTypes = <String>["Reclamo", "Sugerencia", "Otros"];
  List<String> claimCategories = <String>["Ventas", "Mercadería", "Otros"];
  final TextEditingController titleController = new TextEditingController();
  final TextEditingController descriptionController =
      new TextEditingController();

  final _formKey = GlobalKey<FormState>();

  File image;
  List<File> images = new List<File>();

  Image imageLoaded;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _getImage() async {
    var result = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomImagePicker();
      },
    );
    if (result != null) {
      images.add(result);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      key: ValueKey("createContact"),
      logged: true,
      appDrawer: AppDrawer(
        activeRoute: "contacts",
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            color: Colors.blue.shade900,
            height: 120,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "NUEVO CONTACTO",
                      style: Theme.of(context)
                          .primaryTextTheme
                          .title
                          .copyWith(fontSize: 25),
                    ),
                    Text(DateFormat("dd/MM/y").format(DateTime.now()),
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(
                                fontSize: 14, color: Colors.grey.shade400)),
                  ],
                ),
                Positioned(
                  left: 10,
                  top: 10,
                  child: IconButton(
                    key: ValueKey("backButtonCreateNewContact"),
                    onPressed: () => Navigator.of(context).pop(),
                    icon: Theme.of(context).platform == TargetPlatform.iOS
                        ? Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 30,
                          )
                        : Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Form(
              key: _formKey,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30, left: 15, right: 15, bottom: 15),
                    child: Text("SELECCIONE"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 6),
                    child: DropdownButtonFormField<String>(
                      onChanged: (String value) {
                        setState(() {
                          claimType = value;
                        });
                      },
                      hint: Text("Motivo"),
                      value: claimType,
                      items: claimTypes.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 6),
                    child: DropdownButtonFormField<String>(
                      onChanged: (String value) {
                        setState(() {
                          claimCategory = value;
                        });
                      },
                      hint: Text("Tema"),
                      value: claimCategory,
                      items: claimCategories.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "TÍTULO",
                          hasFloatingPlaceholder: true,
                        ),
                        controller: titleController,
                        validator: (value) {
                          if (value.isEmpty) return "Campo obligatorio";
                          return null;
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Descripción",
                        hasFloatingPlaceholder: true,
                      ),
                      controller: descriptionController,
                      validator: (value) {
                        if (value.isEmpty) return "Campo obligatorio";
                        return null;
                      },
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30.0, vertical: 5.0),
                          child: IconButton(
                              icon: Icon(Icons.add_a_photo),
                              onPressed: () async {
                                await _getImage();
                              })),
                    ],
                  ),
                  if (images.length > 0)
                    Container(
                      height: 100,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: ListView.builder(
                        itemCount: images.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            leading: Container(
                                height: 50,
                                width: 50,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(1),
                                  child: Image.file(images[index]),
                                )),
                            title: Text("${widget.imageName}-${index + 1}"),
                            trailing: IconButton(
                              icon: Icon(Icons.cancel),
                              onPressed: () {
                                setState(() {
                                  images.removeAt(index);
                                });
                              },
                            ),
                          );
                        },
                      ),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: BlocBuilder<ContactNewBloc, ContactNewState>(
                              bloc: ContactNewBloc(),
                              builder: (
                                BuildContext context,
                                ContactNewState currentState,
                              ) {
                                if (currentState is InContactNewState) {
                                  return RaisedButton(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 40.0),
                                    child: Text(
                                      "Enviar",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Colors.blue.shade900,
                                    onPressed: () async {
                                      if (claimType == null ||
                                          claimCategory == null) {
                                        Scaffold.of(context).showSnackBar(SnackBar(
                                            content: Text(
                                                'Por favor revise, debe seleccionar algún valor en los campos Motivo y Tema ')));
                                      } else if (_formKey.currentState
                                          .validate()) {
                                        setState(() {
                                          titleController.text = titleController
                                              .text
                                              .toUpperCase();
                                        });
                                        ContactNewBloc().dispatch(
                                            LoadingCreateContactNewEvent(
                                                createContact: CreateContact(
                                                    category: claimCategory,
                                                    type: claimType,
                                                    context: context,
                                                    description:
                                                        descriptionController
                                                            .text,
                                                    title: titleController.text,
                                                    images: images)));
                                      }
                                    },
                                    shape: StadiumBorder(),
                                  );
                                } else if (currentState
                                    is LoadingCreateContactNewState) {
                                  return RaisedButton(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 40.0),
                                    child: Container(
                                      height: 30,
                                      width: 30,
                                      child: CircularProgressIndicator(),
                                    ),
                                    color: Colors.blue.shade900,
                                    onPressed: () {},
                                    shape: StadiumBorder(),
                                  );
                                }
                                return Container();
                              })),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
