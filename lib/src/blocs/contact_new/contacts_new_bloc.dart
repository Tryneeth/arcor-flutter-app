import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class ContactNewBloc extends Bloc<ContactNewEvent, ContactNewState> {
  static final ContactNewBloc _contactNewBlocSingleton =
      new ContactNewBloc._internal();
  factory ContactNewBloc() {
    return _contactNewBlocSingleton;
  }
  ContactNewBloc._internal();

  ContactNewState get initialState => new UnContactNewState();

  @override
  Stream<ContactNewState> mapEventToState(
    ContactNewEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
