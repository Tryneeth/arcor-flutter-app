import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/claims.dart';

import 'index.dart';

@immutable
abstract class ContactNewState extends Equatable {
  ContactNewState([Iterable props]) : super(props);

  /// Copy object for use in action
  ContactNewState copyWith();
}

/// UnInitialized
class UnContactNewState extends ContactNewState {
  @override
  String toString() => 'UnContactNewState';

  @override
  ContactNewState copyWith() {
    return UnContactNewState();
  }
}

/// Initialized
class InContactNewState extends ContactNewState {
  final String imageName;
  InContactNewState({this.imageName});

  @override
  String toString() => 'InContactNewState';

  @override
  ContactNewState copyWith(
      {List<ClaimAPI> contactNew, bool hasReachedMaxData}) {
    return InContactNewState(imageName: imageName);
  }
}

class EndContactNewState extends ContactNewState {
  EndContactNewState();

  @override
  String toString() => 'EndContactNewState';

  @override
  ContactNewState copyWith(
      {List<ClaimAPI> contactNew, bool hasReachedMaxData}) {
    return EndContactNewState();
  }
}

/// On Error
class ErrorContactNewState extends ContactNewState {
  final String errorMessage;

  ErrorContactNewState(this.errorMessage);

  @override
  String toString() => 'ErrorContactNewState';

  @override
  ContactNewState copyWith() {
    return ErrorContactNewState(this.errorMessage);
  }
}

/// Initialized
class LoadingCreateContactNewState extends ContactNewState {
  final String imageName;
  final CreateContact createContact;
  LoadingCreateContactNewState({this.imageName, this.createContact});
  @override
  String toString() => 'LoadingCreateContactNewState';

  @override
  ContactNewState copyWith({CreateContact createContact, String imageName}) {
    return LoadingCreateContactNewState(
        createContact: createContact, imageName: imageName);
  }
}
