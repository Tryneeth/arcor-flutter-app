import 'package:flutter/material.dart';
import 'package:tokin/src/blocs/contact_new/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/ui/home_contacts.dart';

import 'index.dart';

class ContactsPage extends StatelessWidget {
  static const String routeName = "/contacts";

  @override
  Widget build(BuildContext context) {
    var _contactsBloc = new ContactsBloc();
    return new Scaffold(
      body: new ContactsScreen(contactsBloc: _contactsBloc),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue.shade900,
        child: Icon(
          Icons.add,
          size: 55,
        ),
        onPressed: () async {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => ContactNewPage()));
        },
      ),
    );
  }
}
