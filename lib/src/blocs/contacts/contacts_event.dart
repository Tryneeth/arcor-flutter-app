import 'dart:async';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/claims.dart';

import 'index.dart';

@immutable
abstract class ContactsEvent {
  Future<ContactsState> applyAsync(
      {ContactsState currentState, ContactsBloc bloc});

  final ContactsRepository _contactsProvider = new ContactsRepository();
}

class LoadContactsEvent extends ContactsEvent {
  @override
  String toString() => 'LoadContactsEvent';

  @override
  Future<ContactsState> applyAsync(
      {ContactsState currentState, ContactsBloc bloc}) async {
    try {
      return UnContactsState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorContactsState(_?.toString());
    }
  }
}

class RefreshContactsEvent extends ContactsEvent {
  final BuildContext context;

  RefreshContactsEvent(this.context);

  @override
  String toString() => 'RefreshContactsEvent';

  @override
  Future<ContactsState> applyAsync(
      {ContactsState currentState, ContactsBloc bloc}) async {
    try {
      List<ClaimAPI> claimAPIList =
          await _contactsProvider.findAll(true, context);
      return claimAPIList.isEmpty
          ? NoDataContactsState()
          : InContactsState(claimAPIList);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMoreContactsEvent extends ContactsEvent {
  final BuildContext context;
  LoadMoreContactsEvent({this.context});
  @override
  String toString() => 'LoadMoreContactsEvent';

  @override
  Future<ContactsState> applyAsync(
      {ContactsState currentState, ContactsBloc bloc}) async {
    try {
      // Fetch Next Page
      var response = await this._contactsProvider.findAll(true, context);
      return response.isEmpty
          ? InContactsState((currentState as InContactsState).contacts,
              hasReachedMaxData: true)
          : InContactsState(
              (currentState as InContactsState).contacts + response);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}
