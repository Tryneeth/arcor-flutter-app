import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/blocs/contacts/index.dart';
import 'package:tokin/src/controllers/claimsController.dart';
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/ui/home_contacts.dart';
import 'package:tokin/src/ui/shared/no_data.dart';
import 'package:intl/intl.dart';

class ContactsScreen extends StatefulWidget {
  const ContactsScreen({
    Key key,
    @required ContactsBloc contactsBloc,
  })  : _contactsBloc = contactsBloc,
        super(key: key);

  final ContactsBloc _contactsBloc;

  @override
  ContactsScreenState createState() {
    return new ContactsScreenState(_contactsBloc);
  }
}

class ContactsScreenState extends State<ContactsScreen> {
  final ContactsBloc _contactsBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  ContactsScreenState(this._contactsBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _contactsBloc.dispatch(LoadContactsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<ContactsBloc, ContactsState>(
        bloc: widget._contactsBloc,
        builder: (
          BuildContext context,
          ContactsState currentState,
        ) {
          if (currentState is UnContactsState) {
            _contactsBloc.dispatch(RefreshContactsEvent(context));
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorContactsState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Contactos en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataContactsState) {
            return new NoDataWidget(
              message: "No hay datos de Contactos en estos momentos.",
            );
          } else if (currentState is InContactsState) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return NotificationListener<ScrollNotification>(
              //onNotification: _handleScroll,
              child: RefreshIndicator(
                  onRefresh: () async {
                    widget._contactsBloc
                        .dispatch(RefreshContactsEvent(context));
                    return await _refreshCompleter.future;
                  },
                  child: claimListView(currentState.contacts)),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildListItemLoader() => Center(
        child: CircularProgressIndicator(),
      );

  bool _handleScroll(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        _listScrollCtrl.position.extentAfter == 0)
      _contactsBloc.dispatch(LoadMoreContactsEvent());

    return false;
  }

  int _calculateListItemsCount(InContactsState state) {
    if (state.hasReachedMaxData)
      return state.contacts.length;
    else
      return state.contacts.length + 1;
  }

  Widget claimListView(List<ClaimAPI> claimList) {
    return claimList.isNotEmpty
        ? ListView.builder(
            key: ValueKey("claimsList"),
            shrinkWrap: true,
            itemCount: claimList.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Stack(
                  children: <Widget>[
                    claimList[index].estado == "Cerrado"
                        ? Image.asset(
                            "assets/ic_list_contacto_cerrado.png",
                            height: 50,
                            width: 45,
                          )
                        : Image.asset(
                            "assets/ic_list_contacto.png",
                            height: 50,
                            width: 45,
                          ),
                    if (claimList[index].visto.toLowerCase() != "visto")
                      Positioned(
                        left: 0,
                        top: 0,
                        child: Image.asset(
                          "assets/new_novedad_new.png",
                          height: 25,
                        ),
                      )
                  ],
                ),
                title: Text(claimList[index].titulo),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      DateFormat("dd/MM/y").format(claimList[index].fecha),
                      style: Theme.of(context)
                          .primaryTextTheme
                          .body1
                          .copyWith(fontSize: 16),
                    ),
                    Text(
                      claimList[index].estado,
                      style: Theme.of(context)
                          .primaryTextTheme
                          .body1
                          .copyWith(fontSize: 16),
                    ),
                  ],
                ),
                onTap: () async {
                  try {
                    await ClaimsController().updateViewField(claimList[index]);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            ContactDetails(claimAPI: claimList[index])));
                  } catch (ex) {
                    print(ex);
                  }
                },
              );
            })
        : NoDataWidget(
            message:
                "No hay datos de Contactos, puede haber problemas con la conexión");
  }
}
