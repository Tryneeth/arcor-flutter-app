import 'package:tokin/src/models/claims.dart';

class ContactsResponse {
  List<ClaimAPI> results;

  ContactsResponse({this.results});

  ContactsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      results = new List<ClaimAPI>();
      json['data'].forEach((v) {
        results.add(new ClaimAPI.fromJson(v));
      });
    }
  }
}
