import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/claims.dart';

@immutable
abstract class ContactsState extends Equatable {
  ContactsState([Iterable props]) : super(props);

  /// Copy object for use in action
  ContactsState copyWith();
}

/// UnInitialized
class UnContactsState extends ContactsState {
  @override
  String toString() => 'UnContactsState';

  @override
  ContactsState copyWith() {
    return UnContactsState();
  }
}

/// Initialized
class InContactsState extends ContactsState {
  final List<ClaimAPI> contacts;
  final bool hasReachedMaxData;

  InContactsState(this.contacts, {this.hasReachedMaxData = false})
      : super([contacts, hasReachedMaxData]);

  @override
  String toString() => 'InContactsState';

  @override
  ContactsState copyWith({List<ClaimAPI> contacts, bool hasReachedMaxData}) {
    return InContactsState(contacts ?? this.contacts,
        hasReachedMaxData: hasReachedMaxData ?? this.hasReachedMaxData);
  }
}

/// On Error
class ErrorContactsState extends ContactsState {
  final String errorMessage;

  ErrorContactsState(this.errorMessage);

  @override
  String toString() => 'ErrorContactsState';

  @override
  ContactsState copyWith() {
    return ErrorContactsState(this.errorMessage);
  }
}

/// No Data
class NoDataContactsState extends ContactsState {
  @override
  String toString() => 'NoDataContactsState';

  @override
  ContactsState copyWith() {
    return this;
  }
}
