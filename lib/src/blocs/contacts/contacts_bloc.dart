import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  static final ContactsBloc _contactsBlocSingleton =
      new ContactsBloc._internal();
  factory ContactsBloc() {
    return _contactsBlocSingleton;
  }
  ContactsBloc._internal();

  ContactsState get initialState => new UnContactsState();

  @override
  Stream<ContactsState> mapEventToState(
    ContactsEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
