import 'package:flutter/widgets.dart';
import 'package:tokin/src/controllers/claimsController.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

class ContactsRepository {
  DBHelper<ClaimAPI> _dbHelper;

  ContactsRepository() {
    _dbHelper = new DBHelper<ClaimAPI>(
        dbTableName: 'contacts', tableField: ClaimAPI.getFieldsForDB());
  }

  Future<List<ClaimAPI>> findAll(bool update, BuildContext context) async {
    List<ClaimAPI> claimAPIListResult = new List();
    try {
      if (update) {
        var claimAPIList = await ClaimsController().getData(true);
        claimAPIList.forEach((claim) async {
          var dbElement = await _dbHelper.findOne(where: "id ==${claim.id}");
          if (dbElement.length > 0) {
            claim = updateData(dbElement, claim);
          }
          _dbHelper.saveOne(claim.toMap());
        });
      }
      var dbClaims = await _dbHelper.findAll(orderBy: "visto ASC,fecha DESC");
      int visto = 0;
      int noVisto = 0;
      dbClaims.forEach((data) {
        var claim = ClaimAPI.fromJson(data);
        claimAPIListResult.add(claim);
        if (claim.visto.toLowerCase() == "visto") {
          visto++;
        } else {
          noVisto++;
        }
      });
      if (noVisto > 0) {
        ShoppingCartInfo.of(context).flagContact = true;
      } else {
        ShoppingCartInfo.of(context).flagContact = false;
      }
      ShoppingCartInfo.update(context);
      if (visto == 0 || noVisto == 0) {
        claimAPIListResult.sort((a, b) => b.fecha.compareTo(a.fecha));
      }
      return claimAPIListResult;
    } catch (e) {
      print(e);
      List<ClaimAPI> claims = new List();
      var dbClaims = await _dbHelper.findAll(orderBy: "visto ASC,fecha DESC");
      dbClaims.forEach((data) => claims.add(ClaimAPI.fromJson(data)));
      return claims;
      //return filterAndOrder(claims);
    }
  }

  List<ClaimAPI> filterAndOrder(List<ClaimAPI> claimAPIListResult) {
    claimAPIListResult.sort((a, b) => b.fecha.compareTo(a.fecha));
    claimAPIListResult.sort((a, b) => a.visto.compareTo(b.visto));
    return claimAPIListResult;
  }

  Future<Map> storeAllData(BuildContext context) async {
    try {
      Map response = {"flag": false};
      var claimAPIList = await ClaimsController().getData(false);
      claimAPIList.forEach((claim) async {
        var dbElement = await _dbHelper.findOne(where: "id ==${claim.id}");
        if (dbElement.length > 0) {
          claim = updateData(dbElement, claim);
          if (dbElement['visto'].toString().toLowerCase() == "no visto") {
            response["flag"] = true;
          }
        }
        _dbHelper.saveOne(claim.toMap());
      });

      response["values"] = true;
      return response;
    } catch (e) {
      print("Error storeAll: $e");
      return {"values": false, "flag": false};
    }
  }

  ClaimAPI updateData(Map<String, dynamic> dbElement, ClaimAPI claim) {
    if (dbElement['visto'].toString().toLowerCase() !=
        claim.visto.toLowerCase()) {
      claim.visto = dbElement['visto'];
      ClaimsController().updateViewField(claim);
    }
    return claim;
  }
}
