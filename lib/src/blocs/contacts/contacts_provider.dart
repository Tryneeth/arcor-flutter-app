import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/utils/provider_interface.dart';

import '../../models/data_connection.dart';
import 'index.dart';

class ContactsProvider extends IProvider<ClaimAPI> {
  static final Future<String> _contactsEP = new DataConnection().getReclamosV2();

  ContactsProvider()
      : super(
          _contactsEP,
          createModelFromJson: (json) =>
              ContactsResponse.fromJson(json).results,
        );
}
