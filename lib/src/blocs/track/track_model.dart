import 'package:tokin/src/models/tracks.dart';

class TrackResponse {
  List<TrackAPI> results;

  TrackResponse({this.results});

  TrackResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      results = new List<TrackAPI>();
      json['data'].forEach((v) {
        results.add(new TrackAPI.fromJson(v));
      });
    }
  }
}
