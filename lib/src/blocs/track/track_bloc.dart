import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class TrackBloc extends Bloc<TrackEvent, TrackState> {
  static final TrackBloc _trackBlocSingleton = new TrackBloc._internal();
  factory TrackBloc() {
    return _trackBlocSingleton;
  }
  TrackBloc._internal();

  TrackState get initialState => new UnTrackState();

  @override
  Stream<TrackState> mapEventToState(
    TrackEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
