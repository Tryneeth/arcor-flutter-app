import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/tracks.dart';

@immutable
abstract class TrackState extends Equatable {
  TrackState([Iterable props]) : super(props);

  /// Copy object for use in action
  TrackState copyWith();
}

/// UnInitialized
class UnTrackState extends TrackState {
  @override
  String toString() => 'UnTrackState';

  @override
  TrackState copyWith() {
    return UnTrackState();
  }
}

/// Initialized
class InTrackState extends TrackState {
  final int version;
  final List<TrackAPI> track;
  final bool hasReachedMaxData;

  InTrackState(this.version, this.track, {this.hasReachedMaxData = false})
      : super([version, track, hasReachedMaxData]);

  @override
  String toString() => 'InTrackState';

  @override
  TrackState copyWith({List<TrackAPI> track, bool hasReachedMaxData, int version}) {
    return InTrackState(version ?? this.version, track ?? this.track,
        hasReachedMaxData: hasReachedMaxData ?? this.hasReachedMaxData);
  }
}

/// On Error
class ErrorTrackState extends TrackState {
  final String errorMessage;

  ErrorTrackState(this.errorMessage);

  @override
  String toString() => 'ErrorTrackState';

  @override
  TrackState copyWith() {
    return ErrorTrackState(this.errorMessage);
  }
}

/// No Data
class NoDataTrackState extends TrackState {
  @override
  String toString() => 'NoDataTrackState';

  @override
  TrackState copyWith() {
    return this;
  }
}
