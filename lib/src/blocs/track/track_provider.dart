import 'package:tokin/src/models/tracks.dart';
import 'package:tokin/src/utils/provider_interface.dart';

import '../../models/data_connection.dart';
import 'index.dart';

class TrackProvider extends IProvider<TrackAPI> {
  static final Future<String> _trackEP = new DataConnection().getNotificationUri();

  TrackProvider()
      : super(
          _trackEP,
          createModelFromJson: (json) => TrackResponse.fromJson(json).results,
        );
}
