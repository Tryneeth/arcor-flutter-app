import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/blocs/track/index.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/home_widgets.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class TrackScreen extends StatefulWidget {
  const TrackScreen({
    Key key,
    @required TrackBloc trackBloc,
  })  : _trackBloc = trackBloc,
        super(key: key);

  final TrackBloc _trackBloc;

  @override
  TrackScreenState createState() {
    return new TrackScreenState(_trackBloc);
  }
}

class TrackScreenState extends State<TrackScreen> {
  final TrackBloc _trackBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  TrackScreenState(this._trackBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _trackBloc.dispatch(LoadTrackEvent(context));
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<TrackBloc, TrackState>(
        bloc: widget._trackBloc,
        builder: (
          BuildContext context,
          TrackState currentState,
        ) {


          if (currentState is UnTrackState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorTrackState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Seguimiento en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataTrackState) {
            return new NoDataWidget(
              message: "No hay datos de Seguimiento en estos momentos.",
            );
          } else if (currentState is InTrackState) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return NotificationListener<ScrollNotification>(
              //onNotification: _handleScroll,
              child: RefreshIndicator(
                onRefresh: () async {
                  widget._trackBloc
                      .dispatch(RefreshTrackEvent(currentState.track));
                  return await _refreshCompleter.future;
                },
                child: ExpandedTrack(tracks: currentState.track),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildListItemLoader() => Center(
        child: CircularProgressIndicator(),
      );

  bool _handleScroll(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        _listScrollCtrl.position.extentAfter == 0)
      _trackBloc.dispatch(LoadMoreTrackEvent());

    return false;
  }

  int _calculateListItemsCount(InTrackState state) {
    if (state.hasReachedMaxData)
      return state.track.length;
    else
      return state.track.length + 1;
  }
}
