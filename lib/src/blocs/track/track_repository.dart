import 'package:tokin/src/controllers/trackController.dart';
import 'package:tokin/src/models/tracks.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

import 'index.dart';

class TrackRepository {
  final TrackProvider _trackProvider = new TrackProvider();
  DBHelper<TrackAPI> _dbHelper;

  TrackRepository() {
    _dbHelper = new DBHelper<TrackAPI>(
        dbTableName: 'track', tableField: TrackAPI.getFieldsForDB());
  }

  Future<List<TrackAPI>> findAll({update: false}) async {
    List<TrackAPI> trackAPIList = new List();
    var dbData;
    try {
      dbData = await _dbHelper.findAll();
    } catch (e) {
      print(e);
    }
    if (dbData.length > 0 && !update) {
      dbData.forEach((data) => trackAPIList.add(TrackAPI.fromJson(data)));
    } else {
      trackAPIList = await TrackController().getData(false);
      trackAPIList.forEach((not) => _dbHelper.saveOne(not.toMap()));
    }
    return trackAPIList;
  }

  Future<bool> storeAllData() async {
    try {
      var trackAPIList = await TrackController().getData(false);
      if (trackAPIList != null && trackAPIList.length > 0) {
        _dbHelper.clean();
      }
      trackAPIList.forEach((track) => _dbHelper.saveOne(track.toMap()));
      return true;
    } catch (e) {
      print("Error storeAll: $e");
      return false;
    }
  }
}
