import 'dart:async';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/models/tracks.dart';

import 'index.dart';

@immutable
abstract class TrackEvent {
  Future<TrackState> applyAsync({TrackState currentState, TrackBloc bloc});

  final TrackRepository _trackProvider = new TrackRepository();
}

class LoadTrackEvent extends TrackEvent {
  final BuildContext context;

  LoadTrackEvent(this.context);

  @override
  String toString() => 'LoadTrackEvent';

  @override
  Future<TrackState> applyAsync({TrackState currentState, TrackBloc bloc}) async {
    try {
      List<TrackAPI> trackAPIList = await _trackProvider.findAll();
      var response = trackAPIList;

      response.sort((tr1, tr2) {
        return tr1.fechaUltimoMovimiento.isAfter(tr2.fechaUltimoMovimiento) ? -1 : 1;
      });

      ShoppingCartInfo.of(context).flagTrack = false;
      ShoppingCartInfo.update(context);

      return response.isEmpty ? NoDataTrackState() : InTrackState(0, response);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorTrackState(_?.toString());
    }
  }
}

class RefreshTrackEvent extends TrackEvent {
  final List<TrackAPI> track;

  RefreshTrackEvent(this.track);

  @override
  String toString() => 'RefreshTrackEvent';

  @override
  Future<TrackState> applyAsync({TrackState currentState, TrackBloc bloc}) async {
    try {
      List<TrackAPI> trackAPIList = await _trackProvider.findAll(update: true);
      var response = trackAPIList;

      response.sort((tr1, tr2) {
        return tr1.fechaUltimoMovimiento.isAfter(tr2.fechaUltimoMovimiento) ? -1 : 1;
      });

      return response.isEmpty ? NoDataTrackState() : InTrackState(1, response);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMoreTrackEvent extends TrackEvent {
  @override
  String toString() => 'LoadMoreTrackEvent';

  @override
  Future<TrackState> applyAsync({TrackState currentState, TrackBloc bloc}) async {
    try {
      // Fetch Next Page
      var response = await this._trackProvider.findAll();

      var tracks =
          response.isEmpty ? (currentState as InTrackState).track : (currentState as InTrackState).track + response;

      tracks.sort((tr1, tr2) {
        return tr1.fechaUltimoMovimiento.isAfter(tr2.fechaUltimoMovimiento) ? -1 : 1;
      });

      return response.isEmpty ? InTrackState(2, tracks, hasReachedMaxData: true) : InTrackState(2, tracks);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}
