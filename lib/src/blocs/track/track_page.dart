import 'package:flutter/material.dart';

import 'index.dart';

class TrackPage extends StatelessWidget {
  static const String routeName = "/track";

  @override
  Widget build(BuildContext context) {
    var _trackBloc = new TrackBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Track"),
      ),
      body: new TrackScreen(trackBloc: _trackBloc),
    );
  }
}
