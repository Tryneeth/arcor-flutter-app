import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  static final LoginBloc _newsBlocSingleton = new LoginBloc._internal();
  factory LoginBloc() {
    return _newsBlocSingleton;
  }
  LoginBloc._internal();

  LoginState get initialState => new UnLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
