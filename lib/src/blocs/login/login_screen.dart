import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/blocs/welcome/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/models/claims.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/models/tracks.dart';
import 'package:tokin/src/ui/home_widgets.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

import 'index.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    Key key,
    @required LoginBloc loginBloc,
  })  : _loginBloc = loginBloc,
        super(key: key);

  final LoginBloc _loginBloc;

  @override
  LoginScreenState createState() {
    return new LoginScreenState(_loginBloc);
  }
}

class LoginScreenState extends State<LoginScreen> {
  final LoginBloc _loginBloc;
  static const offsetVisibleThreshold = 50;

  LoginScreenState(this._loginBloc);

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  void initState() {
    super.initState();
    _loginBloc.dispatch(InitialLoadLoginEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade400,
      body: ConstrainedBox(
        constraints: BoxConstraints.expand(),
        child: BlocBuilder<LoginBloc, LoginState>(
          bloc: widget._loginBloc,
          builder: (
            context,
            LoginState currentState,
          ) {
            if (currentState is UnLoginState) {
              print(currentState);
              widget._loginBloc.dispatch(InitLoginEvent());
              return loading();
            } else if (currentState is SuccessLoginState) {
              return HomepageUI();
            } else if (currentState is LoadingDataLoginState) {
              print(currentState);
              widget._loginBloc.dispatch(LoadDataLoginEvent(context: context));
              return loading();
            } else if (currentState is ErrorLoginState) {
              print(currentState);
              return LoginWidget(
                key: ValueKey(currentState.hashCode),
                user: currentState.user,
                errorMessage: currentState.errorMessage,
              );
            } else if (currentState is InLoginState) {
              print(currentState);
              return LoginWidget(user: currentState.user, errorMessage: "");
            } else
              return Container();
          },
        ),
      ),
    );
  }

  Widget loading() {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.blue.shade400,
        child: Center(
          child: CircularProgressIndicator(backgroundColor: Colors.white),
        ));
  }
}

class LoginWidget extends StatefulWidget {
  final String user;
  final bool inLoginState;
  final String errorMessage;
  LoginWidget({Key key, this.inLoginState, this.user, this.errorMessage})
      : super(key: key);

  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  String pass = "";
  String user = "";
  bool loading;
  String error = "";
  GlobalKey<FormState> key = new GlobalKey();

  @override
  void initState() {
    user = widget.user;
    loading = false;
    error = widget.errorMessage;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Padding(padding: const EdgeInsets.symmetric(vertical: 15)),
                    Image.asset(
                      "assets/ic_tokin.png",
                      height: MediaQuery.of(context).size.height / 3,
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: Form(
                            key: key,
                            child: Column(
                              children: <Widget>[
                                TextFormField(
                                  style: TextStyle(color: Colors.white),
                                  initialValue: user,
                                  decoration: InputDecoration(
                                      labelText: "Usuario",
                                      labelStyle:
                                          TextStyle(color: Colors.white),
                                      border: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2))),
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    var emailExp = RegExp(
                                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                                    if (value == null || value.isEmpty)
                                      return "Campo obligatorio";
                                    else if (!emailExp.hasMatch(value) &&
                                        value.isNotEmpty)
                                      return "Correo inválido";
                                    return null;
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      user = value;
                                    });
                                  },
                                ),
                                TextFormField(
                                  key: ValueKey("passwordField"),
                                  decoration: InputDecoration(
                                      labelText: "Contraseña",
                                      labelStyle:
                                          TextStyle(color: Colors.white),
                                      border: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white)),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2))),
                                  obscureText: true,
                                  keyboardType: TextInputType.number,
                                  onSaved: (value) {
                                    setState(() {
                                      pass = value;
                                    });
                                  },
                                  validator: (value) {
                                    if (value == null || value == "") {
                                      return "Campo obligatorio";
                                    }
                                    return null;
                                  },
                                ),
                                error == null || error == ""
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: Text(
                                          widget.errorMessage,
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 13),
                                        ),
                                      ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      RaisedButton(
                                        onPressed: () async {
                                          await cleanData();
                                          Navigator.of(context).pushReplacement(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      WelcomeScreen(
                                                          welcomeBloc:
                                                              WelcomeBloc())));
                                        },
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        shape: StadiumBorder(),
                                        elevation: 0.0,
                                        color: Colors.grey.shade300,
                                        child: Text("Cancelar"),
                                      ),
                                      RaisedButton(
                                        key: ValueKey("signinButton"),
                                        onPressed: () async {
                                          if (!loading &&
                                              key.currentState.validate()) {
                                            key.currentState.save();
                                            setState(() {
                                              loading = true;
                                              error = "";
                                            });
                                            LoginBloc().dispatch(LoadLoginEvent(
                                                user: user, pass: pass));
                                          }
                                        },
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        shape: StadiumBorder(),
                                        elevation: 0.0,
                                        color: Colors.grey.shade300,
                                        child: loading
                                            ? Container(
                                                height: 30,
                                                width: 30,
                                                child: Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                ),
                                              )
                                            : Text("Ingresar"),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Future<int> cleanData() async {
    try {
      DBHelper<NotificationAPI> _dbHelperNews = new DBHelper<NotificationAPI>(
          dbTableName: 'news', tableField: NotificationAPI.getFieldsForDB());
      var rowsNews = await _dbHelperNews.deleteAll();

      DBHelper<ActivityAPI> _dbHelperActivity = new DBHelper<ActivityAPI>(
          dbTableName: 'activity', tableField: ActivityAPI.getFieldsForDB());
      var rowsActivity = await _dbHelperActivity.deleteAll();

      DBHelper<ClaimAPI> _dbHelperContacts = new DBHelper<ClaimAPI>(
          dbTableName: 'contacts', tableField: ClaimAPI.getFieldsForDB());
      var rowsClaim = await _dbHelperContacts.deleteAll();

      DBHelper<Poll> _dbHelperPoll = new DBHelper<Poll>(
          dbTableName: 'poll', tableField: Poll.getFieldsForDB());
      var rowsPoll = await _dbHelperPoll.deleteAll();

      DBHelper<TrackAPI> _dbHelperTrack = new DBHelper<TrackAPI>(
          dbTableName: 'track', tableField: TrackAPI.getFieldsForDB());
      var rowsTrack = await _dbHelperTrack.deleteAll();

      await SharedPreferencesController.clear();
      ShoppingCartInfo.of(context).order.products = List<ShoppingCartItem>();
      ShoppingCartInfo.of(context).flagCart = false;
      ShoppingCartInfo.of(context).flagContact = false;
      ShoppingCartInfo.of(context).flagNews = false;
      ShoppingCartInfo.of(context).flagPolls = false;
      ShoppingCartInfo.of(context).flagTrack = false;
      ShoppingCartInfo.of(context).controller = null;
      ShoppingCartInfo.update(context);

      print("rowsNews $rowsNews");
      print("rowsActivity $rowsActivity");
      print("rowsClaim $rowsClaim");
      print("rowsPoll $rowsPoll");
      print("rowsTrack $rowsTrack");
      return rowsNews;
    } catch (e) {
      return 0;
    }
  }
}
