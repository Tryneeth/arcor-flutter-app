import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginState extends Equatable {
  LoginState([Iterable props]) : super(props);

  /// Copy object for use in action
  LoginState copyWith();
}

/// UnInitialized
class UnLoginState extends LoginState {
  @override
  String toString() => 'UnLoginState';

  @override
  LoginState copyWith() {
    return UnLoginState();
  }
}

/// Initialized
class InLoginState extends LoginState {
  final String user;
  InLoginState(this.user) : super([user]);

  @override
  String toString() => 'InLoginState';

  @override
  LoginState copyWith() {
    return InLoginState(this.user);
  }
}

/// LoadingDataLoginState
class LoadingDataLoginState extends LoginState {
  @override
  String toString() => 'LoadingDataLoginState';

  @override
  LoginState copyWith() {
    return LoadingDataLoginState();
  }
}

/// On Error
class ErrorLoginState extends LoginState {
  final String user;
  final String errorMessage;
  final int timeStamp;

  ErrorLoginState(this.errorMessage, {this.user, this.timeStamp})
      : super([user, errorMessage, timeStamp]);

  @override
  String toString() => 'ErrorNewsState';

  @override
  LoginState copyWith() {
    return ErrorLoginState(this.errorMessage,
        user: this.user, timeStamp: DateTime.now().millisecondsSinceEpoch);
  }
}

class SuccessLoginState extends LoginState {
  @override
  String toString() => 'SuccessLoginState';

  @override
  LoginState copyWith() {
    return this;
  }
}

class EndLoginState extends LoginState {
  @override
  String toString() => 'EndLoginState';

  @override
  LoginState copyWith() {
    return this;
  }
}
