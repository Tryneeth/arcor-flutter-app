import 'dart:async';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/register.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/controllers/updateAllData.dart';

import 'index.dart';

@immutable
abstract class LoginEvent {
  Future<LoginState> applyAsync({LoginState currentState, LoginBloc bloc});
}

class InitLoginEvent extends LoginEvent {
  @override
  String toString() => 'InitLoginEvent';

  @override
  Future<LoginState> applyAsync(
      {LoginState currentState, LoginBloc bloc}) async {
    String email = await SharedPreferencesController.getPrefEmail();
    try {
      bool keepSession = await SharedPreferencesController.getPrefKeepSession();

      if (keepSession != null) {
        // return keepSession ? SuccessLoginState() : InLoginState(email);
        return keepSession ? LoadingDataLoginState() : InLoginState(email);
      } else {
        return InLoginState(email);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorLoginState(
          "Error en la carga de datos. Compruebe la conexión",
          user: email);
    }
  }
}

class LoadLoginEvent extends LoginEvent {
  final String user;
  final String pass;
  LoadLoginEvent({this.user, this.pass});

  @override
  String toString() => 'LoadLoginEvent';

  @override
  Future<LoginState> applyAsync(
      {LoginState currentState, LoginBloc bloc}) async {
    try {
      var info;
      if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
        info = await DeviceInfoPlugin().iosInfo;
      } else {
        info = await DeviceInfoPlugin().androidInfo;
      }
      String id = info is IosDeviceInfo
          ? "${info.identifierForVendor}"
          : "${(info as AndroidDeviceInfo).androidId}";
      await RegisterController().toValidate(email: user, phoneId: id);
      bool validation = await validateLogin();
      if (validation) {
        await SharedPreferencesController.setPrefPass(pass);
        await SharedPreferencesController.setPrefEmail(user);
        await SharedPreferencesController.setPrefKeepSession(true);
        return LoadingDataLoginState();
      } else {
        if (currentState is ErrorLoginState) {
          return currentState.copyWith();
        }
        return ErrorLoginState("Contraseña incorrecta",
            user: user, timeStamp: DateTime.now().millisecondsSinceEpoch);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }

  Future<bool> validateLogin() async {
    //User process
    var userBytes = utf8.encode(user);
    var shaUser = sha1.convert(userBytes);

    var base64UserHash =
        utf8.fuse(base64).encode(shaUser.toString().toUpperCase());

    var concatenatedUser =
        "K1${base64UserHash.substring(0, base64UserHash.length - 2)}PK";

    //Pass process
    var passBytes = utf8.encode(pass); // data being hashed
    var shaPass = sha1.convert(passBytes);

    var passBytes2 =
        utf8.encode(shaPass.toString().toUpperCase()); // data being hashed
    var shaPass2 = sha1.convert(passBytes2);

    var base64PassHash =
        utf8.fuse(base64).encode(shaPass2.toString().toUpperCase());

    var concatenatedPass =
        "K2${base64PassHash.substring(0, base64PassHash.length - 2)}ZW";

    //Validation zone
    var k1 = await SharedPreferencesController.getPrefk1();
    var k2 = await SharedPreferencesController.getPrefk2();

    if (k1 == concatenatedUser && k2 == concatenatedPass) {
      print("iguales");
      return true;
    } else {
      print("distintos");
      return false;
    }
  }
}

class LoadDataLoginEvent extends LoginEvent {
  BuildContext context;

  LoadDataLoginEvent({@required this.context});
  @override
  String toString() => 'InitLoginEvent';

  @override
  Future<LoginState> applyAsync(
      {LoginState currentState, LoginBloc bloc}) async {
    String email = await SharedPreferencesController.getPrefEmail();
    try {
      await UpdateData().updateAllData(context);
      await SharedPreferencesController.setPrefKeepSession(true);
      return SuccessLoginState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorLoginState(
          "Error en la carga de datos. Compruebe la conexión.",
          user: email);
    }
  }
}

class InitialLoadLoginEvent extends LoginEvent {
  @override
  String toString() => 'InitLoginEvent';

  @override
  Future<LoginState> applyAsync(
      {LoginState currentState, LoginBloc bloc}) async {
    String email = await SharedPreferencesController.getPrefEmail();
    try {
      return UnLoginState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorLoginState(
          "Error en la carga de datos. Compruebe la conexión.",
          user: email);
    }
  }
}
