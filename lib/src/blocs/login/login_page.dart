import 'package:flutter/material.dart';

import 'index.dart';

class LoginPage extends StatelessWidget {
  static const String routeName = "/login";

  @override
  Widget build(BuildContext context) {
    var _loginBloc = new LoginBloc();
    return new Scaffold(      
      body: new LoginScreen(loginBloc: _loginBloc),
    );
  }
}
