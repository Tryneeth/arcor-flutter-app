// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'polls_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Poll _$PollFromJson(Map<String, dynamic> json) {
  return Poll(
    json['id'] as int,
    json['codigo_encuesta'] as String,
    json['nombre'] as String,
    (json['preguntas'] as List)
        ?.map((e) =>
            e == null ? null : PollQuestion.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['puntos'] as int,
    json['activo'] as bool,
  );
}

Map<String, dynamic> _$PollToJson(Poll instance) => <String, dynamic>{
      'id': instance.id,
      'codigo_encuesta': instance.codigo_encuesta,
      'nombre': instance.nombre,
      'preguntas': instance.preguntas,
      'puntos': instance.puntos,
      'activo': instance.activo,
    };

PollQuestion _$PollQuestionFromJson(Map<String, dynamic> json) {
  return PollQuestion(
    json['id'] as int,
    json['codigo_encuesta'] as String,
    json['codigo_pregunta'] as String,
    json['pregunta'] as String,
    json['tipo_pregunta'] as int,
    (json['opciones'] as List)
        ?.map((e) => e == null
            ? null
            : QuestionOptions.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['loc'] as String,
    json['key'] as String,
    json['category'] as String,
    json['figure'] as String,
    json['imagen_pregunta'] as String,
    json['orden_pregunta'] as int,
    json['opciones_aleatorias'] as bool,
    json['obligatoria'] as bool,
    json['valor_minimo'],
    json['valor_maximo'],
    json['activo'] as bool,
    json['fecha_minima'],
    json['fecha_maxima'],
    json['pregunta_precio'] as bool,
    json['cantidad_estrellas'],
    json['valor_interno'],
    json['codigo_articulo'],
    json['grupo_pregunta'],
    json['tipo_presencia'] as bool,
    json['puntos'],
    json['accion'],
    json['foto_control'] as bool,
    json['foto_novedad'] as bool,
    json['pregunta_ad_hoc'] as bool,
  );
}

Map<String, dynamic> _$PollQuestionToJson(PollQuestion instance) =>
    <String, dynamic>{
      'id': instance.id,
      'codigo_encuesta': instance.codigo_encuesta,
      'codigo_pregunta': instance.codigo_pregunta,
      'pregunta': instance.pregunta,
      'tipo_pregunta': instance.tipo_pregunta,
      'opciones': instance.opciones,
      'loc': instance.loc,
      'key': instance.key,
      'category': instance.category,
      'figure': instance.figure,
      'imagen_pregunta': instance.imagen_pregunta,
      'orden_pregunta': instance.orden_pregunta,
      'opciones_aleatorias': instance.opciones_aleatorias,
      'obligatoria': instance.obligatoria,
      'valor_minimo': instance.valor_minimo,
      'valor_maximo': instance.valor_maximo,
      'activo': instance.activo,
      'fecha_minima': instance.fecha_minima,
      'fecha_maxima': instance.fecha_maxima,
      'pregunta_precio': instance.pregunta_precio,
      'cantidad_estrellas': instance.cantidad_estrellas,
      'valor_interno': instance.valor_interno,
      'codigo_articulo': instance.codigo_articulo,
      'grupo_pregunta': instance.grupo_pregunta,
      'tipo_presencia': instance.tipo_presencia,
      'puntos': instance.puntos,
      'accion': instance.accion,
      'foto_control': instance.foto_control,
      'foto_novedad': instance.foto_novedad,
      'pregunta_ad_hoc': instance.pregunta_ad_hoc,
    };

QuestionOptions _$QuestionOptionsFromJson(Map<String, dynamic> json) {
  return QuestionOptions(
    json['id'] as int,
    json['codigo_pregunta'] as String,
    json['opcion'] as String,
    json['activo'] as bool,
  );
}

Map<String, dynamic> _$QuestionOptionsToJson(QuestionOptions instance) =>
    <String, dynamic>{
      'id': instance.id,
      'codigo_pregunta': instance.codigo_pregunta,
      'opcion': instance.opcion,
      'activo': instance.activo,
    };

RelievePlan _$RelievePlanFromJson(Map<String, dynamic> json) {
  return RelievePlan(
    json['id'] as int,
    json['codigo_plan'] as String,
    json['nombre'] as String,
    json['codigo_encuesta'] as String,
    json['id_canal_arcor'] as int,
    json['fecha_desde'] as String,
    json['fecha_hasta'] as String,
    json['progreso_revelamiento'] as int,
    json['tipo_asignacion_auditores'] as int,
    json['publicacion'] as int,
    json['rotacion_encuestas'] as int,
    json['rotacion_aleatoria'] as bool,
    json['propetario'] as int,
  );
}

Map<String, dynamic> _$RelievePlanToJson(RelievePlan instance) =>
    <String, dynamic>{
      'id': instance.id,
      'codigo_plan': instance.codigo_plan,
      'nombre': instance.nombre,
      'codigo_encuesta': instance.codigo_encuesta,
      'id_canal_arcor': instance.id_canal_arcor,
      'fecha_desde': instance.fecha_desde,
      'fecha_hasta': instance.fecha_hasta,
      'progreso_revelamiento': instance.progreso_revelamiento,
      'tipo_asignacion_auditores': instance.tipo_asignacion_auditores,
      'publicacion': instance.publicacion,
      'rotacion_encuestas': instance.rotacion_encuestas,
      'rotacion_aleatoria': instance.rotacion_aleatoria,
      'propetario': instance.propetario,
    };

PointsRelievePlan _$PointsRelievePlanFromJson(Map<String, dynamic> json) {
  return PointsRelievePlan(
    json['id'] as int,
    json['codigo_plan'] as String,
    json['codigo_punto'] as String,
    json['codigo_distribuidor'] as String,
    json['codigo_ruta_cliente'] as String,
    json['secuencia_cliente'] as String,
  );
}

Map<String, dynamic> _$PointsRelievePlanToJson(PointsRelievePlan instance) =>
    <String, dynamic>{
      'id': instance.id,
      'codigo_plan': instance.codigo_plan,
      'codigo_punto': instance.codigo_punto,
      'codigo_distribuidor': instance.codigo_distribuidor,
      'codigo_ruta_cliente': instance.codigo_ruta_cliente,
      'secuencia_cliente': instance.secuencia_cliente,
    };

PollAction _$PollActionFromJson(Map<String, dynamic> json) {
  return PollAction(
    json['id'] as int,
    json['id_encuesta'] as String,
    json['id_form'] as String,
    json['tiempo'] as int,
    json['orden'] as String,
    json['codigo'] as int,
    json['id_enlace'] as String,
    json['var1'] as String,
    json['tipo1'] as String,
  );
}

Map<String, dynamic> _$PollActionToJson(PollAction instance) =>
    <String, dynamic>{
      'id': instance.id,
      'id_encuesta': instance.id_encuesta,
      'id_form': instance.id_form,
      'tiempo': instance.tiempo,
      'orden': instance.orden,
      'codigo': instance.codigo,
      'id_enlace': instance.id_enlace,
      'var1': instance.var1,
      'tipo1': instance.tipo1,
    };
