import 'dart:async';

import 'package:meta/meta.dart';
import 'package:tokin/src/models/poll.dart';

import 'index.dart';

@immutable
abstract class PollsEvent {
  Future<PollsState> applyAsync({PollsState currentState, PollsBloc bloc});

  final PollsRepository _pollsProvider = new PollsRepository();
}

class LoadPollsEvent extends PollsEvent {
  @override
  String toString() => 'LoadPollsEvent';

  @override
  Future<PollsState> applyAsync(
      {PollsState currentState, PollsBloc bloc}) async {
    try {
      var response = await _pollsProvider.findAll();
      List<Poll> pollApiList = response["polls"];
      List<PollAction> pollActionsList = response["pollActions"];
      return response.isEmpty
          ? NoDataPollsState()
          : InPollsState(pollApiList, pollActionsList);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorPollsState(_?.toString());
    }
  }
}

class RefreshPollsEvent extends PollsEvent {
  final List<PollAPI> polls;

  RefreshPollsEvent(this.polls);

  @override
  String toString() => 'RefreshPollsEvent';

  @override
  Future<PollsState> applyAsync(
      {PollsState currentState, PollsBloc bloc}) async {
    try {
      var response = await _pollsProvider.findAll(update: true);
      List<Poll> pollApiList = response["polls"];
      List<PollAction> pollActionsList = response["pollActions"];

      return response.isEmpty
          ? NoDataPollsState()
          : InPollsState(pollApiList, pollActionsList);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMorePollsEvent extends PollsEvent {
  @override
  String toString() => 'LoadMorePollsEvent';

  @override
  Future<PollsState> applyAsync(
      {PollsState currentState, PollsBloc bloc}) async {
    try {
      // Fetch Next Page
      var response = await _pollsProvider.findAll(update: true);
      List<Poll> pollApiList = response["polls"];
      List<PollAction> pollActionsList = response["pollActions"];
      return response.isEmpty
          ? InPollsState((currentState as InPollsState).polls,
              (currentState as InPollsState).pollActions,
              hasReachedMaxData: true)
          : InPollsState((currentState as InPollsState).polls + pollApiList,
              (currentState as InPollsState).pollActions);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}
