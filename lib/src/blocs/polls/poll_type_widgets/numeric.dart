import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/validators.dart';

import '../polls_model.dart';

class QNumeric extends StatefulWidget {
  final PollQuestion question;

  const QNumeric({Key key, this.question}) : super(key: key);

  @override
  _QNumericState createState() => _QNumericState();
}

class _QNumericState extends State<QNumeric> {
  String numericValue;
  var _ctrl = TextEditingController();

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        numericValue = pref.getString(widget.question.id.toString());
        setState(() {
          _ctrl.text = numericValue;
        });
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(widget.question.id.toString(), numericValue),
    );
  }

  @override
  void initState() {
    _ctrl.addListener(() {
      numericValue = _ctrl.text;
      _saveAnswer();
    });
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextFormField(
          autovalidate: true,
          controller: _ctrl,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
              focusColor: Colors.cyan, border: UnderlineInputBorder()),
          validator: (num) => num != null && num != ""
              ? Validators.numberRange(
                  num,
                  widget.question.valor_minimo != null
                      ? double.tryParse(widget.question.valor_minimo)
                      : null,
                  widget.question.valor_maximo != null
                      ? double.tryParse(widget.question.valor_maximo)
                      : null)
              : null),
    );
  }
}
