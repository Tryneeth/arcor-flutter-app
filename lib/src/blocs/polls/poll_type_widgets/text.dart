import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/blocs/polls/validators.dart';

class QText extends StatefulWidget {
  final PollQuestion question;
  final VoidCallback onAnswer;

  const QText({Key key, this.question, this.onAnswer}) : super(key: key);

  @override
  _QTextState createState() => _QTextState();
}

class _QTextState extends State<QText> {
  String textValue;
  var _ctrl = TextEditingController();
  var _focusNode = FocusNode();

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        textValue = pref.getString(widget.question.id.toString());
        setState(() {
          _ctrl.text = textValue;
        });
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(widget.question.id.toString(), textValue),
    );
  }

  @override
  void initState() {
    _ctrl.addListener(() {
      textValue = _ctrl.text;
      _saveAnswer();
    });
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(20.0),
        child: TextFormField(
          autovalidate: true,
          focusNode: _focusNode,
          controller: _ctrl,
          minLines: 1,
          maxLines: 5,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
              focusColor: Colors.cyan, border: UnderlineInputBorder()),
          validator: (text) => Validators.wordCount(text),
        ));
  }
}
