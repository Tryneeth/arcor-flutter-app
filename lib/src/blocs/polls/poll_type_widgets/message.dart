import 'package:flutter/material.dart';

import '../polls_model.dart';

class QMessage extends StatefulWidget {
  final PollQuestion question;

  const QMessage({Key key, this.question}) : super(key: key);

  @override
  _QMessageState createState() => _QMessageState();
}

class _QMessageState extends State<QMessage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
