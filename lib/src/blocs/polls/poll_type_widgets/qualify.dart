import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../polls_model.dart';

class QQualify extends StatefulWidget {
  final PollQuestion question;
  final VoidCallback onAnswer;

  const QQualify({Key key, this.question, this.onAnswer}) : super(key: key);

  @override
  _QQualifyState createState() => _QQualifyState();
}

class _QQualifyState extends State<QQualify> {
  int qualify = 0;

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        qualify = pref.getInt(widget.question.id.toString()) ?? 0;
        setState(() {});
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setInt(widget.question.id.toString(), qualify),
    );
    widget.onAnswer();
  }

  @override
  void initState() {
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: IconTheme(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  qualify = 1;
                  _saveAnswer();
                });
              },
              child: Icon(qualify >= 1 ? Icons.star : Icons.star_border),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  qualify = 2;
                  _saveAnswer();
                });
              },
              child: Icon(qualify >= 2 ? Icons.star : Icons.star_border),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  qualify = 3;
                  _saveAnswer();
                });
              },
              child: Icon(qualify >= 3 ? Icons.star : Icons.star_border),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  qualify = 4;
                  _saveAnswer();
                });
              },
              child: Icon(qualify >= 4 ? Icons.star : Icons.star_border),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  qualify = 5;
                  _saveAnswer();
                });
              },
              child: Icon(qualify >= 5 ? Icons.star : Icons.star_border),
            ),
          ],
        ),
        data: IconThemeData(size: 40, color: Colors.blue),
      ),
    );
  }
}
