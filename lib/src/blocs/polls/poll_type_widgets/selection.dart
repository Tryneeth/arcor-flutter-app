import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/index.dart';

class QSelection extends StatefulWidget {
  final PollQuestion question;
  final VoidCallback onAnswer;

  const QSelection({Key key, this.question, this.onAnswer}) : super(key: key);

  @override
  _QSelectionState createState() => _QSelectionState();
}

class _QSelectionState extends State<QSelection> {
  List<Widget> options;
  String radioValue;

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        radioValue = pref.getString(widget.question.id.toString());
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(widget.question.id.toString(), radioValue),
    );
    widget.onAnswer();
  }

  @override
  void initState() {
    options = new List<Widget>();

    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    options.clear();
    widget.question.opciones.forEach(
      (qo) => options.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Radio(
                value: qo.opcion,
                groupValue: radioValue,
                onChanged: (value) {
                  setState(() {
                    radioValue = value;
                  });
                  _saveAnswer();
                }),
            Container(
              width: MediaQuery.of(context).size.width / 2,
              child: Text(
                qo.opcion,
                style: Theme.of(context)
                    .primaryTextTheme
                    .title
                    .copyWith(color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[...options],
      ),
    );
  }
}
