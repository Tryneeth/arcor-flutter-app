import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../polls_model.dart';

class QDate extends StatefulWidget {
  final PollQuestion question;

  const QDate({Key key, this.question}) : super(key: key);

  @override
  _QDateState createState() => _QDateState();
}

class _QDateState extends State<QDate> {
  String textValue;
  var _ctrl = TextEditingController();

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        textValue = pref.getString(widget.question.id.toString());
        setState(() {
          _ctrl.text = textValue;
        });
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(widget.question.id.toString(), textValue),
    );
  }

  @override
  void initState() {
    _ctrl.addListener(() {
      textValue = _ctrl.text;
      _saveAnswer();
    });
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            TextField(
              //autovalidate: true,
              readOnly: true,
              controller: _ctrl,
              onTap: () => showDatePicker(
                      context: context,
                      firstDate: DateTime.parse(widget.question.fecha_minima),
                      lastDate: DateTime.parse(widget.question.fecha_maxima),
                      initialDate: DateTime.parse(widget.question.fecha_maxima))
                  .then((value) => _ctrl.text =
                      DateFormat("dd/MM/yyyy").format(value).toString()),
              minLines: 1,
              maxLines: 5,
              decoration: InputDecoration(
                  focusColor: Colors.cyan, border: UnderlineInputBorder()),
            ),
          ],
        ));
  }
}
