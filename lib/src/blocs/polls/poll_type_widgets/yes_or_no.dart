import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../polls_model.dart';

class QYesOrNo extends StatefulWidget {
  final PollQuestion question;
  final answer;
  final VoidCallback onAnswer;
  final VoidCallback positiveAnswer;
  final VoidCallback negativeAnswer;

  const QYesOrNo(
      {Key key,
      @required this.question,
      this.answer,
      this.onAnswer,
      this.positiveAnswer,
      this.negativeAnswer})
      : super(key: key);

  @override
  _QYesOrNoState createState() => _QYesOrNoState();
}

class _QYesOrNoState extends State<QYesOrNo> {
  String radioValue;

  @override
  void initState() {
    //_loadAnswer();
    super.initState();
  }

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        radioValue = pref.getString(widget.question.id.toString());
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(widget.question.id.toString(), radioValue),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Radio(
                  value: "yes",
                  groupValue: radioValue,
                  onChanged: (value) {
                    setState(() {
                      radioValue = value;
                      _saveAnswer();
                      widget.positiveAnswer();
                    });
                  }),
              Text(
                "Si",
                style: Theme.of(context)
                    .primaryTextTheme
                    .title
                    .copyWith(color: Colors.black),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Radio(
                  value: "no",
                  groupValue: radioValue,
                  onChanged: (value) {
                    setState(() {
                      radioValue = value;
                      _saveAnswer();
                      widget.negativeAnswer();
                    });
                  }),
              Text(
                "No",
                style: Theme.of(context)
                    .primaryTextTheme
                    .title
                    .copyWith(color: Colors.black),
              )
            ],
          ),
        ],
      ),
    );
  }
}
