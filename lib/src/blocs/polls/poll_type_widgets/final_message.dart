import 'package:flutter/material.dart';

import '../polls_model.dart';

class QFinalMessage extends StatefulWidget {
  final PollQuestion question;

  const QFinalMessage({Key key, this.question}) : super(key: key);

  @override
  _QFinalMessageState createState() => _QFinalMessageState();
}

class _QFinalMessageState extends State<QFinalMessage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
