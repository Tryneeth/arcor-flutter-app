import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../polls_model.dart';

class QMultipleSelection extends StatefulWidget {
  final PollQuestion question;
  final VoidCallback onAnswer;

  const QMultipleSelection({Key key, this.question, this.onAnswer})
      : super(key: key);

  @override
  _QMultipleSelectionState createState() => _QMultipleSelectionState();
}

class _QMultipleSelectionState extends State<QMultipleSelection> {
  List<Widget> options;
  List<bool> selectedStates;

  _loadAnswer() {
    List<String> status = new List();
    SharedPreferences.getInstance().then((pref) {
      status = pref.getStringList(widget.question.id.toString());
      for (int i = 0; i < widget.question.opciones.length; i++) {
        selectedStates[i] = status[i] == "true";
      }
      setState(() {});
    });
  }

  _saveAnswer() {
    List<String> status = new List();
    SharedPreferences.getInstance().then(
      (pref) {
        for (int i = 0; i < widget.question.opciones.length; i++) {
          status.add(selectedStates[i].toString());
          pref.setStringList(widget.question.id.toString(), status);
        }
      },
    );
  }

  @override
  void initState() {
    options = new List<Widget>();
    selectedStates =
        List<bool>.generate(widget.question.opciones.length, (index) => false);
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    options.clear();
    for (int i = 0; i < widget.question.opciones.length; i++) {
      options.add(Row(
        children: <Widget>[
          Checkbox(
              value: selectedStates[i],
              onChanged: (value) {
                setState(() {
                  selectedStates[i] = value;
                  _saveAnswer();
                });
              }),
          Text(
            widget.question.opciones[i].opcion,
            style: Theme.of(context)
                .primaryTextTheme
                .title
                .copyWith(color: Colors.black),
          ),
        ],
      ));
    }
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[...options],
      ),
    );
  }
}
