import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../polls_model.dart';

class QPicture extends StatefulWidget {
  final PollQuestion question;

  const QPicture({Key key, this.question}) : super(key: key);

  @override
  _QPictureState createState() => _QPictureState();
}

class _QPictureState extends State<QPicture> {
  var imageSelected;

  String path;

  _loadAnswer() {
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        path = pref.getString(widget.question.id.toString());
        if (path != null) imageSelected = File(path);
      });
    });
  }

  _saveAnswer() {
    SharedPreferences.getInstance().then(
      (pref) {
        pref.setString(widget.question.id.toString(), path);
      },
    );
  }

  _cleanPhoto() {
    SharedPreferences.getInstance().then(
      (pref) {
        pref.remove(widget.question.id.toString());
      },
    );
    setState(() => imageSelected = null);
  }

  @override
  void initState() {
    _loadAnswer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              color: Colors.deepOrange,
              onPressed: imageSelected == null
                  ? () async {
                      var image = await ImagePicker.pickImage(
                          source: ImageSource.camera);
                      setState(() {
                        imageSelected = image;
                      });
                      path = image.path;
                      _saveAnswer();
                    }
                  : null,
              shape: StadiumBorder(),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                child: Text(
                  "Tomar foto",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            RaisedButton(
              color: Colors.deepOrange,
              onPressed: imageSelected != null ? () => _cleanPhoto() : null,
              shape: StadiumBorder(),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                child: Text(
                  "Cancelar",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 150,
              width: 150,
              margin: EdgeInsets.all(20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(18),
                child: imageSelected != null
                    ? Image.file(
                        imageSelected,
                        fit: BoxFit.cover,
                      )
                    : Stack(
                        fit: StackFit.expand,
                        alignment: Alignment.center,
                        children: [
                            Icon(
                              Icons.image,
                              color: Colors.grey,
                              size: 100,
                            ),
                            Positioned(
                                bottom: 10,
                                child: Text(
                                  "Imagen no\ndisponible",
                                  textAlign: TextAlign.center,
                                  style:
                                      Theme.of(context).primaryTextTheme.title,
                                ))
                          ]),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.blue, width: 2)),
            )
          ],
        )
      ],
    );
  }
}
