import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class PollsStartScreen extends StatefulWidget {
  const PollsStartScreen({
    Key key,
    @required PollsBloc pollsBloc,
  })  : _pollsBloc = pollsBloc,
        super(key: key);

  final PollsBloc _pollsBloc;

  @override
  PollsStartScreenState createState() {
    return new PollsStartScreenState(_pollsBloc);
  }
}

class PollsStartScreenState extends State<PollsStartScreen> {
  final PollsBloc _pollsBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  PollsStartScreenState(this._pollsBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _pollsBloc.dispatch(LoadPollsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(),
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<PollsBloc, PollsState>(
        bloc: widget._pollsBloc,
        builder: (
          BuildContext context,
          PollsState currentState,
        ) {
          if (currentState is UnPollsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorPollsState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Encuesta en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataPollsState) {
            return new NoDataWidget(
              message: "No hay datos de Encuesta en estos momentos.",
            );
          } else if (currentState is InPollsState) {
            return pollsListView(currentState.polls);
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildListItemLoader() => Center(
        child: CircularProgressIndicator(),
      );

  bool _handleScroll(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        _listScrollCtrl.position.extentAfter == 0)
      _pollsBloc.dispatch(LoadMorePollsEvent());

    return false;
  }

  int _calculateListItemsCount(InPollsState state) {
    if (state.hasReachedMaxData)
      return state.polls.length;
    else
      return state.polls.length + 1;
  }

  checkDoneSurveys(List<Poll> polls) {
    SharedPreferences.getInstance().then((pref) {
      var done = pref.getStringList('surveys-done') ?? [];
      if (done.isNotEmpty)
        polls.removeWhere((poll) => done.indexOf(poll.id.toString()) != -1);
    });
  }

  pollsListView(List polls) {
    checkDoneSurveys(polls);

    return Column(
      children: <Widget>[
        Container(
          constraints: BoxConstraints.expand(height: 120),
          color: Colors.deepOrange,
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              "Tenés ${polls.length} encuestas pendientes.",
              style: Theme.of(context)
                  .primaryTextTheme
                  .title
                  .copyWith(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 50.0, bottom: 20),
                child: Text(
                  "Tomate unos minutos\npara responder.",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).primaryTextTheme.title.copyWith(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                ),
              ),
              RaisedButton(
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => PollsPage(),
                      settings: RouteSettings(name: "PollsList"))),
                  shape: StadiumBorder(),
                  elevation: 1.0,
                  color: Colors.deepOrange,
                  child: Text(
                    "Ingresar",
                    style: TextStyle(color: Colors.white),
                  )),
            ],
          ),
        )
      ],
    );
  }
}
