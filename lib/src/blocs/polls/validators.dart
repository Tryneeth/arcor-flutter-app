class Validators {
  static String numberRange(String number, double min, double max) {
    if (number != null && number != "" && min != null && max != null) {
      var num = number.contains(RegExp(r'\.{1}'))
          ? double.tryParse(number)
          : int.parse(number).toDouble();
      if (num == null)
        return "Error en el formulario";
      else if (num < min || num > max)
        return "Valor fuera de Rango $min a $max";
    } else {
      return null;
    }
    return null;
  }

  static String wordCount(String words) => null;
}
