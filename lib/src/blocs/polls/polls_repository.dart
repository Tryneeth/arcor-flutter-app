import 'package:flutter/material.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

import 'index.dart';

class PollsRepository {
  final PollsProvider _pollsRepository = new PollsProvider();
  final PollActionsProvider _pollActionsProvider = new PollActionsProvider();
  final RelievePlansProvider _relievePlansRepository =
      new RelievePlansProvider();
  final PointsRelievePlansProvider _pointsRelievePlansProvider =
      new PointsRelievePlansProvider();
  final PollsSendProvider _pollsSendProvider = new PollsSendProvider();
  DBHelper<Poll> _dbHelper;

  PollsRepository() {
    _dbHelper = new DBHelper<Poll>(
        dbTableName: 'poll', tableField: Poll.getFieldsForDB());
  }

  Future<Map> findAll({update: false, BuildContext context}) async {
    var clientCode = await SharedPreferencesController.getPreffullClientCode();

    var _data = {
      "modulo_code": "ArcorGev5PDVApp",
      "client_code": clientCode,
      "distributor_code":
          int.parse(await SharedPreferencesController.getPrefDistrib()),
      "auditor_code": "PDV",
    };

    var polls = await _pollsRepository.findAllAsGet(data: _data);
    var revealsPlans = await _relievePlansRepository.findAllAsGet(data: _data);

    var pollActions = await _pollActionsProvider.findAllAsGet(data: _data);

    /// Change Name of Poll by Reveal Plan's Name
    try {
      polls.forEach((Poll poll) {
        var rvPlan = revealsPlans.firstWhere(
            (RelievePlan plan) =>
                plan?.codigo_encuesta == poll?.codigo_encuesta,
            orElse: () => null);
        if (rvPlan != null) poll.changeNameByPlan(rvPlan.nombre);
      });
    } catch (e) {
      print(e);
    }
    var newData = false;
    if (context != null && polls.length > 0) newData = true;
    return {"polls": polls, "pollActions": pollActions, "flag": newData};
  }

  Future<bool> sendData(List<Map> answers) async {
    return await _pollsSendProvider.create(answers);
  }
}
