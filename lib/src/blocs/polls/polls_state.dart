import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/models/poll.dart';

@immutable
abstract class PollsState extends Equatable {
  PollsState([Iterable props]) : super(props);

  /// Copy object for use in pollAction
  PollsState copyWith();
}

/// UnInitialized
class UnPollsState extends PollsState {
  @override
  String toString() => 'UnPollsState';

  @override
  PollsState copyWith() {
    return UnPollsState();
  }
}

/// Initialized
class InPollsState extends PollsState {
  final List<Poll> polls;
  final List<PollAction> pollActions;
  final bool hasReachedMaxData;

  InPollsState(this.polls, this.pollActions, {this.hasReachedMaxData = false})
      : super([polls, hasReachedMaxData]);

  @override
  String toString() => 'InPollsState';

  @override
  PollsState copyWith(
      {List<PollAPI> polls,
      bool hasReachedMaxData,
      List<PollAction> pollActions}) {
    return InPollsState(polls ?? this.polls, pollActions ?? this.pollActions,
        hasReachedMaxData: hasReachedMaxData ?? this.hasReachedMaxData);
  }
}

/// On Error
class ErrorPollsState extends PollsState {
  final String errorMessage;

  ErrorPollsState(this.errorMessage);

  @override
  String toString() => 'ErrorPollsState';

  @override
  PollsState copyWith() {
    return ErrorPollsState(this.errorMessage);
  }
}

/// No Data
class NoDataPollsState extends PollsState {
  @override
  String toString() => 'NoDataPollsState';

  @override
  PollsState copyWith() {
    return this;
  }
}
