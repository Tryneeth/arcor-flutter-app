import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class PollsBloc extends Bloc<PollsEvent, PollsState> {
  static final PollsBloc _pollsBlocSingleton = new PollsBloc._internal();
  factory PollsBloc() {
    return _pollsBlocSingleton;
  }
  PollsBloc._internal();

  PollsState get initialState => new UnPollsState();

  @override
  Stream<PollsState> mapEventToState(
    PollsEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
