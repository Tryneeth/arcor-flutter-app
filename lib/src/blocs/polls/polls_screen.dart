import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/index.dart';
import 'package:tokin/src/blocs/polls/single_poll.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class PollsScreen extends StatefulWidget {
  const PollsScreen({
    Key key,
    @required PollsBloc pollsBloc,
  })  : _pollsBloc = pollsBloc,
        super(key: key);

  final PollsBloc _pollsBloc;

  @override
  PollsScreenState createState() {
    return new PollsScreenState(_pollsBloc);
  }
}

class PollsScreenState extends State<PollsScreen> {
  final PollsBloc _pollsBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  PollsScreenState(this._pollsBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _pollsBloc.dispatch(LoadPollsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.grey.shade200, Colors.grey],
        ),
      ),
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<PollsBloc, PollsState>(
        bloc: widget._pollsBloc,
        builder: (
          BuildContext context,
          PollsState currentState,
        ) {
          if (currentState is UnPollsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorPollsState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Encuestas en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataPollsState) {
            return new NoDataWidget(
              message: "No hay datos de Encuestas en estos momentos.",
            );
          } else if (currentState is InPollsState) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return pollsListView(currentState.polls, currentState.pollActions);
          }
          return Container();
        },
      ),
    );
  }

  checkDoneSurveys(List<Poll> polls) {
    SharedPreferences.getInstance().then((pref) {
      var done = pref.getStringList('surveys-done') ?? [];
      if (done.isNotEmpty)
        polls.removeWhere((poll) => done.indexOf(poll.id.toString()) != -1);
    });
  }

  pollsListView(List<Poll> polls, List<PollAction> actions) {
    checkDoneSurveys(polls);
    return ListView(
      children: <Widget>[
        Container(
          constraints: BoxConstraints.expand(height: 120),
          color: Colors.deepOrange,
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              "Gracias por tomarte unos minutos para contestar las siguientes encuestas.",
              style: Theme.of(context)
                  .primaryTextTheme
                  .title
                  .copyWith(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          itemBuilder: (context, index) => ListTile(
            title: ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: Text("Confirmar"),
                          content: Text(
                              "¿Quiere continuar con el relevamiento de la encuesta?"),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                              child: Text("Cancelar".toUpperCase()),
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                              child: Text("Continuar".toUpperCase()),
                            )
                          ],
                        )).then((val) async {
                  if (val) {
                    var sp = await SharedPreferences.getInstance();
                    var current = sp.getInt("${polls[index].id}-current") ?? 0;
                    var previous =
                        sp.getInt("${polls[index].id}-previous") ?? -1;
                    current = current < 0 ? 0 : current;
                    previous = previous < -1 ? -1 : previous;
                    Navigator.of(context).push(
                      CupertinoPageRoute(
                        title: polls[index].nombre,
                        builder: (context) => SinglePoll(
                          poll: polls[index],
                          currentQuestion: current,
                          previousQuestion: previous,
                          pollActions: actions
                              .where((pollAction) =>
                                  pollAction.id_encuesta ==
                                  polls[index].codigo_encuesta)
                              .toList(),
                        ),
                      ),
                    );
                  }
                });
              },
              leading: Image.asset(
                "assets/survey_pdv.png",
                height: 50,
              ),
              title: Text(
                "${polls[index].nombre}",
              ),
            ),
          ),
          itemCount: polls.length,
        ),
      ],
    );
  }
}

class SlideTransitionRoute extends PageRouteBuilder {
  final Widget enterPage;
  final Widget exitPage;

  SlideTransitionRoute({this.enterPage, this.exitPage})
      : super(
            pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) =>
                enterPage,
            transitionsBuilder: (BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child) =>
                Stack(
                  children: <Widget>[
                    SlideTransition(
                      position: new Tween<Offset>(
                              begin: const Offset(0.0, 0.0),
                              end: const Offset(-1.0, 0.0))
                          .animate(animation),
                      child: exitPage,
                    ),
                    SlideTransition(
                      position: new Tween<Offset>(
                              begin: const Offset(1.0, 0.0), end: Offset.zero)
                          .animate(animation),
                      child: enterPage,
                    )
                  ],
                ));
}
