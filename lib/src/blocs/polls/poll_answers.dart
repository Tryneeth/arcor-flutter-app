import 'package:flutter/material.dart';

class InheritedAnswers {
  final List<dynamic> answers;

  const InheritedAnswers({this.answers = const []});

  @override
  bool operator ==(other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    final InheritedAnswers otherAnswers = other;
    return otherAnswers.answers == answers;
  }

  @override
  int get hashCode => answers.hashCode;

  static InheritedAnswers of(BuildContext context) {
    final _AnswersBindingScope scope =
        context.inheritFromWidgetOfExactType(_AnswersBindingScope);
    return scope.answersBindingState.currentAnswers;
  }

  static void update(BuildContext context, InheritedAnswers newAnswers) {
    final _AnswersBindingScope scope =
        context.inheritFromWidgetOfExactType(_AnswersBindingScope);
    scope.answersBindingState.updateAnswers(newAnswers);
  }
}

class _AnswersBindingScope extends InheritedWidget {
  final _AnswersBindingState answersBindingState;

  const _AnswersBindingScope({
    Key key,
    this.answersBindingState,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_AnswersBindingScope old) {
    return true;
  }
}

class AnswersBinding extends StatefulWidget {
  final InheritedAnswers initialAnswers;
  final Widget child;

  const AnswersBinding(
      {Key key, this.initialAnswers = const InheritedAnswers(), this.child})
      : super(key: key);

  @override
  _AnswersBindingState createState() => _AnswersBindingState();
}

class _AnswersBindingState extends State<AnswersBinding> {
  InheritedAnswers currentAnswers;

  @override
  void initState() {
    super.initState();
    currentAnswers = widget.initialAnswers;
  }

  void updateAnswers(InheritedAnswers newAnswers) {
    if (newAnswers != currentAnswers) {
      setState(() {
        currentAnswers = newAnswers;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _AnswersBindingScope(
      answersBindingState: this,
      child: widget.child,
    );
  }
}
