import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tokin/src/utils/provider_interface.dart';

import 'index.dart';

class PollsProvider extends IProvider<Poll> {
  static final String _pollsEP =
      "http://arcor-celsius-backend-production.us-west-2.elasticbeanstalk.com/api_mobile/encuestas_by_auditor/";

  static final String pollImageUrl =
      "https://s3-us-west-2.amazonaws.com/files.preproduction.arcorcelsius.com/question/";

  static getImageUrl(String resource) {
    return pollImageUrl + resource;
  }

  PollsProvider()
      : super(
          _pollsEP,
          createModelFromJson: (json) => PollsResponse.fromJson(json).results,
        );
}

class RelievePlansProvider extends IProvider<RelievePlan> {
  static final String _relievePlanEP =
      "http://arcor-celsius-backend-preproduction.us-west-2.elasticbeanstalk.com/api_mobile/planes_de_relevamiento_by_auditor/";

  RelievePlansProvider()
      : super(_relievePlanEP,
            createModelFromJson: (json) =>
                RelievePlansResponse.fromJson(json).results);
}

class PointsRelievePlansProvider extends IProvider<PointsRelievePlan> {
  static final String _pointsRelievePlanEP =
      "http://arcor-celsius-backend-preproduction.us-west-2.elasticbeanstalk.com/api_mobile/puntos_plan_by_auditor/";

  PointsRelievePlansProvider()
      : super(_pointsRelievePlanEP,
            createModelFromJson: (json) =>
                PointsRelievePlansResponse.fromJson(json).results);
}

class PollActionsProvider extends IProvider<PollAction> {
  static final String _pollActionsEP =
      "http://arcor-celsius-backend-preproduction.us-west-2.elasticbeanstalk.com/api_mobile/acciones/";

  PollActionsProvider()
      : super(_pollActionsEP,
            createModelFromJson: (json) =>
                PollActionsResponse.fromJson(json).results);
}

class PollsSendProvider {
  static final String _pollsEP =
      "http://arcor-celsius-backend-preproduction.us-west-2.elasticbeanstalk.com/api_mobile/relevamiento_many/";

  PollsSendProvider();

  Future<bool> create(List<Map> answers) async {
    try {
      Map<String, String> headers = new Map();

      headers["Authorization"] =
          "Token 5871c1f3a2def4ef19a6fd8d694e9a72a6ca3758";
      headers["Content-Type"] = "application/json";
      var body = json.encode(answers);

      var response = await http
          .post(Uri.encodeFull(_pollsEP), headers: headers, body: body)
          .timeout(Duration(seconds: 20));
      //var bodyData = json.decode(utf8.decode(response.bodyBytes));
      return false;
    } catch (ex) {
      return false;
    }
  }
}
