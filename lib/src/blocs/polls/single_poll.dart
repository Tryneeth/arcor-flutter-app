import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/blocs/polls/poll_type_widgets/poll_types.dart';
import 'package:tokin/src/blocs/polls/polls_model.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';
import 'package:tokin/src/ui/shared/drawer.dart';

import 'index.dart';

class AnswersInherited extends InheritedWidget {
  final int amountOfQuestions;

  AnswersInherited({this.amountOfQuestions});

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget page;

  SlideRightRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        );
}

class SinglePoll extends StatefulWidget {
  static const String routeName = "Single Poll";
  final Poll poll;
  final List<PollAction> pollActions;
  final int currentQuestion;
  final int previousQuestion;

  const SinglePoll(
      {Key key,
      this.poll,
      this.pollActions,
      this.currentQuestion = 0,
      this.previousQuestion = -1})
      : super(key: key);

  @override
  _SinglePollState createState() => _SinglePollState();
}

class _SinglePollState extends State<SinglePoll> {
  List<Widget> questionsWidgets;
  List<dynamic> answers;
  int currentQuestion;
  int previousQuestion;

  void _nextPage(BuildContext context, int next, int previous) {
    if (next < questionsWidgets.length) {
      _savePoll(current: next, previous: previous);
      Future.delayed(
          Duration(milliseconds: 500),
          () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => SinglePoll(
                    poll: widget.poll,
                    pollActions: widget.pollActions,
                    currentQuestion: next,
                    previousQuestion: previous,
                  ))));
    }
  }

  void _previousPage(BuildContext context, int next, int previous) {
    if (next < questionsWidgets.length) {
      _savePoll(current: next, previous: previous);
      if (next < 0)
        Navigator.of(context).popUntil(ModalRoute.withName("PollsList"));
      else
        Future.delayed(
            Duration(milliseconds: 500),
            () => Navigator.of(context).push(SlideRightRoute(
                    page: SinglePoll(
                  poll: widget.poll,
                  pollActions: widget.pollActions,
                  currentQuestion: next,
                  previousQuestion: previous,
                ))));
    }
  }

  @override
  void initState() {
    currentQuestion = widget.currentQuestion;
    previousQuestion = widget.previousQuestion;
    questionsWidgets = new List();
    answers = new List(widget.poll.preguntas.length);
    widget.poll.preguntas.forEach(
      (q) => questionsWidgets.add(_getQuestionTypeWidget(
        context,
        q,
        onAnswer: () =>
            _nextPage(context, currentQuestion + 1, previousQuestion),
        positiveAnswer: () {
          var qActions = widget.pollActions
              .where((poll) => poll.id_form == q.id.toString())
              .toList();
          if (qActions.isEmpty)
            _nextPage(context, currentQuestion + 1, previousQuestion);
          else {
            var jumpId = qActions
                .firstWhere((qA) => qA.tipo1.toUpperCase() == "SI")
                .var1;
            var nextQuestion = widget.poll.preguntas.indexOf(widget
                .poll.preguntas
                .firstWhere((PollQuestion qu) => qu.id.toString() == jumpId));
            _nextPage(context, nextQuestion, previousQuestion);
          }
        },
        negativeAnswer: () {
          var qActions = widget.pollActions
              .where((poll) => poll.id_form == q.id.toString())
              .toList();
          if (qActions.isEmpty)
            _nextPage(context, currentQuestion + 1, previousQuestion);
          else {
            var jumpId = qActions
                .firstWhere((qA) => qA.tipo1.toUpperCase() == "NO")
                .var1;
            var nextQuestion = widget.poll.preguntas.indexOf(widget
                .poll.preguntas
                .firstWhere((PollQuestion qu) => qu.id.toString() == jumpId));
            _nextPage(context, nextQuestion, previousQuestion);
          }
        },
      )),
    );
    questionsWidgets.add(QFinalMessage(
      question: null,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new AppScaffold(
      logged: true,
      hideBackButton: true,
      activeChart: false,
      appDrawer: AppDrawer(),
      bottom: PreferredSize(
        child: Container(
          height: 55,
          color: Colors.deepOrange,
          child: (currentQuestion == widget.poll.preguntas.length)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Stack(
                        overflow: Overflow.visible,
                        children: <Widget>[
                          Positioned(
                            top: 5,
                            child: FlatButton(
                              child: Wrap(
                                direction: Axis.horizontal,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 8),
                                    padding: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: Colors.white, width: 2)),
                                    child: Icon(
                                      Icons.arrow_back,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    (currentQuestion <
                                            widget.poll.preguntas.length)
                                        ? "Anterior"
                                        : "        ",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                if (previousQuestion == -1)
                                  Navigator.of(context).pop();
                                else {
                                  currentQuestion = previousQuestion;
                                  previousQuestion--;
                                  _previousPage(context, previousQuestion,
                                      previousQuestion - 1);
                                  //Navigator.of(context).pop();
                                }
                              },
                            ),
                          ),
                          Center(
                            child: Text(
                              "Resumen final",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 19,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      child: Wrap(
                        direction: Axis.horizontal,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 8),
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 2)),
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            (currentQuestion < widget.poll.preguntas.length)
                                ? "Anterior"
                                : "        ",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ],
                      ),
                      onPressed: () {
                        if (previousQuestion == -1)
                          Navigator.of(context).pop();
                        else {
                          currentQuestion = previousQuestion;
                          previousQuestion--;
                          _previousPage(
                              context, previousQuestion, previousQuestion - 1);
                          //Navigator.of(context).pop();
                        }
                      },
                    ),
                    if (currentQuestion < widget.poll.preguntas.length - 1)
                      FlatButton(
                        child: Wrap(
                          direction: Axis.horizontal,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: <Widget>[
                            Text(
                              "Siguiente",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 8),
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.white, width: 2)),
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () async {
                          var answ;
                          var pref = await SharedPreferences.getInstance();
                          answ = pref.get(widget
                              .poll.preguntas[currentQuestion].id
                              .toString());
                          if (answ == null) {
                            showModalBottomSheet(
                                context: context,
                                backgroundColor: Colors.transparent,
                                builder: (context) => Container(
                                      height: 50,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 80, horizontal: 20),
                                      decoration: BoxDecoration(
                                        color: Colors.black38,
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "La pregunta es obligatoria",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ));
                          } else {
                            if (currentQuestion < questionsWidgets.length - 1) {
                              previousQuestion = currentQuestion;
                              _nextPage(context, currentQuestion + 1,
                                  previousQuestion);
                            }
                          }
                        },
                      ),
                  ],
                ),
        ),
        preferredSize: Size.fromHeight(50),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.grey.shade200, Colors.grey],
              ),
            ),
            constraints: BoxConstraints.expand(),
          ),
          SafeArea(
            child: Container(
              height: MediaQuery.of(context).size.height - 100,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        if (currentQuestion == widget.poll.preguntas.length)
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Image.asset(
                                  "assets/survey_resumen_pdv.png",
                                  height: 50,
                                ),
                              ),
                              Text(
                                widget.poll.nombre,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                            height: 200,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(color: Colors.blue, width: 2),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                ),
                              ],
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width - 70,
                                    child: (currentQuestion <
                                            widget.poll.preguntas.length)
                                        ? Text(
                                            widget
                                                .poll
                                                .preguntas[currentQuestion]
                                                .pregunta,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .title
                                                .copyWith(color: Colors.black),
                                          )
                                        : Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                "Presione ENVIAR para finalizar la encuesta.",
                                                style: TextStyle(fontSize: 25),
                                                textAlign: TextAlign.center,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 15.0),
                                                child: Text(
                                                  "Muchas Gracias por responder!",
                                                  style:
                                                      TextStyle(fontSize: 25),
                                                  textAlign: TextAlign.center,
                                                ),
                                              )
                                            ],
                                          ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 25.0, left: 20, right: 20),
                          child: LayoutBuilder(
                            builder: (context, constrains) => Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                if (currentQuestion <
                                        widget.poll.preguntas.length &&
                                    widget.poll.preguntas[currentQuestion]
                                            .tipo_pregunta !=
                                        4 &&
                                    widget.poll.preguntas[currentQuestion]
                                            .tipo_pregunta !=
                                        7 &&
                                    widget.poll.preguntas[currentQuestion]
                                            .imagen_pregunta !=
                                        "")
                                  Container(
                                    width: 95,
                                    height: 150,
                                    child: Column(
                                      children: <Widget>[
                                        Flexible(
                                          child: GestureDetector(
                                            onTap: () => showDialog(
                                              barrierDismissible: true,
                                              context: context,
                                              builder: (context) =>
                                                  GestureDetector(
                                                onTap: () =>
                                                    Navigator.of(context).pop(),
                                                child: Image.network(
                                                  PollsProvider.getImageUrl(
                                                      widget
                                                          .poll
                                                          .preguntas[
                                                              currentQuestion]
                                                          .imagen_pregunta),
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                            ),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.blue)),
                                              child: Image.network(
                                                PollsProvider.getImageUrl(widget
                                                    .poll
                                                    .preguntas[currentQuestion]
                                                    .imagen_pregunta),
                                                fit: BoxFit.fitWidth,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: FlatButton(
                                            child: Text("Ampliar"),
                                            onPressed: () => showDialog(
                                              barrierDismissible: true,
                                              context: context,
                                              builder: (context) =>
                                                  GestureDetector(
                                                onTap: () =>
                                                    Navigator.of(context).pop(),
                                                child: Image.network(
                                                  PollsProvider.getImageUrl(
                                                      widget
                                                          .poll
                                                          .preguntas[
                                                              currentQuestion]
                                                          .imagen_pregunta),
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                Flexible(
                                  child: Container(
                                    child: (currentQuestion <
                                            questionsWidgets.length)
                                        ? questionsWidgets[currentQuestion]
                                        : Container(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12.0, top: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          color: Colors.deepOrange,
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                      content: Text(
                                          "¿Está seguro que desea salir de la encuesta? Se guardará su progreso actual."),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () async {
                                            Navigator.of(context).popUntil(
                                                ModalRoute.withName(
                                                    "PollsList"));
                                          },
                                          child: Text("Si"),
                                        ),
                                        FlatButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(),
                                          child: Text("No"),
                                        )
                                      ],
                                    ));
                          },
                          shape: StadiumBorder(),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 12.0),
                            child: Text(
                              "Cancelar",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                        if (currentQuestion == widget.poll.preguntas.length)
                          RaisedButton(
                            color: Colors.green,
                            onPressed: () async {
                              // This code gets all Answers from a Poll
                              List<dynamic> answers = new List();
                              var pref = await SharedPreferences.getInstance();
                              for (var i = 0;
                                  i < widget.poll.preguntas.length;
                                  i++) {
                                PollQuestion a = widget.poll.preguntas[i];
                                if (a != widget.poll.preguntas.last) {
                                  answers.add(pref.get(a.id.toString()));
                                  print(answers.last);
                                }
                              }
                              if (answers.length > 0) {
                                storeResponse(answers).then((_) =>
                                    Navigator.of(context).popUntil(
                                        ModalRoute.withName("PollsList")));
                                var done =
                                    pref.getStringList('surveys-done') ?? [];
                                done.add(widget.poll.id.toString());
                                pref.setStringList('surveys-done', done);
                              }
                            },
                            shape: StadiumBorder(),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 12.0),
                              child: Text(
                                "Enviar",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                          ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _savePoll({@required int current, @required int previous}) async {
    var sp = await SharedPreferences.getInstance();
    sp.setInt('${widget.poll.id}-current', current);
    sp.setInt('${widget.poll.id}-previous', previous);
  }

  Widget _getQuestionTypeWidget(BuildContext context, PollQuestion question,
      {VoidCallback onAnswer,
      dynamic answer,
      VoidCallback positiveAnswer,
      VoidCallback negativeAnswer}) {
    switch (question.tipo_pregunta) {
      case 0: // YES/NO
        return QYesOrNo(
          question: question,
          positiveAnswer: positiveAnswer,
          negativeAnswer: negativeAnswer,
        );
      case 2: // Text
        return QText(
          question: question,
          onAnswer: onAnswer,
        );
      case 1: // Numeric
        return QNumeric(
          question: question,
        );
      case 3: // Multiple Select
        return QSelection(
          question: question,
          onAnswer: onAnswer,
        );
      case 4: // Picture
        return QPicture(
          question: question,
        );
      case 5: // Select
        return QMultipleSelection(
          question: question,
        );
      case 6: // Date
        return QDate(
          question: question,
        );
      case 7: // Qualification
        return QQualify(
          question: question,
          onAnswer: onAnswer,
        );
      case 8: // Message
        return QMessage(
          question: question,
        );
      default:
        return Container();
    }
  }

  Future<void> storeResponse(List<dynamic> answers) async {
    List<Map> responses = new List();
    PollQuestion pregunta;
    for (int i = 0; i < widget.poll.preguntas.length - 1; i++) {
      pregunta = widget.poll.preguntas.elementAt(i);
      var responsePoll = AnswerPoll(
        activo: pregunta.activo,
        codigoDistribuidor: await SharedPreferences.getInstance()
            .then((pref) => pref.getString("ID_DISTRIBUIDOR")),
        codigoEncuesta: widget.poll.codigo_encuesta,
        codigoPlan: null,
        codigoPregunta: pregunta.codigo_pregunta,
        codigoPunto: await SharedPreferences.getInstance()
            .then((pref) => pref.getString("ID_CLIENTE")),
        codigoRelevamiento:
            "PDV${DateTime.now().year}${DateTime.now().month}${DateTime.now().day}${DateTime.now().hour}${DateTime.now().minute}${DateTime.now().second}",
        codigoResultado:
            "PDV${DateTime.now().year}${DateTime.now().month}${DateTime.now().day}${DateTime.now().hour}${DateTime.now().minute}${DateTime.now().second}",
        pregunta: pregunta.pregunta,
        respuesta: answers.elementAt(i),
        id: pregunta.id,
        sincronizado: true,
        tipoPregunta: pregunta.tipo_pregunta,
      );
      responses.add(responsePoll.toJson());
    }
    PollsRepository repository = new PollsRepository();
    try {
      var sendResult = await repository.sendData(responses);
      print(sendResult);
      return sendResult;
    } catch (e) {
      print(e);
    }
  }
}

class AnswerPoll {
  bool activo;
  String codigoDistribuidor;
  String codigoEncuesta;
  String codigoPlan;
  String codigoPregunta;
  String codigoPunto;
  String codigoRelevamiento;
  String codigoResultado;
  int id;
  String pregunta;
  dynamic respuesta;
  bool sincronizado;
  int tipoPregunta;

  AnswerPoll(
      {this.activo,
      this.codigoDistribuidor,
      this.codigoEncuesta,
      this.codigoPlan,
      this.codigoPregunta,
      this.codigoPunto,
      this.codigoRelevamiento,
      this.codigoResultado,
      this.id,
      this.pregunta,
      this.respuesta,
      this.sincronizado,
      this.tipoPregunta});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = new Map();
    map["activo"] = activo;
    map["codigo_distribuidor"] = codigoDistribuidor;
    map["codigo_encuesta"] = codigoEncuesta;
    map["codigo_plan"] = codigoPlan;
    map["codigo_pregunta"] = codigoPregunta;
    map["codigo_punto"] = codigoPunto;
    map["codigo_relevamiento"] = codigoRelevamiento;
    map["codigo_resultado"] = codigoResultado;
    map["id"] = id;
    map["pregunta"] = pregunta;
    map["respuesta"] = respuesta;
    map["sincronizado"] = sincronizado;
    map["tipo_pregunta"] = tipoPregunta;
    return map;
  }
}
