import 'package:json_annotation/json_annotation.dart';
import 'package:tokin/src/utils/db/dbInitHelper.dart';

part 'polls_model.g.dart';

class PollsResponse {
  List<Poll> results;

  PollsResponse({this.results});

  PollsResponse.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      results = new List<Poll>();
      json["data"].forEach((v) {
        results.add(new Poll.fromJson(v));
      });
    }
  }
}

@JsonSerializable()
class Poll {
  final int id;
  final String codigo_encuesta;
  String nombre;
  final List<PollQuestion> preguntas;
  final int puntos;
  final bool activo;

  Poll(this.id, this.codigo_encuesta, this.nombre, this.preguntas, this.puntos,
      this.activo);

  factory Poll.fromJson(Map<String, dynamic> json) => _$PollFromJson(json);

  Map<String, dynamic> toJson() => _$PollToJson(this);

  changeNameByPlan(String name) {
    this.nombre = name;
  }

  static getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "json": DBType.TEXT,
    };
  }
}

@JsonSerializable()
class PollQuestion {
  final int id;
  final String codigo_encuesta;
  final String codigo_pregunta;
  final String pregunta;
  final int tipo_pregunta;
  final List<QuestionOptions> opciones;
  final String loc;
  final String key;
  final String category;
  final String figure;
  final String imagen_pregunta;
  final int orden_pregunta;
  final bool opciones_aleatorias;
  final bool obligatoria;
  final dynamic valor_minimo;
  final dynamic valor_maximo;
  final bool activo;
  final dynamic fecha_minima;
  final dynamic fecha_maxima;
  final bool pregunta_precio;
  final dynamic cantidad_estrellas;
  final dynamic valor_interno;
  final dynamic codigo_articulo;
  final dynamic grupo_pregunta;
  final bool tipo_presencia;
  final dynamic puntos;
  final dynamic accion;
  final bool foto_control;
  final bool foto_novedad;
  final bool pregunta_ad_hoc;

  PollQuestion(
      this.id,
      this.codigo_encuesta,
      this.codigo_pregunta,
      this.pregunta,
      this.tipo_pregunta,
      this.opciones,
      this.loc,
      this.key,
      this.category,
      this.figure,
      this.imagen_pregunta,
      this.orden_pregunta,
      this.opciones_aleatorias,
      this.obligatoria,
      this.valor_minimo,
      this.valor_maximo,
      this.activo,
      this.fecha_minima,
      this.fecha_maxima,
      this.pregunta_precio,
      this.cantidad_estrellas,
      this.valor_interno,
      this.codigo_articulo,
      this.grupo_pregunta,
      this.tipo_presencia,
      this.puntos,
      this.accion,
      this.foto_control,
      this.foto_novedad,
      this.pregunta_ad_hoc);

  factory PollQuestion.fromJson(Map<String, dynamic> json) =>
      _$PollQuestionFromJson(json);

  Map<String, dynamic> toJson() => _$PollQuestionToJson(this);
}

@JsonSerializable()
class QuestionOptions {
  final int id;
  final String codigo_pregunta;
  final String opcion;
  final bool activo;

  QuestionOptions(this.id, this.codigo_pregunta, this.opcion, this.activo);

  factory QuestionOptions.fromJson(Map<String, dynamic> json) =>
      _$QuestionOptionsFromJson(json);

  Map<String, dynamic> toJson() => _$QuestionOptionsToJson(this);
}

class RelievePlansResponse {
  List<RelievePlan> results;

  RelievePlansResponse({this.results});

  RelievePlansResponse.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      results = new List<RelievePlan>();
      json["data"].forEach((v) {
        results.add(new RelievePlan.fromJson(v));
      });
    }
  }
}

@JsonSerializable()
class RelievePlan {
  final int id;
  final String codigo_plan;
  final String nombre;
  final String codigo_encuesta;
  final int id_canal_arcor;
  final String fecha_desde;
  final String fecha_hasta;
  final int progreso_revelamiento;
  final int tipo_asignacion_auditores;
  final int publicacion;
  final int rotacion_encuestas;
  final bool rotacion_aleatoria;
  final int propetario;

  RelievePlan(
      this.id,
      this.codigo_plan,
      this.nombre,
      this.codigo_encuesta,
      this.id_canal_arcor,
      this.fecha_desde,
      this.fecha_hasta,
      this.progreso_revelamiento,
      this.tipo_asignacion_auditores,
      this.publicacion,
      this.rotacion_encuestas,
      this.rotacion_aleatoria,
      this.propetario);

  factory RelievePlan.fromJson(Map<String, dynamic> json) =>
      _$RelievePlanFromJson(json);

  Map<String, dynamic> toJson() => _$RelievePlanToJson(this);
}

class PointsRelievePlansResponse {
  List<PointsRelievePlan> results;

  PointsRelievePlansResponse({this.results});

  PointsRelievePlansResponse.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      results = new List<PointsRelievePlan>();
      json["data"].forEach((v) {
        results.add(new PointsRelievePlan.fromJson(v));
      });
    }
  }
}

@JsonSerializable()
class PointsRelievePlan {
  final int id;
  final String codigo_plan;
  final String codigo_punto;
  final String codigo_distribuidor;
  final String codigo_ruta_cliente;
  final String secuencia_cliente;

  PointsRelievePlan(
      this.id,
      this.codigo_plan,
      this.codigo_punto,
      this.codigo_distribuidor,
      this.codigo_ruta_cliente,
      this.secuencia_cliente);

  factory PointsRelievePlan.fromJson(Map<String, dynamic> json) =>
      _$PointsRelievePlanFromJson(json);

  Map<String, dynamic> toJson() => _$PointsRelievePlanToJson(this);
}

class PollActionsResponse {
  List<PollAction> results;

  PollActionsResponse({this.results});

  PollActionsResponse.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      results = new List<PollAction>();
      json["data"].forEach((v) {
        results.add(new PollAction.fromJson(v));
      });
    }
  }
}

@JsonSerializable()
class PollAction {
  final int id;
  final String id_encuesta;
  final String id_form;
  final int tiempo;
  final String orden;
  final int codigo;
  final String id_enlace;
  final String var1;
  final String tipo1;

  PollAction(
    this.id,
    this.id_encuesta,
    this.id_form,
    this.tiempo,
    this.orden,
    this.codigo,
    this.id_enlace,
    this.var1,
    this.tipo1,
  );

  factory PollAction.fromJson(Map<String, dynamic> json) =>
      _$PollActionFromJson(json);

  Map<String, dynamic> toJson() => _$PollActionToJson(this);
}
