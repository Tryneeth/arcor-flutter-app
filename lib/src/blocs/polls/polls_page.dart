import 'package:flutter/material.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';

import 'index.dart';

class PollsPage extends StatelessWidget {
  static const String routeName = "/polls";

  @override
  Widget build(BuildContext context) {
    var _pollsBloc = new PollsBloc();
    return new AppScaffold(
      logged: true,
      activeChart: false,
      child: new PollsScreen(pollsBloc: _pollsBloc),
    );
  }
}
