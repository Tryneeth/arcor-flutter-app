import 'dart:async';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/register.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'index.dart';

@immutable
abstract class WelcomeEvent {
  Future<WelcomeState> applyAsync(
      {WelcomeState currentState, WelcomeBloc bloc});

  final WelcomeRepository _welcomeProvider = new WelcomeRepository();
}

class LoadWelcomeEvent extends WelcomeEvent {
  @override
  String toString() => 'LoadWelcomeEvent';

  Future<String> _getPhoneId() async {
    if (foundation.defaultTargetPlatform == TargetPlatform.iOS) {
      return (await DeviceInfoPlugin().iosInfo).identifierForVendor;
    } else {
      return (await DeviceInfoPlugin().androidInfo).androidId;
    }
  }

  @override
  Future<WelcomeState> applyAsync(
      {WelcomeState currentState, WelcomeBloc bloc}) async {
    try {
      var logged = await SharedPreferencesController.getPrefKeepSession();
      if (!logged) {
        var emailVerified =
            await SharedPreferencesController.getPrefEmailVerified();
        if (!emailVerified) {
          await SharedPreferencesController.setPrefCloseInitialDialog(false);
          return UnWelcomeState(phoneInfo: await _getPhoneId());
        } else {
          return InWelcomeState();
        }
      } else {
        await RegisterController().toValidate(
            email: await SharedPreferencesController.getPrefEmail(),
            phoneId: await _getPhoneId());

        return LoggedWelcomeState();
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorWelcomeState(_?.toString());
    }
  }
}

class InWelcomeEvent extends WelcomeEvent {
  @override
  String toString() => 'InWelcomeEvent';

  @override
  Future<WelcomeState> applyAsync(
      {WelcomeState currentState, WelcomeBloc bloc}) async {
    try {
      return InWelcomeState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMoreWelcomeEvent extends WelcomeEvent {
  @override
  String toString() => 'LoadMoreWelcomeEvent';

  @override
  Future<WelcomeState> applyAsync(
      {WelcomeState currentState, WelcomeBloc bloc}) async {
    try {
      return InWelcomeState();
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}

class RegisterWelcomeEvent extends WelcomeEvent {
  final String info;
  final String email;

  RegisterWelcomeEvent({this.info, this.email});
  @override
  String toString() => 'LoadMoreWelcomeEvent';

  @override
  Future<WelcomeState> applyAsync(
      {WelcomeState currentState, WelcomeBloc bloc}) async {
    try {
      return RegisterWelcomeState(info: this.info, email: this.email);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}
