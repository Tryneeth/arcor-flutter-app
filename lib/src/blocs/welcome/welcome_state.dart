import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class WelcomeState extends Equatable {
  WelcomeState([Iterable props]) : super(props);

  /// Copy object for use in action
  WelcomeState copyWith();
}

class PreWelcomeState extends WelcomeState {
  @override
  String toString() => 'PreWelcomeState';

  @override
  WelcomeState copyWith() {
    return this;
  }
}

/// UnInitialized
class UnWelcomeState extends WelcomeState {
  final String phoneInfo;
  UnWelcomeState({this.phoneInfo});
  @override
  String toString() => 'UnWelcomeState';

  @override
  WelcomeState copyWith() {
    return UnWelcomeState(phoneInfo: this.phoneInfo);
  }
}

/// Initialized
class InWelcomeState extends WelcomeState {
  @override
  String toString() => 'InWelcomeState';

  @override
  WelcomeState copyWith() {
    return InWelcomeState();
  }
}

/// On Error
class ErrorWelcomeState extends WelcomeState {
  final String errorMessage;

  ErrorWelcomeState(this.errorMessage);

  @override
  String toString() => 'ErrorWelcomeState';

  @override
  WelcomeState copyWith() {
    return ErrorWelcomeState(this.errorMessage);
  }
}

/// No Data
class LoggedWelcomeState extends WelcomeState {
  @override
  String toString() => 'LoggedWelcomeState';

  @override
  WelcomeState copyWith() {
    return this;
  }
}

class RegisterWelcomeState extends WelcomeState {
  final String info;
  final String email;
  RegisterWelcomeState({this.info, this.email});
  @override
  String toString() => 'RegisterWelcomeState';

  @override
  WelcomeState copyWith() {
    return this;
  }
}
