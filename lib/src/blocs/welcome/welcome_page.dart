import 'package:flutter/material.dart';

import 'index.dart';

class WelcomePage extends StatelessWidget {
  static const String routeName = "/welcome";

  @override
  Widget build(BuildContext context) {
    var _welcomeBloc = new WelcomeBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Welcome"),
      ),
      body: new WelcomeScreen(welcomeBloc: _welcomeBloc),
    );
  }
}
