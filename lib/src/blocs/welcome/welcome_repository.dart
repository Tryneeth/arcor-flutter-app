import 'package:tokin/src/controllers/register.dart';

import 'index.dart';

class WelcomeRepository {
  final WelcomeProvider _trackProvider = new WelcomeProvider();

  Future<bool> validateEmail(String email, String info) async {
    var result =
        await RegisterController().toValidate(email: email, phoneId: info);
    return result;
  }
}
