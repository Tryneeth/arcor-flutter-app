import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tokin/src/blocs/login/index.dart';
import 'package:tokin/src/blocs/register/index.dart';
import 'package:tokin/src/blocs/welcome/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/ui/tc.dart';
import 'package:tokin/src/ui/welcome.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({
    Key key,
    @required WelcomeBloc welcomeBloc,
  })  : _welcomeBloc = welcomeBloc,
        super(key: key);

  final WelcomeBloc _welcomeBloc;

  @override
  WelcomeScreenState createState() {
    return WelcomeScreenState(_welcomeBloc);
  }
}

class WelcomeScreenState extends State<WelcomeScreen> {
  final WelcomeBloc _welcomeBloc;

  WelcomeScreenState(this._welcomeBloc);

  @override
  void initState() {
    super.initState();
    _welcomeBloc.dispatch(LoadWelcomeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<WelcomeBloc, WelcomeState>(
        bloc: widget._welcomeBloc,
        builder: (
          BuildContext context,
          WelcomeState currentState,
        ) {
          if (currentState is PreWelcomeState) {
            return Container(
                color: Colors.blue.shade400,
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          } else if (currentState is UnWelcomeState) {
            return Scaffold(
                body: FirstMeet(
              currentState: currentState,
            ));
          } else if (currentState is InWelcomeState) {
            return PresentationUi();
          } else if (currentState is RegisterWelcomeState) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RegisterScreen(
                    phoneId: currentState.info,
                    email: currentState.email,
                    registerBloc: RegisterBloc())));
            return Container();
          } else if (currentState is ErrorWelcomeState) {
            return Container(
                child: Text(
              currentState.errorMessage,
            ));
          } else if (currentState is LoggedWelcomeState) {
            return LoginScreen(
              loginBloc: LoginBloc(),
            );
          }
          return Container(
              color: Colors.blue.shade400,
              child: Center(
                child: CircularProgressIndicator(),
              ));
        },
      ),
    );
  }
}

class FirstMeet extends StatefulWidget {
  FirstMeet({Key key, @required this.currentState}) : super(key: key);
  final UnWelcomeState currentState;

  @override
  _FirstMeetState createState() => _FirstMeetState();
}

class _FirstMeetState extends State<FirstMeet> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) => WillPopScope(
              onWillPop: () =>
                  SharedPreferencesController.getPrefCloseInitialDialog(),
              child: AlertDialog(
                  title: Text("Aviso Importante".toUpperCase()),
                  content:
                      RegisterContent(info: widget.currentState.phoneInfo))));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade400,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "assets/ic_tokin.png",
                    height: MediaQuery.of(context).size.height / 3,
                  ),
                  Text(
                    "Bienvenido a nuestro\ncanal de comunicación",
                    style: Theme.of(context).primaryTextTheme.title,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            Positioned(
                left: 0.0,
                right: 0.0,
                bottom: MediaQuery.of(context).size.height / 8,
                child: Container(
                  height: 90,
                  child: Image.asset(
                    "assets/arcor.png",
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

class RegisterContent extends StatefulWidget {
  final String info;

  RegisterContent({Key key, this.info}) : super(key: key);

  @override
  _RegisterContentState createState() => _RegisterContentState();
}

class _RegisterContentState extends State<RegisterContent> {
  String email = "";
  bool charging;
  GlobalKey<FormState> _key = GlobalKey();
  bool terms;

  @override
  void initState() {
    print(WelcomeBloc().currentState);
    super.initState();
    terms = false;
    charging = false;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _key,
        child: Container(
          height: MediaQuery.of(context).size.height - 100,
          width: MediaQuery.of(context).size.width - 50,
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Text(
                        "Para el correcto funcionamiento de TOKIN, le solicitamos el uso de sus datos de Email e ID del dispositivo para el registro de su equipo en nuestros servidores."),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(widget.info),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0.0),
                        child: TextFormField(
                            key: ValueKey("email"),
                            decoration: InputDecoration(
                              labelText: "Email",
                              hasFloatingPlaceholder: true,
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: (value) {
                              var emailExp = RegExp(
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                              if (value.isEmpty)
                                return "Campo obligatorio";
                              else if (!emailExp.hasMatch(value) &&
                                  value.isNotEmpty) return "Correo inválido";
                              return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                email = value;
                              });
                            })),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text("Muchas Gracias"),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                              key: ValueKey("termsConditionCheckBox"),
                              value: terms,
                              onChanged: (value) {
                                setState(() {
                                  terms = value;
                                });
                              }),
                          Column(
                            children: <Widget>[
                              Text(
                                "Acepto los ",
                                style: TextStyle(fontSize: 12),
                              ),
                              InkWell(
                                onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TermsAndConditionsUI(
                                              readOnly: false,
                                            ))),
                                child: Text(
                                  "Términos y Condiciones",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      charging
                          ? FlatButton(
                              onPressed: () {},
                              child: CircularProgressIndicator(),
                            )
                          : FlatButton(
                              onPressed: terms
                                  ? () async {
                                      if (_key.currentState.validate()) {
                                        _key.currentState.save();
                                        setState(() {
                                          charging = true;
                                        });
                                        if (!await WelcomeRepository()
                                            .validateEmail(
                                                email, widget.info)) {
                                          Navigator.of(context).pushReplacement(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RegisterScreen(
                                                          phoneId: widget.info,
                                                          email: email,
                                                          registerBloc:
                                                              RegisterBloc())));
                                          await SharedPreferencesController
                                              .setPrefCloseInitialDialog(true);
                                        } else {
                                          await SharedPreferencesController
                                              .setPrefTermConditions(terms);
                                          await SharedPreferencesController
                                              .setPrefEmail(email);
                                          await SharedPreferencesController
                                              .setPrefCloseInitialDialog(true);
                                          WelcomeBloc()
                                              .dispatch(InWelcomeEvent());
                                          Navigator.of(context).pop();
                                        }
                                      }
                                    }
                                  : null,
                              child: Text(
                                "ACEPTAR",
                              ),
                            )
                    ],
                  ),
                ],
              )
            ],
          ),
        ));
  }
}

class PresentationUi extends StatefulWidget {
  PresentationUi({Key key}) : super(key: key);

  @override
  _PresentationState createState() => _PresentationState();
}

class _PresentationState extends State<PresentationUi> {
  var _pageController = PageController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((duration) async {
      await PermissionHandler().requestPermissions([
        PermissionGroup.photos,
        PermissionGroup.camera,
        PermissionGroup.contacts,
        PermissionGroup.storage
      ]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      children: <Widget>[
        Scaffold(
            backgroundColor: Colors.blue.shade400,
            body: GestureDetector(
                onTap: () {
                  _pageController.jumpToPage(1);
                },
                behavior: HitTestBehavior.translucent,
                child: SafeArea(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              "assets/ic_tokin.png",
                              height: MediaQuery.of(context).size.height / 3,
                            ),
                            Text(
                              "Bienvenido a nuestro\ncanal de comunicación",
                              style: Theme.of(context).primaryTextTheme.title,
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                      Positioned(
                          left: 0.0,
                          right: 0.0,
                          bottom: MediaQuery.of(context).size.height / 6,
                          child: Container(
                            height: 90,
                            child: Image.asset(
                              "assets/arcor.png",
                            ),
                          ))
                    ],
                  ),
                ))),
        InitialHelpScreen()
      ],
    );
  }
}
