import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class WelcomeBloc extends Bloc<WelcomeEvent, WelcomeState> {
  static final WelcomeBloc _WelcomeBlocSingleton = new WelcomeBloc._internal();
  factory WelcomeBloc() {
    return _WelcomeBlocSingleton;
  }
  WelcomeBloc._internal();

  WelcomeState get initialState => new PreWelcomeState();

  @override
  Stream<WelcomeState> mapEventToState(
    WelcomeEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
