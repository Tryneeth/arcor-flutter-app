import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'package:tokin/src/blocs/news/index.dart';
import 'package:tokin/src/blocs/single_new/index.dart';
import 'package:tokin/src/models/data_connection.dart';
import 'package:tokin/src/ui/shared/no_data.dart';
import 'package:tokin/src/controllers/newsController.dart';
import 'package:tokin/src/models/new.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({
    Key key,
    @required NewsBloc newsBloc,
  })  : _newsBloc = newsBloc,
        super(key: key);

  final NewsBloc _newsBloc;

  @override
  NewsScreenState createState() {
    return new NewsScreenState(_newsBloc);
  }
}

class NewsScreenState extends State<NewsScreen> {
  final NewsBloc _newsBloc;
  final ScrollController _listScrollCtrl = new ScrollController();
  GlobalKey<AnimatedListState> _animatedListKey =
      new GlobalKey<AnimatedListState>();
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  NewsScreenState(this._newsBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _newsBloc.dispatch(ReloadingNewsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<NewsBloc, NewsState>(
        bloc: widget._newsBloc,
        builder: (
          BuildContext context,
          NewsState currentState,
        ) {
          if (currentState is UnNewsState) {
            print(currentState);
            _newsBloc.dispatch(LoadNewsEvent(context));
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorNewsState) {
            print(currentState);
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos Novedades en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataNewsState) {
            print(currentState);
            return new NoDataWidget(
              message: "No hay datos de Novedades en estos momentos.",
            );
          } else if (currentState is InNewsState) {
            print(currentState);
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return NotificationListener<ScrollNotification>(
                //onNotification: _handleScroll,
                child: RefreshIndicator(
              onRefresh: () async {
                widget._newsBloc
                    .dispatch(RefreshNewsEvent(currentState.news, context));
                return await _refreshCompleter.future;
              },
              child: newsListView(currentState.news),
            ));
          } else if (currentState is DetailsNewsState) {
            print(currentState);
            return SingleNewScreen(
                singleNew: currentState.news, newsBloc: SingleNewBloc(),imagesEndpoint:currentState.imagesEndpoint);
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildListItemLoader() => Center(
        child: CircularProgressIndicator(),
      );

  bool _handleScroll(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        _listScrollCtrl.position.extentAfter == 0)
      _newsBloc.dispatch(LoadMoreNewsEvent(context));

    return false;
  }

  int _calculateListItemsCount(InNewsState state) {
    if (state.hasReachedMaxData)
      return state.news.length;
    else
      return state.news.length + 1;
  }

  Widget newsListView(
    List<NotificationAPI> data,
  ) {
    if (data.length == 0) {
      return NoDataWidget(
        message: "No hay datos de Novedades en estos momentos",
      );
    }
    return ListView.builder(
        key: ValueKey("newsList"),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () async {
              if (data[index].visto.toLowerCase() != null &&
                  (data[index].visto.toLowerCase() == "no visto")) {
                await NewsController().updateViewField(data[index]);
              }
              widget._newsBloc.dispatch(DetailsNewsEvent(news: data[index]));
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(color: Colors.grey.shade200, width: 1))),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            data[index].categoria,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .title
                                .copyWith(color: Colors.blue),
                          ),
                          if (data[index].opcionCompra ?? false)
                            Container(
                                decoration: ShapeDecoration(
                                  color: Colors.green.shade800,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(18.0),
                                  ),
                                ),
                                alignment: Alignment.center,
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 2),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 1),
                                            child: Text(
                                              "COMPRAR",
                                              style: TextStyle(
                                                fontSize: 9,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white,
                                              ),
                                              textAlign: TextAlign.center,
                                            )),
                                        Icon(
                                          Icons.shopping_cart,
                                          color: Colors.white,
                                          size: 12,
                                        )
                                      ],
                                    ))),
                          Text(DateFormat("dd/MM/y")
                              .format(data[index].fechaUltimaActualizacion))
                        ],
                      )),
                  data[index].imagenAdjunto.isNotEmpty
                      ? Stack(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.only(top: 15),
                              //height: 200,
                              child: Center(
                                  child: FutureBuilder(
                                future: DataConnection().obtainImageUrl(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData &&
                                      snapshot.connectionState ==
                                          ConnectionState.done) {
                                    return CachedNetworkImage(
                                      imageUrl: snapshot.data +
                                          data[index].imagenAdjunto,
                                      errorWidget: (context, url, error) =>
                                          Container(),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                              )),
                            ),
                            if (data[index].visto.toLowerCase() != "visto")
                              Positioned(
                                right: 0,
                                child: Image.asset(
                                  "assets/new_novedad_new.png",
                                  height: 50,
                                ),
                              ),
                          ],
                        )
                      : Container(),
                  data[index].imagenAdjunto.isEmpty
                      ? Stack(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 12.0, left: 20, right: 20),
                              child: Container(
                                constraints: BoxConstraints.expand(height: 50),
                                child: Text(
                                  data[index].titulo,
                                  textAlign: TextAlign.left,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .title
                                      .copyWith(
                                          color: Colors.blue,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w300),
                                ),
                              ),
                            ),
                            if (data[index].visto.toLowerCase() != "visto")
                              Positioned(
                                right: 0,
                                child: Image.asset(
                                  "assets/new_novedad_new.png",
                                  height: 40,
                                ),
                              ),
                          ],
                        )
                      : Padding(
                          padding: const EdgeInsets.only(
                              top: 12.0, left: 20, right: 20),
                          child: Container(
                            constraints: BoxConstraints.expand(height: 50),
                            child: Text(
                              data[index].titulo,
                              textAlign: TextAlign.left,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .title
                                  .copyWith(
                                      color: Colors.blue,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w300),
                            ),
                          ),
                        ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 12.0, left: 20, right: 20),
                    child: Row(
                      children: <Widget>[
                        Text(
                          data[index].descripcion.length > 30
                              ? "${data[index].descripcion.substring(0, 10)}..."
                              : data[index].descripcion,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .body1
                              .copyWith(fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
