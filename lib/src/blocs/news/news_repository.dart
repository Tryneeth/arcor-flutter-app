import 'package:flutter/material.dart';
import 'package:tokin/src/controllers/newsController.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

import '../../models/new.dart';
import 'index.dart';

class NewsRepository {
  final NewsProvider _newsRepository = new NewsProvider();
  DBHelper<NotificationAPI> _dbHelper;

  NewsRepository() {
    _dbHelper = new DBHelper<NotificationAPI>(
        dbTableName: 'news', tableField: NotificationAPI.getFieldsForDB());
  }

  Future<List<NotificationAPI>> findAll(
      BuildContext context, bool update) async {
    List<NotificationAPI> notificationApiListResult = new List();
    var dbData;
    try {
      if (update) {
        var notificationApiList = await NewsController().getData(true);
        notificationApiList.forEach((news) async {
          var dbElement = await _dbHelper.findOne(where: "id ==${news.id}");
          if (dbElement.length > 0) {
            news = await updateData(dbElement, news);
          }
          _dbHelper.saveOne(news.toMap());
        });
      }
      dbData = await _dbHelper.findAll(
          orderBy: "visto ASC,fecha_ultima_actualizacion DESC");
      int visto = 0;
      int noVisto = 0;
      dbData.forEach((data) {
        var news = NotificationAPI.fromJson(data);
        if (isValid(news)) {
          notificationApiListResult.add(news);
          if (news.visto.toLowerCase() == "visto") {
            visto++;
          } else {
            noVisto++;
          }
        }
      });
      if (noVisto > 0) {
        ShoppingCartInfo.of(context).flagNews = true;
      } else {
        ShoppingCartInfo.of(context).flagNews = false;
      }
      ShoppingCartInfo.update(context);
      if (visto == 0 || noVisto == 0) {
        notificationApiListResult.sort((a, b) => b.fecha.compareTo(a.fecha));
      }
      return notificationApiListResult;
    } catch (e) {
      print(e);
      List<NotificationAPI> notifications = new List();
      dbData = await _dbHelper.findAll(
          orderBy: "visto ASC,fecha_ultima_actualizacion DESC");
      dbData
          .forEach((data) => notifications.add(NotificationAPI.fromJson(data)));

      return filterAndOrderData(notifications);
    }
  }

  List<NotificationAPI> filterAndOrderData(
      List<NotificationAPI> notificationApiListResult) {
    notificationApiListResult = notificationApiListResult
        .where((item) =>
            item.eliminado != 1 &&
            DateTime.now().difference(item.fechaUltimaActualizacion).inDays <=
                45)
        .toList();
    return notificationApiListResult;
  }

  bool isValid(NotificationAPI news) {
    return news.eliminado != 1 &&
        DateTime.now().difference(news.fechaUltimaActualizacion).inDays <= 45;
  }

  Future<Map> storeAllData(BuildContext context) async {
    try {
      Map response = {"flag": false};
      var notificationApiList = await NewsController().getData(false);
      notificationApiList.forEach((news) async {
        var dbElement = await _dbHelper.findOne(where: "id ==${news.id}");
        if (dbElement.length > 0) {
          news = await updateData(dbElement, news);
          if (dbElement['visto'].toString().toLowerCase() == "no visto") {
            response["flag"] = true;
          }
        }
        //ShoppingCartInfo.update(context);
        _dbHelper.saveOne(news.toMap());
      });
      response["values"] = true;
      return response;
    } catch (e) {
      print("Error storeAll: $e");
      return {"values": false, "flag": false};
    }
  }

  Future<NotificationAPI> updateData(
      Map<String, dynamic> dbElement, NotificationAPI news) async {
    if (dbElement['visto'].toString().toLowerCase() !=
        news.visto.toLowerCase()) {
      news.visto = dbElement['visto'];
      await NewsController().updateViewField(news);
    }
    return news;
  }
}
