import 'dart:async';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/data_connection.dart';

import '../../models/new.dart';
import 'index.dart';

@immutable
abstract class NewsEvent {
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc});

  final NewsRepository _newsProvider = new NewsRepository();
}

class LoadNewsEvent extends NewsEvent {
  final BuildContext context;
  LoadNewsEvent(this.context);
  @override
  String toString() => 'LoadNewsEvent';

  @override
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc}) async {
    try {
      List<NotificationAPI> notificationApiList =
          await _newsProvider.findAll(context,true);
      return notificationApiList.isEmpty
          ? NoDataNewsState()
          : InNewsState(notificationApiList);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorNewsState(_?.toString());
    }
  }
}

class RefreshNewsEvent extends NewsEvent {
  final List<NotificationAPI> news;
  final BuildContext context;
  RefreshNewsEvent(this.news,this.context);

  @override
  String toString() => 'RefreshNewsEvent';

  @override
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc}) async {
    try {
      List<NotificationAPI> notificationApiList =
          await _newsProvider.findAll(context,true);
      return notificationApiList.isEmpty
          ? NoDataNewsState()
          : InNewsState(notificationApiList);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMoreNewsEvent extends NewsEvent {
  final BuildContext context;
  LoadMoreNewsEvent(this.context);
  @override
  String toString() => 'LoadMoreNewsEvent';

  @override
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc}) async {
    try {
      // Fetch Next Page
      var response = await this._newsProvider.findAll(context,false);
      return response.isEmpty
          ? InNewsState((currentState as InNewsState).news,
              hasReachedMaxData: true)
          : InNewsState((currentState as InNewsState).news + response);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}

class DetailsNewsEvent extends NewsEvent {
  final NotificationAPI news;
  DetailsNewsEvent({this.news});
  @override
  String toString() => 'DetailsNewsEvent';

  @override
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc}) async {
    try {
      var imagesEndpoint= await DataConnection().obtainImageUrl();
      return DetailsNewsState(news: news,imagesEndpoint: imagesEndpoint);
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}

class ReloadingNewsEvent extends NewsEvent {
  @override
  String toString() => 'ReloadingNewsEvent';

  @override
  Future<NewsState> applyAsync({NewsState currentState, NewsBloc bloc}) async {
    try {
      return UnNewsState();
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}