import 'package:flutter/material.dart';

import 'index.dart';

class NewsPage extends StatelessWidget {
  static const String routeName = "/news";

  @override
  Widget build(BuildContext context) {
    var _newsBloc = new NewsBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Novedades"),
      ),
      body: new NewsScreen(newsBloc: _newsBloc),
    );
  }
}
