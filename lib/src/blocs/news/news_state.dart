import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../models/new.dart';

@immutable
abstract class NewsState extends Equatable {
  NewsState([Iterable props]) : super(props);

  /// Copy object for use in action
  NewsState copyWith();
}

/// UnInitialized
class UnNewsState extends NewsState {
  @override
  String toString() => 'UnNewsState';

  @override
  NewsState copyWith() {
    return UnNewsState();
  }
}

/// Initialized
class InNewsState extends NewsState {
  final List<NotificationAPI> news;
  final bool hasReachedMaxData;

  InNewsState(this.news, {this.hasReachedMaxData = false})
      : super([news, hasReachedMaxData]);

  @override
  String toString() => 'InNewsState';

  @override
  NewsState copyWith({List<NotificationAPI> news, bool hasReachedMaxData}) {
    return InNewsState(news ?? this.news,
        hasReachedMaxData: hasReachedMaxData ?? this.hasReachedMaxData);
  }
}

/// On Error
class ErrorNewsState extends NewsState {
  final String errorMessage;

  ErrorNewsState(this.errorMessage);

  @override
  String toString() => 'ErrorNewsState';

  @override
  NewsState copyWith() {
    return ErrorNewsState(this.errorMessage);
  }
}

/// No Data
class NoDataNewsState extends NewsState {
  @override
  String toString() => 'NoDataNewsState';

  @override
  NewsState copyWith() {
    return this;
  }
}

/// No Data
class DetailsNewsState extends NewsState {
  final NotificationAPI news;
  final String imagesEndpoint;

  DetailsNewsState({this.news, this.imagesEndpoint});

  @override
  String toString() => 'NoDataNewsState';

  @override
  NewsState copyWith() {
    return this;
  }
}
