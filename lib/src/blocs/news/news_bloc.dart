import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  static final NewsBloc _newsBlocSingleton = new NewsBloc._internal();
  factory NewsBloc() {
    return _newsBlocSingleton;
  }
  NewsBloc._internal();

  NewsState get initialState => new UnNewsState();

  @override
  Stream<NewsState> mapEventToState(
    NewsEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
