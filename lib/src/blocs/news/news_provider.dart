import 'package:tokin/src/utils/provider_interface.dart';

import '../../models/data_connection.dart';
import '../../models/new.dart';
import 'index.dart';

class NewsProvider extends IProvider<NotificationAPI> {
  static final Future<String> _newsEP = new DataConnection().getNotificationUri();

  NewsProvider()
      : super(
          _newsEP,
          createModelFromJson: (json) => NewsResponse.fromJson(json).results,
        );
}
