import 'dart:async';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/awards.dart';
import 'index.dart';

@immutable
abstract class AwardsEvent {
  Future<AwardsState> applyAsync({AwardsState currentState, AwardsBloc bloc});

  final AwardsRepository _awardsProvider = new AwardsRepository();
}

class LoadAwardsEvent extends AwardsEvent {
  @override
  String toString() => 'LoadAwardsEvent';

  @override
  Future<AwardsState> applyAsync(
      {AwardsState currentState, AwardsBloc bloc}) async {
    try {
      var passSha = await calcHash();
      var user = await SharedPreferencesController.getPrefEmail();
      var url =
          "https://tokinpremia.com.ar/?tokin_user=$user&tokin_hash=$passSha";
      var awards = await _awardsProvider.findAll();
      if (awards.isValid()) {
        return InAwardsState(
            name: awards.name,
            image: awards.avatar,
            credits: awards.creditosDisponibles,
            url: url);
      } else {
        return ErrorAwardsState("Ha ocurrido un problema con los datos");
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorAwardsState(_?.toString());
    }
  }

  Future<String> calcHash() async {
    var pass = await SharedPreferencesController.getPrefPass();

    //Pass process
    var passBytes = utf8.encode(pass); // data being hashed
    var shaPass = sha1.convert(passBytes);
    return shaPass.toString().toUpperCase();
  }
}
