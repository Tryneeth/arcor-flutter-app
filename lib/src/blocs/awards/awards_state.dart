import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AwardsState extends Equatable {
  AwardsState([Iterable props]) : super(props);

  /// Copy object for use in action
  AwardsState copyWith();
}

/// UnInitialized
class UnAwardsState extends AwardsState {
  @override
  String toString() => 'UnAwardsState';

  @override
  AwardsState copyWith() {
    return UnAwardsState();
  }
}

/// Initialized
class InAwardsState extends AwardsState {
  final String name;
  final String image;
  final int credits;
  final String url;
  InAwardsState({this.name, this.image, this.credits,this.url}):super([name,image,credits,url]);

  @override
  String toString() => 'InAwardsState';

  @override
  AwardsState copyWith() {
    return InAwardsState(name: name, image: image, credits: credits);
  }
}

/// On Error
class ErrorAwardsState extends AwardsState {
  final String errorMessage;

  ErrorAwardsState(this.errorMessage);

  @override
  String toString() => 'ErrorAwardsState';

  @override
  AwardsState copyWith() {
    return ErrorAwardsState(this.errorMessage);
  }
}

/// No Data
class NoDataAwardsState extends AwardsState {
  @override
  String toString() => 'NoDataAwardsState';

  @override
  AwardsState copyWith() {
    return this;
  }
}
