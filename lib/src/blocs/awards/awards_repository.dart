import 'package:tokin/src/controllers/awardController.dart';
import 'package:tokin/src/models/awards.dart';

class AwardsRepository {

  Future<Awards> findAll({update: false}) async {
    return AwardController().getData();
  }

}
