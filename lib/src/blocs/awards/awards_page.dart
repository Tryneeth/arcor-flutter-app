import 'package:flutter/material.dart';

import 'index.dart';

class AwardsPage extends StatelessWidget {
  static const String routeName = "/awards";

  @override
  Widget build(BuildContext context) {
    var awardsBloc = new AwardsBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Premios"),
      ),
      body: new AwardsScreen(awardsBloc: awardsBloc),
    );
  }
}
