import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'index.dart';
import 'package:tokin/src/ui/shared/no_data.dart';
import 'package:url_launcher/url_launcher.dart';

class AwardsScreen extends StatefulWidget {
  const AwardsScreen({
    Key key,
    @required AwardsBloc awardsBloc,
  })  : _awardsBloc = awardsBloc,
        super(key: key);

  final AwardsBloc _awardsBloc;

  @override
  AwardsScreenState createState() {
    return new AwardsScreenState(_awardsBloc);
  }
}

class AwardsScreenState extends State<AwardsScreen> {
  final AwardsBloc _awardsBloc;
  static const offsetVisibleThreshold = 50;
  String selectedFilter;

  AwardsScreenState(this._awardsBloc);

  @override
  void initState() {
    super.initState();
    _awardsBloc.dispatch(LoadAwardsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<AwardsBloc, AwardsState>(
        bloc: widget._awardsBloc,
        builder: (
          BuildContext context,
          AwardsState currentState,
        ) {
          if (currentState is UnAwardsState) {
            return awardsView(true, "Cargando", 0, "image", "");
          } else if (currentState is ErrorAwardsState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "Ha ocurrido un problema con los datos. Consulte con el administrador.",
            ));
          } else if (currentState is NoDataAwardsState) {
            return new NoDataWidget(
              message: "No hay datos en estos momentos.",
            );
          } else if (currentState is InAwardsState) {
            return awardsView(false, currentState.name, currentState.credits,
                currentState.image, currentState.url);
          }
          return Container();
        },
      ),
    );
  }

  Widget awardsView(
      bool loading, String name, int points, String image, String url) {
    return Container(
      constraints: BoxConstraints.expand(),
      color: Color.fromRGBO(56, 167, 222, 1),
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Container(
            height: MediaQuery.of(context).size.width*.7,
            width: MediaQuery.of(context).size.width*.7,
            color: Colors.white,
            child: CachedNetworkImage(
              imageUrl: image,
              errorWidget: (context, url, error) => Container(),
              placeholder: (context, url) => Center(
                  child: Container(
                      height: 40,
                      width: 40,
                      child: CircularProgressIndicator())),
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Créditos: ",
                style: TextStyle(fontSize: 18),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(250, 197, 58, 1),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Text(
                  "$points",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Text(
            "$name",
            style: Theme.of(context).primaryTextTheme.title.copyWith(
                color: Color.fromRGBO(28, 76, 151, 1),
                fontWeight: FontWeight.bold),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          loading
              ? RaisedButton(
                  onPressed: () {},
                  elevation: 0.0,
                  color: Color.fromRGBO(28, 76, 151, 1),
                  shape: StadiumBorder(),
                  child: Container(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(),
                  ),
                )
              : RaisedButton(
                  onPressed: () {
                    launchURL(url);
                  },
                  elevation: 0.0,
                  color: Color.fromRGBO(28, 76, 151, 1),
                  shape: StadiumBorder(),
                  child: Text(
                    "Canjear",
                    style: TextStyle(color: Colors.white),
                  ),
                )
        ],
      ),
    );
  }

  launchURL(String url) async {
    print(url);

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
