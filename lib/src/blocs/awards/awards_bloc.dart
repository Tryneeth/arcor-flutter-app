import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:tokin/src/blocs/awards/index.dart';

class AwardsBloc extends Bloc<AwardsEvent, AwardsState> {
  static final AwardsBloc _awardsBlocSingleton =
      new AwardsBloc._internal();
  factory AwardsBloc() {
    return _awardsBlocSingleton;
  }
  AwardsBloc._internal();

  AwardsState get initialState => new UnAwardsState();

  @override
  Stream<AwardsState> mapEventToState(
    AwardsEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
