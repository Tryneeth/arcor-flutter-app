import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:tokin/src/models/new.dart';

import 'index.dart';

class SingleNewBloc extends Bloc<SingleNewEvent, SingleNewState> {
  static final SingleNewBloc _newsBlocSingleton = new SingleNewBloc._internal();
  factory SingleNewBloc() {
    return _newsBlocSingleton;
  }
  SingleNewBloc._internal();

  SingleNewState get initialState => new UnSingleNewState();

  @override
  Stream<SingleNewState> mapEventToState(
    SingleNewEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
