import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pinch_zoom_image/pinch_zoom_image.dart';
import 'package:tokin/src/blocs/cart/cart_provider.dart';
import 'package:tokin/src/blocs/news/index.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/order.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';

import 'index.dart';

class SingleNewScreen extends StatefulWidget {
  const SingleNewScreen({
    Key key,
    @required SingleNewBloc newsBloc,
    @required NotificationAPI singleNew,
    @required String imagesEndpoint,
  })  : _newsBloc = newsBloc,
        _singleNew = singleNew,
        _imagesEndpoint = imagesEndpoint,
        super(key: key);

  final SingleNewBloc _newsBloc;
  final NotificationAPI _singleNew;
  final String _imagesEndpoint;

  @override
  SingleNewScreenState createState() {
    return new SingleNewScreenState(_newsBloc, _singleNew);
  }
}

class SingleNewScreenState extends State<SingleNewScreen> {
  final SingleNewBloc _newsBloc;
  final NotificationAPI _singleNew;

  SingleNewScreenState(this._newsBloc, this._singleNew);

  @override
  void initState() {
    super.initState();
    _newsBloc.dispatch(FirstLoadSingleNewEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<SingleNewBloc, SingleNewState>(
        bloc: widget._newsBloc,
        builder: (
          BuildContext context,
          SingleNewState currentState,
        ) {
          if (currentState is UnSingleNewState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is FirstChargingSingleNewState) {
            _newsBloc.dispatch(LoadSingleNewEvent(singleNew: _singleNew));
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              bottom: BottomBarFirstCharging(),
              singleNew: _singleNew,
            );
          } else if (currentState is ErrorSingleNewState) {
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              bottom: BottomBarError(
                errorMessage: currentState.errorMessage,
              ),
              singleNew: _singleNew,
            );
          } else if (currentState is OutOfDateSingleNewState) {
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              bottom: BottomBarOutOfDate(),
              singleNew: _singleNew,
            );
          } else if (currentState is InShopSingleNewState) {
            ShoppingCartItem shoppingItem;
            try {
              shoppingItem = ShoppingCartInfo.of(context)
                  .order
                  .products
                  .singleWhere((item) => item.product.id == widget._singleNew.id);
            } catch (e) {
              shoppingItem = null;
            }
            int inShoppingCart = 0;
            if (shoppingItem != null) {
              inShoppingCart = shoppingItem.ammount;
            }
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              bottom: BottomBar(
                  initialPrice: currentState.price,
                  singleNew: _singleNew,
                  inShoppingCart: inShoppingCart,
                  product: currentState.product),
              singleNew: _singleNew,
            );
          } else if (currentState is ChargingDataNewState) {
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              bottom: BottomBarChargingData(),
              singleNew: _singleNew,
            );
          } else if (currentState is NoShopSingleNewState) {
            return SingleNew(
              imagesEndpoint: widget._imagesEndpoint,
              singleNew: _singleNew,
            );
          } else {
            return AppScaffold(
                logged: true,
                child: Center(
                  child: Text("Ha ocurrido un error"),
                ));
          }
        },
      ),
    );
  }
}

class SingleNew extends StatelessWidget with DateFormatter {
  final NotificationAPI singleNew;
  final Widget bottom;
  final String imagesEndpoint;

  const SingleNew({Key key, this.singleNew, this.bottom, @required this.imagesEndpoint}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          key: ValueKey("singleNew"),
          children: <Widget>[
            Flexible(
              child: ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(vertical: 20.0),
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                      Container(
                          alignment: Alignment.topLeft,
                          child: IconButton(
                            key: ValueKey("singleNewsBackButton"),
                            onPressed: () {
                              NewsBloc newsBloc = new NewsBloc();
                              newsBloc.dispatch(ReloadingNewsEvent());
                            },
                            icon: Icon(
                              defaultTargetPlatform == TargetPlatform.iOS ? CupertinoIcons.back : Icons.arrow_back,
                              color: Colors.lightBlue,
                            ),
                          )),
                      Container(
                        child: Center(
                          child: Text(
                            singleNew.categoria,
                            style: Theme.of(context).primaryTextTheme.title.copyWith(color: Colors.lightBlue),
                          ),
                        ),
                      ),
                      Container(
                        child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
                          Text(
                            DateFormat('dd/MM/y').format(DateTime.now()),
                            style: TextStyle(fontSize: 12),
                          )
                        ]),
                      ),
                    ]),
                  ),
                  Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
                      child: Container(
                          constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width),
                          child: Text(
                            singleNew.titulo,
                            style: Theme.of(context).primaryTextTheme.title.copyWith(color: Colors.lightBlue),
                          ))),
                  Container(
                      child: Center(
                          child: singleNew.imagenAdjunto.length != 0
                              ? GestureDetector(
                                  onTap: () => showDialog(
                                      barrierDismissible: true,
                                      context: context,
                                      builder: (context) => GestureDetector(
                                          onTap: () => Navigator.of(context).pop(),
                                          child: Center(
                                              child: PinchZoomImage(
                                            image: CachedNetworkImage(
                                              imageUrl: imagesEndpoint + singleNew.imagenAdjunto,
                                              errorWidget: (context, url, error) => Container(
                                                height: 20,
                                                color: Colors.blue,
                                              ),
                                              placeholder: (context, url) => CircularProgressIndicator(),
                                            ),
                                            zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
                                            hideStatusBarWhileZooming: true,
                                            onZoomStart: () {
                                              print('Zoom started');
                                            },
                                            onZoomEnd: () {
                                              print('Zoom finished');
                                            },
                                          )))),
                                  child: CachedNetworkImage(
                                    imageUrl: imagesEndpoint + singleNew.imagenAdjunto,
                                    useOldImageOnUrlChange: true,
                                    errorWidget: (context, url, error) => Container(
                                      height: 20,
                                      color: Colors.red,
                                    ),
                                    placeholder: (context, url) => CircularProgressIndicator(),
                                  ),
                                )
                              : Container())),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
                    child: Row(
                      children: <Widget>[
                        new Container(
                          child: Text(
                            singleNew.descripcion,
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              wordSpacing: 2,
                            ),
                          ),
                          width: MediaQuery.of(context).size.width - 40,
                        ),
                      ],
                    ),
                  ),
                  if (singleNew.opcionCompra)
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Vigente desde el ${DateFormat('dd/MM/y').format(DateFormatter.toFormatDate(singleNew.fechaInicio))} hasta ${DateFormat('dd/MM/y').format(DateFormatter.toFormatDate(singleNew.fechaFin))}.",
                              textAlign: TextAlign.justify,
                              style: TextStyle(fontSize: 13),
                            ),
                            width: MediaQuery.of(context).size.width - 40,
                          ),
                          Container(
                            child: Text(
                              "Tope máximo de compra por cliente ${singleNew.topeCantidadVenta}.",
                              textAlign: TextAlign.justify,
                              style: TextStyle(fontSize: 13),
                            ),
                          ),
                        ],
                      ),
                    )
                ],
              ),
            ),
            if (bottom != null) bottom,
          ],
        ));
  }
}

///BottomBar: bar after the singleNew view, this is the area for start the shopping of a product
class BottomBar extends StatefulWidget {
  final double _initialPrice;
  final NotificationAPI _singleNew;
  final int _inShoppingCart;
  final Product _product;

  BottomBar({
    Key key,
    @required NotificationAPI singleNew,
    @required double initialPrice,
    @required int inShoppingCart,
    @required Product product,
  })  : _initialPrice = initialPrice,
        _singleNew = singleNew,
        _inShoppingCart = inShoppingCart,
        _product = product,
        super(key: key);

  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  double price;
  int inShoppingCart;

  @override
  void initState() {
    inShoppingCart = widget._inShoppingCart;
    price = widget._initialPrice;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      color: Colors.black.withOpacity(0.75),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Text(
              "Precio s/imp:",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(
            child: inShoppingCart > 0
                ? Text(
                    "\$ ${(inShoppingCart * price).toStringAsFixed(2)}",
                    style: TextStyle(color: Colors.white, fontSize: 13),
                  )
                : Text(
                    "\$ $price",
                    style: TextStyle(color: Colors.white, fontSize: 13),
                  ),
          ),
          inShoppingCart > 0
              ? Container(
                  child: FlatButton(
                      color: Colors.green,
                      padding: const EdgeInsets.all(0),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      onPressed: () {},
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              color: Colors.white,
                              icon: Icon(
                                Icons.remove_circle_outline,
                                size: 22,
                              ),
                              onPressed: () async {
                                ShoppingCartItem shoppingItem;
                                try {
                                  shoppingItem = ShoppingCartInfo.of(context)
                                      .order
                                      .products
                                      .singleWhere((item) => item.product.id == widget._singleNew.id);
                                } catch (e) {
                                  shoppingItem = null;
                                }

                                if (shoppingItem != null) {
                                  int min = int.tryParse(shoppingItem.articles.unidadMinimaVenta);
                                  //This if is for validate that exists a minimum limmit
                                  if ((min != null && shoppingItem.ammount > min) ||
                                      (min == null && shoppingItem.ammount > 1)) {
                                    var itemToAdd = shoppingItem;
                                    itemToAdd.ammount--;
                                    var products = ShoppingCartInfo.of(context).order.products;
                                    var indexOfItem = products.indexOf(shoppingItem);
                                    products[indexOfItem] = itemToAdd;
                                    ShoppingCartInfo.update(context);

                                    setState(() {
                                      inShoppingCart--;
                                    });
                                  } else {
                                    ShoppingCartInfo.of(context).order.products.remove(shoppingItem);
                                    ShoppingCartInfo.update(context);
                                    setState(() {
                                      inShoppingCart = 0;
                                    });
                                  }
                                  //Sub cicle for update cart in sharedPreferences
                                  var products = ShoppingCartInfo.of(context).order.products;
                                  List<String> productJsons = List();
                                  products.forEach((item) => productJsons.add(item.toJson()));
                                  await SharedPreferencesController.setPrefCart(productJsons);
                                  //end sub cicle
                                  if (ShoppingCartInfo.of(context).order.products.length == 0) {
                                    ShoppingCartInfo.of(context).flagCart = false;
                                    ShoppingCartInfo.update(context);
                                  }
                                }
                              },
                            ),
                            Text("$inShoppingCart", style: TextStyle(color: Colors.white, fontSize: 13)),
                            IconButton(
                              color: Colors.white,
                              icon: Icon(
                                Icons.add_circle_outline,
                                size: 22,
                              ),
                              onPressed: () async {
                                ShoppingCartItem shoppingItem;
                                try {
                                  shoppingItem = ShoppingCartInfo.of(context)
                                      .order
                                      .products
                                      .singleWhere((item) => item.product.id == widget._singleNew.id);
                                } catch (e) {
                                  shoppingItem = null;
                                }
                                if (shoppingItem != null &&
                                    shoppingItem.product.topeCantidadVenta != null &&
                                    shoppingItem.product.topeCantidadVenta > shoppingItem.ammount) {
                                  var itemToAdd = shoppingItem;
                                  itemToAdd.ammount++;
                                  var products = ShoppingCartInfo.of(context).order.products;
                                  var indexOfItem = products.indexOf(shoppingItem);
                                  products[indexOfItem] = itemToAdd;
                                  ShoppingCartInfo.update(context);
                                  //Sub cicle for update cart in sharedPreferences
                                  List<String> productJsons = List();
                                  products.forEach((item) => productJsons.add(item.toJson()));
                                  await SharedPreferencesController.setPrefCart(productJsons);
                                  //end sub cicle
                                  setState(() {
                                    inShoppingCart++;
                                  });
                                } else if (shoppingItem != null &&
                                    shoppingItem.product.topeCantidadVenta != null &&
                                    shoppingItem.product.topeCantidadVenta == shoppingItem.ammount) {
                                  showModalBottomSheet(
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      builder: (context) => Container(
                                            height: 50,
                                            margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                                            decoration: BoxDecoration(
                                              color: Colors.white.withOpacity(0.8),
                                              borderRadius: BorderRadius.circular(15),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "El pedido no puede superar la cantidad disponible: ${shoppingItem.product.topeCantidadVenta} ",
                                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ));
                                }
                              },
                            ),
                          ],
                        ),
                      )),
                )
              : Container(
                  child: FlatButton(
                      color: Colors.red,
                      padding: const EdgeInsets.all(5),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      onPressed: () async {
                        int ammount = int.tryParse(widget._product.unidadMinimaVenta);
                        var product = ShoppingCartItem(
                            ammount: ammount == null ? 1 : ammount,
                            product: widget._singleNew,
                            articles: widget._product);
                        ShoppingCartInfo.of(context).order.idPedido = await CartProvider.generateIDPedido(context);
                        ShoppingCartInfo.of(context).order.products.add(product);
                        ShoppingCartInfo.of(context).flagCart = true;
                        ShoppingCartInfo.update(context);
                        var products = ShoppingCartInfo.of(context).order.products;
                        List<String> productJsons = List();
                        products.forEach((item) => productJsons.add(item.toJson()));
                        await SharedPreferencesController.setPrefCart(productJsons);
                        setState(() {
                          print("aumento el inshoppingCat");
                          inShoppingCart++;
                        });
                      },
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          child: Text("COMPRAR", style: TextStyle(color: Colors.white, fontSize: 14)))),
                )
        ],
      ),
    );
  }
}

class BottomBarError extends StatelessWidget {
  const BottomBarError({Key key, this.errorMessage}) : super(key: key);
  final String errorMessage;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      color: Colors.black.withOpacity(0.75),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 3),
            child: Text(
              "Precio s/imp:",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2),
            child: Text(
              errorMessage,
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(child: Container())
        ],
      ),
    );
  }
}

class BottomBarChargingData extends StatelessWidget {
  const BottomBarChargingData({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      color: Colors.black.withOpacity(0.75),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2.5),
            child: Text(
              "Precio s/imp:",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(
            child: Container(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                backgroundColor: Colors.transparent,
              ),
            ),
          ),
          Container(
              constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2.5),
              child: Text(
                "Consultando Datos del Producto.",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 13),
              ))
        ],
      ),
    );
  }
}

class BottomBarOutOfDate extends StatelessWidget {
  const BottomBarOutOfDate({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      color: Colors.black.withOpacity(0.75),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 3),
            child: Text(
              "Precio s/imp:",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2.8),
            child: Text(
              "Compra no vigente.",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 3.2),
            child: FlatButton(
                color: Colors.grey,
                padding: const EdgeInsets.all(5),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                onPressed: () {},
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    child: Text("COMPRAR", style: TextStyle(color: Colors.white, fontSize: 14)))),
          )
        ],
      ),
    );
  }
}

class BottomBarFirstCharging extends StatelessWidget {
  const BottomBarFirstCharging({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
        color: Colors.black.withOpacity(0.75),
        child: Center(
          child: CircularProgressIndicator(),
        ));
  }
}
