import 'package:tokin/src/utils/provider_interface.dart';

import '../../models/data_connection.dart';
import '../../models/new.dart';
import 'index.dart';

class SingleNewProvider extends IProvider<NotificationAPI> {
  static final Future<String> _shopEP = new DataConnection().getNotificationUri();

  SingleNewProvider()
      : super(
          _shopEP,
          createModelFromJson: (json) => SingleNewResponse.fromJson(json).results,
        );
}
