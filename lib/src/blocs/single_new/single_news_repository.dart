import 'package:tokin/src/controllers/singleNewController.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/product.dart';

class SingleNewRepository {
  SingleNewRepository();

  static Future<Product> getProduct(NotificationAPI singleNew) async {
    SingleNewsController singleNewsController = new SingleNewsController();
    var response = await singleNewsController.getData(singleNew);
    if (response.length != 0) {
      return response.first;
    } else {
      return null;
    }
  }
}
