import 'package:flutter/material.dart';
import 'package:tokin/src/models/new.dart';

import 'index.dart';

class SingleNewPage extends StatelessWidget {
  final NotificationAPI singleNew;
  final String imagesEndpoint;
  static const String routeName = "/news";

  SingleNewPage({Key key, this.singleNew, this.imagesEndpoint})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _newsBloc = new SingleNewBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("SingleNew"),
      ),
      body: new SingleNewScreen(
        newsBloc: _newsBloc,
        singleNew: singleNew,
        imagesEndpoint: imagesEndpoint,
      ),
    );
  }
}
