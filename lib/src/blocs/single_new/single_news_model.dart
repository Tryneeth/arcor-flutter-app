import '../../models/new.dart';

class SingleNewResponse {
  List<NotificationAPI> results;

  SingleNewResponse({this.results});

  SingleNewResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      results = new List<NotificationAPI>();
      json['data'].forEach((v) {
        results.add(new NotificationAPI.fromJson(v));
        results = results
          .where((item) =>
              item.eliminado != 1 &&
              DateTime.now().difference(item.fechaUltimaActualizacion).inDays <=
                  45)
          .toList();
        results.sort((a, b) => b.fecha.compareTo(a.fecha));
        results.sort((a, b) => a.visto.compareTo(b.visto));
      });
    }
  }
}
