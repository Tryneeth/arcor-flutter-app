import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/product.dart';

@immutable
abstract class SingleNewState extends Equatable {
  SingleNewState([Iterable props]) : super(props);

  /// Copy object for use in action
  SingleNewState copyWith();
}

/// State active while charge view
class UnSingleNewState extends SingleNewState {
  @override
  String toString() => 'UnSingleNewState';

  @override
  SingleNewState copyWith() {
    return UnSingleNewState();
  }
}
/// State active while charge view
class FirstChargingSingleNewState extends SingleNewState {
  @override
  String toString() => 'FirstChargingSingleNewState';

  @override
  SingleNewState copyWith() {
    return FirstChargingSingleNewState();
  }
}

/// Charging Data
class ChargingDataNewState extends SingleNewState {
  @override
  String toString() => 'ChargingDataNewState';

  @override
  SingleNewState copyWith() {
    return ChargingDataNewState();
  }
}

/// On Error, No price, No exist product, Out of stock
class ErrorSingleNewState extends SingleNewState {
  final String errorMessage;

  ErrorSingleNewState(this.errorMessage);

  @override
  String toString() => 'ErrorSingleNewState';

  @override
  SingleNewState copyWith() {
    return ErrorSingleNewState(this.errorMessage);
  }
}

/// Out of Date
class OutOfDateSingleNewState extends SingleNewState {
  @override
  String toString() => 'OutOfDateSingleNewState';

  @override
  SingleNewState copyWith() {
    return this;
  }
}

/// In Shop
class InShopSingleNewState extends SingleNewState {
  final double price;
  final Product product;

  InShopSingleNewState({@required this.price, @required this.product});
  @override
  String toString() => 'InShopSingleNewState';

  @override
  SingleNewState copyWith() {
    return this;
  }
}

/// No Shop
class NoShopSingleNewState extends SingleNewState {
  @override
  String toString() => 'NoShopSingleNewState';

  @override
  SingleNewState copyWith() {
    return this;
  }
}
