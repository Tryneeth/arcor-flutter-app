import 'dart:async';

import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/dateFormatter.dart';

import '../../models/new.dart';
import 'index.dart';

@immutable
abstract class SingleNewEvent {
  Future<SingleNewState> applyAsync(
      {SingleNewState currentState, SingleNewBloc bloc});
}

class LoadSingleNewEvent extends SingleNewEvent with DateFormatter {
  final NotificationAPI singleNew;
  LoadSingleNewEvent({@required this.singleNew});
  @override
  String toString() => 'LoadSingleNewEvent';

  @override
  Future<SingleNewState> applyAsync(
      {SingleNewState currentState, SingleNewBloc bloc}) async {
    try {
      if (singleNew.opcionCompra) {
        var initialDate =
            DateFormatter.toFormatDate(singleNew.fechaInicioCompra);
        var endDate = DateFormatter.toFormatDate(singleNew.fechaFinCompra);
        bool isAfterInitialDate = initialDate.isBefore(DateTime.now());
        bool isBeforeEndDate = endDate.isAfter(DateTime.now());
        if (isAfterInitialDate && isBeforeEndDate) {
          var product = await SingleNewRepository.getProduct(singleNew);
          if (product != null) {
            var precioLista = product.precioLista != ""
                ? double.parse(product.precioLista)
                : 0;

            if (precioLista > 0) {
              return InShopSingleNewState(price: precioLista, product: product);
            } else {
              return ErrorSingleNewState(
                  "Producto inexistente. Consulte al vendedor");
            }
          } else {
            return ErrorSingleNewState("Error obteniendo los datos");
          }
        } else {
          return OutOfDateSingleNewState();
        }
      } else {
        return NoShopSingleNewState();
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorSingleNewState(_?.toString());
    }
  }
}

class FirstLoadSingleNewEvent extends SingleNewEvent {
  FirstLoadSingleNewEvent();

  @override
  String toString() => 'FirstLoadSingleNewEvent';

  @override
  Future<SingleNewState> applyAsync(
      {SingleNewState currentState, SingleNewBloc bloc}) async {
    try {
      return FirstChargingSingleNewState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class RefreshSingleNewEvent extends SingleNewEvent {
  final List<NotificationAPI> news;

  RefreshSingleNewEvent(this.news);

  @override
  String toString() => 'RefreshSingleNewEvent';

  @override
  Future<SingleNewState> applyAsync(
      {SingleNewState currentState, SingleNewBloc bloc}) async {
    try {
      return ErrorSingleNewState("");
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}
