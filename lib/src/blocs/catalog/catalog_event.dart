import 'dart:async';
import 'dart:developer' as developer;

import 'package:tokin/src/blocs/catalog/index.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/models/product.dart';

@immutable
abstract class CatalogEvent {
  Stream<CatalogState> applyAsync({CatalogState currentState, CatalogBloc bloc});

  final CatalogRepository _catalogRepository = CatalogRepository();
}

class UnCatalogEvent extends CatalogEvent {
  @override
  Stream<CatalogState> applyAsync({CatalogState currentState, CatalogBloc bloc}) async* {
    yield UnCatalogState(0);
  }
}

class LoadCatalogEvent extends CatalogEvent {
  @override
  String toString() => 'LoadCatalogEvent';

  LoadCatalogEvent();

  @override
  Stream<CatalogState> applyAsync({CatalogState currentState, CatalogBloc bloc}) async* {
    try {
      if (currentState is InCatalogState)
        yield currentState;
      else {
        List<Product> products = List();

        yield UnCatalogState(0, loadingMessage: "Cargando Productos...");
        int page = 1;
        var allProducts = await this._catalogRepository.getProductsFromCatalog(page++);
        var totalPages = this._catalogRepository.getProductsPageCount();
        products.addAll(allProducts);

        yield UnCatalogState(page, loadingMessage: "Cargando página $page de $totalPages");
        while (allProducts != null && allProducts.isNotEmpty && page <= totalPages) {
          yield UnCatalogState(page, loadingMessage: "Cargando página $page de $totalPages");
          allProducts = await this._catalogRepository.getProductsFromCatalog(page++);
          products.addAll(allProducts);
        }

        yield UnCatalogState(page, loadingMessage: "Obteniendo Productos Favoritos...");
        products = await _catalogRepository.getFavoriteProducts(products);

        yield UnCatalogState(page, loadingMessage: "Categorizando Productos...");
        var categorizedProducts = await this._catalogRepository.arrangeProductsByCategory(products);

        yield InCatalogState(0, categorizedProducts);
      }
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadCatalogEvent', error: _, stackTrace: stackTrace);
      yield ErrorCatalogState(0, _?.toString());
    }
  }
}

class SetFavoriteEvent extends CatalogEvent {
  final Product product;
  final bool favoriteFlag;

  SetFavoriteEvent(this.product, this.favoriteFlag);

  @override
  Stream<CatalogState> applyAsync({CatalogState currentState, CatalogBloc bloc}) async* {
    try {
      if (currentState is InCatalogState) await this._catalogRepository.setFavoriteProduct(product, favoriteFlag);
      yield currentState;
    } catch (_, stackTrace) {
      developer.log('$_', name: 'SetFavoriteEvent ${product.codigoArticulo}', error: _, stackTrace: stackTrace);
      yield ErrorCatalogState(0, _?.toString());
    }
  }
}

class LoadSingleCategoryEvent extends CatalogEvent {
  final List<Product> products;

  LoadSingleCategoryEvent(this.products);

  @override
  Stream<CatalogState> applyAsync({CatalogState currentState, CatalogBloc bloc}) async* {
    if(currentState is InCatalogState)
      yield SingleCategoryState(0, products: products);
    else yield currentState;
  }

}
