import 'package:tokin/src/blocs/catalog/index.dart';
import 'package:tokin/src/models/product.dart';

class CatalogRepository {
  final CatalogProvider _catalogProvider = CatalogProvider();

  Future<List<Product>> getProductsFromCatalog(int page) => _catalogProvider.getProductsFromCatalog(page);

  Future<Map<String, List<Product>>> arrangeProductsByCategory(List<Product> products) =>
      _catalogProvider.arrangeProductsByCategory(products);

  int getProductsPageCount() => _catalogProvider.productsPageCount;

  Future<void> setFavoriteProduct(Product product, bool favoriteFlag) =>
      _catalogProvider.setFavoriteProduct(product, favoriteFlag);

  Future<List<Product>> getFavoriteProducts(List<Product> products) => _catalogProvider.getFavoriteProducts(products);
}
