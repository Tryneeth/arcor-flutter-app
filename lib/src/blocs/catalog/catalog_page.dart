import 'package:flutter/material.dart';
import 'package:tokin/src/blocs/catalog/index.dart';

class CatalogPage extends StatefulWidget {
  static const String routeName = '/catalog';

  @override
  _CatalogPageState createState() => _CatalogPageState();
}

class _CatalogPageState extends State<CatalogPage> {
  final _catalogBloc = CatalogBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Catalog'),
      ),
      body: CatalogScreen(catalogBloc: _catalogBloc),
    );
  }
}
