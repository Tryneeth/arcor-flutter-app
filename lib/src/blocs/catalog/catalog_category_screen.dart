import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tokin/src/blocs/catalog/index.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';

class CatalogCategoryScreen extends StatefulWidget {
  final String title;
  final List<Product> products;

  const CatalogCategoryScreen(
      {Key key, @required this.products, @required this.title})
      : assert(title != null),
        super(key: key);

  @override
  _CatalogCategoryScreenState createState() => _CatalogCategoryScreenState();
}

class _CatalogCategoryScreenState extends State<CatalogCategoryScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Map<int, List<Product>> pagedProducts = Map();
  int page = 1;
  var _searchController = TextEditingController();
  List<Product> searchedProducts = List();
  ScrollController _controller;
  String message = "";

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
  }

  @override
  void initState() {
    pagedProducts = _paginate();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(_scrollListener);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleChild: Text(widget.title ?? ""),
      logged: true,
      floatingActionButton: CatalogFloatingActionButton(
          animationController: _animationController),
      bottomNavigationBar: (pagedProducts.length > 1)
          ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    height: 50,
                    padding: EdgeInsets.only(bottom: 3, left: 5, right: 5),
                    child: Center(
                      child: GridView.count(
                        crossAxisCount: 1,
                        childAspectRatio: 1,
                        shrinkWrap: true,
                        mainAxisSpacing: 8.0,
                        scrollDirection: Axis.horizontal,
                        children: _pageNumbers(pagedProducts),
                      ),
                    ),
                  ),
                )
              ],
            )
          : null,
      child: Column(
        children: <Widget>[
          LayoutBuilder(
            builder: (context, constrains) {
              return Container(
                width: constrains.maxWidth,
                height: 50,
                decoration: BoxDecoration(color: Colors.blue, boxShadow: [
                  BoxShadow(blurRadius: 8, offset: Offset(0, -5))
                ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _searchController,
                          maxLines: 1,
                          style: TextStyle(color: Colors.white),
                          onEditingComplete: () => _search(widget.products),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        _search(widget.products);
                      },
                      splashColor: Colors.deepOrange,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    if (widget.title == "Todo")
                      InkWell(
                        onTap: () {},
                        splashColor: Colors.deepOrange,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            "assets/catalog/codigo-de-barras.svg",
                            height: 25,
                          ),
                        ),
                      ),
                    if (widget.title == "Todo")
                      InkWell(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            "assets/catalog/filtrar.svg",
                            height: 18,
                          ),
                        ),
                      ),
                  ],
                ),
              );
            },
          ),
          Expanded(
            child: GridView.count(
              primary: false,
              controller: _controller,
              crossAxisCount: 2,
              padding:
                  EdgeInsets.only(left: 10.0, right: 10, top: 8.0, bottom: 60),
              childAspectRatio: 9 / 16,
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
              shrinkWrap: true,
              children: allProducts(pagedProducts[page], _animationController),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _pageNumbers(Map<int, List<Product>> pagedProducts) {
    List<Widget> result = List();
    pagedProducts.forEach((num, products) {
      result.add(GestureDetector(
        onTap: () {
          _controller.animateTo(0,
              duration: Duration(milliseconds: 200), curve: Curves.easeIn);
          setState(() {
            page = num;
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: page != num
                  ? BorderRadius.circular(50)
                  : BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    ),
              color: page != num ? Colors.transparent : Colors.white,
              border: Border.all(color: Colors.white, width: 2)),
          child: Center(
              child: Text(
            "$num",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: page != num ? Colors.white : Colors.black),
          )),
        ),
      ));
    });

    return result;
  }

  List<Widget> allProducts(
      List<Product> products, AnimationController animationController) {
    List<Widget> result = List();
    products.forEach((p) {
      int isInShoppingCart;
      try {
        var shoppingItem = ShoppingCartInfo.of(context)
            .order
            .products
            .singleWhere(
                (item) => item.articles.codigoArticulo == p.codigoArticulo);
        isInShoppingCart = shoppingItem.ammount;
      } catch (e) {
        isInShoppingCart = 0;
      }
      result.add(SingleGridProduct(
          key: Key(p.hashCode.toString()),
          product: p,
          inShoppingCart: isInShoppingCart,
          animationController: animationController));
    });
    return result;
  }

  Map<int, List<Product>> _paginate({List<Product> products}) {
    Map<int, List<Product>> resultProducts = Map();

    List<Product> allProducts = products ?? widget.products;

    if (allProducts.length > 20) {
      var divisions = allProducts.length / 20;
      for (var i = 1; i <= divisions; i++) {
        resultProducts
            .addAll({i: allProducts.getRange(20 * i - 20, 20 * i).toList()});
      }
    } else {
      resultProducts.addAll({1: allProducts});
    }
    return resultProducts;
  }

  void _search(List<Product> catProducts) {
    var searchString = _searchController.text.split(" ");

    searchedProducts.clear();

    searchString.forEach((str) {
      searchedProducts.addAll(catProducts
          .where(
              (p) => p.descArticulo.toLowerCase().contains(str.toLowerCase()))
          .toList());
    });
    setState(() {
      FocusScope.of(context).requestFocus(FocusNode());
      pagedProducts = _paginate(products: searchedProducts);
    });
  }
}

class SingleGridProduct extends StatefulWidget {
  SingleGridProduct(
      {Key key,
      @required this.product,
      this.inShoppingCart,
      this.animationController})
      : super(key: key);

  final Product product;
  final int inShoppingCart;
  AnimationController animationController;

  @override
  _SingleGridProductState createState() => _SingleGridProductState();
}

class _SingleGridProductState extends State<SingleGridProduct> {
  var inShoppingCart = 0;

  @override
  void initState() {
    inShoppingCart = widget.inShoppingCart;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CatalogSingleProductScreen(
                  product: widget.product,
                )));
      },
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Center(
                    child: CachedNetworkImage(
                      imageUrl: widget.product.getThumbnailUrl(),
                      fit: BoxFit.contain,
                      errorWidget: (context, _, __) => Image.asset(
                        "assets/ic_img_tokin.png",
                        color: Colors.grey,
                        width: 200,
                        height: 180,
                      ),
                      width: 200,
                      height: 180,
                    ),
                  ),
                  if (widget.product.highlighted)
                    Positioned(
                      left: 10,
                      top: 0,
                      child: SvgPicture.asset(
                        "assets/catalog/medalla.svg",
                        width: 30,
                        height: 30,
                      ),
                    ),
                  Positioned(
                    right: 10,
                    top: 0,
                    child: GestureDetector(
                        child: widget.product.favorite
                            ? Icon(
                                Icons.favorite,
                                color: Colors.deepOrange,
                              )
                            : Icon(Icons.favorite_border),
                        onTap: () {
                          setState(() {
                            widget.product.favorite = !widget.product.favorite;
                          });
                          CatalogBloc().dispatch(SetFavoriteEvent(
                              widget.product, widget.product.favorite));
                        }),
                  )
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(widget.product.descArticulo),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                    "Precio s/imp: \$${widget.product.precioListaDoubleDigits}")
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: inShoppingCart > 0
                        ? Container(
                            child: FlatButton(
                                color: Colors.green,
                                disabledColor: Colors.green,
                                padding: const EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                onPressed: null,
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      IconButton(
                                          color: Colors.white,
                                          icon: Icon(
                                            Icons.remove_circle_outline,
                                            size: 20,
                                          ),
                                          onPressed: () {
                                            widget.animationController
                                                .forward();
                                            removeFromShoppingCart(
                                                context, widget.product);
                                            setState(() {
                                              inShoppingCart--;
                                            });
                                          }),
                                      Text("$inShoppingCart",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13)),
                                      IconButton(
                                        color: Colors.white,
                                        icon: Icon(
                                          Icons.add_circle_outline,
                                          size: 20,
                                        ),
                                        onPressed: () {
                                          widget.animationController.forward();
                                          addToShoppingCart(context,
                                              widget.product, inShoppingCart);
                                          setState(() {
                                            inShoppingCart++;
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                )),
                          )
                        : Container(
                            child: FlatButton(
                                color: Colors.deepOrange,
                                padding: const EdgeInsets.all(5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                onPressed: () {
                                  widget.animationController.forward();
                                  addToShoppingCart(
                                      context, widget.product, inShoppingCart);
                                  setState(() {
                                    inShoppingCart++;
                                  });
                                },
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    child: Text("Agregar",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14)))),
                          )),
              ],
            )
          ],
        ),
      ),
    );
  }
}
