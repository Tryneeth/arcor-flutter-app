import 'package:equatable/equatable.dart';
import 'package:tokin/src/models/product.dart';

abstract class CatalogState extends Equatable {
  /// notify change state without deep clone state
  final int version;
  
  final List propss;
  CatalogState(this.version,[this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  CatalogState getStateCopy();

  CatalogState getNewVersion();

  @override
  List<Object> get props => ([version, ...propss ?? []]);
}

/// UnInitialized
class UnCatalogState extends CatalogState {
  final String loadingMessage;

  UnCatalogState(int version, {this.loadingMessage}) : super(version, [loadingMessage]);

  @override
  String toString() => 'UnCatalogState';

  @override
  UnCatalogState getStateCopy() {
    return UnCatalogState(0, loadingMessage: this.loadingMessage);
  }

  @override
  UnCatalogState getNewVersion() {
    return UnCatalogState(version+1, loadingMessage: this.loadingMessage);
  }
}

/// Initialized
class InCatalogState extends CatalogState {
  final Map<String, List<Product>> catProducts;

  InCatalogState(int version, this.catProducts) : super(version, [catProducts]);

  @override
  String toString() => 'InCatalogState $catProducts';

  @override
  InCatalogState getStateCopy() {
    return InCatalogState(this.version, this.catProducts);
  }

  @override
  InCatalogState getNewVersion() {
    return InCatalogState(version+1, this.catProducts);
  }
}

/// Single Category View
class SingleCategoryState extends CatalogState {
  final int version;
  final List<Product> products;

  SingleCategoryState(this.version, {this.products}) : super(version, [products]);


  @override
  CatalogState getNewVersion() {
    // TODO: implement getNewVersion
    return null;
  }

  @override
  CatalogState getStateCopy() {
    // TODO: implement getStateCopy
    return null;
  }
}

class ErrorCatalogState extends CatalogState {
  final String errorMessage;

  ErrorCatalogState(int version, this.errorMessage): super(version, [errorMessage]);
  
  @override
  String toString() => 'ErrorCatalogState';

  @override
  ErrorCatalogState getStateCopy() {
    return ErrorCatalogState(this.version, this.errorMessage);
  }

  @override
  ErrorCatalogState getNewVersion() {
    return ErrorCatalogState(version+1, this.errorMessage);
  }
}
