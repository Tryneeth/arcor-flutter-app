import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tokin/src/blocs/catalog/catalog_bloc.dart';
import 'package:tokin/src/blocs/catalog/catalog_event.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/ui/shared/app-scaffold.dart';

class CatalogSingleProductScreen extends StatefulWidget {
  final Product product;

  const CatalogSingleProductScreen({Key key, this.product}) : super(key: key);

  @override
  _CatalogSingleProductScreenState createState() =>
      _CatalogSingleProductScreenState();
}

class _CatalogSingleProductScreenState
    extends State<CatalogSingleProductScreen> {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleChild: Text("Producto"),
      logged: true,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        widget.product.descArticulo,
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .title
                            .copyWith(color: Colors.lightBlue),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Center(
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border:
                        Border.all(color: Colors.deepOrangeAccent, width: 2)),
                child: CachedNetworkImage(
                  imageUrl: widget.product.getThumbnailUrl(),
                  fit: BoxFit.contain,
                  errorWidget: (context, _, __) => Image.asset(
                    "assets/ic_img_tokin.png",
                    color: Colors.grey,
                    width: 300,
                    height: 280,
                  ),
                  width: 300,
                  height: 280,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: widget.product.highlighted
                  ? MainAxisAlignment.spaceBetween
                  : MainAxisAlignment.end,
              children: <Widget>[
                if (widget.product.highlighted)
                  SvgPicture.asset(
                    "assets/catalog/medalla.svg",
                    width: 30,
                    height: 30,
                  ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                      child: widget.product.favorite
                          ? Icon(
                              Icons.favorite,
                              color: Colors.deepOrange,
                            )
                          : Icon(Icons.favorite_border),
                      onTap: () {
                        setState(() {
                          widget.product.favorite = !widget.product.favorite;
                        });
                        CatalogBloc().dispatch(SetFavoriteEvent(
                            widget.product, widget.product.favorite));
                      }),
                )
              ],
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Cod: ${widget.product.codigoArticulo}",
                          style: TextStyle(color: Colors.lightBlue),
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              "${widget.product.descArticulo}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.deepOrange),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0),
                      child: Center(
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color: Colors.deepOrange, width: 2)),
                          child: Text(
                            "${ProductType.getString(widget.product.idDivision)}"
                                .toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text(
                              "Unidad Venta:",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            Text(widget.product.unidadMedidaVenta)
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              "Cant. Mínima Compra:",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            Text(widget.product.unidadMinimaVenta)
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                "Precio Sin Impuesto",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.lightGreen),
                              ),
                              Text(
                                "\$${widget.product.precioListaDoubleDigits}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 30,
                                    color: Colors.lightGreen),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
