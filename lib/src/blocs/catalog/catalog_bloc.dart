import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:tokin/src/blocs/catalog/index.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  static final CatalogBloc _catalogBlocSingleton = CatalogBloc._internal();
  factory CatalogBloc() {
    return _catalogBlocSingleton;
  }
  CatalogBloc._internal();
  
  @override
  Future<void> dispose() async{
    _catalogBlocSingleton.dispose();
    super.dispose();
  }

  @override
  CatalogState get initialState => UnCatalogState(0);

  @override
  Stream<CatalogState> mapEventToState(
    CatalogEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'CatalogBloc', error: _, stackTrace: stackTrace);
      yield currentState;
    }
  }
}
