import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tokin/src/blocs/cart/cart_provider.dart';
import 'package:tokin/src/blocs/catalog/index.dart';
import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/new.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/models/shop_iw.dart';
import 'package:tokin/src/ui/shared/loading.dart';

class CatalogScreen extends StatefulWidget {
  const CatalogScreen({
    Key key,
    @required CatalogBloc catalogBloc,
  })  : _catalogBloc = catalogBloc,
        super(key: key);

  final CatalogBloc _catalogBloc;

  @override
  CatalogScreenState createState() {
    return CatalogScreenState();
  }
}

void addToShoppingCart(
    BuildContext context, Product productToAdd, int inShoppingCart) async {
  if (inShoppingCart == 0) {
    int amount = int.tryParse(productToAdd.unidadMinimaVenta);
    var product = ShoppingCartItem(
        ammount: amount == null ? 1 : amount,
        product: new NotificationAPI(
            titulo: productToAdd.descArticulo,
            imagenAdjunto: productToAdd.getThumbnailUrl(),
            codigoProducto: productToAdd.codigoArticulo,
            activo: true,
            opcionCompra: true,
            topeCantidadVenta: 9999999),
        articles: productToAdd);
    ShoppingCartInfo.of(context).order.idPedido =
        await CartProvider.generateIDPedido(context);
    ShoppingCartInfo.of(context).order.products.add(product);
    ShoppingCartInfo.of(context).flagCart = true;
    ShoppingCartInfo.update(context);
    var products = ShoppingCartInfo.of(context).order.products;
    List<String> productJsons = List();
    try {
      products.forEach((item) => productJsons.add(item.toJson()));
    } catch (e) {
      print(e);
    }
    await SharedPreferencesController.setPrefCart(productJsons);
  } else {
    ShoppingCartItem shoppingItem;
    try {
      shoppingItem = ShoppingCartInfo.of(context).order.products.singleWhere(
          (item) =>
              item.articles.codigoArticulo == productToAdd.codigoArticulo);
    } catch (e) {
      shoppingItem = null;
    }
    var itemToAdd = shoppingItem;
    itemToAdd.ammount++;
    var products = ShoppingCartInfo.of(context).order.products;
    var indexOfItem = products.indexOf(shoppingItem);
    products[indexOfItem] = itemToAdd;
    ShoppingCartInfo.update(context);
    //Sub cicle for update cart in sharedPreferences
    List<String> productJsons = List();
    products.forEach((item) => productJsons.add(item.toJson()));
    await SharedPreferencesController.setPrefCart(productJsons);
    //end sub cicle

  }
}

void removeFromShoppingCart(
    BuildContext context, Product productToRemove) async {
  ShoppingCartItem shoppingItem;
  try {
    shoppingItem = ShoppingCartInfo.of(context).order.products.singleWhere(
        (item) =>
            item.articles.codigoArticulo == productToRemove.codigoArticulo);
  } catch (e) {
    shoppingItem = null;
  }

  if (shoppingItem != null) {
    //This if is for validate that exists a minimum limmit
    if (shoppingItem.ammount > 1) {
      var itemToAdd = shoppingItem;
      itemToAdd.ammount--;
      var products = ShoppingCartInfo.of(context).order.products;
      var indexOfItem = products.indexOf(shoppingItem);
      products[indexOfItem] = itemToAdd;
      ShoppingCartInfo.update(context);
    } else {
      ShoppingCartInfo.of(context).order.products.remove(shoppingItem);
      ShoppingCartInfo.update(context);
    }
    //Sub cicle for update cart in sharedPreferences
    var products = ShoppingCartInfo.of(context).order.products;
    List<String> productJsons = List();
    products.forEach((item) => productJsons.add(item.toJson()));
    await SharedPreferencesController.setPrefCart(productJsons);
    //end sub cicle
    if (ShoppingCartInfo.of(context).order.products.length == 0) {
      ShoppingCartInfo.of(context).flagCart = false;
      ShoppingCartInfo.update(context);
    }
  }
}

class CatalogScreenState extends State<CatalogScreen> {
  CatalogScreenState();

  var _searchController = TextEditingController();
  Map<String, List<Product>> searchedProducts = Map();

  @override
  void initState() {
    super.initState();
    this._load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CatalogBloc, CatalogState>(
        bloc: widget._catalogBloc,
        builder: (
          BuildContext context,
          CatalogState currentState,
        ) {
          if (currentState is UnCatalogState) {
            return LoadingIndicator(
              message: currentState.loadingMessage,
            );
          }
          if (currentState is ErrorCatalogState) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(currentState.errorMessage ?? 'Error'),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: RaisedButton(
                    color: Colors.blue,
                    child: Text('reload'),
                    onPressed: () => this._load(),
                  ),
                ),
              ],
            ));
          }
          if (currentState is InCatalogState) {
            return Scaffold(
                body: Column(
              children: <Widget>[
                LayoutBuilder(
                  builder: (context, constrains) {
                    return Container(
                      width: constrains.maxWidth,
                      height: 50,
                      decoration: BoxDecoration(color: Colors.blue, boxShadow: [
                        BoxShadow(blurRadius: 8, offset: Offset(0, -5))
                      ]),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: _searchController,
                                maxLines: 1,
                                style: TextStyle(color: Colors.white),
                                onEditingComplete: () =>
                                    _search(currentState.catProducts),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              _search(currentState.catProducts);
                            },
                            splashColor: Colors.deepOrange,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.search,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {},
                            splashColor: Colors.deepOrange,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset(
                                "assets/catalog/codigo-de-barras.svg",
                                height: 25,
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {},
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset(
                                "assets/catalog/filtrar.svg",
                                height: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: _getCategoryLists(_searchController.text.isEmpty
                        ? currentState.catProducts
                        : searchedProducts),
                  ),
                ),
              ],
            ));
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void _search(Map<String, List<Product>> catProducts) {
    var searchString = _searchController.text.split(" ");

    searchedProducts.clear();

    catProducts.forEach((cat, products) {
      searchString.forEach((str) {
        if (!searchedProducts.containsKey(cat))
          searchedProducts.addAll({
            cat: products
                .where((p) =>
                    p.descArticulo.toLowerCase().contains(str.toLowerCase()))
                .toList()
          });
        else {
          searchedProducts[cat].addAll(products
              .where((p) =>
                  p.descArticulo.toLowerCase().contains(str.toLowerCase()))
              .toList());
        }
      });
    });
    setState(() {
      FocusScope.of(context).requestFocus(FocusNode());
    });
  }

  void _load() {
    widget._catalogBloc.dispatch(LoadCatalogEvent());
  }

  List<Widget> _getCategoryLists(Map<String, List<Product>> products) {
    List<Widget> catLists = List();

    ProductType.getTypesAsArray().forEach((type) {
      if (products[type].length > 0 && type != ProductType.OTHERS)
        catLists.add(
          CategoryList(
              category: type,
              header: ProductType.getString(type),
              products: products[type]),
        );
    });

    var allProducts = <Product>[];

    products.forEach((_, p) => allProducts.addAll(p));

    return catLists
      ..add(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 12.0),
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              onPressed: () {
                loadSingleCategory(context, allProducts, "Todo");
              },
              child: Text("Ver todo"),
            ),
          ),
        ],
      ));
  }
}

class CategoryList extends StatefulWidget {
  final String header;
  final List<Product> products;
  final String category;

  CategoryList({Key key, this.header, this.products, this.category})
      : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  var headerStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.bold);
  var moreThanFive = false;

  @override
  void initState() {
    moreThanFive = widget.products.length > 5;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 370,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  "${widget.header}:".toUpperCase(),
                  style: headerStyle,
                ),
              ),
              FlatButton(
                  onPressed: () {
                    loadSingleCategory(
                        context, widget.products, widget.category);
                  },
                  child: Text(
                    "Ver más",
                    style: headerStyle,
                  ))
            ],
          ),
          Expanded(
            child: _itemList(widget.products),
          ),
        ],
      ),
    );
  }

  ListView _itemList(List<Product> products) {
    List<Widget> items = List();
    for (var i = 0; i < products.length && i < 5; i++) {
      int isInShoppingCart;
      try {
        var shoppingItem = ShoppingCartInfo.of(context)
            .order
            .products
            .singleWhere((item) =>
                item.articles.codigoArticulo == products[i].codigoArticulo);
        isInShoppingCart = shoppingItem.ammount;
      } catch (e) {
        isInShoppingCart = 0;
      }
      if (widget.category != ProductType.HIGHLIGHTED) {
        items.add(CatalogHighlightedListItem(
          product: products[i],
          isInShoppingCart: isInShoppingCart,
          key: Key(products[i].codigoArticulo),
        ));
      } else {
        items.add(CatalogListItem(
          product: products[i],
          isInShoppingCart: isInShoppingCart,
          key: Key(products[i].codigoArticulo),
        ));
      }
    }

    if (moreThanFive)
      items.add(
          CatalogSeeMore(products: widget.products, category: widget.category));

    var listView = ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: <Widget>[...items],
    );

    return listView;
  }
}

class CatalogListItem extends StatefulWidget {
  final Product product;
  final int isInShoppingCart;

  const CatalogListItem(
      {Key key, @required this.product, @required this.isInShoppingCart})
      : assert(product != null),
        super(key: key);

  @override
  _CatalogListItemState createState() => _CatalogListItemState();
}

class _CatalogListItemState extends State<CatalogListItem> {
  int inShoppingCart;

  @override
  void initState() {
    super.initState();
    inShoppingCart = widget.isInShoppingCart;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CatalogSingleProductScreen(
                  product: widget.product,
                )));
      },
      child: Container(
        margin: EdgeInsets.only(left: 20, bottom: 12),
        width: 300,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Center(
              child: CachedNetworkImage(
                imageUrl: widget.product.getThumbnailUrl(),
                fit: BoxFit.contain,
                errorWidget: (context, _, __) => Image.asset(
                  "assets/ic_img_tokin.png",
                  color: Colors.grey,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 270,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.white60,
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(color: Colors.grey, width: 2)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.product.descArticulo),
                        Text(
                          "Precio s/imp: \$${widget.product.precioListaDoubleDigits}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                child: inShoppingCart > 0
                                    ? Container(
                                        child: FlatButton(
                                            color: Colors.green,
                                            disabledColor: Colors.green,
                                            padding: const EdgeInsets.all(0),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8)),
                                            onPressed: null,
                                            child: Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  IconButton(
                                                      color: Colors.white,
                                                      icon: Icon(
                                                        Icons
                                                            .remove_circle_outline,
                                                        size: 20,
                                                      ),
                                                      onPressed: () {
                                                        removeFromShoppingCart(
                                                            context,
                                                            widget.product);
                                                        setState(() {
                                                          inShoppingCart--;
                                                        });
                                                      }),
                                                  Text("$inShoppingCart",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 13)),
                                                  IconButton(
                                                    color: Colors.white,
                                                    icon: Icon(
                                                      Icons.add_circle_outline,
                                                      size: 20,
                                                    ),
                                                    onPressed: () {
                                                      addToShoppingCart(
                                                          context,
                                                          widget.product,
                                                          inShoppingCart);
                                                      setState(() {
                                                        inShoppingCart++;
                                                      });
                                                    },
                                                  ),
                                                ],
                                              ),
                                            )),
                                      )
                                    : Container(
                                        child: FlatButton(
                                            color: Colors.deepOrange,
                                            padding: const EdgeInsets.all(5),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8)),
                                            onPressed: () {
                                              addToShoppingCart(
                                                  context,
                                                  widget.product,
                                                  inShoppingCart);
                                              setState(() {
                                                inShoppingCart++;
                                              });
                                            },
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 10,
                                                        vertical: 5),
                                                child: Text("Agregar",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 14)))),
                                      )),
                          ],
                        ),
                      ],
                    ))
              ],
            ),
            if (widget.product.highlighted)
              Positioned(
                left: 10,
                top: 10,
                child: SvgPicture.asset(
                  "assets/catalog/medalla.svg",
                  width: 30,
                  height: 30,
                ),
              ),
            Positioned(
              right: 10,
              top: 10,
              child: GestureDetector(
                  child: widget.product.favorite
                      ? Icon(
                          Icons.favorite,
                          color: Colors.deepOrange,
                        )
                      : Icon(Icons.favorite_border),
                  onTap: () {
                    setState(() {
                      widget.product.favorite = !widget.product.favorite;
                    });
                    CatalogBloc().dispatch(SetFavoriteEvent(
                        widget.product, widget.product.favorite));
                  }),
            )
          ],
        ),
      ),
    );
  }
}

class CatalogSeeMore extends StatefulWidget {
  final List<Product> products;
  final String category;

  CatalogSeeMore({Key key, this.products, this.category}) : super(key: key);

  @override
  _CatalogSeeMoreState createState() => _CatalogSeeMoreState();
}

class _CatalogSeeMoreState extends State<CatalogSeeMore> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        loadSingleCategory(context, widget.products, widget.category);
      },
      child: Container(
        margin: EdgeInsets.only(left: 20, bottom: 12),
        width: 300,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 270,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.white60,
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(color: Colors.grey, width: 2)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Icon(
                            Icons.add_circle,
                            size: 150,
                            color: Colors.deepOrange,
                          ),
                        ),
                        Text(
                          "Ver más",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ],
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}

class CatalogHighlightedListItem extends StatefulWidget {
  final Product product;
  final int isInShoppingCart;

  const CatalogHighlightedListItem(
      {Key key, @required this.product, @required this.isInShoppingCart})
      : assert(product != null),
        assert(isInShoppingCart != null),
        super(key: key);

  @override
  _CatalogHighlightedListItemState createState() =>
      _CatalogHighlightedListItemState();
}

class _CatalogHighlightedListItemState
    extends State<CatalogHighlightedListItem> {
  int inShoppingCart;

  @override
  void initState() {
    super.initState();
    inShoppingCart = widget.isInShoppingCart;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CatalogSingleProductScreen(
                  product: widget.product,
                )));
      },
      child: Container(
        margin: EdgeInsets.only(left: 20, bottom: 12),
        padding: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
            color: Colors.white60,
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.grey, width: 2)),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: CachedNetworkImage(
                    imageUrl: widget.product.getThumbnailUrl(),
                    fit: BoxFit.contain,
                    errorWidget: (context, _, __) => Image.asset(
                      "assets/ic_img_tokin.png",
                      color: Colors.grey,
                      width: 200,
                      height: 180,
                    ),
                    width: 200,
                    height: 180,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: 270,
                        padding: EdgeInsets.all(12),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(widget.product.descArticulo),
                            Text(
                              "Precio s/imp: \$${widget.product.precioListaDoubleDigits}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                    child: inShoppingCart > 0
                                        ? Container(
                                            child: FlatButton(
                                                color: Colors.green,
                                                disabledColor: Colors.green,
                                                padding:
                                                    const EdgeInsets.all(0),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                onPressed: null,
                                                child: Container(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      IconButton(
                                                          color: Colors.white,
                                                          icon: Icon(
                                                            Icons
                                                                .remove_circle_outline,
                                                            size: 20,
                                                          ),
                                                          onPressed: () {
                                                            removeFromShoppingCart(
                                                                context,
                                                                widget.product);
                                                            setState(() {
                                                              inShoppingCart--;
                                                            });
                                                          }),
                                                      Text("$inShoppingCart",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 13)),
                                                      IconButton(
                                                        color: Colors.white,
                                                        icon: Icon(
                                                          Icons
                                                              .add_circle_outline,
                                                          size: 20,
                                                        ),
                                                        onPressed: () {
                                                          addToShoppingCart(
                                                              context,
                                                              widget.product,
                                                              inShoppingCart);
                                                          setState(() {
                                                            inShoppingCart++;
                                                          });
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                )),
                                          )
                                        : Container(
                                            child: FlatButton(
                                                color: Colors.deepOrange,
                                                padding:
                                                    const EdgeInsets.all(5),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                onPressed: () {
                                                  addToShoppingCart(
                                                      context,
                                                      widget.product,
                                                      inShoppingCart);
                                                  setState(() {
                                                    inShoppingCart++;
                                                  });
                                                },
                                                child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10,
                                                        vertical: 5),
                                                    child: Text("Agregar",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14)))),
                                          )),
                              ],
                            ),
                          ],
                        ))
                  ],
                ),
              ],
            ),
            if (widget.product.highlighted)
              Positioned(
                left: 10,
                top: 0,
                child: SvgPicture.asset(
                  "assets/catalog/medalla.svg",
                  width: 30,
                  height: 30,
                ),
              ),
            Positioned(
              right: 10,
              top: 0,
              child: GestureDetector(
                  child: widget.product.favorite
                      ? Icon(
                          Icons.favorite,
                          color: Colors.deepOrange,
                        )
                      : Icon(Icons.favorite_border),
                  onTap: () {
                    setState(() {
                      widget.product.favorite = !widget.product.favorite;
                    });
                    CatalogBloc().dispatch(SetFavoriteEvent(
                        widget.product, widget.product.favorite));
                  }),
            )
          ],
        ),
      ),
    );
  }
}

void loadSingleCategory(
    BuildContext context, List<Product> products, String type) {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => CatalogCategoryScreen(
          products: products, title: ProductType.getString(type))));
}

class CatalogFloatingActionButton extends StatefulWidget {
  AnimationController animationController;
  CatalogFloatingActionButton({Key key, this.animationController})
      : super(key: key);

  @override
  _CatalogFloatingActionButtonState createState() =>
      _CatalogFloatingActionButtonState();
}

class _CatalogFloatingActionButtonState
    extends State<CatalogFloatingActionButton>
    with SingleTickerProviderStateMixin {
  Animation<double> _fade;

  @override
  void initState() {
    widget.animationController.addListener(() {
      setState(() {});
    });
    _fade = Tween<double>(begin: 0, end: 1).animate(widget.animationController);
    super.initState();
  }

  String totalAmmount() {
    double ammount = 0;
    var products = ShoppingCartInfo.of(context).order.products;
    products.forEach((f) {
      ammount += f.ammount * double.parse(f.articles.precioLista);
    });
    return ammount.toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 5,
      children: <Widget>[
        Opacity(
            opacity: _fade.value,
            child: Container(
              height: 40,
              width: MediaQuery.of(context).size.width * 0.7 * _fade.value,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.green.shade700,
              ),
              child: _fade.value > 0.7
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Total S/Impuestos:",
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                        Text(
                          "\$ ${totalAmmount()}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    )
                  : Container(),
            )),
        Container(
            height: 40,
            width: 40,
            child: FloatingActionButton(
              onPressed: () {
                widget.animationController.value ==
                        widget.animationController.upperBound
                    ? widget.animationController.reverse()
                    : widget.animationController.forward();
              },
              backgroundColor: Colors.green.shade700,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              tooltip: "Carrito",
              child: Image.asset(
                "assets/catalog/calculator.png",
                height: 25,
                color: Colors.white,
              ),
            ))
      ],
    );
  }
}
