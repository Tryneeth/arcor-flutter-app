import 'dart:async';

import 'package:tokin/src/controllers/sharedPreferencesController.dart';
import 'package:tokin/src/models/product.dart';
import 'package:tokin/src/utils/base_http.dart';

class CatalogProvider extends DioBase {
  int productsPageCount;
  String _loadBaseEndpoint;
  String _updateBaseEndpoint;

  CatalogProvider() {
    SharedPreferencesController.getUrlServiceLoadTokin().then((val) => _loadBaseEndpoint = val);
    SharedPreferencesController.getUrlServiceUpdateTokin().then((val) => _updateBaseEndpoint = val);
  }

  Future<List<Product>> getProductsFromCatalog(int page) async {
    var _endpoint = "$_loadBaseEndpoint/get_articulos/";
    Map<String, dynamic> bodyForRequest = {
      "codigo_articulo": [],
      ...await getCatalogBodyData(),
      ...await getBodyData(),
      "page": page ?? 1
    };
    bodyForRequest["id_lista_precio"]="NENE";
    var response = await client.post(_endpoint, data: bodyForRequest);
    this.productsPageCount = response.data["total_page"];

    List<Product> products = new List<Product>();

    if (response.data["data"] == [] || response.data["total_page"] < page) return products;

    for (var i = 0; i < (response.data["data"] as List).length; i++) {
      products.add(Product.fromJson(response.data["data"][i]));
    }

    return products;
  }

  Future<Map<String, List<Product>>> arrangeProductsByCategory(List<Product> products) async {
    Map<String, List<Product>> result = {
      ProductType.CANDIES: List<Product>(),
      ProductType.ICE_CREAMS: List<Product>(),
      ProductType.FLOUR: List<Product>(),
      ProductType.FOOD: List<Product>(),
      ProductType.CHOCOLATES: List<Product>(),
      ProductType.OTHERS: List<Product>()
    };

    var highlighted = await this._getHighlightedProducts(products);

    products = _highlightProducts(highlighted[ProductType.HIGHLIGHTED], products);

    products.forEach((pr) {
      if (result.containsKey(pr.idDivision))
        result[pr.idDivision].add(pr);
      else
        result[ProductType.OTHERS].add(pr);
    });

    result.addAll(highlighted);

    return result;
  }

  List<Product> _highlightProducts(List<Product> highlightedProducts, List<Product> products) {
    List<Product> result = List();

    products.forEach((prod) {
      result.add(prod..highlighted = highlightedProducts.contains(prod));
    });

    return result;
  }

  Future<Map<String, List<Product>>> _getHighlightedProducts(List<Product> products) async {
    var _endpoint = "$_loadBaseEndpoint/get_tarjeta_tokin";
    Map<String, dynamic> bodyForRequest = {...await getCatalogBodyData(), ...await getBodyData(), "page": 1};
    bodyForRequest["id_lista_precio"]="NENE";
    Map<String, List<Product>> result = {
      ProductType.HIGHLIGHTED: List<Product>(),
    };

    var response = await client.post(_endpoint, data: bodyForRequest);

    for (var i = 0; i < (response.data["data"] as List).length; i++) {
      var highlightedProduct = products.firstWhere((el) {
        return response.data["data"][i]["Articulo"] == el.codigoArticulo;
      }, orElse: () => null);
      if (highlightedProduct != null) {
        result[ProductType.HIGHLIGHTED].add(highlightedProduct..highlighted = true);
      }
      highlightedProduct = null;
    }

    return result;
  }

  Future<void> setFavoriteProduct(Product product, bool favoriteFlag) async {
    var _endpoint = "$_updateBaseEndpoint/update_producto_favorito";
    Map<String, dynamic> bodyForRequest = {
      "records": [
        {
          ...await getCatalogBodyData(),
          ...await getBodyData(),
          "codigo_articulo": product.codigoArticulo,
          "favorito": favoriteFlag ? "1" : "0"
        }
      ]
    };
    bodyForRequest["id_lista_precio"]="NENE";
    await client.post(_endpoint, data: bodyForRequest);
  }

  Future<List<Product>> getFavoriteProducts(List<Product> products) async {
    var _endpoint = "$_loadBaseEndpoint/get_producto_favorito";
    Map<String, dynamic> bodyForRequest = {
      ...await getCatalogBodyData(),
      ...await getBodyData(),
    };
    bodyForRequest["id_lista_precio"]="NENE";
    var response = await client.post(_endpoint, data: bodyForRequest);

    for (var i = 0; i < (response.data["data"] as List).length; i++) {
      products = products.map((el) {
        if (response.data["data"][i]["CodigoArticulo"] == el.codigoArticulo) {
          el.favorite = response.data["data"][i]["Favorito"] == "1";
        }
        return el;
      }).toList();
    }

    return products;
  }
}
