import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/activities_viewmodel.dart';
import 'package:tokin/src/models/activity.dart';

@immutable
abstract class ActivityState extends Equatable {
  ActivityState([Iterable props]) : super(props);

  /// Copy object for use in action
  ActivityState copyWith();
}

/// UnInitialized
class UnActivityState extends ActivityState {
  @override
  String toString() => 'UnActivityState';

  @override
  ActivityState copyWith() {
    return UnActivityState();
  }
}

/// Initialized
class InActivityState extends ActivityState {
  final ActivitiesViewModel activityVM;
  final bool hasReachedMaxData;

  InActivityState(this.activityVM, {this.hasReachedMaxData = false})
      : super([activityVM, hasReachedMaxData]);

  @override
  String toString() => 'InActivityState';

  @override
  ActivityState copyWith({List<ActivityAPI> activity, bool hasReachedMaxData}) {
    return InActivityState(activity ?? this.activityVM,
        hasReachedMaxData: hasReachedMaxData ?? this.hasReachedMaxData);
  }
}

/// On Error
class ErrorActivityState extends ActivityState {
  final String errorMessage;

  ErrorActivityState(this.errorMessage);

  @override
  String toString() => 'ErrorActivityState';

  @override
  ActivityState copyWith() {
    return ErrorActivityState(this.errorMessage);
  }
}

/// No Data
class NoDataActivityState extends ActivityState {
  @override
  String toString() => 'NoDataActivityState';

  @override
  ActivityState copyWith() {
    return this;
  }
}
