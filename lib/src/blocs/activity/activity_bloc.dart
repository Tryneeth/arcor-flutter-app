import 'dart:async';

import 'package:bloc/bloc.dart';

import 'index.dart';

class ActivityBloc extends Bloc<ActivityEvent, ActivityState> {
  static final ActivityBloc _activityBlocSingleton =
      new ActivityBloc._internal();
  factory ActivityBloc() {
    return _activityBlocSingleton;
  }
  ActivityBloc._internal();

  ActivityState get initialState => new UnActivityState();

  @override
  Stream<ActivityState> mapEventToState(
    ActivityEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
