import 'dart:async';

import 'package:meta/meta.dart';
import 'package:tokin/src/controllers/activities_viewmodel.dart';
import 'package:tokin/src/models/activity.dart';

import 'index.dart';

@immutable
abstract class ActivityEvent {
  Future<ActivityState> applyAsync(
      {ActivityState currentState, ActivityBloc bloc});

  final ActivityRepository _activityProvider = new ActivityRepository();
}

class LoadActivityEvent extends ActivityEvent {
  @override
  String toString() => 'LoadActivityEvent';

  @override
  Future<ActivityState> applyAsync(
      {ActivityState currentState, ActivityBloc bloc}) async {
    try {
      ActivitiesViewModel activityApiList = await _activityProvider.findAll();
      var response = activityApiList;
      return response.activities.isEmpty
          ? NoDataActivityState()
          : InActivityState(response);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorActivityState(_?.toString());
    }
  }
}

class RefreshActivityEvent extends ActivityEvent {
  final ActivitiesViewModel activityVM;

  RefreshActivityEvent(this.activityVM);

  @override
  String toString() => 'RefreshActivityEvent';

  @override
  Future<ActivityState> applyAsync(
      {ActivityState currentState, ActivityBloc bloc}) async {
    try {
      ActivitiesViewModel activityAPIList =
          await _activityProvider.findAll(update: true);
      var response = activityAPIList;
      return response.activities.isEmpty
          ? NoDataActivityState()
          : InActivityState(response);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return currentState;
    }
  }
}

class LoadMoreActivityEvent extends ActivityEvent {
  @override
  String toString() => 'LoadMoreActivityEvent';

  @override
  Future<ActivityState> applyAsync(
      {ActivityState currentState, ActivityBloc bloc}) async {
    try {
      // Fetch Next Page
      var response = await this._activityProvider.findAll();
      return response.activities.isEmpty
          ? InActivityState((currentState as InActivityState).activityVM,
              hasReachedMaxData: true)
          : InActivityState((currentState as InActivityState).activityVM
            ..activities.addAll(response.activities));
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}

class FilterActivityEvent extends ActivityEvent {
  final String filter;

  FilterActivityEvent(this.filter);

  @override
  String toString() => 'FilterActivityEvent';

  @override
  Future<ActivityState> applyAsync(
      {ActivityState currentState, ActivityBloc bloc}) async {
    try {
      List<ActivityAPI> filteredActivities;
      // Fetch Next Page
      var response = await this._activityProvider.findAll();

      switch (filter) {
        case "FA":
          filteredActivities =
              response.activities.where((act) => act.movType == "FA").toList();
          break;
        case "NC":
          filteredActivities =
              response.activities.where((act) => act.movType == "NC").toList();
          break;
        default:
          filteredActivities = response.activities;
          break;
      }

      return InActivityState(ActivitiesViewModel(
          activities: filteredActivities,
          currentAccount: response.currentAccount));
    } catch (error, stackTrace) {
      print('$error $stackTrace');
      return currentState;
    }
  }
}
