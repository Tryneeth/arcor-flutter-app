import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/utils/provider_interface.dart';

import '../../models/data_connection.dart';
import 'index.dart';

class ActivityProvider extends IProvider<ActivityAPI> {
  static final Future<String> _activityEP = new DataConnection().getActivityUrl();

  ActivityProvider()
      : super(
          _activityEP,
          createModelFromJson: (json) =>
              ActivityResponse.fromJson(json).results,
        );
}
