import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokin/src/blocs/activity/index.dart';
import 'package:tokin/src/controllers/activities_viewmodel.dart';
import 'package:tokin/src/ui/home_widgets.dart';
import 'package:tokin/src/ui/shared/no_data.dart';

class ActivityScreen extends StatefulWidget {
  const ActivityScreen({
    Key key,
    @required ActivityBloc activityBloc,
  })  : _activityBloc = activityBloc,
        super(key: key);

  final ActivityBloc _activityBloc;

  @override
  ActivityScreenState createState() {
    return new ActivityScreenState(_activityBloc);
  }
}

class ActivityScreenState extends State<ActivityScreen> {
  final ActivityBloc _activityBloc;
  Completer<void> _refreshCompleter;
  static const offsetVisibleThreshold = 50;

  String selectedFilter;

  ActivitiesViewModel baseVM;

  ActivityScreenState(this._activityBloc);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _activityBloc.dispatch(LoadActivityEvent());
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: BlocBuilder<ActivityBloc, ActivityState>(
        bloc: widget._activityBloc,
        builder: (
          BuildContext context,
          ActivityState currentState,
        ) {
          if (currentState is UnActivityState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorActivityState) {
            return new Container(
                child: new NoDataWidget(
              message:
                  "No hay datos de Mi Actividad en estos momentos debido a problemas con la conexión",
            ));
          } else if (currentState is NoDataActivityState) {
            return new NoDataWidget(
              message: "No hay datos de Mi Actividad en estos momentos.",
            );
          } else if (currentState is InActivityState) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return NotificationListener<ScrollNotification>(
              child: RefreshIndicator(
                  onRefresh: () async {
                    widget._activityBloc.dispatch(
                        RefreshActivityEvent(currentState.activityVM));
                    return await _refreshCompleter.future;
                  },
                  child: HomeActivity(
                    viewModel: currentState.activityVM,
                  )),
            );
          }
          return Container();
        },
      ),
    );
  }
}
