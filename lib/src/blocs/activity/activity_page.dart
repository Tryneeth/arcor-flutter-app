import 'package:flutter/material.dart';

import 'index.dart';

class ActivityPage extends StatelessWidget {
  static const String routeName = "/activity";

  @override
  Widget build(BuildContext context) {
    var _activityBloc = new ActivityBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Activity"),
      ),
      body: new ActivityScreen(activityBloc: _activityBloc),
    );
  }
}
