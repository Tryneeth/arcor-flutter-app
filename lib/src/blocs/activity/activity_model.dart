import 'package:tokin/src/models/activity.dart';

class ActivityResponse {
  List<ActivityAPI> results;

  ActivityResponse({this.results});

  ActivityResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      results = new List<ActivityAPI>();
      json['data'].forEach((v) {
        results.add(new ActivityAPI.fromJson(v));
      });
    }
  }
}
