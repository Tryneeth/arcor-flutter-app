import 'package:shared_preferences/shared_preferences.dart';
import 'package:tokin/src/controllers/activities_viewmodel.dart';
import 'package:tokin/src/models/activity.dart';
import 'package:tokin/src/models/current_account.dart';
import 'package:tokin/src/utils/db/dbhelper.dart';

class ActivityRepository {
  DBHelper<ActivityAPI> _dbHelper;

  ActivityRepository() {
    _dbHelper = new DBHelper<ActivityAPI>(
        dbTableName: 'activity', tableField: ActivityAPI.getFieldsForDB());
  }

  Future<ActivitiesViewModel> findAll({update: false}) async {
    ActivitiesViewModel activityVM = new ActivitiesViewModel(
        activities: List<ActivityAPI>(), currentAccount: CurrentAccountAPI());
    var shared = await SharedPreferences.getInstance();
    var dbData;
    try {
      dbData = await _dbHelper.findAll();
    } catch (e) {
      print(e);
    }
    if (dbData.length > 0 && !update) {
      dbData.forEach(
          (data) => activityVM.activities.add(ActivityAPI.fromJson(data)));
      activityVM.currentAccount =
          CurrentAccountAPI.fromString(shared.getString("account"));
    } else {
      activityVM = await ActivitiesViewModel.getActivitiesViewModel();
      activityVM.activities.forEach((act) => _dbHelper.saveOne(act.toMap()));
      shared.setString("account", activityVM.currentAccount.toString());
    }
    return activityVM;
  }
}
