import 'package:flutter/material.dart';

var tokinThemeData = new ThemeData(
    primaryColor: Color.fromRGBO(60, 60, 60, 1),
    accentColor: Colors.blue,
    primaryTextTheme: TextTheme(
      button: TextStyle(
        color: Colors.white,
      ),
      body1: TextStyle(
        color: Colors.black,
      ),
    ),
    errorColor: Colors.red.shade900,
    scaffoldBackgroundColor: Colors.white,
    buttonTheme: ButtonThemeData(
      buttonColor: Color.fromRGBO(0, 131, 220, 1),
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.white,
    ),
    buttonColor: Color.fromRGBO(0, 131, 220, 1));
